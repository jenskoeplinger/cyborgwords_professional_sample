/*
 * gamePrepareLevel.js 2012-07-26
 * Copyright (C) 2012 Jens Koeplinger
 */

var CONVEYOR_SHOOT_XPOS    =  30;
var CONVEYOR_SHOOT_YPOS    = -83;
var CONVEYOR_TRASH_XPOS    = 380;
var CONVEYOR_TRASH_YPOS    =  80;
var CONVEYOR_CONVEYOR_XPOS =  10;
var CONVEYOR_CONVEYOR_YPOS =  80;
var CONVEYOR_LETTER0_XPOS  =  35;

var SCORENUMBER_XPOS       =  10;
var SCORENUMBER_YPOS       = 110;
var SCORENUMBER_WIDTH      =  50;
var SCORENUMBER_HEIGHT     =  70;

var GAME_GRID_OFFSET_Y      = 175;
var GAME_GRID_SIZE          =  70;
var GAME_GRID_TILE_OFFSET_X =   0;
var GAME_GRID_TILE_OFFSET_Y =   0;
var GAME_LTR_LOCKED_SIZE    =  62;
var GAME_LTR_LOCKED_OFFSET  =   4;

var GAME_EXIT_WIDTH        =  65;
var GAME_EXIT_HEIGHT       =  65;
var GAME_EXIT_TOP          = 665;
var GAME_EXIT_LEFT         = 410;

var GAME_SOUND_WIDTH       = 65;
var GAME_SOUND_HEIGHT      = 65;
var GAME_SOUND_TOP         = 665;
var GAME_SOUND_LEFT        = 330;

var INTRO_LEVEL_WIDTH      = 150;
var INTRO_LEVEL_HEIGHT     =  61;
var INTRO_LEVEL_LEFT       = 120;
var INTRO_LEVEL_TOP        =  55;
var INTRO_LEVELNUM_WIDTH   =  60;
var INTRO_LEVELNUM_HEIGHT  =  90;
var INTRO_LEVELNUM_LEFT    = 300;
var INTRO_LEVELNUM_TOP     =  40;

var levelBuildingSequence  = null;
var backgroundIsLoaded     = null;

var levelLetterFlyInX      = null;
var levelLetterFlyInY      = null;
var levelLetterFlyInType   = null;
var levelLetterFlyInBitmap = null;

var touchHasReleased       = true;

var introLevelSprite       = null;
var introLevelNumber       = null;

var selectGameScoringModelInvoked = null;

function gamePrepareLevel( tGame ) {

  switch ( tGame.getSubState() ) {

    case 0: // initialize

      var newSubstate = 1;

      sysLogger.addLine( "gamePrepareLevel()",
          LOGGER_SOURCE_SEQUENCER );
      
      if ( selectGameScoringModelInvoked == null ) gameScoringModel = tGame.getGamemodeDefault();
      
      sysLogger.addLine( "default game scoring mode is "
                         + gameScoringModel,
                         LOGGER_SOURCE_SEQUENCER );
      
      if ( gameScoringModel == SCORING_MODEL_NONE ) {
        
        sysLogger.addLine( "no default scoring model set; redirecting",
                           LOGGER_SOURCE_SEQUENCER );
        
        selectGameScoringModelInvoked = true;
        
        tGame.setState( GAME_SELECT_GAMEMODE );
        tGame.setSubState( 0 );
        
        return;
        
      }
      
      selectGameScoringModelInvoked = null;
      
      sysLogger.addLine( "preparing level: " + tGame.getLevelNumber(),
          LOGGER_SOURCE_SEQUENCER );
      sysLogger.addLine( "cyborg words version " + cyborgWordsVersion );
      
      if ( ( cyborgWordsVersion == VERSION_FREE ) 
        && ( tGame.getLevelNumber() > VERSION_FREE_MAXLEVEL ) ) {
        // this should never be here
        
        sysLogger.addLine( "illegal level for version" );
        newSubstate = 0; // hard stop
        break;
        
      }
      
      tele.addRecord(
          "graphics complexity",
          tGame.getGraphicsComplexity(),
          TELEMETRY_SINGLE );

      level.clearGrid( vScreen, true );
      level.loadGrid( tGame.getLevelNumber() );

      for ( var i = 0; i < 6; i++ ) tGame.setConveyorTile( i, LEVEL_TILE_NONE );
      
      var sprite = new Sprite( level.getBgPicture(), 490, 735 );
      sprite.setImgNode( document.getElementById( "bitmap0" ) );
      vScreen.putSprite( sprite, 0 );
 
      // initialize screen building counters
     
      levelBuildingSequence  = 0;
      backgroundIsLoaded     = false;
      
      levelLetterFlyInX      = 0;
      levelLetterFlyInY      = 0;
      levelLetterFlyInType   = Math.floor( Math.random() * 8 );
      levelLetterFlyInBitmap = ID_GRID_START;
          // grid is placed on bitmap ids
          // ID_GRID_START through ( ID_GRID_START + 48 )
      
      tGame.playMediaSoundtrack( FILE_SOUND_PREPARELEVEL );
      
      tGame.setSubState( newSubstate );

      break;

    case 1: // build screen elements

      if ( ( levelBuildingSequence < 4 ) && ( !( backgroundIsLoaded ) ) ) {
        // fly-in background

        var menuBgFade = null;

        if ( levelBuildingSequence > 0 ) {
      
          tele.addRecord(
              "background resize",
              tGame.getLastSequencerDuraction(),
              TELEMETRY_AVERAGE );
        
        }
        
        switch ( levelBuildingSequence ) {
          
          case 0: menuBgFade = 0.55; break;
          case 1: menuBgFade = 0.85; break;
          case 2: menuBgFade = 0.95; break;
          
          case 3: menuBgFade = 1;
                  backgroundIsLoaded = true;

                  teleLogger.addLine( tele.getNameValueString( null, "<br />" ) );
                  
                  break;
            
        }

        if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE ) {
          // slower graphics, simplify animation

            menuBgFade = 1;
            backgroundIsLoaded = true;

        }

        var sprite = vScreen.getSpriteById( 0 );
        sprite.setWidth(  Math.floor( 490 * menuBgFade ) );
        sprite.setHeight( Math.floor( 735 * menuBgFade ) );
        sprite.show();
        sprite.move( Math.floor( 245 * ( 1. - menuBgFade ) ),
                     Math.floor( 367 * ( 1. - menuBgFade ) ) );

      }

      // intro level number fly-in/-out
      
      var movingLSteps = null;
        
      if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE ) {

        movingLSteps = 4;

      } else {

        movingLSteps = 7;

      }

      switch ( levelBuildingSequence ) {
        
        case 5:
          
          introLevelSprite = new Sprite( FILE_IMAGE_INTRO_LEVEL,
                                         INTRO_LEVEL_WIDTH,
                                         INTRO_LEVEL_HEIGHT );
            
          introLevelSprite.setImgNode(
              document.getElementById( "bitmap" + ID_INTRO_LEVELNUM ) );
          vScreen.putSprite( introLevelSprite, ID_INTRO_LEVELNUM );
          introLevelSprite.show();
          introLevelSprite.move( INTRO_LEVEL_LEFT, -INTRO_LEVEL_HEIGHT );

          tGame.moveSprite(
                introLevelSprite,
                INTRO_LEVEL_LEFT,
                INTRO_LEVEL_TOP,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );
          
          break;
          
        case 6:
          
          introLevelNumber = new Number(
                               ID_INTRO_LEVELNUM + 1,
                               2,
                               INTRO_LEVELNUM_LEFT,
                               -INTRO_LEVELNUM_HEIGHT,
                               INTRO_LEVELNUM_WIDTH,
                               INTRO_LEVELNUM_HEIGHT,
                               NUMBER_COLORS_MAP[ "gameLevel" ] );

          introLevelNumber.setValue( tGame.getLevelNumber() );  
          introLevelNumber.show();

          tGame.moveNumber( introLevelNumber,
                        INTRO_LEVELNUM_LEFT,
                        INTRO_LEVELNUM_TOP,
                        movingLSteps,
                        GAME_MOVESPRITE_SINE_FLYIN );
          
          break;
          
        case 45:
          
          tGame.moveSprite(
                introLevelSprite,
                INTRO_LEVEL_LEFT,
                -INTRO_LEVEL_HEIGHT,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );

          break;
          
        case 46:
          
          tGame.moveNumber( introLevelNumber,
                        INTRO_LEVELNUM_LEFT,
                        -INTRO_LEVELNUM_HEIGHT,
                        movingLSteps,
                        GAME_MOVESPRITE_SINE_FLYOUT,
                        true );
          
          break;
          
      }

      if ( ( levelBuildingSequence > 0 ) && ( levelLetterFlyInY < LEVEL_GRID_MAX_Y ) ) {
        // prepare for grid fly-in

        levelLetterFlyInX++;
        levelLetterFlyInBitmap++;
          
        if ( levelLetterFlyInX == LEVEL_GRID_MAX_X ) {
          
          levelLetterFlyInX = 0;
          levelLetterFlyInY++;
          
        }
        
      }
      
      if ( ( levelLetterFlyInY < LEVEL_GRID_MAX_Y )
           && ( !( level.getGridXY( levelLetterFlyInX, levelLetterFlyInY ) == LEVEL_GRID_TYPE_NONE ) )
      ) {
        // let the grid fly in, bitmap1 through bitmap49

        var gridType = level.getGridXY( levelLetterFlyInX, levelLetterFlyInY );
        
        sysLogger.addLine( "level grid ("
            + levelLetterFlyInX + ", "
            + levelLetterFlyInY + ") type "
            + gridType,
            LOGGER_SOURCE_SEQUENCER );
        
        tele.addRecord(
            "level grid fly in",
            tGame.getLastSequencerDuraction(),
            TELEMETRY_AVERAGE );
        
        var gridFileName = LEVEL_GRID_FILES[ gridType ];
        var sprite = new Sprite( gridFileName, GAME_GRID_SIZE, GAME_GRID_SIZE );
        sprite.setImgNode( document.getElementById( "bitmap" + levelLetterFlyInBitmap ) );
        vScreen.putSprite( sprite, levelLetterFlyInBitmap );
        sprite.show();
        
        switch ( levelLetterFlyInType ) {
          // have different fly-ins for variation fun        
          
          case 0:
            sprite.move( 215, 740 );
            break;
            
          case 1:
            sprite.move( levelLetterFlyInX * 70 + GAME_GRID_TILE_OFFSET_X, 740 );
            break;
            
          case 2:
            sprite.move( 210, 210 + 175 );
            break;
            
          case 3:
            if ( levelLetterFlyInX < 3 ) {
              sprite.move( -70, levelLetterFlyInY * 70 + GAME_GRID_TILE_OFFSET_Y + GAME_GRID_OFFSET_Y );
            } else {
              sprite.move( 490, levelLetterFlyInY * 70 + GAME_GRID_TILE_OFFSET_Y + GAME_GRID_OFFSET_Y );
            }
            break;
            
          case 4:
            sprite.move( ( levelLetterFlyInX - 3.5 ) * 140 + 210,
                         ( levelLetterFlyInY - 3.5 ) * 140 + 210 + GAME_GRID_OFFSET_Y
            );
            break;
            
          case 5:
            sprite.move( -70, levelLetterFlyInY * 70 + GAME_GRID_TILE_OFFSET_Y + GAME_GRID_OFFSET_Y );
            break;
            
          case 6:
            sprite.move( 490, levelLetterFlyInY * 70 + GAME_GRID_TILE_OFFSET_Y + GAME_GRID_OFFSET_Y );
            break;
            
          case 7:
            sprite.move( Math.cos( levelBuildingSequence / 8. ) * 300 + 210,
                         Math.sin( levelBuildingSequence / 8. ) * 300 + 210
                             + GAME_GRID_OFFSET_Y );
            break;       
            
        }

        // tell the Game sequencer to fly this sprite to its game
        // position over the next few cycles

        var movingSSteps = null;
        
        if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE ) {

          movingSSteps = 4;

        } else {

          movingSSteps = 7;

        }

        tGame.moveSprite(
            sprite,
            levelLetterFlyInX * 70 + GAME_GRID_TILE_OFFSET_X,
            levelLetterFlyInY * 70 + GAME_GRID_TILE_OFFSET_Y + GAME_GRID_OFFSET_Y,
            movingSSteps,
            GAME_MOVESPRITE_SINE_FLYIN );
        
      }
      
      levelBuildingSequence++;
      
      if ( levelLetterFlyInY == LEVEL_GRID_MAX_Y ) {
        // grid is built; move to the next substate
        
        tGame.setSubState( 2 );
        levelBuildingSequence = 0;
        
        teleLogger.addLine( tele.getNameValueString( null, "<br />" ) );
        sysLogger.addLine( "move gamePrepareLevel to substate 2",
                           LOGGER_SOURCE_SEQUENCER );
        
      }
      
      break;

    case 2: // move in remaining items

      var movingSSteps = 7;
      if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE )
          movingSSteps = 4;

      switch ( levelBuildingSequence ) {

        case 0: // trash bin fly in

          var sprite = new Sprite( FILE_IMAGE_TRASHBIN, 80, 95 );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_TRASHBIN ) );
          vScreen.putSprite( sprite, ID_TRASHBIN );
          sprite.show();
          sprite.move( CONVEYOR_TRASH_XPOS, -70 );

          tGame.moveSprite(
              sprite,
              CONVEYOR_TRASH_XPOS,
              CONVEYOR_TRASH_YPOS,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );

          break;

        case 1: // trash bin lid fly in

          var sprite = new Sprite( FILE_IMAGE_TRASHBINLID, 80, 18 );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_TRASHBINLID ) );
          vScreen.putSprite( sprite, ID_TRASHBINLID );
          sprite.show();
          sprite.move( CONVEYOR_TRASH_XPOS, -88 );

          tGame.moveSprite(
              sprite,
              CONVEYOR_TRASH_XPOS,
              CONVEYOR_TRASH_YPOS - 18,
              movingSSteps + 2,
              GAME_MOVESPRITE_SINE_FLYIN );

          break;          

        case 2: // letter shoot fly in

          var sprite = new Sprite( FILE_IMAGE_LETTERSHOOT, 80, 95 );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_LETTERSHOOT ) );
          vScreen.putSprite( sprite, ID_LETTERSHOOT );
          sprite.show();
          sprite.move( CONVEYOR_SHOOT_XPOS, -95 );

          tGame.moveSprite(
              sprite,
              CONVEYOR_SHOOT_XPOS,
              CONVEYOR_SHOOT_YPOS,
              movingSSteps - 1,
              GAME_MOVESPRITE_SINE_FLYIN );

          break;

        case 3: // conveyor fly in (belt and main)

          var sprite = new Sprite( FILE_IMAGE_CONVEYORBELT, 330, 30 );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_CONVEYORBELT ) );
          vScreen.putSprite( sprite, ID_CONVEYORBELT );
          sprite.show();
          sprite.move( -330, CONVEYOR_CONVEYOR_YPOS );

          tGame.moveSprite(
              sprite,
              CONVEYOR_CONVEYOR_XPOS + 15,
              CONVEYOR_CONVEYOR_YPOS,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );

          var sprite = new Sprite( FILE_IMAGE_CONVEYORMAIN, 360, 34 );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_CONVEYORMAIN ) );
          vScreen.putSprite( sprite, ID_CONVEYORMAIN );
          sprite.show();
          sprite.move( -345, CONVEYOR_CONVEYOR_YPOS - 2 );

          tGame.moveSprite(
              sprite,
              CONVEYOR_CONVEYOR_XPOS,
              CONVEYOR_CONVEYOR_YPOS - 2,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );

          break;

        case 4:
          
          gameInjectNewLetter( tGame, level.getNextTile() );
          break;
                    
        case 12:
          
          gameMoveConveyor( tGame );
          break;
          
        case 20:

          gameInjectNewLetter( tGame, level.getNextTile() );
          break;

        case 28:
          
          gameMoveConveyor( tGame );
          break;
          
        case 36:
          
          gameInjectNewLetter( tGame, level.getNextTile() );
          break;
          
        case 39:
          
          scoreNumber = new Number(
                               ID_NUM_SCORESTART,
                               6,
                               SCORENUMBER_XPOS - 50,
                               SCORENUMBER_YPOS,
                               SCORENUMBER_WIDTH,
                               SCORENUMBER_HEIGHT,
                               NUMBER_COLORS_MAP[ "gameScore" ] );

          scoreNumber.setValue( 0 );
          tGame.setLevelScore( 0 );
          
          scoreNumber.show();

          break;

        case 40:

          tGame.moveNumber( scoreNumber,
                        SCORENUMBER_XPOS,
                        SCORENUMBER_YPOS,
                        movingSSteps,
                        GAME_MOVESPRITE_SINE_FLYIN );

          break;

        case 41: // let's play!

          tGame.setState( GAME_LEVEL_PLAYING );
          tGame.setSubState( 0 );

          // fly in "exit" and "sound" buttons
          
          var sprite = new Sprite( FILE_IMAGE_EXIT, GAME_EXIT_WIDTH, GAME_EXIT_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_EXIT ) );
          vScreen.putSprite( sprite, ID_EXIT );
          sprite.show();
          sprite.move( GAME_EXIT_LEFT, 735 );

          tGame.moveSprite(
              sprite,
              GAME_EXIT_LEFT,
              GAME_EXIT_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );

          var fileSound = mediaFileArray[ gameMediaUserPreference ];
          
          sprite = new Sprite( fileSound, GAME_SOUND_WIDTH, GAME_SOUND_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_SOUND ) );
          vScreen.putSprite( sprite, ID_SOUND );
          sprite.show();
          sprite.move( GAME_SOUND_LEFT, 735 );

          tGame.moveSprite(
              sprite,
              GAME_SOUND_LEFT,
              GAME_SOUND_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );

          break;
          
        default:

      }

      levelBuildingSequence++;

      break;

    default: // start level

      tGame.setState( GAME_LEVEL_PLAYING );
      tGame.setSubState( 0 );

  }

}
