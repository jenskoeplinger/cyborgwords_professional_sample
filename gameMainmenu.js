/*
 * gameMainemenu.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 *
 */

var menuScrollText =
    ".@HELLO"
  + "  AND  "
  + "WELCOME"
  + ".  TO  "
  + ".CYBORG"
  + " WORDS "
  + ". @# @ "
  + ". DRAG "
  + "  AND  "
  + ". DROP "
  + ".LETTER"
  + " TILES "
  + ". FROM "
  + "  THE  "
  + "  TOP  "
  + ". INTO "
  + "  THE  "
  + ". GRID "
  + " BELOW "
  + ".  TO  "
  + ". FORM "
  + "   A   "
  + ". WORD "
  + ".  #   "
  + "  GET  "
  + " EXTRA "
  + " BONUS "
  + "  FOR  "
  + "FORMING"
  + "   A   "
  + "  NEW  "
  + ". WORD "
  + ". BOTH "
  + ". WAYS "
  + ".  @@  "
  + ". MOVE "
  + ".SKINNY"
  + ". GRID "
  + " TILES "
  + ".  TO  "
  + "  NEW  "
  + ".PLACES"
  + "  @@@  "
  + " CHECK "
  + "  FOR  "
  + ". WORD "
  + "  AND  "
  + ".LETTER"
  + " MULTS "
  + ". @# @ "
  + "   I   "
  + ". HOPE "
  + "  YOU  "
  + ". LIKE "
  + ".  MY  "
  + ". GAME "
  + ".  #   "
  + ". RUFF "
  + ". RUFF "
  + ".  @@  "
  + ". HAVE "
  + "  FUN  "
  + ". @@@# "
  + "       ";

var menuScrollPosition            = null;
var menuScrollCount               = null;
var menuScrollTempDisplacement    = 0;
var menuScrollTempDisplacementPre = 0;

var menuMyMagic = "";
var menuMyMagicLength = 26;

var menuMagicToggleGametype    = "53ead17a";
var menuMagicInfinitePerks     = "bc4b7478";
var menuMagicClearAll          = "871cef0b";

var menuScrollShift = new Array();
menuScrollShift[  1 ] =   5;
menuScrollShift[  2 ] =  10;
menuScrollShift[  3 ] =  30;
menuScrollShift[  4 ] =  60;
menuScrollShift[  5 ] = 110;
menuScrollShift[  6 ] = 180;
menuScrollShift[  7 ] = 310;
menuScrollShift[  8 ] = 380;
menuScrollShift[  9 ] = 430;
menuScrollShift[ 10 ] = 460;
menuScrollShift[ 11 ] = 480;
menuScrollShift[ 12 ] = 485;

var MAINMENU_SELECT_NOTHING    = -1;
var MAINMENU_SELECT_PLAY       = 0;
var MAINMENU_SELECT_HIGHSCORES = 1;
var MAINMENU_SELECT_HELP       = 2;
var MAINMENU_SELECT_MAGIC1     = 3;
var MAINMENU_SELECT_MAGIC2     = 4;
var MAINMENU_SELECT_MAGIC3     = 5;
var MAINMENU_SELECT_SOUND      = 6;
var MAINMENU_SELECT_LIKEME     = 7;

var MAINMENU_SOUND_WIDTH       = 65;
var MAINMENU_SOUND_HEIGHT      = 65;
var MAINMENU_SOUND_LEFT        = 410;
var MAINMENU_SOUND_TOP         = 286;

var MAINMENU_LIKEME_WIDTH      = 65;
var MAINMENU_LIKEME_HEIGHT     = 65;
var MAINMENU_LIKEME_LEFT       = 15;
var MAINMENU_LIKEME_TOP        = 286;

var mainmenuSelectNextstate = null;

var menuSelectBounds = new Array();
var menuSelectBoundsCount = 8;

menuSelectBounds[ 0 ] = new Array();
menuSelectBounds[ 0 ][ "id" ] = MAINMENU_SELECT_PLAY;
menuSelectBounds[ 0 ][ "x1" ] = 0;
menuSelectBounds[ 0 ][ "x2" ] = 163;
menuSelectBounds[ 0 ][ "y1" ] = 380;
menuSelectBounds[ 0 ][ "y2" ] = 550;

menuSelectBounds[ 1 ] = new Array();
menuSelectBounds[ 1 ][ "id" ] = MAINMENU_SELECT_HIGHSCORES;
menuSelectBounds[ 1 ][ "x1" ] = 164;
menuSelectBounds[ 1 ][ "x2" ] = 326;
menuSelectBounds[ 1 ][ "y1" ] = 380;
menuSelectBounds[ 1 ][ "y2" ] = 550;

menuSelectBounds[ 2 ] = new Array();
menuSelectBounds[ 2 ][ "id" ] = MAINMENU_SELECT_HELP;
menuSelectBounds[ 2 ][ "x1" ] = 326;
menuSelectBounds[ 2 ][ "x2" ] = 490;
menuSelectBounds[ 2 ][ "y1" ] = 380;
menuSelectBounds[ 2 ][ "y2" ] = 550;

menuSelectBounds[ 3 ] = new Array();
menuSelectBounds[ 3 ][ "id" ] = MAINMENU_SELECT_MAGIC1;
menuSelectBounds[ 3 ][ "x1" ] = 0;
menuSelectBounds[ 3 ][ "x2" ] = 163;
menuSelectBounds[ 3 ][ "y1" ] = 0;
menuSelectBounds[ 3 ][ "y2" ] = 160;

menuSelectBounds[ 4 ] = new Array();
menuSelectBounds[ 4 ][ "id" ] = MAINMENU_SELECT_MAGIC2;
menuSelectBounds[ 4 ][ "x1" ] = 164;
menuSelectBounds[ 4 ][ "x2" ] = 326;
menuSelectBounds[ 4 ][ "y1" ] = 0;
menuSelectBounds[ 4 ][ "y2" ] = 160;

menuSelectBounds[ 5 ] = new Array();
menuSelectBounds[ 5 ][ "id" ] = MAINMENU_SELECT_MAGIC3;
menuSelectBounds[ 5 ][ "x1" ] = 327;
menuSelectBounds[ 5 ][ "x2" ] = 490;
menuSelectBounds[ 5 ][ "y1" ] = 0;
menuSelectBounds[ 5 ][ "y2" ] = 160;

menuSelectBounds[ 6 ] = new Array();
menuSelectBounds[ 6 ][ "id" ] = MAINMENU_SELECT_SOUND;
menuSelectBounds[ 6 ][ "x1" ] = MAINMENU_SOUND_LEFT - 10;
menuSelectBounds[ 6 ][ "x2" ] = MAINMENU_SOUND_LEFT + MAINMENU_SOUND_WIDTH + 10;
menuSelectBounds[ 6 ][ "y1" ] = MAINMENU_SOUND_TOP - 10;
menuSelectBounds[ 6 ][ "y2" ] = MAINMENU_SOUND_TOP + MAINMENU_SOUND_HEIGHT + 10;

menuSelectBounds[ 7 ] = new Array();
menuSelectBounds[ 7 ][ "id" ] = MAINMENU_SELECT_LIKEME;
menuSelectBounds[ 7 ][ "x1" ] = MAINMENU_LIKEME_LEFT - 10;
menuSelectBounds[ 7 ][ "x2" ] = MAINMENU_LIKEME_LEFT + MAINMENU_LIKEME_WIDTH + 10;
menuSelectBounds[ 7 ][ "y1" ] = MAINMENU_LIKEME_TOP - 10;
menuSelectBounds[ 7 ][ "y2" ] = MAINMENU_LIKEME_TOP + MAINMENU_LIKEME_HEIGHT + 10;

var menuY      = 595;
var menuBgFade = 1;

function gameMainmenu( tGame ) {
  // shows main menu animations; handles mouse clicks

  switch ( tGame.getSubState() ) {

    case 0: // enter main menu

      sysLogger.addLine( "gameMainemenu()", LOGGER_SOURCE_SEQUENCER );

      tele.addRecord(
          "graphics complexity",
          tGame.getGraphicsComplexity(),
          TELEMETRY_SINGLE );
      
      menuScrollPosition = 0;
      menuScrollCount    = 0;

      for ( var i = 0; i < 7; i++ ) {
      // register scroll message letter sprites, all hidden for now

        var sprite = new Sprite(
            FILE_IMAGE_LTR_PREFIX + "Blank" + FILE_IMAGE_LTR_SUFFIX, 70, 70 );
        sprite.setImgNode( document.getElementById( "bitmap" + ( i + 10 ) ) );
        vScreen.putSprite( sprite, ( i + 10 ) );
        sprite.move( i * 70, menuY );
        sprite.hide();

      }

      // random letter fun: print some random letters to the sys logger, print hash
      for ( var i = 0; i < 20; i++ ) {

        var randomLetters = level.returnRandomLetters( 80 );
        sysLogger.addLine( "random: " + randomLetters );

      }

      // start soundtrack (if user selection) and show sound icon
      tGame.playMediaSoundtrack( FILE_SOUND_MENU );
      
      var fileSound = mediaFileArray[ gameMediaUserPreference ];
          
      var sprite = new Sprite( fileSound, MAINMENU_SOUND_WIDTH, MAINMENU_SOUND_HEIGHT );
      sprite.setImgNode(
              document.getElementById( "bitmap" + ID_SOUND ) );
      vScreen.putSprite( sprite, ID_SOUND );
      sprite.show();
      sprite.move( MAINMENU_SOUND_LEFT, MAINMENU_SOUND_TOP );

      // show "like me" button
      var sprite = new Sprite( FILE_IMAGE_LIKEME, MAINMENU_LIKEME_WIDTH, MAINMENU_LIKEME_HEIGHT );
      sprite.setImgNode(
              document.getElementById( "bitmap" + ID_LIKEME ) );
      vScreen.putSprite( sprite, ID_LIKEME );
      sprite.show();
      sprite.move( MAINMENU_LIKEME_LEFT, MAINMENU_LIKEME_TOP );
      
      tGame.setSubState( 1 );
      break;

    case 1: // perpetual animations

      switch ( menuScrollCount ) {

        case 0: // prepare for new letters sliding in

          var allBlank = true;
          menuScrollTempDisplacementPre = menuScrollTempDisplacement;
          menuScrollTempDisplacement = 0;

          for ( var i = 7; i < 14; i++ ) {
          // register next set of scroll message letter sprites

            var thisLtr = menuScrollText.substring( menuScrollPosition, menuScrollPosition + 1 );
            var thisSize = 70;

            switch ( thisLtr ) {
              
              case " ":
                thisLtr = "Blank";
                break;
                
              case ".":
                thisLtr = "Blank";
                menuScrollTempDisplacement = -35;
                break;
                
              case "@":
                thisLtr = "Paw";
                allBlank = false;
                break;
                
              case "#":
                thisLtr = "Cyborg";
                allBlank = false;
                thisSize = 140;
                break;
                
              default:
                allBlank = false;
                
            }

            var sprite = new Sprite(
                FILE_IMAGE_LTR_PREFIX + thisLtr + FILE_IMAGE_LTR_SUFFIX, thisSize, thisSize );
            sprite.setImgNode( document.getElementById( "bitmap" + ( i + 10 ) ) );
            vScreen.putSprite( sprite, ( i + 10 ) );
            sprite.move( menuScrollTempDisplacement + i * 70, menuY + ( 70 - thisSize ) / 2 );
            sprite.show();

            if ( thisLtr == "Blank" ) sprite.hide();
  
            menuScrollPosition++;

          }

          if ( allBlank ) menuScrollPosition = 0;

          break;

        case 1: // move new letters in a notch
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:

          for ( i = 0; i < 7; i++ ) {

            vScreen.getSpriteById( i + 10 ).move(
                menuScrollTempDisplacementPre
                    + i * 70
                    - menuScrollShift[ menuScrollCount ] );

          }

          for ( i = 7; i < 14; i++ ) {

            vScreen.getSpriteById( i + 10 ).move(
                menuScrollTempDisplacement
                    + i * 70
                    - menuScrollShift[ menuScrollCount ] );

          }

          break;

        case 13: // letters have arrived; switch bitmaps over

          menuScrollPosition -= 7;

          for ( var i = 0; i < 7; i++ ) {
          // register next set of scroll message letter sprites

            var thisLtr = "Blank";
            var thisSize = 70;

            if ( menuScrollPosition >= 0 ) {

              thisLtr =
                  menuScrollText.substring( menuScrollPosition, menuScrollPosition + 1 );

              switch ( thisLtr ) {
              
                case " ":
                  thisLtr = "Blank";
                  break;
                
                case ".":
                  thisLtr = "Blank";
                  break;
                
                case "@":
                  thisLtr = "Paw";
                  break;
                
                case "#":
                  thisLtr = "Cyborg";
                  thisSize = 140;
                  break;
                
                default:
                
              }

            }

            var sprite = new Sprite(
                FILE_IMAGE_LTR_PREFIX + thisLtr + FILE_IMAGE_LTR_SUFFIX, thisSize, thisSize );
            sprite.setImgNode( document.getElementById( "bitmap" + ( i + 10 ) ) );
            vScreen.putSprite( sprite, ( i + 10 ) );
            sprite.move( menuScrollTempDisplacement + i * 70, menuY + ( 70 - thisSize ) / 2 );
            sprite.show();

            if ( thisLtr == "Blank" ) sprite.hide();
  
            menuScrollPosition++;

          }

          // hide the ones the just scrolled in

          for ( var i = 7; i < 14; i++ ) {

            vScreen.getSpriteById( i + 10 ).hide();

          }

          break;

        case 30: // time to slide the next ones in

          menuScrollCount = -1;
          break;

        default: // do nothing

      }

      menuScrollCount++;

      // check user interaction

      if ( touch.getTouchOccurred() && ( !( touch.getIsTouching() ) ) ) {

        var thisX        = touch.getTouchX();
        var thisY        = touch.getTouchY();
        var thisSelected = MAINMENU_SELECT_NOTHING;
        
        for ( var i = 0; i < menuSelectBoundsCount; i++ ) {
         
          if (    ( thisX > menuSelectBounds[ i ][ "x1" ] )
               && ( thisX < menuSelectBounds[ i ][ "x2" ] )
               && ( thisY > menuSelectBounds[ i ][ "y1" ] )
               && ( thisY < menuSelectBounds[ i ][ "y2" ] ) ) {
            
            thisSelected = menuSelectBounds[ i ][ "id" ];
            i = menuSelectBoundsCount;
          
            sysLogger.addLine( "user selected item " + thisSelected,
                               LOGGER_SOURCE_SEQUENCER );
          
          }
          
        }
         
        var selectedCheckMagic = false;
         
        switch ( thisSelected ) {
          
          case MAINMENU_SELECT_PLAY:
          
            sysLogger.addLine( "selection: play", LOGGER_SOURCE_SEQUENCER );
            
            menuScrollCount         = 0;
            menuPlayRenderMode      = MENU_PLAY_RENDER_LEVELGRID;
            menuPlayWhereFrom       = MENU_PLAY_WHEREFROM_MAIN;
            
            // get the highest allowable "next" level
            
            menuPlayNextLevel = tGame.getHighestNextLevel();
      
            if ( menuPlayNextLevel == null ) {
        
              sysLogger.addLine( "(play) newbie", LOGGER_SOURCE_HIGHSCORE );

              menuPlayNextLevel = 1;
          
            }

            if ( cyborgWordsVersion == VERSION_ADDICT ) {
          
              menuPlayNextLevel = LEVEL_HIGHESTLEVEL;
              tGame.setHighestNextLevel( LEVEL_HIGHESTLEVEL );
          
              sysLogger.addLine( "(play) paid version, all levels are unlocked",
                                  LOGGER_SOURCE_HIGHSCORE );
          
            } else {

              sysLogger.addLine( "(play) free version, user must unlock levels",
                                  LOGGER_SOURCE_HIGHSCORE );
          
            }

            sysLogger.addLine( "(play) highest 'next level' is " + menuPlayNextLevel,
                               LOGGER_SOURCE_HIGHSCORE );
            
            // now get the level that user user last selected in the menu, or that
            // would be the logical 'next' playable level from prior completion
            
            menuPlayCurrentLevel = tGame.getCurrentMenuLevel();

            if ( !( menuPlayCurrentLevel ) ) {
        
              menuPlayCurrentLevel = 1;
        
              sysLogger.addLine( "(play) newbie, start with level 1",
                                 LOGGER_SOURCE_HIGHSCORE );
        
            } else {

                sysLogger.addLine( "(play) read menu level " + menuPlayCurrentLevel,
                                   LOGGER_SOURCE_HIGHSCORE );
        
            }
            
            // now let's see whether this level is playable (control)
            
            if ( menuPlayNextLevel > LEVEL_HIGHESTLEVEL ) menuPlayNextLevel = LEVEL_HIGHESTLEVEL;
            if ( menuPlayCurrentLevel > LEVEL_HIGHESTLEVEL ) menuPlayCurrentLevel = 1;
            if ( menuPlayCurrentLevel > menuPlayNextLevel ) menuPlayCurrentLevel = menuPlayNextLevel;
                
            if ( !( cyborgWordsVersion == VERSION_ADDICT )
                 && ( menuPlayCurrentLevel > VERSION_FREE_MAXLEVEL ) ) {
              // send user to level selector screen for buy-flow instead
             
              mainmenuSelectNextstate = GAME_MENU_PLAY;
              
            } else {
              // update level selection variables and play!
              
              tGame.setCurrentMenuLevel( menuPlayCurrentLevel );
              tGame.setHighestNextLevel( menuPlayNextLevel );
              tGame.setLevelNumber( menuPlayCurrentLevel );
            
              mainmenuSelectNextstate = GAME_LEVEL_PREPARING;
              
            }
            
            tGame.playMediaEffect( FILE_SOUND_PICKUP );
            
            tGame.setSubState( 2 );

            break;

          case MAINMENU_SELECT_HIGHSCORES:
            
            sysLogger.addLine( "selection: highscores", LOGGER_SOURCE_SEQUENCER );

            menuScrollCount         = 0;
            menuPlayRenderMode      = MENU_PLAY_RENDER_LEVELGRID;
            menuPlayWhereFrom       = MENU_PLAY_WHEREFROM_MAIN;
            
            mainmenuSelectNextstate = GAME_MENU_PLAY;

            tGame.playMediaEffect( FILE_SOUND_PICKUP );
            
            tGame.setSubState( 2 );

            break;
            
          case MAINMENU_SELECT_HELP:
            
            sysLogger.addLine( "selection: help", LOGGER_SOURCE_SEQUENCER );
            
            tGame.playMediaEffect( FILE_SOUND_PICKUP );
            
            tGame.setState( GAME_HELP );
            tGame.setSubState( 0 );
            
            break;
            
          case MAINMENU_SELECT_MAGIC1:
            
            sysLogger.addLine( "selection: magic1", LOGGER_SOURCE_SEQUENCER );
            menuMyMagic += "1";
            selectedCheckMagic = true;
            
            break;
            
          case MAINMENU_SELECT_MAGIC2:
            
            sysLogger.addLine( "selection: magic2", LOGGER_SOURCE_SEQUENCER );
            menuMyMagic += "2";
            selectedCheckMagic = true;
            
            break;
            
          case MAINMENU_SELECT_MAGIC3:
            
            sysLogger.addLine( "selection: magic3", LOGGER_SOURCE_SEQUENCER );
            menuMyMagic += "3";
            selectedCheckMagic = true;
            
            break;
            
          case MAINMENU_SELECT_SOUND:
            
            sysLogger.addLine( "toggle sound preference", LOGGER_SOURCE_SEQUENCER );
            
            gameMediaUserPreference = tGame.toggleMediaPreference();
                  
            var soundSprite = vScreen.getSpriteById( ID_SOUND );
            soundSprite.show( mediaFileArray[ gameMediaUserPreference ] );
            
            break;
            
          case MAINMENU_SELECT_LIKEME:
            
            sysLogger.addLine( "user likes this game", LOGGER_SOURCE_SEQUENCER );
            
            tGame.stopMediaSoundtrack();
            tGame.playMediaEffect( FILE_SOUND_CRISSCROSS, true );
            
            tGame.setSubState( 22 );
            
          default:
            // do nothing
            
            sysLogger.addLine( "user touch without selection",
                               LOGGER_SOURCE_SEQUENCER );
            
        }
        
        touch.setTouchOccurred( false );

        // check for magic sequence entered
        
        if ( selectedCheckMagic && ( menuMyMagic.length >= menuMyMagicLength ) ) {
          
          if ( menuMyMagic.length > menuMyMagicLength ) {
            
              menuMyMagic = menuMyMagic.substr(
                                menuMyMagic.length - menuMyMagicLength,
                                menuMyMagicLength );
              
          }
          
          sysLogger.addLine( "magic: " + menuMyMagic, LOGGER_SOURCE_SEQUENCER );
        
          var magicHighscore = new Highscore();
          var magicHash  = magicHighscore.lesserHash( menuMyMagic );
          var magicHash2 = magicHighscore.lesserHash( menuMyMagic.substr( 0, 13 ) );
        
          switch ( ( magicHash + magicHash2 ) ) {
          
            case menuMagicToggleGametype:
            
              sysLogger.addLine( "user entered magic sequence to change gamemode" );
            
              cyborgWordsVersion = 1 - cyborgWordsVersion;
              menuMyMagic = "";
            
              tGame.setState( GAME_INITIALIZING );
              tGame.setSubState( GAME_INITIALIZING_ENTRYSUBSTATE );
            
              break;
            
            case menuMagicInfinitePerks:
            
              sysLogger.addLine( "user entered magic sequence for infinite perks" );
            
              GAME_PAUSETHINK_NUM_FREE     = 99;
              GAME_PAUSETHINK_NUM_ADDICT   = 99;
              GAME_RECYCLE_NUM_FREE        = 99;
              GAME_RECYCLE_NUM_ADDICT      = 99;
              GAME_SHOWNEXTFIVE_NUM_FREE   = 99;
              GAME_SHOWNEXTFIVE_NUM_ADDICT = 99;
            
              menuMyMagic = "";

              tGame.setState( GAME_INITIALIZING );
              tGame.setSubState( GAME_INITIALIZING_ENTRYSUBSTATE );
            
              break;
            
            case menuMagicClearAll:
            
              sysLogger.addLine( "user entered magic sequence for clearing all local storage" );
            
              window.localStorage.clear();
              
              menuMyMagic = "";
            
              tGame.setState( GAME_INITIALIZING );
              tGame.setSubState( GAME_INITIALIZING_ENTRYSUBSTATE );
                        
              break;
            
          }
          
        }
        
      }

      break;

    case 2: // prepare for start new game screen or level select screen

      if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE ) {
        // simple graphics, just go to the next screen

        menuScrollCount = 100;
        
      }

      if ( menuScrollCount > 0 ) {
      
        tele.addRecord(
            "background resize",
            tGame.getLastSequencerDuraction(),
            TELEMETRY_AVERAGE );
        
      }

      // make a swift going-out animation
      switch ( menuScrollCount ) {

        case 0: menuBgFade = 0.95; break;
        case 1: menuBgFade = 0.85; break;
        case 2: menuBgFade = 0.55; break;

        default:

          menuBgFade = 0;
          tGame.setState( mainmenuSelectNextstate );
          tGame.setSubState( 0 );

          teleLogger.addLine( tele.getNameValueString( null, "<br />" ) );
         
      }

      if ( menuBgFade < 0.1 ) {
        // clear all icons

          for ( i = 0; i < MAX_SPRITES; i++ ) { vScreen.removeSprite( i ); }

      } else {

        var sprite = vScreen.getSpriteById( 0 );
        sprite.setWidth(  Math.floor( 490 * menuBgFade ) );
        sprite.setHeight( Math.floor( 735 * menuBgFade ) );
        sprite.show();
        sprite.move( Math.floor( 245 * ( 1. - menuBgFade ) ),
                     Math.floor( 367 * ( 1. - menuBgFade ) ) );

        for ( i = 0; i < 14; i++ ) {
          // move scroll letters out

          var lSprite = vScreen.getSpriteById( i + 10 );
          lSprite.move( lSprite.getXpos(), lSprite.getYpos() + 110 * ( 1. - menuBgFade ) );

        }

      }

      menuScrollCount++;

      break;
      
    case 22: // go to store to "like me"

      deviceSpecificsOpenLikeMe();

    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;

    case 31: // in case the app isn't stopped now, resume level menu
      
      tGame.setSubState( 1 );
      break;

  }

}
