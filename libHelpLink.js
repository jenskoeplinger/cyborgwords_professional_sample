/*
 * libHelpLink.js 2012-07-29
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New HelpLink()
 *
 * HelpLink.getLinkX() -- returns int
 * HelpLink.getLinkY() -- returns int
 * HelpLink.getLinkWidth() -- returns int
 * HelpLink.getLinkHeight() -- returns int
 * HelpLink.getFilename() -- returns String
 * HelpLink.getLinkType() -- returns int
 * HelpLink.getLinkTarget() -- returns Object ( null, HelpScreen, or String )
 * HelpLink.setLinkX( int )
 * HelpLink.setLinkY( int )
 * HelpLink.setLinkWidth( int )
 * HelpLink.setLinkHeight( int )
 * HelpLink.setFilename( String )
 * HelpLink.setLinkType( int )
 * HelpLink.setLinkTarget( Object )
 *
 */

var HELPLINK_TYPE_HOME     = 1; // leave help and go to home; no target
var HELPLINK_TYPE_FORWARD  = 2; // gets one level deeper into the help tree
var HELPLINK_TYPE_BACK     = 3; // gets one level up in the help tree
var HELPLINK_TYPE_URL      = 4; // opens an external URL
var HELPLINK_TYPE_SPECIAL  = 5; // custom action only for that help screen
var HELPLINK_TYPE_DISABLED = 6; // grayed out round; no target

function HelpLink() {

  var linkX = null;
  var linkY = null;
  var linkWidth = null;
  var linkHeight = null;
  var linkFilename = null;
  var linkType = null;
  var linkTarget = null;
  
  this.getLinkX        = function() { return linkX;        }
  this.getLinkY        = function() { return linkY;        }
  this.getLinkWidth    = function() { return linkWidth;    }
  this.getLinkHeight   = function() { return linkHeight;   }
  this.getFilename     = function() { return linkFilename; }
  this.getLinkType     = function() { return linkType;     }
  this.getLinkTarget   = function() { return linkTarget;   }

  this.setLinkX        = function( cValue ) { linkX = cValue;        }
  this.setLinkY        = function( cValue ) { linkY = cValue;        }
  this.setLinkWidth    = function( cValue ) { linkWidth = cValue;    }
  this.setLinkHeight   = function( cValue ) { linkHeight = cValue;   }
  this.setFilename     = function( cValue ) { linkFilename = cValue; }
  this.setLinkType     = function( cValue ) { linkType = cValue;     }
  this.setLinkTarget   = function( cValue ) { linkTarget = cValue;   }

}
