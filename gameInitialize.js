/*
 * gameInitialize.js 2012-08-03
 * Copyright (C) 2012 Jens Koeplinger
 */

var GAME_INITIALIZING_ENTRYSUBSTATE = 24;
var GAME_INITIALIZING_DELAY         = 2;

var gameInitializingDelayCount      = null;
var gameInitializingStartTime       = null;

var GAME_INITIALIZING_MINDURATION   = 5000; // ms

function gameInitialize( tGame ) {
  // load main menu; at this point:
  // - loading screen is in sprite id 50,
  // - all other sprite ids are hidden.

  var wListSpriteYPos     = 650;
  var wListSpriteSizePre  = 40;
  var wListSpriteSizePost = 68;

  switch ( tGame.getSubState() ) {

    // load ENABLE word list (begin with 2 letters)

    case 0:
      
      sysLogger.addLine( "gameInitialize()", LOGGER_SOURCE_SEQUENCER );
      
      gameLoadingSequence = 0;
      gameInitializingDelayCount = 0;
      gameInitializingStartTime = ( new Date() ).getTime();
      
      showSpriteloading( "L", wListSpriteSizePre, 52, 0 );
      tGame.setSubState( 1 );

      break;

    case 1:
      
      showSpriteloading( "L", wListSpriteSizePost, 52, 0 );
      tGame.setSubState( 2 );

      break;

    case 2:
      
      if ( trueAfterDelay() ) {
      
        loadWList2();
        showSpriteloading( "O", wListSpriteSizePre, 53, 70 );
        tGame.setSubState( 3 );
        
      }

      break;

    case 3:
      
      showSpriteloading( "O", wListSpriteSizePost, 53, 70 );
      tGame.setSubState( 4 );

      tele.addRecord(
          "word list",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_EXPONENTIAL + 0.5 );

      break;

    case 4:
      
      if ( trueAfterDelay() ) {

        loadWList3();
        showSpriteloading( "A", wListSpriteSizePre, 54, 140 );
        tGame.setSubState( 5 );

        tele.addRecord(
            "recovery",
            tGame.getLastSequencerDuraction(),
            TELEMETRY_AVERAGE );
        
      }

      break;

    case 5:
      
      showSpriteloading( "A", wListSpriteSizePost, 54, 140 );
      tGame.setSubState( 6 );

      tele.addRecord(
          "word list",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_EXPONENTIAL + 0.5 );

      break;

    case 6:
      
      if ( trueAfterDelay() ) {
        
        loadWList4();
        showSpriteloading( "D", wListSpriteSizePre, 55, 210 );
        tGame.setSubState( 7 );

        tele.addRecord(
            "recovery",
            tGame.getLastSequencerDuraction(),
            TELEMETRY_AVERAGE );
        
      }

      break;

    case 7:
      
      showSpriteloading( "D", wListSpriteSizePost, 55, 210 );
      tGame.setSubState( 8 );

      tele.addRecord(
          "word list",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_EXPONENTIAL + 0.5 );

      break;

    case 8:
      
      if ( trueAfterDelay() ) {
        
        loadWList5();
        showSpriteloading( "I", wListSpriteSizePre, 56, 280 );
        tGame.setSubState( 9 );

        tele.addRecord(
            "recovery",
            tGame.getLastSequencerDuraction(),
            TELEMETRY_AVERAGE );

      }
        
      break;

    case 9:
      
      showSpriteloading( "I", wListSpriteSizePost, 56, 280 );
      tGame.setSubState( 10 );

      tele.addRecord(
          "word list",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_EXPONENTIAL + 0.5 );

      break;

    case 10:
      
      if ( trueAfterDelay() ) {
        
        loadWList6();
        showSpriteloading( "N", wListSpriteSizePre, 57, 350 );
        tGame.setSubState( 11 );

        tele.addRecord(
            "recovery",
            tGame.getLastSequencerDuraction(),
            TELEMETRY_AVERAGE );
        
      }

      break;

    case 11:
      
      showSpriteloading( "N", wListSpriteSizePost, 57, 350 );
      tGame.setSubState( 12 );

      tele.addRecord(
          "word list",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_EXPONENTIAL + 0.5 );

      break;

    case 12:
      
      if ( trueAfterDelay() ) {
      
        loadWList7();
        showSpriteloading( "G", wListSpriteSizePre, 58, 420 );
        tGame.setSubState( 13 );

        tele.addRecord(
            "recovery",
            tGame.getLastSequencerDuraction(),
            TELEMETRY_AVERAGE );
        
      }

      break;

    case 13:
      
      showSpriteloading( "G", wListSpriteSizePost, 58, 420 );
      tGame.setSubState( 14 );

      tele.addRecord(
          "word list",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_EXPONENTIAL + 0.5 );

      break;

    case 14:

      tele.addRecord(
          "recovery",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_AVERAGE );

      // check telemetry from loading the word list whether we'll have
      // to slow down the graphics complexity
      
      if ( tele.getValue( "word list" ) >= TELEMETRY_WORDLIST_MINHIGHGRAPHICS ) {

        var logS = "word list telemetry at "
            + tele.getValue( "word list" )
            + "ms (should be less than "
            + TELEMETRY_WORDLIST_MINHIGHGRAPHICS
            + "ms); switch to simple graphics";

        sysLogger.addLine( logS, LOGGER_SOURCE_SEQUENCER );
        
        tGame.setGraphicsComplexity( GAME_COMPLEXITY_SIMPLE );
        
      }
      
      if ( tele.getValue( "recovery" )
           > ( GAME_FRAMEDURATION_TARGET * TELEMETRY_RECOVERY_MINHIGHGRAPHICS ) ) {
        
        var logS = "recovery telemetry at "
            + tele.getValue( "recovery" )
            + "ms (shoul not be more than a factor "
            + TELEMETRY_RECOVERY_MINHIGHGRAPHICS
            + " over "
            + GAME_FRAMEDURATION_TARGET
            + "ms); switch to simple graphics";
            
        sysLogger.addLine( logS, LOGGER_SOURCE_SEQUENCER );
        
        tGame.setGraphicsComplexity( GAME_COMPLEXITY_SIMPLE );
        
      }
      
      tGame.setSubState( 15 );
      break;
      
    case 15:
      
      tGame.playMediaEffect( FILE_SOUND_PICKUP );
      tGame.setSubState( 16 );
      break;
      
    case 16:
      
      gameMediaUserPreference = tGame.getMediaPreference();
      
      if ( gameMediaUserPreference == null ) {
        
        sysLogger.addLine( "no media preference found; assign default", LOGGER_SOURCE_SEQUENCER );
        
        gameMediaUserPreference = GAME_MEDIA_PREFERENCE_CYBORG;
        tGame.setMediaPreference( gameMediaUserPreference );
        
      }
      
      sysLogger.addLine( "user media preference is "
                         + gameMediaUserPreference,
                         LOGGER_SOURCE_SEQUENCER );
      
      tGame.setSubState( 17 );
      break;
      
    case 17:

      tGame.setSubState( 18 );
      break;
      
    case 18:

      tGame.playMediaSoundtrack( FILE_SOUND_MENU );
      tGame.setSubState( 19 );
      break;

    case 19: // for fast devices, wait at least a minimum duration on initializing

      var thisTime = ( new Date() ).getTime();
      
      if ( ( thisTime - gameInitializingStartTime ) > GAME_INITIALIZING_MINDURATION )
        tGame.setSubState( 20 );
      
      break;
      
    // transition animation

    case 20: // load main menu background

      tele.addRecord(
          "graphics complexity",
          tGame.getGraphicsComplexity(),
          TELEMETRY_SINGLE );

      tGame.setSubState( 21 );

      break;

    case 21: // move background out and main menu picture in

      if ( tGame.getGraphicsComplexity() >= GAME_COMPLEXITY_RICH ) { 

        var sprite = vScreen.getSpriteById( 50 );
        sprite.setWidth( 441 );
        sprite.setHeight( 661 );
        sprite.show();
        sprite.move( 25, 37 );

      }

      tele.addRecord(
          "background resize",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_AVERAGE );

      tGame.setSubState( 22 );
      break;

    case 22:

      if ( tGame.getGraphicsComplexity() >= GAME_COMPLEXITY_RICH ) {

        var sprite = vScreen.getSpriteById( 50 );
        sprite.setWidth( 343 );
        sprite.setHeight( 515 );
        sprite.show();
        sprite.move( 75, 111 );

      }

      tele.addRecord(
          "background resize",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_AVERAGE );

      tGame.setSubState( 23 );

      break;

    case 23:

      vScreen.removeSprite( 50 );

      for ( var i = 52; i < 59; i++ ) vScreen.removeSprite( i );

      tele.addRecord(
          "background resize",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_AVERAGE );

      tGame.setSubState( 24 );
      
      break;
      
    case 24:
      
      if ( tGame.getGraphicsComplexity() >= GAME_COMPLEXITY_RICH ) {

        var sprite = new Sprite( FILE_SPLASH_MENUBG_PREFIX
                                 + cyborgWordsVersion
                                 + FILE_SPLASH_GAMEBG_SUFFIX,
                                 343, 515 );
        sprite.setImgNode( document.getElementById( "bitmap0" ) );
        vScreen.putSprite( sprite, 0 );

        sprite.move( 75, 111 );
        sprite.show();

      }

      tGame.setSubState( 25 );
      
      break;

    case 25:

      if ( tGame.getGraphicsComplexity() >= GAME_COMPLEXITY_RICH ) {

        var sprite = vScreen.getSpriteById( 0 );
        sprite.setWidth( 441 );
        sprite.setHeight( 661 );
        sprite.show();
        sprite.move( 25, 37 );

      }

      tGame.setSubState( 26 );

      break;

    case 26:

      if ( tGame.getGraphicsComplexity() >= GAME_COMPLEXITY_RICH ) {

        var sprite = vScreen.getSpriteById( 0 );
        sprite.setWidth( 490 );
        sprite.setHeight( 735 );
        sprite.show();
        sprite.move( 0, 0 );

      } else {
        // draw the whole background
       
        var sprite = new Sprite( FILE_SPLASH_MENUBG_PREFIX
                                 + cyborgWordsVersion
                                 + FILE_SPLASH_GAMEBG_SUFFIX,
                                 490, 735 );
        sprite.setImgNode( document.getElementById( "bitmap0" ) );
        vScreen.putSprite( sprite, 0 );
        sprite.show();
        sprite.move( 0, 0 );
        
      }

      tele.addRecord(
          "background resize",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_AVERAGE );

      tGame.setSubState( 27 );

      break;

    default: // remove the "loading" splash screen, and advance to main menu

      tele.addRecord(
          "background resize",
          tGame.getLastSequencerDuraction(),
          TELEMETRY_AVERAGE );

      teleLogger.addLine( tele.getNameValueString( null, "<br />" ) );

      tGame.setState( GAME_MAINMENU );
      tGame.setSubState( 0 );

  }

  function showSpriteloading(ltrString, bitmapSize, bitmapNum, xOffset) {

    var sprite = new Sprite(
                         FILE_IMAGE_LTR_PREFIX + ltrString + FILE_IMAGE_LTR_SUFFIX,
                         bitmapSize,
                         bitmapSize );
    sprite.setImgNode( document.getElementById( "bitmap" + bitmapNum ) );
    vScreen.putSprite( sprite, bitmapNum );
    sprite.move(
               xOffset + ( 70 - bitmapSize ) / 2,
               wListSpriteYPos + ( 70 - bitmapSize ) / 2 );
    sprite.show();

  }
  
  function trueAfterDelay() {

    if ( gameInitializingDelayCount == null ) return true;
    
    gameInitializingDelayCount++;
    
    if ( gameInitializingDelayCount >= GAME_INITIALIZING_DELAY ) {
      
      gameInitializingDelayCount = 0;
      return true;
      
    } else {
      
      return false;
      
    }
    
  }

}

