/*
 * libScoringEngine.js 2012-07-29
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New ScoringEngine( Game )
 * 
 * ScoringEngine.calculateNonChaosWords()
 * ScoringEngine.checkForWordsHorizontal( Level, int row )
 * ScoringEngine.checkForWordsVertical( Level, int column )
 * ScoringEngine.dumpWordsFoundToLog( logger )
 * ScoringEngine.getFirstWordFound( [ boolean forceClean ] ) -- returns Array
 * ScoringEngine.resetWordsFound()
 * 
 */

var SCORING_MODEL_NONE  = -1;
var SCORING_MODEL_CLEAN = 0;
var SCORING_MODEL_CHAOS = 1;

var SCORING_LOGGING_INSANITY = false;

function ScoringEngine( tGame ) {
  // constructor

  var wordsFound = new Array();
  // format: array index is 0..( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y - 1 )
  // entries are:
  //   null -- not yet processed or no letter; otherwise:
  //   Array[] with entries;
  //     [ 0 ] -- horizontally:
  //                0 if isolated,
  //                1 if part of word,
  //                2 if part of nonword.
  //     [ 1 ] -- vertically:
  //                0 if isolated,
  //                1 if part of word,
  //                2 if part of nonword.
  //     [ 2 ] -- horizontally only if part of word: word begin X
  //     [ 3 ] -- vertically only if part of word: word begin Y
  //     [ 4 ] -- horizontally only if part of word: word length
  //     [ 5 ] -- vertically only if part of word: word length
  //     [ 6 ] -- true is part of chaos word (i.e., mismatch elsewhere),
  //              otherwise false if clean
  //     [ 7 ] -- xPosition of parent index (redundant)
  //     [ 8 ] -- yPosition of parent index (redundant)
  //     [ 9 ] -- true if tile was returned in getFirstWordFound
  
  this.resetWordsFound = function() {
   
    wordsFound = new Array();
    
  }
  
  this.calculateNonChaosWords = function() {
   
    var startX      = 0;
    var startY      = 0;
    var gridIdx     = 0;
    
    for ( var i = 0; i < ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ); i++ ) {
      // first pass: wipe chaos bit clean
      
      if ( !( wordsFound[ i ] == null ) ) wordsFound[ i ][ 6 ] = false;
      
    }
    
    while ( ( startX < LEVEL_GRID_MAX_X ) && ( startY < LEVEL_GRID_MAX_Y ) ) {
      // second pass: get all chaos words and flag all letters accordingly
      
      if ( !( wordsFound[ gridIdx ] == null ) ) {
        // first determine whether the word would only count in chaos mode
        
        var isChaos    = false;
        var propagateX = false;
        var propagateY = false;
        
        if ( wordsFound[ gridIdx ][ 6 ] == true ) isChaos = true;
        
        if (    ( wordsFound[ gridIdx ][ 0 ] == 2 )
             || ( wordsFound[ gridIdx ][ 1 ] == 2 ) ) {
          // letter is part of a nonword -> is chaos
          
          isChaos = true;
        
        }
          
        if (    ( startX == wordsFound[ gridIdx ][ 2 ] )
             && ( wordsFound[ gridIdx ][ 0 ] == 1 ) ) {
          // this is the beginning of a found horizontal word;
          // check all the other letters; if they are not locked yet
          // and part of a chaos word, then this is a chaos word overall
          
          propagateX = true;
          
          for ( var i = 1; i < wordsFound[ gridIdx ][ 4 ]; i++ ) {
              
            if ( wordsFound[ gridIdx + i ][ 1 ] == 2 ) {
              
              if ( !( level.getTileIsLocked( startX + i, startY ) ) ) {
              
                isChaos = true;
                
              }
              
            }
              
          }

        }
          
        if (    ( startY == wordsFound[ gridIdx ][ 3 ] )
             && ( wordsFound[ gridIdx ][ 1 ] == 1 ) ) {
          // this is the beginning of a found vertical word;
          // check all the other letters
                    
          propagateY = true;
          
          for ( var i = 1; i < wordsFound[ gridIdx ][ 5 ]; i++ ) {
              
            if ( wordsFound[ gridIdx + ( i * LEVEL_GRID_MAX_X ) ][ 0 ] == 2 ) {
              
              if ( !( level.getTileIsLocked( startX, startY + ( i * LEVEL_GRID_MAX_X ) ) ) ) {
              
                isChaos = true;
                
              }
                
            }
              
          }
            
        }
        
        // now mark this letter with isChaos; if isChaos then also
        // propagate this along any words that this letter is part of;
        // exception: tiles that are already locked are always flagged "clean"
        
        if ( level.getTileIsLocked( startX, startY ) ) {
          // tile is already locked; is nonchaos by default even if movable
          // tiles elsewhere in the word make it a nonsense word overall
          
          wordsFound[ gridIdx ][ 6 ] = false;
          
        } else {
        
          wordsFound[ gridIdx ][ 6 ] = isChaos;
          
        }
        
        if ( propagateX && isChaos ) {
         
          for ( var i = 1; i < wordsFound[ gridIdx ][ 4 ]; i++ ) {
            
              if ( level.getTileIsLocked( startX + i, startY ) ) {
               
                wordsFound[ gridIdx + i ][ 6 ] = false;
                
              } else {
                
                wordsFound[ gridIdx + i ][ 6 ] = true;
                
              }
              
          }
          
        }
        
        if ( propagateY && isChaos ) {
         
          for ( var i = 1; i < wordsFound[ gridIdx ][ 5 ]; i++ ) {
            
            if ( level.getTileIsLocked( startX, startY + ( i * LEVEL_GRID_MAX_X ) ) ) {
              
              wordsFound[ gridIdx + ( i * LEVEL_GRID_MAX_X ) ][ 6 ] = false;
              
            } else {
            
              wordsFound[ gridIdx + ( i * LEVEL_GRID_MAX_X ) ][ 6 ] = true;
              
            }
            
          }
          
        }
        
      }
      
      startX  += 1;
      gridIdx += 1;
      
      if ( startX >= LEVEL_GRID_MAX_X ) {
       
        startX  = 0;
        startY += 1;
        
      }
      
    }
    
  }
  
  this.checkForWordsHorizontal = function( inLevel, rowNum ) {
    
    var thisX          = 0;
    var startX         = null;
    var thisTile       = null;
    var thisWord       = null;
    
    var gridIdx = rowNum * LEVEL_GRID_MAX_X;
    
    while ( thisX < LEVEL_GRID_MAX_X ) {
      
      thisTile = inLevel.getTilePlacedXY( thisX, rowNum );
      thisWord = "";
      startX   = null;
      startIdx = null;
      
      if ( thisTile == LEVEL_TILE_NONE ) wordsFound[ gridIdx ] = null;
      
      while ( !( thisTile == LEVEL_TILE_NONE ) ) {
        
        if ( startX == null ) {
          
          startX   = thisX;
          startIdx = gridIdx;
          
        }
            
        thisWord += LEVEL_TILES[ thisTile ][ 0 ];
        thisX    += 1;
        gridIdx  += 1;
        
        if ( thisX < LEVEL_GRID_MAX_X ) {
          
          thisTile = inLevel.getTilePlacedXY( thisX, rowNum );
          
          if ( thisTile == LEVEL_TILE_NONE ) wordsFound[ gridIdx ] = null;
          
        } else {
         
          thisTile = LEVEL_TILE_NONE;
          
        }
        
      }
      
      if ( !( thisWord == "" ) ) {
        
        var wordLength = thisWord.length;
        
        if ( wordLength == 1 ) {
          // isolated letter
          
          if ( wordsFound[ startIdx ] == null ) wordsFound[ startIdx ] = new Array();
          
          wordsFound[ startIdx ][ 0 ] = 0;
          wordsFound[ startIdx ][ 2 ] = startX;
          wordsFound[ startIdx ][ 4 ] = 1;
          wordsFound[ startIdx ][ 7 ] = startX;
          wordsFound[ startIdx ][ 8 ] = rowNum;
          
        } else {
          // check for word
          
          var isWord = 2;
          if ( wList[ thisWord ] == 1 ) isWord = 1;
          
          var thisIdx = startIdx;
          
          for( var i = startX; i < thisX; i++ ) {
           
            if ( wordsFound[ thisIdx ] == null ) wordsFound[ thisIdx ] = new Array();
            
            wordsFound[ thisIdx ][ 0 ] = isWord;
            wordsFound[ thisIdx ][ 2 ] = startX;
            wordsFound[ thisIdx ][ 4 ] = thisX - startX;
            wordsFound[ thisIdx ][ 7 ] = i;
            wordsFound[ thisIdx ][ 8 ] = rowNum;
            
            thisIdx += 1;
            
          }
          
        }
        
      }
      
      thisX   += 1;
      gridIdx += 1;
      
    }
    
  }
  
  this.checkForWordsVertical = function( inLevel, columnNum ) {
    
    var thisY          = 0;
    var startY         = null;
    var thisTile       = null;
    var thisWord       = null;
    
    var gridIdx = columnNum;
    
    while ( thisY < LEVEL_GRID_MAX_Y ) {
      
      thisTile = inLevel.getTilePlacedXY( columnNum, thisY );
      thisWord = "";
      startY   = null;
      startIdx = null;
      
      if ( thisTile == LEVEL_TILE_NONE ) wordsFound[ gridIdx ] = null;
      
      while ( !( thisTile == LEVEL_TILE_NONE ) ) {
        
        if ( startY == null ) {
          
          startY   = thisY;
          startIdx = gridIdx;
          
        }
            
        thisWord += LEVEL_TILES[ thisTile ][ 0 ];
        thisY    += 1;
        gridIdx  += LEVEL_GRID_MAX_X;
        
        if ( thisY < LEVEL_GRID_MAX_Y ) {
          
          thisTile = inLevel.getTilePlacedXY( columnNum, thisY );
          
          if ( thisTile == LEVEL_TILE_NONE ) wordsFound[ gridIdx ] = null;
          
        } else {
         
          thisTile = LEVEL_TILE_NONE;
          
        }
        
      }
      
      if ( !( thisWord == "" ) ) {
        
        var wordLength = thisWord.length;
        
        if ( wordLength == 1 ) {
          // isolated letter
          
          if ( wordsFound[ startIdx ] == null ) wordsFound[ startIdx ] = new Array();
          
          wordsFound[ startIdx ][ 1 ] = 0;
          wordsFound[ startIdx ][ 3 ] = startY;
          wordsFound[ startIdx ][ 5 ] = 1;
          wordsFound[ startIdx ][ 7 ] = columnNum;
          wordsFound[ startIdx ][ 8 ] = startY;
          
        } else {
          // check for word
          
          var isWord = 2;
          if ( wList[ thisWord ] == 1 ) isWord = 1;
          
          var thisIdx = startIdx;
          
          for( var i = startY; i < thisY; i++ ) {
           
            if ( wordsFound[ thisIdx ] == null ) wordsFound[ thisIdx ] = new Array();
            
            wordsFound[ thisIdx ][ 1 ] = isWord;
            wordsFound[ thisIdx ][ 3 ] = startY;
            wordsFound[ thisIdx ][ 5 ] = thisY - startY;
            wordsFound[ thisIdx ][ 7 ] = columnNum;
            wordsFound[ thisIdx ][ 8 ] = i;
            
            thisIdx += LEVEL_GRID_MAX_X;
            
          }
          
        }
        
      }
      
      thisY   += 1;
      gridIdx += LEVEL_GRID_MAX_X;
      
    }
    
  }
  
  this.getFirstWordFound = function( forceClean ) {
    // loops through the wordsFound array and first searches
    // for crisscross words, then for regular words found
    
    var wordVal      = null;
    var isCrissCross = null;
    var isClean      = null;
    var isWord       = null;
    
    for( var passNum = 1; passNum < 3; passNum++ ) {
      
      for( var gridIdx = 0;
           gridIdx < ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y );
           gridIdx++ ) {
        
        if ( !( wordsFound[ gridIdx ] == null ) ) {
          // letter exists
          
          if ( !(    ( wordsFound[ gridIdx ][ 0 ] == null )
                  || ( wordsFound[ gridIdx ][ 1 ] == null )
                  || ( wordsFound[ gridIdx ][ 9 ] == true ) ) ) {
            // letter was checked horizontally and vertically for a word,
            // and wasn't returned as part of a word found yet, either
            
            wordVal = wordsFound[ gridIdx ][ 0 ] + 4 * wordsFound[ gridIdx ][ 1 ];
          
            if ( wordVal == 5 ) isCrissCross = true; else isCrissCross = false;
            if ( wordsFound[ gridIdx ][ 6 ] == false ) isClean = true; else isClean = false;
            
            if (    ( wordsFound[ gridIdx ][ 0 ] == 1 )
                 || ( wordsFound[ gridIdx ][ 1 ] == 1 ) ) {
              
              isWord = true;
            
            } else {
              
              isWord = false;
              
            }

            if ( forceClean && ( !( isClean ) ) ) isWord = false;
            if ( ( passNum == 1 ) && ( !( isCrissCross ) ) ) isWord = false;
            
            if ( isWord ) {
              // return this position as word(s) found;
              // mark as such (set index [9] to true)
              
              if ( wordsFound[ gridIdx ][ 0 ] == 1 ) {
               
                for ( var i = wordsFound[ gridIdx ][ 2 ];
                      i < ( wordsFound[ gridIdx ][ 2 ] + wordsFound[ gridIdx ][ 4 ] );
                      i++ ) {
                       
                  wordsFound[ i + LEVEL_GRID_MAX_X * wordsFound[ gridIdx ][ 8 ]
                            ][ 9 ] = true;
                        
                }
                
              }
              
              if ( wordsFound[ gridIdx ][ 1 ] == 1 ) {
               
                for ( var i = wordsFound[ gridIdx ][ 3 ];
                      i < ( wordsFound[ gridIdx ][ 3 ] + wordsFound[ gridIdx ][ 5 ] );
                      i++ ) {
                       
                  wordsFound[ wordsFound[ gridIdx ][ 7 ] + LEVEL_GRID_MAX_X * i
                            ][ 9 ] = true;
                  
                }
                
              }
              
              return wordsFound[ gridIdx ];
              
            }
          
          }
        
        }
        
      }
            
    }
    
    return null;
    
  }
  
  this.dumpWordsFoundToLog = function( logger ) {
  
    logger.addLine( "ScoringEngine wordsFound[] dump", LOGGER_SOURCE_SCORING );
    var textToLog = "";
    
    for ( var i = 0; i < ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ); i++ ) {
      
      if ( wordsFound[ i ] == null ) {
        
        textToLog += "[" + i + "]=null -- ";
        
      } else {
      
        textToLog += "[" + i + "]={";
        
        for ( var j = 0; j < wordsFound[ i ].length; j++ ) {
          
          if (    ( wordsFound[ i ][ j ] == undefined )
               || ( wordsFound[ i ][ j ] == null      ) ) {
            
            textToLog += ",";
            
          } else {
           
            textToLog += "" + wordsFound[ i ][ j ] + ",";
            
          }
          
        }
        
        textToLog += "} -- ";
       
      }
    
    }
    
    logger.addLine( textToLog,
                    LOGGER_SOURCE_SCORING );
  
  }

}
