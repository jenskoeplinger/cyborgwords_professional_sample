/*
 * gameLowerLid.js 2012-06-14
 * Copyright (C) 2012 Jens Koeplinger
 */

function gameLowerLid( tGame ) {
  // lowers the lid of the trash can and letter under it (if any)

  sysLogger.addLine( "gameLowerLid()", LOGGER_SOURCE_SEQUENCER );

  // letter shoot animation
  
  var movingSSteps = 5;
  if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE )
      movingSSteps = 3;
  
  var sprite = vScreen.getSpriteById( ID_TRASHBINLID );
  
  tGame.moveSprite(
      sprite,
      CONVEYOR_TRASH_XPOS,
      CONVEYOR_TRASH_YPOS - 18,
      movingSSteps,
      GAME_MOVESPRITE_SINE_FLYOUT );

  gameLidIsOpen = false;

  sprite = vScreen.getSpriteById( tGame.getConveyorPosImg( 5 ) );
  var rTile = tGame.getConveyorTile( 5 );
  
  if ( !( rTile == LEVEL_TILE_NONE ) ) {
    // before sliding it down, capture the letter tile for
    // possible later recycling
    
    sysLogger.addLine( "capture tile '"
                       + rTile
                       + "' for possible recycling later",
                       LOGGER_SOURCE_SEQUENCER );
    
    gameRecycleLetterTile = rTile;
    
  } else if ( !( gameRecycleIsShowing ) ) {
    
    gameRecycleLetterTile = LEVEL_TILE_NONE;
    
  }

  if ( sprite ) {

    if ( sprite.getIsPickedUp() ) {
      // force detach from Touch and force slide into the trash bin
      
      touch.setPickedUpSprite();
      sprite.setIsPickedUp( false );
      
      sprite.setWidth( 70 );
      sprite.setHeight( 70 );
      sprite.show();
      
      var pxPos = sprite.getXposPickedUp();
      var pyPos = sprite.getYposPickedUp();
      sprite.move( pxPos, pyPos );
      
    }
    
    tGame.moveSprite(
        sprite,
        CONVEYOR_TRASH_XPOS + 5,
        CONVEYOR_TRASH_YPOS,
        movingSSteps,
        GAME_MOVESPRITE_SINE_FLYOUT,
        true );

  }

}
