/*
 * gameHelp.js 2012-08-18
 * Copyright (C) 2012 Jens Koeplinger
 */

var currentHelpScreen       = null;
var previousHelpScreen      = null;

var helpScreenTransition    = null;
var helpScreenMovingSteps   = null;

var HELP_SCREEN_TRANSITION_NONE      = 0;
var HELP_SCREEN_TRANSITION_MOVERIGHT = 1;
var HELP_SCREEN_TRANSITION_MOVELEFT  = 2;

var currentHelpBodyId       = null; // the image ID that is currently showing
var currentHelpIconId       = null;

var HELP_SCREEN_ICON_MAXCOUNT = 10;

var HELP_SCREEN_BODY_ID     = new Array(); // maps currentHelpBodyId to actual IMG node

var helpScreen1Intro        = null; // HelpScreen objects
var helpScreen2Store        = null;
var helpScreen3Gamemode     = null;
var helpScreen4Terms        = null;
var helpScreen5Credits      = null;
var helpScreen6Thanks       = null;
var helpScreen7Help         = null;
var helpScreen8Tips         = null;
var helpScreen9Helpers      = null;
var helpScreen10ChaosExpert = null;

var HELP_MENU_BUTTONBOTTOM_YPOS = 592;
var HELP_MENU_BUTTONBOTTOM_SIZE = 133;
var HELP_MENU_BUTTONLEFT_XPOS   =  20;
var HELP_MENU_BUTTONMIDDLE_XPOS = 173;
var HELP_MENU_BUTTONRIGHT_XPOS  = 326;
var HELP_MENU_BACKBUTTON_SIZE   =  65;
var HELP_MENU_BACKBUTTON_XPOS   =   2;
var HELP_MENU_BACKBUTTON_YPOS   =   1;
var HELP_MENU_STATICHEADER_XPOS =  70;

var HELP_MENU_BACKGROUND_OFFSET_Y = 85;

function gameHelp( tGame ) {

  // ---------
  // sequencer
  
  switch ( tGame.getSubState() ) {

    case 0: // remove all icons, reset all variables

      sysLogger.addLine( "gameHelp()", LOGGER_SOURCE_SEQUENCER );

      for ( var i = 0; i < MAX_SPRITES; i++ ) {
        
        vScreen.removeSprite( i );
        
      }

      if ( helpScreen1Intro == null ) loadHelpscreenVariables();
      
      currentHelpScreen     = null;
      previousHelpScreen    = null;
      helpScreenTransition  = null;
      helpScreenMovingSteps = 4;
      
      updateScoringGamemodeIcons();
      
      tGame.setSubState( 5 );

      break;
      
    case 5: // transition engine start

      if ( currentHelpScreen == null ) {
        
        sysLogger.addLine( "intro screen", LOGGER_SOURCE_SEQUENCER );
        
        currentHelpScreen    = helpScreen1Intro;
        helpScreenTransition = HELP_SCREEN_TRANSITION_MOVERIGHT;
        
      }

      if ( currentHelpScreen.getBackgroundFile() == null ) {
        
        sysLogger.addLine( "chose default background file", LOGGER_SOURCE_SEQUENCER );
        currentHelpScreen.setBackgroundFile( FILE_HELP_BACKGROUND_DEFAULT );
        
      }
      
      var redrawBackground = false;
      
      if ( previousHelpScreen == null ) {
        
        sysLogger.addLine( "force background redraw since no previous screen", LOGGER_SOURCE_SEQUENCER );
        redrawBackground = true;
        
      } else if ( !( previousHelpScreen.getBackgroundFile() == currentHelpScreen.getBackgroundFile() ) ) {
        
        sysLogger.addLine( "force redraw background since previous screen differed", LOGGER_SOURCE_SEQUENCER );
        redrawBackground = true;
        
      }

      if ( redrawBackground ) {
        
        sysLogger.addLine( "redraw background", LOGGER_SOURCE_SEQUENCER );
        
        vScreen.removeSprite( ID_HELP_HEADER );
        vScreen.removeSprite( ID_HELP_BODY_1 );
        vScreen.removeSprite( ID_HELP_BODY_2 );
        
        for ( var i = ID_HELP_ICON_START; i <= ID_HELP_ICON_MAXID; i++ ) {
          
          vScreen.removeSprite( i );
          
        }
        
        sysLogger.addLine( "new background: "
                           + currentHelpScreen.getBackgroundFile(),
                           LOGGER_SOURCE_SEQUENCER );
        
        var sprite = new Sprite( currentHelpScreen.getBackgroundFile(), 490, 735 );
            
        sprite.setImgNode(
                document.getElementById( "bitmap" + ID_HELP_BG ) );
        vScreen.putSprite( sprite, ID_HELP_BG );
        sprite.show();
        sprite.move( 0, 0 );
        
        tGame.setSubState( 6 ); // give a bit of time
        
      } else {
        
        tGame.setSubState( 9 );
        
      }
      
      break;
      
    case 6: // give some time after background file change
    case 7:
    case 8:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 9: // draw header, remove buttons that aren't on the screen, and prepare help body slide-in
      
      if ( currentHelpScreen.getHasStaticHeader() ) {
        
        sysLogger.addLine( "draw static header icon", LOGGER_SOURCE_SEQUENCER );
        
        var sprite = new Sprite( FILE_MENU_GAMETITLE_SMALL,
                                 MENU_PLAY_GAMETITLE_WIDTH,
                                 HELP_MENU_BACKBUTTON_SIZE );
        
        sprite.setImgNode(
                document.getElementById( "bitmap" + ID_HELP_HEADER ) );
        vScreen.putSprite( sprite, ID_HELP_HEADER );
        sprite.show();
        sprite.move( HELP_MENU_STATICHEADER_XPOS, HELP_MENU_BACKBUTTON_YPOS );
        
      }

      if ( !( previousHelpScreen == null ) ) {
        
        sysLogger.addLine( "remove all icons that were not on the previous help screen",
                           LOGGER_SOURCE_SEQUENCER );
        
        for (var i = ID_HELP_ICON_START; i < ( ID_HELP_ICON_START + HELP_SCREEN_ICON_MAXCOUNT ); i++ ) {
          
          var sprite = vScreen.getSpriteById( i );
          var fileName = null;
          
          if ( sprite ) {
            
            fileName = sprite.getFilename();
            
            if ( currentHelpScreen.getAllLinkFilenames().indexOf( fileName ) == -1 ) {
              
              vScreen.removeSprite( i );
              
            }
          
          }
          
          // remove any shadow / gray out (if any)
          vScreen.removeSprite( i + HELP_SCREEN_ICON_MAXCOUNT );
          
        }
        
      }
      
      // prepare for current help body (if any) slide out
      
      var slideOutId      = HELP_SCREEN_BODY_ID[ currentHelpBodyId ];
      var spriteOut       = vScreen.getSpriteById( slideOutId );
      var slideOutSteps   = helpScreenMovingSteps;
      var slideOutXTarget = null;
      
      if ( spriteOut ) {
        
        sysLogger.addLine( "slide out previous help body", LOGGER_SOURCE_SEQUENCER );
        
        switch ( helpScreenTransition ) {
          
          case HELP_SCREEN_TRANSITION_MOVERIGHT:
            
            slideOutXTarget = 490;
            break;
            
          case HELP_SCREEN_TRANSITION_MOVELEFT:
            
            slideOutXTarget = -490;
            break;
            
          default:

            slideOutSteps   = 1;
            slideOutXTarget = 490;
          
        }
        
        tGame.moveSprite(
                spriteOut,
                slideOutXTarget,
                HELP_MENU_BACKGROUND_OFFSET_Y,
                slideOutSteps * 2,
                GAME_MOVESPRITE_SHOWANDOUT,
                true );
        
      } else {
       
        sysLogger.addLine( "no previous help body existed, no slide-out", LOGGER_SOURCE_SEQUENCER );
        
      }
      
      // prepare for help body (if any) slide in
      
      currentHelpBodyId = 1 - currentHelpBodyId;
      
      var slideInId         = HELP_SCREEN_BODY_ID[ currentHelpBodyId ];
      var spriteIn          = vScreen.getSpriteById( slideInId );
      var slideInSteps      = helpScreenMovingSteps;
      var slideInXStart     = -slideOutXTarget;

      if ( spriteIn ) vScreen.removeSprite( slideInId );
      
      var slideInFilename = currentHelpScreen.getDisplayFile();
      
      if ( slideInFilename ) {
       
        sysLogger.addLine( "slide in help body file: "
                           + slideInFilename,
                           LOGGER_SOURCE_SEQUENCER );
        
        var spriteIn = new Sprite( slideInFilename,
                                   490,
                                   490 );
        
        spriteIn.setImgNode(
                document.getElementById( "bitmap" + slideInId ) );
        vScreen.putSprite( spriteIn, slideInId );
        spriteIn.show();
        spriteIn.move( slideInXStart, HELP_MENU_BACKGROUND_OFFSET_Y );
        
        tGame.moveSprite(
                spriteIn,
                0,
                HELP_MENU_BACKGROUND_OFFSET_Y,
                slideOutSteps * 2,
                GAME_MOVESPRITE_SHOWANDOUT );

      }
      
      tGame.setSubState( 15 ); // speed-up
      break;
      
    case 10: // give some time to draw and let the slide-in/-out animation play
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 20: // background, header, and body slide in has happened; draw all icons

      currentHelpIconId = ID_HELP_ICON_START;
      var allLinks      = currentHelpScreen.getAllLinks();
      var thisLink      = null;
      var linkFilename  = null;

      sysLogger.addLine( "draw all link icons", LOGGER_SOURCE_SEQUENCER );
      
      for ( thisLinkId in allLinks ) {
        
        thisLink = allLinks[ thisLinkId ];
        
        linkFilename = thisLink.getFilename();
        
        if ( linkFilename ) {
          
          sysLogger.addLine( "draw icon: "
                             + linkFilename,
                             LOGGER_SOURCE_SEQUENCER );
          
          var sprite = new Sprite( linkFilename,
                                   thisLink.getLinkWidth(),
                                   thisLink.getLinkHeight() );
        
          sprite.setImgNode(
                    document.getElementById( "bitmap" + currentHelpIconId ) );
          vScreen.putSprite( sprite, currentHelpIconId );
          sprite.show();
          sprite.move( thisLink.getLinkX(), thisLink.getLinkY() );

          if ( thisLink.getLinkType() == HELPLINK_TYPE_DISABLED ) {
            // gray out this link icon by placing something opaque over it
            
            var grayOutIconId = currentHelpIconId + HELP_SCREEN_ICON_MAXCOUNT;
            
            sysLogger.addLine( "disabled link, gray out",
                               LOGGER_SOURCE_SEQUENCER );
        
            sprite = new Sprite( FILE_SPLASH_OPAQUE_ROUND_50,
                                 thisLink.getLinkWidth() - 6,
                                 thisLink.getLinkHeight() - 6 );
            
            sprite.setImgNode( document.getElementById( "bitmap" + grayOutIconId ) );
            vScreen.putSprite( sprite, grayOutIconId );
            sprite.show();
            sprite.move( thisLink.getLinkX() + 3, thisLink.getLinkY() + 3 );
            
          }
          
        }
        
        currentHelpIconId++;
        
      }

      touch.setTouchOccurred( false );
      
      tGame.setSubState( 30 );
      
      break;

    case 30: // wait for user touch

      if ( touch.getTouchOccurred() && ( !( touch.getIsTouching() ) ) ) {

        var thisX = touch.getTouchX();
        var thisY = touch.getTouchY();
        touch.setTouchOccurred( false );

        var allLinks      = currentHelpScreen.getAllLinks();
        var thisLink      = null;
        var foundLink     = null;

        sysLogger.addLine( "touch at "
                           + thisX
                           + ", "
                           + thisY
                           + "; check all links",
                           LOGGER_SOURCE_SEQUENCER );
      
        for ( thisLinkId in allLinks ) {
        
          thisLink = allLinks[ thisLinkId ];

          if (    ( thisX > thisLink.getLinkX() )
               && ( thisX < ( thisLink.getLinkX() + thisLink.getLinkWidth() ) )
               && ( thisY > thisLink.getLinkY() )
               && ( thisY < ( thisLink.getLinkY() + thisLink.getLinkHeight() ) ) ) {
           
            sysLogger.addLine( "found touch within bounds of: "
                               + thisLink.getFilename(),
                               LOGGER_SOURCE_SEQUENCER );
            
            foundLink = thisLink;
            
          }
        
        }
        
        if ( foundLink && ( !( foundLink.getLinkType() == HELPLINK_TYPE_DISABLED ) ) ) {
          
          tGame.playMediaEffect( FILE_SOUND_PICKUP );

          switch ( foundLink.getLinkType() ) {
            
            case HELPLINK_TYPE_HOME:
              
              sysLogger.addLine( "leave help and go to main menu", LOGGER_SOURCE_SEQUENCER );
              
              for ( var i = 0; i < MAX_SPRITES; i++ ) {
        
                vScreen.removeSprite( i );
        
              }

              tGame.setState( GAME_INITIALIZING );
              tGame.setSubState( GAME_INITIALIZING_ENTRYSUBSTATE );

              break;
              
            case HELPLINK_TYPE_FORWARD:
              
              sysLogger.addLine( "slide to the left", LOGGER_SOURCE_SEQUENCER );
              
              helpScreenTransition = HELP_SCREEN_TRANSITION_MOVELEFT;
              previousHelpScreen   = currentHelpScreen;
              currentHelpScreen    = foundLink.getLinkTarget();
              
              tGame.setSubState( 5 );
              
              break;
              
            case HELPLINK_TYPE_BACK:
              
              sysLogger.addLine( "slide to the right", LOGGER_SOURCE_SEQUENCER );
              
              helpScreenTransition = HELP_SCREEN_TRANSITION_MOVERIGHT;
              previousHelpScreen   = currentHelpScreen;
              currentHelpScreen    = foundLink.getLinkTarget();
              
              tGame.setSubState( 5 );
              
              break;
              
            case HELPLINK_TYPE_URL:
              
              sysLogger.addLine( "open external URL: "
                                 + foundLink.getLinkTarget(),
                                 LOGGER_SOURCE_SEQUENCER );
              
              tGame.stopMediaSoundtrack();
              tGame.playMediaEffect( FILE_SOUND_CRISSCROSS, true );

              if ( osRequiresStoreActionEan ) {

                var thisExtraValue = osStoreActionExtraValue;
                
                if ( thisY < 250 ) thisExtraValue = osStoreActionExtraValueFree;

                window.plugins.webintent.openEan(
                  {
                    intentAction: foundLink.getLinkTarget(),
                    intentEan: thisExtraValue
                  },
                  function() {},
                  function(e) {
                  alert( osRequiresStoreActionEanFailMessage );
                  }
                );

              } else {

                window.location.replace( foundLink.getLinkTarget() );

              }
              
              tGame.setSubState( 40 );
              
              break;
              
            case HELPLINK_TYPE_SPECIAL:
              
              sysLogger.addLine( "special link type detected with target: "
                                 + foundLink.getLinkTarget(),
                                 LOGGER_SOURCE_SEQUENCER );
              
              executeSpecialLink( foundLink );
              
              break;
              
            default:
              
              sysLogger.addLine( "unknown link type, ignore: "
                                 + foundLink.getLinkType(),
                                 LOGGER_SOURCE_SEQUENCER );
              
          }
          
        }
        
      }
      
      break;
      
    case 40: // post-URL open wait and go back to main menu
    case 41:
    case 42:
    case 43:
    case 44:
    case 45:
    case 46:
    case 47:
    case 48:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 49: // in case the app isn't stopped by now, resume help

      tGame.setSubState( 30 );
      break;

    default:
      // do nothing anymore, now and forever after
      
  }
  
  // -----------------
  // private functions
  
  function executeSpecialLink( foundLink ) {
    
    switch ( foundLink.getLinkTarget() ) {

      case "chaos":
        
        tGame.setGamemodeDefault( SCORING_MODEL_CHAOS );
        updateScoringGamemodeIcons();
        
        break;
        
      case "expert":

        tGame.setGamemodeDefault( SCORING_MODEL_CLEAN );
        updateScoringGamemodeIcons();
        
        break;
        
      case "nodefault":
        
        tGame.setGamemodeDefault( SCORING_MODEL_NONE );
        updateScoringGamemodeIcons();
        
        break;
        
      default:
        sysLogger.addLine( "unknown link target, ignore: "
                           + foundLink.getLinkTarget(),
                           LOGGER_SOURCE_SEQUENCER );
      
    }
    
    // set to substate that redraws all icons
    tGame.setSubState( 20 );
    
  }
  
  function updateScoringGamemodeIcons() {

    var chaosLink     = helpScreen3Gamemode.getLinkByTargetString( "chaos" );
    var expertLink    = helpScreen3Gamemode.getLinkByTargetString( "expert" );
    var nodefaultLink = helpScreen3Gamemode.getLinkByTargetString( "nodefault" );
    
    switch ( tGame.getGamemodeDefault() ) {
      
      case SCORING_MODEL_CHAOS:
        
        chaosLink.setFilename( FILE_HELP_SELECT_CHAOS_CHECKED );
        expertLink.setFilename( FILE_HELP_SELECT_EXPERT_UNCHECKED );
        nodefaultLink.setFilename( FILE_HELP_SELECT_NODEFAULT_UNCHECKED );
        
        break;
        
      case SCORING_MODEL_CLEAN:
        
        chaosLink.setFilename( FILE_HELP_SELECT_CHAOS_UNCHECKED );
        expertLink.setFilename( FILE_HELP_SELECT_EXPERT_CHECKED );
        nodefaultLink.setFilename( FILE_HELP_SELECT_NODEFAULT_UNCHECKED );
        
        break;
        
      case SCORING_MODEL_NONE:
        
        chaosLink.setFilename( FILE_HELP_SELECT_CHAOS_UNCHECKED );
        expertLink.setFilename( FILE_HELP_SELECT_EXPERT_UNCHECKED );
        nodefaultLink.setFilename( FILE_HELP_SELECT_NODEFAULT_CHECKED );
        
        break;
      
    }
        
  }
  
  function loadHelpscreenVariables() {
    
    sysLogger.addLine( "load helpscreen variables", LOGGER_SOURCE_SEQUENCER );
    
    HELP_SCREEN_BODY_ID[ 0 ] = ID_HELP_BODY_1;
    HELP_SCREEN_BODY_ID[ 1 ] = ID_HELP_BODY_2;
    
    var tmpLlink = null;

    helpScreen1Intro        = new HelpScreen();
    helpScreen2Store        = new HelpScreen();
    helpScreen3Gamemode     = new HelpScreen();
    helpScreen4Terms        = new HelpScreen();
    helpScreen5Credits      = new HelpScreen();
    helpScreen6Thanks       = new HelpScreen();
    helpScreen7Help         = new HelpScreen();
    helpScreen8Tips         = new HelpScreen();
    helpScreen9Helpers      = new HelpScreen();
    helpScreen10ChaosExpert = new HelpScreen();
    
    // help intro page
    
    helpScreen1Intro.setBackgroundFile( FILE_SPLASH_LOADING );
    helpScreen1Intro.setDisplayFile( FILE_HELP_BODY_1 );
    helpScreen1Intro.setHasStaticHeader( false );
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 10 );
    tmpLink.setLinkY( 230 );
    tmpLink.setLinkWidth( HELP_MENU_BACKBUTTON_SIZE );
    tmpLink.setLinkHeight( HELP_MENU_BACKBUTTON_SIZE );
    tmpLink.setFilename( FILE_MENU_BACK );
    tmpLink.setLinkType( HELPLINK_TYPE_HOME );
    tmpLink.setLinkTarget( null );
    helpScreen1Intro.addLink( tmpLink );
    
    helpScreen1Intro.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_HELP, 
                                  helpScreen7Help ) );
    
    helpScreen1Intro.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_SETTINGS, 
                                  helpScreen3Gamemode ) );
    
    helpScreen1Intro.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_STORE, 
                                  helpScreen2Store ) );
    
    // store options
    
    if ( osHasHDVersion ) {   

      helpScreen2Store.setDisplayFile( FILE_HELP_BODY_2 );
      
    } else {
      
      helpScreen2Store.setDisplayFile( FILE_HELP_BODY_2_NO_HD );
      
    }
    
    helpScreen2Store.setHasStaticHeader( true );
    
    helpScreen2Store.addLink( returnHelpLinkBackbutton() );
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 0 );
    tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y );
    tmpLink.setLinkWidth( 490 );
    tmpLink.setLinkHeight( 110 );
    tmpLink.setFilename( null );
    tmpLink.setLinkType( HELPLINK_TYPE_URL );
    tmpLink.setLinkTarget( STORE_LINK_URL_FREE_REGULAR[ whichOS ] );
    helpScreen2Store.addLink( tmpLink );
    
    if ( osHasHDVersion ) {
    
      tmpLink = new HelpLink();
      tmpLink.setLinkX( 0 );
      tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 112 );
      tmpLink.setLinkWidth( 490 );
      tmpLink.setLinkHeight( 120 );
      tmpLink.setFilename( null );
      tmpLink.setLinkType( HELPLINK_TYPE_URL );
      tmpLink.setLinkTarget( STORE_LINK_URL_FREE_HD[ whichOS ] );
      helpScreen2Store.addLink( tmpLink );
      
    }
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 0 );
    tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 235 );
    tmpLink.setLinkWidth( 490 );
    tmpLink.setLinkHeight( 130 );
    tmpLink.setFilename( null );
    tmpLink.setLinkType( HELPLINK_TYPE_URL );
    tmpLink.setLinkTarget( STORE_LINK_URL_ACE_REGULAR[ whichOS ] );
    helpScreen2Store.addLink( tmpLink );

    if ( osHasHDVersion ) {
    
      tmpLink = new HelpLink();
      tmpLink.setLinkX( 0 );
      tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 375 );
      tmpLink.setLinkWidth( 490 );
      tmpLink.setLinkHeight( 110 );
      tmpLink.setFilename( null );
      tmpLink.setLinkType( HELPLINK_TYPE_URL );
      tmpLink.setLinkTarget( STORE_LINK_URL_ACE_HD[ whichOS ] );
      helpScreen2Store.addLink( tmpLink );
      
    }
    
    // gamemode selection screen
    
    helpScreen3Gamemode.setDisplayFile( FILE_HELP_BODY_3 );
    helpScreen3Gamemode.setHasStaticHeader( true );
    
    helpScreen3Gamemode.addLink( returnHelpLinkBackbutton() );
    
    helpScreen3Gamemode.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_CREDITS, 
                                  helpScreen5Credits ) );
    
    helpScreen3Gamemode.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_THANKS, 
                                  helpScreen6Thanks ) );
    
    helpScreen3Gamemode.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_TERMS, 
                                  helpScreen4Terms ) );
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 0 );
    tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 61 );
    tmpLink.setLinkWidth( 130 );
    tmpLink.setLinkHeight( 130 );
    if ( tGame.getGamemodeDefault() == SCORING_MODEL_CHAOS ) {
      tmpLink.setFilename( FILE_HELP_SELECT_CHAOS_CHECKED );
    } else {
      tmpLink.setFilename( FILE_HELP_SELECT_CHAOS_UNCHECKED );
    }
    tmpLink.setLinkType( HELPLINK_TYPE_SPECIAL );
    tmpLink.setLinkTarget( "chaos" );
    helpScreen3Gamemode.addLink( tmpLink );
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 0 );
    tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 196 );
    tmpLink.setLinkWidth( 130 );
    tmpLink.setLinkHeight( 130 );
    if ( tGame.getGamemodeDefault() == SCORING_MODEL_CLEAN ) {
      tmpLink.setFilename( FILE_HELP_SELECT_EXPERT_CHECKED );
    } else {
      tmpLink.setFilename( FILE_HELP_SELECT_EXPERT_UNCHECKED );
    }
    tmpLink.setLinkType( HELPLINK_TYPE_SPECIAL );
    tmpLink.setLinkTarget( "expert" );
    helpScreen3Gamemode.addLink( tmpLink );
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 0 );
    tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 339 );
    tmpLink.setLinkWidth( 130 );
    tmpLink.setLinkHeight( 130 );
    if ( tGame.getGamemodeDefault() == SCORING_MODEL_NONE ) {
      tmpLink.setFilename( FILE_HELP_SELECT_NODEFAULT_CHECKED );
    } else {
      tmpLink.setFilename( FILE_HELP_SELECT_NODEFAULT_UNCHECKED );
    }
    tmpLink.setLinkType( HELPLINK_TYPE_SPECIAL );
    tmpLink.setLinkTarget( "nodefault" );
    helpScreen3Gamemode.addLink( tmpLink );
    
    // terms and conditions screen
    
    helpScreen4Terms.setDisplayFile( FILE_HELP_BODY_4 );
    helpScreen4Terms.setHasStaticHeader( true );
    
    helpScreen4Terms.addLink( returnHelpLinkBackbutton(
                                  HELPLINK_TYPE_BACK,
                                  helpScreen3Gamemode ) );
    
    helpScreen4Terms.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_CREDITS, 
                                  helpScreen5Credits ) );
    
    helpScreen4Terms.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_THANKS, 
                                  helpScreen6Thanks ) );
    
    helpScreen4Terms.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_TERMS, 
                                  null ) );
    
    // credits screen
    
    helpScreen5Credits.setDisplayFile( FILE_HELP_BODY_5 );
    helpScreen5Credits.setHasStaticHeader( true );
    
    helpScreen5Credits.addLink( returnHelpLinkBackbutton(
                                  HELPLINK_TYPE_BACK,
                                  helpScreen3Gamemode ) );
    
    helpScreen5Credits.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_CREDITS, 
                                  null ) );
    
    helpScreen5Credits.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_THANKS, 
                                  helpScreen6Thanks ) );
    
    helpScreen5Credits.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_TERMS, 
                                  helpScreen4Terms ) );
    
    // acknowledgments
    
    helpScreen6Thanks.setDisplayFile( FILE_HELP_BODY_6 );
    helpScreen6Thanks.setHasStaticHeader( true );
    
    helpScreen6Thanks.addLink( returnHelpLinkBackbutton(
                                  HELPLINK_TYPE_BACK,
                                  helpScreen3Gamemode ) );
    
    helpScreen6Thanks.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_CREDITS, 
                                  helpScreen5Credits ) );
    
    helpScreen6Thanks.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_THANKS, 
                                  null ) );
    
    helpScreen6Thanks.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_TERMS, 
                                  helpScreen4Terms ) );
    
    // game help screen
    
    helpScreen7Help.setDisplayFile( FILE_HELP_BODY_7 );
    helpScreen7Help.setHasStaticHeader( true );
    
    helpScreen7Help.addLink( returnHelpLinkBackbutton() );
    
    helpScreen7Help.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_TIPSTRICKS, 
                                  helpScreen8Tips ) );
    
    helpScreen7Help.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_LITTLEHELPERS, 
                                  helpScreen9Helpers ) );
    
    helpScreen7Help.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_CHAOS,
                                  helpScreen10ChaosExpert ) );
    
    // tips and tricks
    
    helpScreen8Tips.setDisplayFile( FILE_HELP_BODY_8 );
    helpScreen8Tips.setHasStaticHeader( true );
    
    helpScreen8Tips.addLink( returnHelpLinkBackbutton(
                                  HELPLINK_TYPE_BACK,
                                  helpScreen7Help ) );
    
    helpScreen8Tips.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_TIPSTRICKS, 
                                  null ) );
    
    helpScreen8Tips.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_LITTLEHELPERS, 
                                  helpScreen9Helpers ) );
    
    helpScreen8Tips.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_CHAOS,
                                  helpScreen10ChaosExpert ) );
    
    // little helpers
    
    helpScreen9Helpers.setDisplayFile( FILE_HELP_BODY_9 );
    helpScreen9Helpers.setHasStaticHeader( true );
    
    helpScreen9Helpers.addLink( returnHelpLinkBackbutton(
                                  HELPLINK_TYPE_BACK,
                                  helpScreen7Help ) );
    
    helpScreen9Helpers.addLink( returnHelpLinkBottombuttonRight(
                                  FILE_HELP_SUBLINK_TIPSTRICKS, 
                                  helpScreen8Tips ) );
    
    helpScreen9Helpers.addLink( returnHelpLinkBottombuttonMiddle(
                                  FILE_HELP_SUBLINK_LITTLEHELPERS, 
                                  null ) );
    
    helpScreen9Helpers.addLink( returnHelpLinkBottombuttonLeft(
                                  FILE_HELP_SUBLINK_CHAOS,
                                  helpScreen10ChaosExpert ) );
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 400 );
    tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 400 );
    tmpLink.setLinkWidth( 90 );
    tmpLink.setLinkHeight( 90 );
    tmpLink.setFilename( null );
    tmpLink.setLinkType( HELPLINK_TYPE_FORWARD );
    tmpLink.setLinkTarget( helpScreen2Store );
    helpScreen9Helpers.addLink( tmpLink );
    
    // explain chaos vs expert mode
    
    helpScreen10ChaosExpert.setDisplayFile( FILE_HELP_BODY_10 );
    helpScreen10ChaosExpert.setHasStaticHeader( true );
    
    helpScreen10ChaosExpert.addLink( returnHelpLinkBackbutton(
                                       HELPLINK_TYPE_BACK,
                                       helpScreen7Help ) );
    
    helpScreen10ChaosExpert.addLink( returnHelpLinkBottombuttonRight(
                                       FILE_HELP_SUBLINK_TIPSTRICKS, 
                                       helpScreen8Tips ) );
    
    helpScreen10ChaosExpert.addLink( returnHelpLinkBottombuttonMiddle(
                                       FILE_HELP_SUBLINK_LITTLEHELPERS, 
                                       helpScreen9Helpers ) );
    
    helpScreen10ChaosExpert.addLink( returnHelpLinkBottombuttonLeft(
                                       FILE_HELP_SUBLINK_CHAOS,
                                       null ) );
    
    tmpLink = new HelpLink();
    tmpLink.setLinkX( 400 );
    tmpLink.setLinkY( HELP_MENU_BACKGROUND_OFFSET_Y + 400 );
    tmpLink.setLinkWidth( 90 );
    tmpLink.setLinkHeight( 90 );
    tmpLink.setFilename( null );
    tmpLink.setLinkType( HELPLINK_TYPE_FORWARD );
    tmpLink.setLinkTarget( helpScreen3Gamemode );
    helpScreen10ChaosExpert.addLink( tmpLink );
    
  }
  
  function returnHelpLinkBackbutton( linkType, linkTarget ) {
   
    var tmpLink = new HelpLink();
    
    if ( !( linkType ) ) linkType = HELPLINK_TYPE_HOME;
    
    tmpLink.setLinkX( HELP_MENU_BACKBUTTON_XPOS );
    tmpLink.setLinkY( HELP_MENU_BACKBUTTON_YPOS );
    tmpLink.setLinkWidth( HELP_MENU_BACKBUTTON_SIZE );
    tmpLink.setLinkHeight( HELP_MENU_BACKBUTTON_SIZE );
    tmpLink.setFilename( FILE_MENU_BACK );
    tmpLink.setLinkType( linkType );
    tmpLink.setLinkTarget( linkTarget );

    return tmpLink;
    
  }
  
  function returnHelpLinkBottombuttonLeft( linkFilename, linkTarget ) {
   
    var tmpLink = new HelpLink();
    
    tmpLink.setLinkX( HELP_MENU_BUTTONLEFT_XPOS );
    tmpLink.setLinkY( HELP_MENU_BUTTONBOTTOM_YPOS );
    tmpLink.setLinkWidth( HELP_MENU_BUTTONBOTTOM_SIZE );
    tmpLink.setLinkHeight( HELP_MENU_BUTTONBOTTOM_SIZE );
    tmpLink.setFilename( linkFilename );
    
    if ( linkTarget == null ) {
      
      tmpLink.setLinkType( HELPLINK_TYPE_DISABLED );
      
    } else {
    
      tmpLink.setLinkType( HELPLINK_TYPE_FORWARD );
      
    }
    
    tmpLink.setLinkTarget( linkTarget );

    return tmpLink;
    
  }
  
  function returnHelpLinkBottombuttonMiddle( linkFilename, linkTarget ) {
   
    var tmpLink = new HelpLink();
    
    tmpLink.setLinkX( HELP_MENU_BUTTONMIDDLE_XPOS );
    tmpLink.setLinkY( HELP_MENU_BUTTONBOTTOM_YPOS );
    tmpLink.setLinkWidth( HELP_MENU_BUTTONBOTTOM_SIZE );
    tmpLink.setLinkHeight( HELP_MENU_BUTTONBOTTOM_SIZE );
    tmpLink.setFilename( linkFilename );
    
    if ( linkTarget == null ) {
      
      tmpLink.setLinkType( HELPLINK_TYPE_DISABLED );
      
    } else {
    
      tmpLink.setLinkType( HELPLINK_TYPE_FORWARD );
      
    }
    
    tmpLink.setLinkTarget( linkTarget );

    return tmpLink;
    
  }
  
  function returnHelpLinkBottombuttonRight( linkFilename, linkTarget ) {
   
    var tmpLink = new HelpLink();
    
    tmpLink.setLinkX( HELP_MENU_BUTTONRIGHT_XPOS );
    tmpLink.setLinkY( HELP_MENU_BUTTONBOTTOM_YPOS );
    tmpLink.setLinkWidth( HELP_MENU_BUTTONBOTTOM_SIZE );
    tmpLink.setLinkHeight( HELP_MENU_BUTTONBOTTOM_SIZE );
    tmpLink.setFilename( linkFilename );
    
    if ( linkTarget == null ) {
      
      tmpLink.setLinkType( HELPLINK_TYPE_DISABLED );
      
    } else {
    
      tmpLink.setLinkType( HELPLINK_TYPE_FORWARD );
      
    }
    
    tmpLink.setLinkTarget( linkTarget );

    return tmpLink;
    
  }
  
}
