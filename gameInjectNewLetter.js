/*
 * gameInjectNewLetter.js 2012-06-17
 * Copyright (C) 2012 Jens Koeplinger
 *
 */

function gameInjectNewLetter( tGame, ltrTile ) {
  // injecjts a new tile: letter shoot and letter drop animation

  sysLogger.addLine( "gameInjectNewLetter(" + ltrTile + ")", LOGGER_SOURCE_SEQUENCER );

  tGame.setConveyorTile( 0, ltrTile );

  if ( ltrTile == LEVEL_TILE_NONE ) return;

  // letter shoot animation
  
  var movingSSteps = 7;
  if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE )
      movingSSteps = 4;
  
  var sprite = vScreen.getSpriteById( ID_LETTERSHOOT );
  
  sprite.move( CONVEYOR_SHOOT_XPOS, CONVEYOR_SHOOT_YPOS + 40 );

  tGame.moveSprite(
      sprite,
      CONVEYOR_SHOOT_XPOS,
      CONVEYOR_SHOOT_YPOS,
      movingSSteps - 1,
      GAME_MOVESPRITE_SINE_FLYIN );
  
  // dropping letter
    
  var spriteFilename =
      FILE_IMAGE_LTR_PREFIX
      + LEVEL_TILES[ ltrTile ][ 1 ]
      + FILE_IMAGE_LTR_SUFFIX;

  var imgId = tGame.getConveyorPosImg( 0 );
 
  sysLogger.addLine( "sprite file name:"
                     + spriteFilename
                     + ", imgId="
                     + imgId,
                     LOGGER_SOURCE_SEQUENCER );
  
  sprite = new Sprite( spriteFilename, 70, 70 );
  
  sprite.setImgNode(
      document.getElementById( "bitmap" + imgId ) );

  vScreen.putSprite( sprite, imgId );
  sprite.show();
  sprite.move( CONVEYOR_LETTER0_XPOS, -40 );

  tGame.moveSprite(
      sprite,
      CONVEYOR_LETTER0_XPOS,
      CONVEYOR_CONVEYOR_YPOS - 70,
      movingSSteps,
      GAME_MOVESPRITE_SINE_FLYOUT );

}
