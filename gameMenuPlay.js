/*
 * gameMenuPlay.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 */

// positions:
var MENU_PLAY_BACK_TOP           = 2;
var MENU_PLAY_BACK_LEFT          = 1;
var MENU_PLAY_BACK_WIDTH         = 65;
var MENU_PLAY_BACK_HEIGHT        = 65;

var MENU_PLAY_GAMETITLE_TOP      = MENU_PLAY_BACK_TOP;
var MENU_PLAY_GAMETITLE_LEFT     = 68;
var MENU_PLAY_GAMETITLE_WIDTH    = 422;
var MENU_PLAY_GAMETITLE_HEIGHT   = MENU_PLAY_BACK_HEIGHT;

var MENU_PLAY_LEVEL_TOP          = 90;
var MENU_PLAY_LEVEL_LEFT         = 50;
var MENU_PLAY_LEVEL_WIDTH        = 130;
var MENU_PLAY_LEVEL_HEIGHT       = 70;

var MENU_PLAY_LEVELNUMBER_TOP    = 90;
var MENU_PLAY_LEVELNUMBER_LEFT   = 200;
var MENU_PLAY_LEVELNUMBER_WIDTH  = 40;
var MENU_PLAY_LEVELNUMBER_HEIGHT = 67;
var MENU_PLAY_LEVELNUMBER_COLOR  = NUMBER_COLORS_MAP[ "menuSelectLevel" ];

var MENU_PLAY_GRID_TOP           = 200;
var MENU_PLAY_GRID_LEFT          = 0;
var MENU_PLAY_GRID_WIDTH_SINGLE  = 55;
var MENU_PLAY_GRID_HEIGHT_SINGLE = 55;

var MENU_PLAY_UP_TOP             = 268;
var MENU_PLAY_UP_LEFT            = 410; // 395 + ( 30 / 2 )
var MENU_PLAY_UP_WIDTH           = 65;
var MENU_PLAY_UP_HEIGHT          = 65;

var MENU_PLAY_PLAY_TOP           = 334;
var MENU_PLAY_PLAY_LEFT          = 395;
var MENU_PLAY_PLAY_WIDTH         = 95;
var MENU_PLAY_PLAY_HEIGHT        = 95;

var MENU_PLAY_DOWN_TOP           = 433;
var MENU_PLAY_DOWN_LEFT          = MENU_PLAY_UP_LEFT;
var MENU_PLAY_DOWN_WIDTH         = MENU_PLAY_UP_WIDTH;
var MENU_PLAY_DOWN_HEIGHT        = MENU_PLAY_UP_HEIGHT;

var MENU_PLAY_LOCKED_TOP         = 600;
var MENU_PLAY_LOCKED_LEFT        = 2;
var MENU_PLAY_LOCKED_WIDTH       = 490;
var MENU_PLAY_LOCKED_HEIGHT      = 100;
var MENU_PLAY_PURCHASE_WIDTH     = 490;
var MENU_PLAY_PURCHASE_HEIGHT    = 100;

var MENU_PLAY_HIGHSCORE_TOP      = 630;
var MENU_PLAY_HIGHSCORE_LEFT     = 0;
var MENU_PLAY_HIGHSCORE_WIDTH    = 490;
var MENU_PLAY_HIGHSCORE_HEIGHT   = 55;
var MENU_PLAY_EXIT_WIDTH         = 490;
var MENU_PLAY_EXIT_HEIGHT        = 55;

var MENU_PLAY_HIGHSCORENUMBER_TOP    = 158;
var MENU_PLAY_HIGHSCORENUMBER_LEFT   = 369;
var MENU_PLAY_HIGHSCORENUMBER_WIDTH  = 20;
var MENU_PLAY_HIGHSCORENUMBER_HEIGHT = 33;
var MENU_PLAY_HIGHSCORENUMBER_COLOR  = NUMBER_COLORS_MAP[ "menuSelectScore" ];

var MENU_PLAY_HIGHSCORESTARS_TOP     = 92;
var MENU_PLAY_HIGHSCORESTARS_LEFT    = 290;
var MENU_PLAY_HIGHSCORESTARS_WIDTH   = 180;
var MENU_PLAY_HIGHSCORESTARS_HEIGHT  = 60;

var MENU_PLAY_RENDER_LEVELGRID   = 0; // shows grid as defined by level
var MENU_PLAY_RENDER_LEVELPLAYED = 1; // shows grid as in high score

var MENU_PLAY_WHEREFROM_MAIN      = 0; // user entered screen from main menu
var MENU_PLAY_WHEREFROM_HIGHSCORE = 1; // user entered screen from highscores

var MENU_PLAY_SELECT_NOTHING      = -1;
var MENU_PLAY_SELECT_BACK         = 0;
var MENU_PLAY_SELECT_GAMETITLE    = 1;
var MENU_PLAY_SELECT_PLAY         = 2;
var MENU_PLAY_SELECT_UP           = 3;
var MENU_PLAY_SELECT_DOWN         = 4;
var MENU_PLAY_SELECT_HIGHSCORE    = 5;

var menuPlayBounds = new Array();
var menuPlayBoundsCount = 9;

menuPlayBounds[ 0 ] = new Array();
menuPlayBounds[ 0 ][ "id" ] = MENU_PLAY_SELECT_BACK;
menuPlayBounds[ 0 ][ "x1" ] = MENU_PLAY_BACK_LEFT;
menuPlayBounds[ 0 ][ "x2" ] = MENU_PLAY_BACK_LEFT + MENU_PLAY_BACK_WIDTH;
menuPlayBounds[ 0 ][ "y1" ] = MENU_PLAY_BACK_TOP;
menuPlayBounds[ 0 ][ "y2" ] = MENU_PLAY_BACK_TOP + MENU_PLAY_BACK_HEIGHT;

menuPlayBounds[ 1 ] = new Array();
menuPlayBounds[ 1 ][ "id" ] = MENU_PLAY_SELECT_BACK;
menuPlayBounds[ 1 ][ "x1" ] = MENU_PLAY_GAMETITLE_LEFT;
menuPlayBounds[ 1 ][ "x2" ] = MENU_PLAY_GAMETITLE_LEFT + MENU_PLAY_GAMETITLE_WIDTH;
menuPlayBounds[ 1 ][ "y1" ] = MENU_PLAY_GAMETITLE_TOP;
menuPlayBounds[ 1 ][ "y2" ] = MENU_PLAY_GAMETITLE_TOP + MENU_PLAY_GAMETITLE_HEIGHT;

menuPlayBounds[ 2 ] = new Array();
menuPlayBounds[ 2 ][ "id" ] = MENU_PLAY_SELECT_NOTHING;
menuPlayBounds[ 2 ][ "x1" ] = 0;
menuPlayBounds[ 2 ][ "x2" ] = 1;
menuPlayBounds[ 2 ][ "y1" ] = MENU_PLAY_LEVELNUMBER_TOP;
menuPlayBounds[ 2 ][ "y2" ] = MENU_PLAY_LEVELNUMBER_TOP + MENU_PLAY_LEVELNUMBER_HEIGHT;

menuPlayBounds[ 3 ] = new Array();
menuPlayBounds[ 3 ][ "id" ] = MENU_PLAY_SELECT_HIGHSCORE;
menuPlayBounds[ 3 ][ "x1" ] = MENU_PLAY_GRID_LEFT;
menuPlayBounds[ 3 ][ "x2" ] = MENU_PLAY_GRID_LEFT + 7 * MENU_PLAY_GRID_WIDTH_SINGLE;
menuPlayBounds[ 3 ][ "y1" ] = MENU_PLAY_GRID_TOP;
menuPlayBounds[ 3 ][ "y2" ] = MENU_PLAY_GRID_TOP  + 7 * MENU_PLAY_GRID_HEIGHT_SINGLE;

menuPlayBounds[ 4 ] = new Array();
menuPlayBounds[ 4 ][ "id" ] = MENU_PLAY_SELECT_PLAY;
menuPlayBounds[ 4 ][ "x1" ] = MENU_PLAY_PLAY_LEFT;
menuPlayBounds[ 4 ][ "x2" ] = MENU_PLAY_PLAY_LEFT + MENU_PLAY_PLAY_WIDTH;
menuPlayBounds[ 4 ][ "y1" ] = MENU_PLAY_PLAY_TOP;
menuPlayBounds[ 4 ][ "y2" ] = MENU_PLAY_PLAY_TOP + MENU_PLAY_PLAY_HEIGHT;

menuPlayBounds[ 5 ] = new Array();
menuPlayBounds[ 5 ][ "id" ] = MENU_PLAY_SELECT_UP;
menuPlayBounds[ 5 ][ "x1" ] = MENU_PLAY_UP_LEFT;
menuPlayBounds[ 5 ][ "x2" ] = MENU_PLAY_UP_LEFT + MENU_PLAY_UP_WIDTH;
menuPlayBounds[ 5 ][ "y1" ] = MENU_PLAY_UP_TOP;
menuPlayBounds[ 5 ][ "y2" ] = MENU_PLAY_UP_TOP + MENU_PLAY_UP_HEIGHT;

menuPlayBounds[ 6 ] = new Array();
menuPlayBounds[ 6 ][ "id" ] = MENU_PLAY_SELECT_DOWN;
menuPlayBounds[ 6 ][ "x1" ] = MENU_PLAY_DOWN_LEFT;
menuPlayBounds[ 6 ][ "x2" ] = MENU_PLAY_DOWN_LEFT + MENU_PLAY_DOWN_WIDTH;
menuPlayBounds[ 6 ][ "y1" ] = MENU_PLAY_DOWN_TOP;
menuPlayBounds[ 6 ][ "y2" ] = MENU_PLAY_DOWN_TOP + MENU_PLAY_DOWN_HEIGHT;

menuPlayBounds[ 7 ] = new Array();
menuPlayBounds[ 7 ][ "id" ] = MENU_PLAY_SELECT_HIGHSCORE;
menuPlayBounds[ 7 ][ "x1" ] = MENU_PLAY_HIGHSCORESTARS_LEFT;
menuPlayBounds[ 7 ][ "x2" ] = MENU_PLAY_HIGHSCORESTARS_LEFT + MENU_PLAY_HIGHSCORESTARS_WIDTH;
menuPlayBounds[ 7 ][ "y1" ] = MENU_PLAY_HIGHSCORESTARS_TOP;
menuPlayBounds[ 7 ][ "y2" ] = MENU_PLAY_HIGHSCORENUMBER_TOP + MENU_PLAY_HIGHSCORENUMBER_HEIGHT;

menuPlayBounds[ 8 ] = new Array();
menuPlayBounds[ 8 ][ "id" ] = MENU_PLAY_SELECT_HIGHSCORE;
menuPlayBounds[ 8 ][ "x1" ] = 0;
menuPlayBounds[ 8 ][ "x2" ] = 490;
menuPlayBounds[ 8 ][ "y1" ] = MENU_PLAY_HIGHSCORE_TOP - MENU_PLAY_HIGHSCORE_HEIGHT;
menuPlayBounds[ 8 ][ "y2" ] = MENU_PLAY_HIGHSCORE_TOP + ( 2 * MENU_PLAY_HIGHSCORE_HEIGHT );

var menuBuildingSequence = null;
var menuPlayRenderMode   = null;
var menuPlayWhereFrom    = null;
var menuPlayCurrentLevel = 0;

var menuPlayHighscore       = null; // Highscore object
var menuPlayHighscoreNumber = null; // number object
var menuPlayHighscoreStars  = null; // sprite holding the stars
var menuPlayHighscoreText   = null; // sprite holding text
var menuPlayNextLevel       = null; // next highest level to unlock

var MENU_PLAY_CANPLAY_YES      = 0;
var MENU_PLAY_CANPLAY_LOCKED   = 1;
var MENU_PLAY_CANPLAY_PURCHASE = 2;

function gameMenuPlay( tGame ) {
  // menu selection to play next level (or select another)

  var movingSSteps = null;

  if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE ) {

    movingSSteps = 3;

  } else {

    movingSSteps = 4;

  }

  switch ( tGame.getSubState() ) {

    case 0:
      
      sysLogger.addLine( "gameMenuPlay()", LOGGER_SOURCE_SEQUENCER );
      sysLogger.addLine( "entering from "
                         + menuPlayWhereFrom
                         + " in render mode "
                         + menuPlayRenderMode
                         + " for level "
                         + menuPlayCurrentLevel,
                         LOGGER_SOURCE_SEQUENCER );
      
      // give a bit of loading time for slower devices
      if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE )
        menuBuildingSequence = -10;
      else
        menuBuildingSequence = 0;
      
      // get the highest 'next' level to play
      
      menuPlayNextLevel    = tGame.getHighestNextLevel();
      
      if ( menuPlayNextLevel == null ) {
        
        sysLogger.addLine( "newbie", LOGGER_SOURCE_HIGHSCORE );

        menuPlayNextLevel = 1;
        
      }

      if ( cyborgWordsVersion == VERSION_ADDICT ) {
          
        menuPlayNextLevel = LEVEL_HIGHESTLEVEL;
        tGame.setHighestNextLevel( LEVEL_HIGHESTLEVEL );
          
        sysLogger.addLine( "paid version, all levels are unlocked",
                           LOGGER_SOURCE_HIGHSCORE );
          
      } else {

        sysLogger.addLine( "free version, user must unlock levels",
                           LOGGER_SOURCE_HIGHSCORE );
          
      }

      sysLogger.addLine( "highest 'next level' is " + menuPlayNextLevel,
                         LOGGER_SOURCE_HIGHSCORE );

      // get the current selected menu level
      
      menuPlayCurrentLevel = tGame.getCurrentMenuLevel();

      if ( !( menuPlayCurrentLevel ) ) {
        
        menuPlayCurrentLevel = 1;
        
        sysLogger.addLine( "newbie, start with level 1",
                           LOGGER_SOURCE_HIGHSCORE );
        
      } else {

                sysLogger.addLine( "read menu level " + menuPlayCurrentLevel,
                                   LOGGER_SOURCE_HIGHSCORE );
        
      }
      
      var sprite = new Sprite( FILE_SPLASH_LEVELSELECT, 490, 735 );

      sprite.setImgNode(
              document.getElementById( "bitmap0" ) );
      vScreen.putSprite( sprite, 0 );
      sprite.show();
      sprite.move( 0, 0 );
      
      tGame.setSubState( 1 );

      break;

    case 1: // building screen from blank

      switch ( menuBuildingSequence ) {
        
        case 0: // fly in "Level"

          var sprite = new Sprite( FILE_MENU_LEVEL,
                                   MENU_PLAY_LEVEL_WIDTH,
                                   MENU_PLAY_LEVEL_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ID_MENU_20ITEMS ) );
          vScreen.putSprite( sprite, ID_MENU_20ITEMS );
          sprite.show();
          sprite.move( MENU_PLAY_LEVEL_LEFT, -MENU_PLAY_LEVEL_HEIGHT );

          tGame.moveSprite(
              sprite,
              MENU_PLAY_LEVEL_LEFT,
              MENU_PLAY_LEVEL_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );

        case 1: // fly in level number
          
          gameMenuLevelNumber = new Number(
                               ID_MENU_20ITEMS + 1,
                               2,
                               MENU_PLAY_LEVELNUMBER_LEFT,
                               -MENU_PLAY_LEVELNUMBER_HEIGHT,
                               MENU_PLAY_LEVELNUMBER_WIDTH,
                               MENU_PLAY_LEVELNUMBER_HEIGHT,
                               MENU_PLAY_LEVELNUMBER_COLOR );

          gameMenuLevelNumber.setValue( menuPlayCurrentLevel );          
          gameMenuLevelNumber.show();

          tGame.moveNumber( gameMenuLevelNumber,
                        MENU_PLAY_LEVELNUMBER_LEFT,
                        MENU_PLAY_LEVELNUMBER_TOP,
                        movingSSteps,
                        GAME_MOVESPRITE_SINE_FLYIN );
          
          break;
          
        case 2: // fly-in level up

          var sprite = new Sprite( FILE_MENU_UP,
                                   MENU_PLAY_UP_WIDTH,
                                   MENU_PLAY_UP_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ( ID_MENU_20ITEMS + 3 ) ) );
          vScreen.putSprite( sprite, ( ID_MENU_20ITEMS + 3 ) );
          sprite.show();
          sprite.move( MENU_PLAY_UP_LEFT, 735 );

          tGame.moveSprite(
              sprite,
              MENU_PLAY_UP_LEFT,
              MENU_PLAY_UP_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );
          
          break;
          
        case 3: // fly-in play

          var sprite = new Sprite( FILE_MENU_PLAY,
                                   MENU_PLAY_PLAY_WIDTH,
                                   MENU_PLAY_PLAY_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ( ID_MENU_20ITEMS + 4 ) ) );
          vScreen.putSprite( sprite, ( ID_MENU_20ITEMS + 4 ) );
          sprite.show();
          sprite.move( MENU_PLAY_PLAY_LEFT, 735 );

          tGame.moveSprite(
              sprite,
              MENU_PLAY_PLAY_LEFT,
              MENU_PLAY_PLAY_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );
          
          break;

        case 4: // fly-in level down

          var sprite = new Sprite( FILE_MENU_DOWN, MENU_PLAY_DOWN_WIDTH, MENU_PLAY_DOWN_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ( ID_MENU_20ITEMS + 5 ) ) );
          vScreen.putSprite( sprite, ( ID_MENU_20ITEMS + 5 ) );
          sprite.show();
          sprite.move( MENU_PLAY_DOWN_LEFT, 735 );

          tGame.moveSprite(
              sprite,
              MENU_PLAY_DOWN_LEFT,
              MENU_PLAY_DOWN_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );
          
          break;

        case 5: // fly-in back button

          var sprite = new Sprite( FILE_MENU_BACK,
                                   MENU_PLAY_BACK_WIDTH,
                                   MENU_PLAY_BACK_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ( ID_MENU_20ITEMS + 6 ) ) );
          vScreen.putSprite( sprite, ( ID_MENU_20ITEMS + 6 ) );
          sprite.show();
          sprite.move( MENU_PLAY_BACK_LEFT, -MENU_PLAY_BACK_HEIGHT );

          tGame.moveSprite(
              sprite,
              MENU_PLAY_BACK_LEFT,
              MENU_PLAY_BACK_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );
          
          break;

        case 6: // fly-in game title

          var sprite = new Sprite( FILE_MENU_GAMETITLE_SMALL,
                                   MENU_PLAY_GAMETITLE_WIDTH,
                                   MENU_PLAY_GAMETITLE_HEIGHT );
          sprite.setImgNode(
              document.getElementById( "bitmap" + ( ID_MENU_20ITEMS + 7 ) ) );
          vScreen.putSprite( sprite, ( ID_MENU_20ITEMS + 7 ) );
          sprite.show();
          sprite.move( MENU_PLAY_GAMETITLE_LEFT, -MENU_PLAY_GAMETITLE_HEIGHT );

          tGame.moveSprite(
              sprite,
              MENU_PLAY_GAMETITLE_LEFT,
              MENU_PLAY_GAMETITLE_TOP,
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );
          
          break;

        case 7:
        case 8: // give just a tad of time

          break;
          
        case 9: // move on
                    
          tGame.setSubState( 9 );
          break;
          
        default:
          // do nothing
          
      }

      menuBuildingSequence++;

      break;

    case 9: // entry point for level selected / redraw screen

      // go ahead and read highscore object (if any)
      menuPlayHighscore = new Highscore( menuPlayCurrentLevel );
      menuPlayHighscore.read();

      tGame.setCurrentMenuLevel( menuPlayCurrentLevel );
      
      if ( menuPlayCurrentLevel == 1 ) {
        // lowest level, gray out the "down" button
        
        sysLogger.addLine( "gray out 'down' button",
                        LOGGER_SOURCE_SEQUENCER );
        
        var sprite = new Sprite( FILE_SPLASH_OPAQUE_ROUND_50,
                                 MENU_PLAY_DOWN_WIDTH,
                                 MENU_PLAY_DOWN_HEIGHT );
        sprite.setImgNode( document.getElementById( "bitmap" + ID_MENU_AUXGRAY1 ) );
        vScreen.putSprite( sprite, ID_MENU_AUXGRAY1 );
        sprite.show();
        sprite.move( MENU_PLAY_DOWN_LEFT, MENU_PLAY_DOWN_TOP );
        
      } else {
        // make sure "up" button is visible
        
        vScreen.removeSprite( ID_MENU_AUXGRAY1 );
        
      }
      
      if ( menuPlayCurrentLevel == LEVEL_HIGHESTLEVEL ) {
        
        // highest level, gray out the "up" button
        
        sysLogger.addLine( "gray out 'up' button",
                        LOGGER_SOURCE_SEQUENCER );
        
        var sprite = new Sprite( FILE_SPLASH_OPAQUE_ROUND_50,
                                 MENU_PLAY_UP_WIDTH,
                                 MENU_PLAY_UP_HEIGHT );
        sprite.setImgNode( document.getElementById( "bitmap" + ( ID_MENU_AUXGRAY2 ) ) );
        vScreen.putSprite( sprite, ( ID_MENU_AUXGRAY2 ) );
        sprite.show();
        sprite.move( MENU_PLAY_UP_LEFT, MENU_PLAY_UP_TOP );
        
      } else {
        // make sure "up" button is visible
        
        vScreen.removeSprite( ID_MENU_AUXGRAY2 );
        
      }
      
      if ( !( checkMenuPlayCanPlay() == MENU_PLAY_CANPLAY_YES ) ) {
        // user can't play this level; gray out; force grid-only
            
        sysLogger.addLine( "can't play level, gray out play button",
                        LOGGER_SOURCE_SEQUENCER );

        var sprite = new Sprite( FILE_SPLASH_OPAQUE_ROUND_50,
                             MENU_PLAY_PLAY_WIDTH,
                             MENU_PLAY_PLAY_HEIGHT );
        sprite.setImgNode( document.getElementById( "bitmap" + ( ID_MENU_PLAYGRAY2 ) ) );
        vScreen.putSprite( sprite, ( ID_MENU_PLAYGRAY2 ) );
        sprite.show();
        sprite.move( MENU_PLAY_PLAY_LEFT, MENU_PLAY_PLAY_TOP );
        
        // should always be grid-only render mode; force anyway
        if ( !( menuPlayRenderMode == MENU_PLAY_RENDER_LEVELGRID ) ) {
              
           sysLogger.addLine( "wrong render mode "
                              + menuPlayRenderMode
                              + ", force "
                              + MENU_PLAY_RENDER_LEVELGRID,
                              LOGGER_SOURCE_SEQUENCER );

        }
            
        menuPlayRenderMode = MENU_PLAY_RENDER_LEVELGRID;
            
      } else {
        // make sure screen is not grayed out
        
        vScreen.removeSprite( ID_MENU_PLAYGRAY2 );
        
      }

      if ( !( menuPlayHighscore.getLevelScore() ) ) {
        // redundant safeguard: force grid-only rendering

        if ( !( menuPlayRenderMode == MENU_PLAY_RENDER_LEVELGRID ) ) {
              
           sysLogger.addLine( "wrong render mode "
                              + menuPlayRenderMode
                              + ", force "
                              + MENU_PLAY_RENDER_LEVELGRID,
                              LOGGER_SOURCE_SEQUENCER );

        }

        menuPlayRenderMode = MENU_PLAY_RENDER_LEVELGRID;
            
      }

      switch ( menuPlayRenderMode ) {
            
        case MENU_PLAY_RENDER_LEVELGRID:
                            
          tGame.setSubState( 2 );
          break;
              
        case MENU_PLAY_RENDER_LEVELPLAYED:
                            
          tGame.setSubState( 4 );
          break;
            
        default:
          // do nothing
              
      }

      break;
      
    case 2: // show grid from level definition

      sysLogger.addLine( "prepare render grid from level",
                         LOGGER_SOURCE_SEQUENCER );
      
      menuBuildingSequence = 0;
      
      level.loadGrid( menuPlayCurrentLevel );

      // see if there's an existing high score
      menuPlayHighscore = new Highscore( menuPlayCurrentLevel );
      menuPlayHighscore.read();

      wipeLetterTilesIfAny();
      showHighScoreIfAny();
      
      tGame.setSubState( 3 );
      break;

    case 3: // build grid tiles

      if ( menuBuildingSequence == 0 ) {
        // first entry

        if ( !( gameMenuLevelNumber.getValue() == menuPlayCurrentLevel ) ) {

          gameMenuLevelNumber.setValue( menuPlayCurrentLevel );
          gameMenuLevelNumber.show();
                  
        }
        
        menuBuildingSequence++;
        
      } else {
        // build

        menuBuildingSequence
            = buildGrid( menuBuildingSequence, level.getGrid() );
        
        if ( menuBuildingSequence > ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) ) {
          
          tGame.setSubState( 10 );
          
        }
        
      }

      checkUserTouch();
      break;
      
    case 4: // build played level grid
      
      sysLogger.addLine( "prepare building grid and letter tiles from highscore",
                         LOGGER_SOURCE_SEQUENCER );

      menuBuildingSequence = 0;
      
      level.loadGrid( menuPlayCurrentLevel ); // get the stars-per-level
      
      // high score must exist
      menuPlayHighscore = new Highscore( menuPlayCurrentLevel );
      menuPlayHighscore.read();
      
      wipeLetterTilesIfAny();
      wipeGridIfAny();
      showHighScoreIfAny();
      
      tGame.setSubState( 5 );
      
      break;
      
    case 5: // build grid tiles from high score

      if ( menuBuildingSequence == 0 ) {
        // first entry
        
        if ( !( gameMenuLevelNumber.getValue() == menuPlayCurrentLevel ) ) {

          gameMenuLevelNumber.setValue( menuPlayCurrentLevel );
          gameMenuLevelNumber.show();
                 
        }
       
        menuBuildingSequence++;
  
      } else {
        // build

        menuBuildingSequence
            = buildGrid( menuBuildingSequence, menuPlayHighscore.getGrid() );
        
        if ( menuBuildingSequence > ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) ) {
          
          menuBuildingSequence = 1;
          tGame.setSubState( 6 );
          
        }

      }

      checkUserTouch();
      break;

    case 6: // wait a little

      menuBuildingSequence++;
      
      if ( menuBuildingSequence > 6 ) {

        menuBuildingSequence = 1;
        tGame.setSubState( 7 );
        
      }
      
      checkUserTouch();
      break;
      
    case 7: // build letter tiles from high score

      var goAgain = 1;

      while ( goAgain > 0 ) {
        // loop to ensure we draw one tile per cycle

        var xPos     = ( menuBuildingSequence - 1 ) % LEVEL_GRID_MAX_X;
        var yPos     = Math.floor( ( menuBuildingSequence - 1 ) / LEVEL_GRID_MAX_X );
        var xyString = "" + xPos + "," + yPos;
        
        var tileType = menuPlayHighscore.getTilesPlaced()[ xyString ];
      
        if ( !( tileType == LEVEL_TILE_NONE ) ) {
          // draw letter tile
                
          var tileFileName  = FILE_IMAGE_LTR_PREFIX
                              + LEVEL_TILES[ tileType ][ 1 ]
                              + FILE_IMAGE_LTR_SUFFIX;
          var tileBitmap    = ID_LTR_START + menuBuildingSequence - 1;
        
          var sprite = new Sprite( tileFileName,
                                   MENU_PLAY_GRID_WIDTH_SINGLE - 1,
                                   MENU_PLAY_GRID_HEIGHT_SINGLE - 1 );
        
          sprite.setImgNode( document.getElementById( "bitmap" + tileBitmap ) );
          vScreen.putSprite( sprite, tileBitmap );
          sprite.show();
          sprite.move( MENU_PLAY_GRID_LEFT + MENU_PLAY_GRID_WIDTH_SINGLE  * xPos,
                       MENU_PLAY_GRID_TOP  + MENU_PLAY_GRID_HEIGHT_SINGLE * yPos );
          
          goAgain--;

        }

        menuBuildingSequence++;
        
        if ( menuBuildingSequence > ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) ) {
          
          tGame.setSubState( 10 );
          goAgain = 0;
          
        }
        
      }
      
      checkUserTouch();
      break;
      
    case 10: // waiting for user interaction

      checkUserTouch();
      break;
      
    case 20: // back button pressed

      tGame.setState(    GAME_INITIALIZING );
      tGame.setSubState( GAME_INITIALIZING_ENTRYSUBSTATE );
      break;

    case 21: // play button pressed

      sysLogger.addLine( "set next level number " + menuPlayCurrentLevel,
                         LOGGER_SOURCE_SEQUENCER );

      tGame.setLevelNumber( menuPlayCurrentLevel );
      tGame.setState( GAME_LEVEL_PREPARING );
      tGame.setSubState( 0 );
      break;
      
    case 22: // go to store

      deviceSpecificsOpenStore();

    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;

    case 31: // in case the app isn't stopped now, resume level menu
      
      tGame.setSubState( 10 );
      break;
      
    default:
      // do nothing

  }
  
  function checkUserTouch() {
    // check user interaction

    if ( touch.getTouchOccurred() ) {

      var thisX        = touch.getTouchX();
      var thisY        = touch.getTouchY();
      var thisSelected = MENU_PLAY_SELECT_NOTHING;
        
      for ( var i = 0; i < menuPlayBoundsCount; i++ ) {
         
        if (    ( thisX > menuPlayBounds[ i ][ "x1" ] )
             && ( thisX < menuPlayBounds[ i ][ "x2" ] )
             && ( thisY > menuPlayBounds[ i ][ "y1" ] )
             && ( thisY < menuPlayBounds[ i ][ "y2" ] ) ) {
           
          thisSelected = menuPlayBounds[ i ][ "id" ];
          i = menuPlayBoundsCount;
          
          sysLogger.addLine( "user selected item " + thisSelected,
                             LOGGER_SOURCE_SEQUENCER );
          
        }
         
      }
         
      switch ( thisSelected ) {

        case MENU_PLAY_SELECT_BACK:
          
          sysLogger.addLine( "selection: back", LOGGER_SOURCE_SEQUENCER );
          
          flyAllTheStuffOut();
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
          tGame.setSubState( 20 );
          
          break;
          
        case MENU_PLAY_SELECT_GAMETITLE:

          sysLogger.addLine( "selection: game title", LOGGER_SOURCE_SEQUENCER );
          // no action
          
          break;
          
        case MENU_PLAY_SELECT_PLAY:

          sysLogger.addLine( "selection: play", LOGGER_SOURCE_SEQUENCER );
          
          if ( checkMenuPlayCanPlay() == MENU_PLAY_CANPLAY_YES ) {
            
            flyAllTheStuffOut();
            tGame.playMediaEffect( FILE_SOUND_COUNTDOWN );
            
            tGame.setSubState( 21 );
            
          } else {
            
            sysLogger.addLine( "can't play this level", LOGGER_SOURCE_SEQUENCER );
            
          }
          
          break;
          
        case MENU_PLAY_SELECT_UP:

          sysLogger.addLine( "selection: up", LOGGER_SOURCE_SEQUENCER );
          
          if ( menuPlayCurrentLevel == LEVEL_HIGHESTLEVEL ) break;
          
          menuPlayCurrentLevel++;
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
          tGame.setSubState( 9 );

          break;
          
        case MENU_PLAY_SELECT_DOWN:

          sysLogger.addLine( "selection: down", LOGGER_SOURCE_SEQUENCER );
          
          if ( menuPlayCurrentLevel == 1 ) break;
          
          menuPlayCurrentLevel--;
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
          tGame.setSubState( 9 );

          break;
          
        case MENU_PLAY_SELECT_HIGHSCORE:

          sysLogger.addLine( "selection: highscore", LOGGER_SOURCE_SEQUENCER );
          
          switch ( checkMenuPlayCanPlay() ) {
            
            case MENU_PLAY_CANPLAY_PURCHASE:
              
              sysLogger.addLine( "purchase", LOGGER_SOURCE_SEQUENCER );
              
              tGame.stopMediaSoundtrack();
              tGame.playMediaEffect( FILE_SOUND_CRISSCROSS, true );
              
              tGame.setSubState( 22 );
              
              break;
              
            case MENU_PLAY_CANPLAY_LOCKED:
              // do nothing
              
              break;
              
            case MENU_PLAY_CANPLAY_YES:
           
              vScreen.removeSprite( ID_MENU_HIGHSCORETEXT );
              
              if ( menuPlayRenderMode == MENU_PLAY_RENDER_LEVELGRID ) {
           
                if ( menuPlayHighscore.getLevelScore() ) {
                  
                  menuPlayRenderMode = MENU_PLAY_RENDER_LEVELPLAYED;
                  tGame.playMediaEffect( FILE_SOUND_DROP );
                  
                  tGame.setSubState( 9 );
                  
                }
            
              } else {
               
                menuPlayRenderMode = MENU_PLAY_RENDER_LEVELGRID;
                tGame.playMediaEffect( FILE_SOUND_DROP );
                
                tGame.setSubState( 9 );
                
              }
              
              break;
        
            default:
              // do nothing
            
          }
                    
          break;
          
        default:
          // do nothing
            
          sysLogger.addLine( "user touch without selection",
                             LOGGER_SOURCE_SEQUENCER );
            
      }
        
      touch.setTouchOccurred( false );

    }
    
  }

  function checkMenuPlayCanPlay() {
   
    if ( ( cyborgWordsVersion == VERSION_FREE ) 
        && ( menuPlayCurrentLevel > VERSION_FREE_MAXLEVEL ) )
            return MENU_PLAY_CANPLAY_PURCHASE;
    
    if ( menuPlayCurrentLevel > menuPlayNextLevel )
            return MENU_PLAY_CANPLAY_LOCKED;
    
    return MENU_PLAY_CANPLAY_YES;
    
  }
  
  function wipeLetterTilesIfAny() {
   
    for ( var i = ID_LTR_START; i < ( ID_LTR_START + 49 ); i++ ) {
     
      vScreen.removeSprite( i );
      
    }
    
  }

  function wipeGridIfAny() {
   
    for ( var i = ID_GRID_START; i < ( ID_GRID_START + 49 ); i++ ) {
     
      vScreen.removeSprite( i );
      
    }
    
  }

  function showHighScoreIfAny() {
    
    var thisLevelScore = menuPlayHighscore.getLevelScore();
    
    if ( !( thisLevelScore ) ) {
      // no high score exists
      
      vScreen.removeSpriteObject( menuPlayHighscoreStars  );
      
      var sprite = null;
      
      switch ( checkMenuPlayCanPlay() ) {
       
        case MENU_PLAY_CANPLAY_PURCHASE:
          
          sprite = new Sprite( FILE_MENU_LEVELPURCHASE,
                               MENU_PLAY_PURCHASE_WIDTH,
                               MENU_PLAY_PURCHASE_HEIGHT );
          break;
          
        case MENU_PLAY_CANPLAY_LOCKED:
          
          sprite = new Sprite( FILE_MENU_LEVELLOCKED,
                               MENU_PLAY_LOCKED_WIDTH,
                               MENU_PLAY_LOCKED_HEIGHT );
          break;
          
        default:
          vScreen.removeSpriteObject( menuPlayHighscoreText );
        
      }
      
      if ( sprite ) {
        // show locked or purchase icon
        
        menuPlayHighscoreText = sprite;
        
        menuPlayHighscoreText.setImgNode( document.getElementById( "bitmap" + ID_MENU_HIGHSCORETEXT ) );
        vScreen.putSprite( menuPlayHighscoreText, ID_MENU_HIGHSCORETEXT );
        menuPlayHighscoreText.show();
        menuPlayHighscoreText.move( MENU_PLAY_LOCKED_LEFT, MENU_PLAY_LOCKED_TOP );
        
      }
      
      if ( menuPlayHighscoreNumber ) menuPlayHighscoreNumber.hide();
      
    } else {
      // high score exists
      
      // (1) show "highscore" or "exit" icon
      
      var showHighscorePointsNumber = null;
      
      switch ( menuPlayRenderMode ) {
            
        case MENU_PLAY_RENDER_LEVELGRID: // show "highscore"
                            
          menuPlayHighscoreText = new Sprite( FILE_MENU_HIGHSCORE,
                                              MENU_PLAY_HIGHSCORE_WIDTH,
                                              MENU_PLAY_HIGHSCORE_HEIGHT );
          
          showHighscorePointsNumber = false;
          
          break;
            
        case MENU_PLAY_RENDER_LEVELPLAYED: // show "exit"
                            
          menuPlayHighscoreText = new Sprite( FILE_MENU_EXIT,
                                              MENU_PLAY_EXIT_WIDTH,
                                              MENU_PLAY_EXIT_HEIGHT );
          
          showHighscorePointsNumber = true;
          
          break;
            
        default:
          // do nothing
          
      }

      menuPlayHighscoreText.setImgNode( document.getElementById( "bitmap" + ID_MENU_HIGHSCORETEXT ) );
      vScreen.putSprite( menuPlayHighscoreText, ID_MENU_HIGHSCORETEXT );
      menuPlayHighscoreText.show();
      menuPlayHighscoreText.move( MENU_PLAY_HIGHSCORE_LEFT, MENU_PLAY_HIGHSCORE_TOP );

      // (2) show score ( reserve 6 digits, 20ITEMS+11 through 20ITEMS+16 )
      
      if ( showHighscorePointsNumber ) {
      
        var hNumberLeft = MENU_PLAY_HIGHSCORESTARS_LEFT
                          + ( MENU_PLAY_HIGHSCORESTARS_WIDTH  / 2 )
                          - ( MENU_PLAY_HIGHSCORENUMBER_WIDTH / 2 );
                        
        var tempScore   = thisLevelScore;
      
        if ( tempScore < 1 ) tempScore = 1;
        var hDigits     = Math.floor( Math.log( tempScore ) / Math.LN10 );
        hNumberLeft     = hNumberLeft - hDigits * MENU_PLAY_HIGHSCORENUMBER_WIDTH / 2;
            
        menuPlayHighscoreNumber = new Number(
                                 ID_MENU_30ITEMS,
                                 6,
                                 hNumberLeft,
                                 MENU_PLAY_HIGHSCORENUMBER_TOP,
                                 MENU_PLAY_HIGHSCORENUMBER_WIDTH,
                                 MENU_PLAY_HIGHSCORENUMBER_HEIGHT,
                                 MENU_PLAY_HIGHSCORENUMBER_COLOR );

        menuPlayHighscoreNumber.setValue( thisLevelScore );          
        menuPlayHighscoreNumber.show();
        
      } else {
       
        if ( menuPlayHighscoreNumber ) menuPlayHighscoreNumber.hide();
        
      }

      // (3) show stars
      
      var fileName = level.getStarsFilename( thisLevelScore );
      
      if ( fileName ) {
      
        menuPlayHighscoreStars = new Sprite( fileName,
                                             MENU_PLAY_HIGHSCORESTARS_WIDTH,
                                             MENU_PLAY_HIGHSCORESTARS_HEIGHT );
        menuPlayHighscoreStars.setImgNode( document.getElementById( "bitmap" + ( ID_MENU_20ITEMS + 10 ) ) );
        vScreen.putSprite( menuPlayHighscoreStars, ( ID_MENU_20ITEMS + 16 ) );
        menuPlayHighscoreStars.show();
        menuPlayHighscoreStars.move( 495, 740 );

        tGame.moveSprite(
              menuPlayHighscoreStars,
              MENU_PLAY_HIGHSCORESTARS_LEFT,
              MENU_PLAY_HIGHSCORESTARS_TOP,
              25,
              GAME_MOVESPRITE_ZIGZAG_5 );
        
      } else {
       
        vScreen.removeSprite( ID_MENU_20ITEMS + 16 );
        
      }
      
    }
    
  }

  function buildGrid( menuBuildingSequence, inGrid ) {
    // fly in grid tiles; menuBuildingSequence = 1..49;
    // returns new menuBuildingSequence
    
    var goAgain = 1;
    if ( menuPlayRenderMode == MENU_PLAY_RENDER_LEVELGRID ) goAgain = 2;
    
    while ( goAgain > 0 ) {
      // loop to ensure we draw one tile per cycle

      var xPos = ( menuBuildingSequence - 1 ) % LEVEL_GRID_MAX_X;
      var yPos = Math.floor( ( menuBuildingSequence - 1 ) / LEVEL_GRID_MAX_X );
            
      if ( menuPlayRenderMode == MENU_PLAY_RENDER_LEVELGRID ) {
        // do some little yPos magic for fun
        
        switch ( menuPlayCurrentLevel % 8 ) {
       
          case 0: break;
          case 1: yPos = ( yPos * 2 ) % LEVEL_GRID_MAX_Y; break;
          case 2: yPos = ( yPos * 3 ) % LEVEL_GRID_MAX_Y; break;
          case 3: yPos = ( yPos * 5 ) % LEVEL_GRID_MAX_Y; break;
          case 4: yPos =   ( ( LEVEL_GRID_MAX_Y - yPos ) - 1);                          break;
          case 5: yPos = ( ( ( LEVEL_GRID_MAX_Y - yPos ) - 1) * 2 ) % LEVEL_GRID_MAX_Y; break;
          case 6: yPos = ( ( ( LEVEL_GRID_MAX_Y - yPos ) - 1) * 3 ) % LEVEL_GRID_MAX_Y; break;
          case 7: yPos = ( ( ( LEVEL_GRID_MAX_Y - yPos ) - 1) * 5 ) % LEVEL_GRID_MAX_Y; break;
        
        }
        
      }
      
      if ( xPos == 0 ) {
        // first tile in a new row; clear out entire row
        
        for ( var i = 0; i < LEVEL_GRID_MAX_X; i++ ) {
         
          vScreen.removeSprite( ID_GRID_START + ( yPos * LEVEL_GRID_MAX_X ) + i );
          
        }
        
      }
      
      if ( menuPlayRenderMode == MENU_PLAY_RENDER_LEVELGRID ) {
        // fill right-to-left
        
        var xPos = ( LEVEL_GRID_MAX_X - xPos ) - 1;
        
      }

      var xyString = "" + xPos + "," + yPos;

      var gridType   = inGrid[ xyString ];
      var gridBitmap = ID_GRID_START + ( yPos * LEVEL_GRID_MAX_X ) + xPos;
            
      if ( !( gridType == LEVEL_GRID_TYPE_NONE ) ) {
        // draw grid tile
        
        var gridFileName = LEVEL_GRID_FILES[ gridType ];
       
        var sprite = new Sprite( gridFileName,
                                 MENU_PLAY_GRID_WIDTH_SINGLE  - 1,
                                 MENU_PLAY_GRID_HEIGHT_SINGLE - 1 );
        sprite.setImgNode( document.getElementById( "bitmap" + gridBitmap ) );
        vScreen.putSprite( sprite, gridBitmap );
        sprite.show();
        
        // different fly-in patterns
        var startX = null;
        var startY = null;
        var patternNum = null;
        
        if ( menuPlayRenderMode == MENU_PLAY_RENDER_LEVELGRID ) {
          
          patternNum = menuPlayCurrentLevel % 5;
          
        } else {
          // when showing user highscore, always show right-away
          
          patternNum = 5;
          
        }
        
        switch ( patternNum ) {
         
          case 0:
          
            startX = MENU_PLAY_GRID_LEFT + 3 * MENU_PLAY_GRID_WIDTH_SINGLE ;
            startY = MENU_PLAY_GRID_TOP  + 3 * MENU_PLAY_GRID_HEIGHT_SINGLE;
            break;
            
          case 1:
            
            startX = MENU_PLAY_GRID_LEFT + 3 * MENU_PLAY_GRID_WIDTH_SINGLE;
            startY = MENU_PLAY_GRID_TOP + ( MENU_PLAY_GRID_HEIGHT_SINGLE * yPos );
            break;
            
          case 2:
            
            startX = MENU_PLAY_GRID_WIDTH_SINGLE * ( xPos - LEVEL_GRID_MAX_X );
            startY = MENU_PLAY_GRID_TOP + ( MENU_PLAY_GRID_HEIGHT_SINGLE * yPos );
            break;
          
          case 3:
            
            startX = MENU_PLAY_GRID_LEFT + 3 * MENU_PLAY_GRID_WIDTH_SINGLE ;
            startY = MENU_PLAY_GRID_TOP  + 3 * MENU_PLAY_GRID_HEIGHT_SINGLE;
            break;
          
          case 4:
            
            startX = MENU_PLAY_GRID_LEFT + 6 * MENU_PLAY_GRID_WIDTH_SINGLE ;
            startY = MENU_PLAY_GRID_TOP + ( MENU_PLAY_GRID_HEIGHT_SINGLE * yPos );
            break;
            
          case 5:
            
            startX = MENU_PLAY_GRID_LEFT + ( MENU_PLAY_GRID_WIDTH_SINGLE  * xPos );
            startY = MENU_PLAY_GRID_TOP  + ( MENU_PLAY_GRID_HEIGHT_SINGLE * yPos );
          
        }
        
        sprite.move( startX, startY );
        
        tGame.moveSprite(
              sprite,
              MENU_PLAY_GRID_LEFT + ( MENU_PLAY_GRID_WIDTH_SINGLE  * xPos ),
              MENU_PLAY_GRID_TOP  + ( MENU_PLAY_GRID_HEIGHT_SINGLE * yPos ),
              movingSSteps,
              GAME_MOVESPRITE_SINE_FLYIN );
            
        goAgain--;
        
      }
          
      menuBuildingSequence++;
          
      if ( menuBuildingSequence > ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) ) {
        // all grid tiles are placed

        goAgain = 0;
        
      }
          
    }
    
    return menuBuildingSequence;
  
  }
  
  function flyAllTheStuffOut() {
    
    // delete most of the icons
    wipeLetterTilesIfAny();
    wipeGridIfAny();
    vScreen.removeSprite( 0 );
    
    for ( var i = ID_MENU_20ITEMS + 8; i < ( ID_MENU_20ITEMS + 20 ); i++ ) {
      
      vScreen.removeSprite( i );
      
    }
          
    for ( var i = ID_MENU_30ITEMS; i < ( ID_MENU_30ITEMS + 6 ); i++ ) {
            
      vScreen.removeSprite( i );
      
    }
    
    // fly out buttons
          
    for ( var i = ID_MENU_20ITEMS; i < ( ID_MENU_20ITEMS + 8 ); i++ ) {
           
      sprite = vScreen.getSpriteById( i );

      if ( sprite ) {
            
        // fly out and destroy at end
        var yPosTo = sprite.getYpos();
          
        if ( yPosTo > 200 ) yPosTo = 735; else yPosTo = -200;
            
        tGame.moveSprite(
            sprite,
            sprite.getXpos(),
            yPosTo,
            movingSSteps,
            GAME_MOVESPRITE_SINE_FLYOUT,
            true );
              
      }
            
    }

  }

}
