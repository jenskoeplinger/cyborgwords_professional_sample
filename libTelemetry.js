/*
 * libTelemetry.js 2012-04-28
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Telemetry()
 *
 * Telemetry.addRecord( String name, double value, double telemetryType )
 * Telemetry.getNameValueString( String name ) -- returns String
 * Telemetry.getValue( String name ) -- returns double
 * Telemetry.getValueCount( String name ) -- returns int
 *
 */

var TELEMETRY_SINGLE      = 0;
var TELEMETRY_AVERAGE     = 1;
var TELEMETRY_EXPONENTIAL = 2; // set 2.5 to have 50% weighting

/*
 * 
 */

function Telemetry() {
  // constructor: the Telemetry object is a container
  // of individual telemetry values that can be added
  // on the fly and printed when needed

  var tRecords = new Array();
  var tRCount  = new Array();
  
  this.addRecord = function( tName, tValue, tType ) {
    // add a new record

    // initialize defaults if needed
    if ( !( tType ) ) tType = TELEMETRY_SINGLE;

    if ( !( tRCount[ tName ] ) ) {

      tRecords[ tName ] = tValue;
      tRCount[ tName ] = 0;

    }

    // now record the actual telemetry value

    var tTypeInt = Math.floor( tType );
    var tCount = tRCount[ tName ] + 1;
    tRCount[ tName ] = tCount;

    switch ( tTypeInt ) {

      case 1: // average

        var prevValue = tRecords[ tName ];
        var newValue = ( ( prevValue * ( tCount - 1 ) ) + tValue ) / tCount;

        tRecords[ tName ] = newValue;

        break;

      case 2: // exponential weighted

        var prevValue = tRecords[ tName ];
        var weighting = tType - tTypeInt;
        var newValue = tValue * ( 1. - weighting ) + prevValue * weighting;

        tRecords[ tName ] = newValue;

        break;

      default: // single value

        tRecords[ tName ] = tValue;

    }

  }

  this.getValue = function( tName ) {

    return tRecords[ tName ];

  }
  
  this.getValueCount = function( tName ) {

    return tCount[ tName ];

  }

  this.getNameValueString = function( tName, newLineString ) {
    // returns a name/value string of the tName/tRecords combination;
    // if null or false, returns all known tRecords

    var returnString = "";

    if ( tName ) {

      returnString = tName
          + " = " + tRecords[ tName ]
          + " (x" + tRCount[ tName ] + ")";

    } else {

      var newLineS = "\n";
      if ( newLineString ) newLineS = newLineString;

      for (thisName in tRecords) {

        returnString += thisName
          + " = " + tRecords[ thisName ]
          + " (x" + tRCount[ thisName ] + ")" + newLineS;

      }

    }

    return returnString;

  }  

}
