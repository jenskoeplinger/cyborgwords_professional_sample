/*
 * gameRaiseLid.js 2012-06-14
 * Copyright (C) 2012 Jens Koeplinger
 */

var gameLidIsOpen = false;

function gameRaiseLid( tGame ) {
  // raises the lid of the trash can

  sysLogger.addLine( "gameRaiseLid()", LOGGER_SOURCE_SEQUENCER );

  // letter shoot animation
  
  var movingSSteps = 5;
  if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE )
      movingSSteps = 3;
  
  var sprite = vScreen.getSpriteById( ID_TRASHBINLID );
  
  tGame.moveSprite(
      sprite,
      CONVEYOR_TRASH_XPOS,
      0,
      movingSSteps,
      GAME_MOVESPRITE_SINE_FLYIN );

  gameLidIsOpen = true;

}
