/*
 * gameLevelComplete.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 */

var finishHighscore      = null;
var finishIsPass         = null;
var finishIsNewHighscore = null;
var finishNextLevelNum   = null;
var finishMovingSteps    = 4;

var finishScoreInitial      = null; // game score without expert bonus
var finishScoreFinal        = null; // game score plus expert bonus (if any)
var finishScoreBonusCnt     = null; // expert bonus; will be destroyed during bonus count-up
var finishScoreNumberObject = null;

var wantHelpIsShowing    = null;

var finishIcons = new Array();

var FINISH_RESUME_SUBSTATE_FROM_BONUS_COUNTUP = 8;
var FINISH_STATE_BONUS_COUNTUP = 50;

var expertBonusCountupSequence = null;
var exportBonusTotalCount      = null;
var exportBonusFramesPerCount  = 2;
var exportBonusFramesPerTick   = 15;
var FINISH_EXPORT_MIN_COUNT    = 60; // show at least for 3 seconds if expert mode

function gameLevelComplete( tGame ) {
  // end of level screen

  switch ( tGame.getSubState() ) {

    case 0:
      
      sysLogger.addLine( "gameLevelComplete()", LOGGER_SOURCE_SEQUENCER );

      // initialized icon arrays
      
      finishIcons[ 0 ] = new Array();
      finishIcons[ 0 ][ "id" ]            = ID_FINISH;
      finishIcons[ 0 ][ "left" ]          = 10;
      finishIcons[ 0 ][ "top" ]           = 30;
      finishIcons[ 0 ][ "width" ]         = 220;
      finishIcons[ 0 ][ "height" ]        = 80;
      finishIcons[ 0 ][ "filename" ]      = FILE_FINISH_LEVEL;
      finishIcons[ 0 ][ "numberleft" ]    = 245;
      finishIcons[ 0 ][ "numbertopdiff" ] = 5;
      finishIcons[ 0 ][ "numberwidth" ]   = 47;
      finishIcons[ 0 ][ "numberheight" ]  = 67;
      finishIcons[ 0 ][ "numberdigits" ]  = 2;
      finishIcons[ 0 ][ "numbercolor" ]   = NUMBER_COLORS_MAP[ "gameoverLevel" ];

      finishIcons[ 1 ] = new Array();
      finishIcons[ 1 ][ "id" ]            = ID_FINISH + 3;
      finishIcons[ 1 ][ "top" ]           = 110;
      finishIcons[ 1 ][ "filename" ]      = FILE_FINISH_SCORE;
      finishIcons[ 1 ][ "numberdigits" ]  = 6;
      finishIcons[ 1 ][ "numbercolor" ]   = NUMBER_COLORS_MAP[ "gameoverScore" ];

      finishIcons[ 2 ] = new Array();
      finishIcons[ 2 ][ "id" ]            = ID_FINISH + 10;
      finishIcons[ 2 ][ "top" ]           = 190;
      finishIcons[ 2 ][ "filename" ]      = FILE_FINISH_RESULT;
      finishIcons[ 2 ][ "numbertopdiff" ] = 0;
      finishIcons[ 2 ][ "numberwidth" ]   = 220;
      finishIcons[ 2 ][ "numberheight" ]  = 170;

      finishIcons[ 3 ] = new Array();
      finishIcons[ 3 ][ "id" ]            = ID_FINISH + 12;
      finishIcons[ 3 ][ "top" ]           = 735;
      finishIcons[ 3 ][ "left" ]          = 45;
      finishIcons[ 3 ][ "width" ]         = 400;
      finishIcons[ 3 ][ "filename" ]      = FILE_FINISH_PLAYNEXT;
      finishIcons[ 3 ][ "numbertopdiff" ] = -285;

      finishIcons[ 4 ] = new Array();
      finishIcons[ 4 ][ "id" ]            = ID_FINISH + 13;
      finishIcons[ 4 ][ "top" ]           = 735;
      finishIcons[ 4 ][ "left" ]          = 45;
      finishIcons[ 4 ][ "width" ]         = 400;
      finishIcons[ 4 ][ "filename" ]      = FILE_FINISH_TRYAGAIN;
      finishIcons[ 4 ][ "numbertopdiff" ] = -195;

      finishIcons[ 5 ] = new Array();
      finishIcons[ 5 ][ "id" ]            = ID_FINISH + 14;
      finishIcons[ 5 ][ "top" ]           = 735;
      finishIcons[ 5 ][ "left" ]          = 45;
      finishIcons[ 5 ][ "width" ]         = 400;
      finishIcons[ 5 ][ "filename" ]      = FILE_FINISH_MAINMENU;
      finishIcons[ 5 ][ "numbertopdiff" ] = -105;

      finishIcons[ 6 ] = new Array();
      finishIcons[ 6 ][ "id" ]            = ID_FINISH + 15;
      finishIcons[ 6 ][ "top" ]           = 735;
      finishIcons[ 6 ][ "left" ]          = 25;
      finishIcons[ 6 ][ "width" ]         = 180;
      finishIcons[ 6 ][ "height" ]        = 60;
      finishIcons[ 6 ][ "numbertopdiff" ] = -455;

      finishIcons[ 7 ] = new Array();
      finishIcons[ 7 ][ "id" ]            = ID_FINISH + 16;
      finishIcons[ 7 ][ "top" ]           = 735;
      finishIcons[ 7 ][ "left" ]          = 45;
      finishIcons[ 7 ][ "width" ]         = 400;
      finishIcons[ 7 ][ "filename" ]      = FILE_FINISH_WANTHELP;
      finishIcons[ 7 ][ "numbertopdiff" ] = -375;

      finishIcons[ 8 ] = new Array();
      finishIcons[ 8 ][ "id" ]            = ID_FINISH + 17;
      finishIcons[ 8 ][ "top" ]           = 735;
      finishIcons[ 8 ][ "left" ]          = 68;
      finishIcons[ 8 ][ "width" ]         = 353;
      finishIcons[ 8 ][ "height" ]        = 44;
      finishIcons[ 8 ][ "filename" ]      = FILE_FINISH_EXPERTBONUS;
      finishIcons[ 8 ][ "numbertopdiff" ] = -530;

      for ( var i = 1; i < 9; i++ ) {
        // set default values for display arrays
  
        for ( thisId in finishIcons[ 0 ] ) {
    
          if ( !( finishIcons[ i ][ thisId ] ) )
            finishIcons[ i ][ thisId ] = finishIcons[ 0 ][ thisId ];
            
        }
  
      }

      // prepare level score variables
      
      finishScoreInitial   = tGame.getLevelScore();
      finishScoreFinal     = finishScoreInitial;
      
      if ( gameScoringModel == SCORING_MODEL_CLEAN ) {
        
          finishScoreBonusCnt  = tGame.getExpertBonusFromScore( finishScoreInitial );
          finishScoreFinal    += finishScoreBonusCnt;
          
      } else {
        
          finishScoreBonusCnt = 0;
          
      }

      // see whether this could be a new high score
      
      finishHighscore = new Highscore( level.getNumber() );
      finishHighscore.read();
      
      finishIsNewHighscore = false;
      
      if ( finishHighscore.getLevelScore() ) {
        
        if ( finishScoreFinal > finishHighscore.getLevelScore() ) {
        
          finishIsNewHighscore = true;
          sysLogger.addLine( "improved existing highscore", LOGGER_SOURCE_SEQUENCER );
          
        }
        
      } else {
        
        finishIsNewHighscore = true;
        sysLogger.addLine( "established first highscore", LOGGER_SOURCE_SEQUENCER );
        
      }

      // see whether the user passed the level
      
      if ( level.getMinimumPassScore() <= finishScoreFinal ) {
       
        finishIsPass = true;
        sysLogger.addLine( "user passed this level", LOGGER_SOURCE_SEQUENCER );
        
      } else {
       
        finishIsPass = false;
        finishIsNewHighscore = false;
        sysLogger.addLine( "user did not pass the level", LOGGER_SOURCE_SEQUENCER );
        
      }
      
      // get the next level number
      //  0: did not pass
      // -1: highest level reached
      
      if ( ( finishIsPass )
           || ( tGame.getHighestNextLevel() > tGame.getLevelNumber() ) ) {
        
        finishNextLevelNum = level.getNextLevelNumber();
        sysLogger.addLine( "user can play next level " + finishNextLevelNum,
                           LOGGER_SOURCE_SEQUENCER );
        
      } else {
        
        finishNextLevelNum = 0;
        
      }
      
      // update highscore data
      
      if ( finishIsNewHighscore ) {
       
        sysLogger.addLine( "write new highscore to local storage",
                           LOGGER_SOURCE_SEQUENCER );
        
        finishHighscore.setGrid(        level.getGrid()         );
        finishHighscore.setTilesPlaced( level.getTilesPlaced()  );
        finishHighscore.setLevelScore(  finishScoreFinal        );
        
        finishHighscore.write();
        
      }
      
      // update next allowable level
      
      if ( finishNextLevelNum > tGame.getHighestNextLevel() ) {
        
        sysLogger.addLine( "highest playable level is now "
                           + finishNextLevelNum,
                           LOGGER_SOURCE_SEQUENCER );
        tGame.setHighestNextLevel( finishNextLevelNum );
        
      }
      
      // update menu selected level if level passed and not highest level
      
      if ( finishIsPass ) {
       
        if ( finishNextLevelNum > 0 ) {
          
          tGame.setCurrentMenuLevel( finishNextLevelNum );
          
        }
        
      }
      
      // remove "exit" and "sound" buttons
      
      vScreen.removeSprite( ID_EXIT );
      vScreen.removeSprite( ID_SOUND );

      tGame.stopMediaSoundtrack();
            
      tGame.setSubState( 1 );
      break;

    case 1: // level

      drawFinishIcon( 0 );
      tGame.setSubState( 2 );
      break;
      
    case 2:
      
      drawFinishNumber( 0, tGame.getLevelNumber() );
      
    case 3:
    case 4:
    case 5:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;

    case 6: // score

      drawFinishIcon( 1 );
      tGame.setSubState( 7 );
      break;

    case 7:
            
      if ( finishScoreFinal > 99999 )
          finishIcons[ 1 ][ "numberleft" ] -= finishIcons[ 0 ][ "numberwidth" ];
      
      finishScoreNumberObject = drawFinishNumber( 1, finishScoreInitial );
      
      // branch for "expert bonus" if expert game mode
      if ( gameScoringModel == SCORING_MODEL_CLEAN ) {
        
        exportBonusTotalCount = 0;
        expertBonusCountupSequence = 0;
        
        tGame.setSubState( FINISH_STATE_BONUS_COUNTUP );
        break;
        
      }

    case 8: // sub state: FINISH_RESUME_SUBSTATE_FROM_BONUS_COUNTUP
    case 9:
    case 10:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 11: // result
      
      drawFinishIcon( 2 );
      tGame.setSubState( 12 );
      break;
      
    case 12:
      
      var fileName = null;
      
      if ( finishIsPass ) {
        
        if ( finishIsNewHighscore ) {
       
          fileName = FILE_FINISH_NEWHIGHSCORE;
          
        } else {
          
          fileName = FILE_FINISH_PASS;
          
        }
        
      } else {
        
        fileName = FILE_FINISH_FAIL;
        
      }
      
      var sprite = new Sprite( fileName,
                               finishIcons[ 2 ][ "numberwidth" ],
                               finishIcons[ 2 ][ "numberheight" ] );
      sprite.setImgNode(
          document.getElementById( "bitmap" + ( finishIcons[ 2 ][ "id" ] + 1 ) ) );
      vScreen.putSprite( sprite, finishIcons[ 2 ][ "id" ] + 1 );
      sprite.show();
      sprite.move( 490,
                   finishIcons[ 2 ][ "top" ] + finishIcons[ 2 ][ "numbertopdiff" ] );

      tGame.moveSprite(
          sprite,
          finishIcons[ 2 ][ "numberleft" ],
          finishIcons[ 2 ][ "top" ] + finishIcons[ 2 ][ "numbertopdiff" ],
          finishMovingSteps,
          GAME_MOVESPRITE_SINE_FLYIN );
      
      tGame.setSubState( 13 );
      break;
      
    case 13: // fly-in stars (if any)

      var fileName = null;
      wantHelpIsShowing = false;

      if ( finishIsPass ) {
       
        fileName = level.getStarsFilename( finishScoreFinal );
        
        if ( fileName ) {
          
          sysLogger.addLine( "show award " + fileName, LOGGER_SOURCE_SEQUENCER );
          finishIcons[ 6 ][ "filename" ] = fileName;
          flyInFinishIcon( 6, GAME_MOVESPRITE_ZIGZAG_5, 35 );
        
        }
        
      }
      
      if ( !( fileName ) ) {
        
        sysLogger.addLine( "didn't pass; show and blink help link", LOGGER_SOURCE_SEQUENCER );
        flyInFinishIcon( 7, GAME_MOVESPRITE_ZIGZAG_5, 35 );
        
        wantHelpIsShowing = true;
        tGame.playMediaEffect( FILE_SOUND_STAR0, true );
          
      }
      
      switch ( fileName ) {
        
        case FILE_MENU_STARS_1:

          sysLogger.addLine( "passed with one star; show help link", LOGGER_SOURCE_SEQUENCER );
          flyInFinishIcon( 7, GAME_MOVESPRITE_ZIGZAG_5, 3 );
        
          wantHelpIsShowing = true;
        
          tGame.playMediaEffect( FILE_SOUND_STAR1, true );
          break;
        
        case FILE_MENU_STARS_2:
          
          tGame.playMediaEffect( FILE_SOUND_STAR2, true );
          break;
          
        case FILE_MENU_STARS_3:
          
          tGame.playMediaEffect( FILE_SOUND_STAR3, true );
          break;
        
      }
      
      tGame.setSubState( 14 );
      break;
      
    case 14: // play next level

      if ( finishNextLevelNum > 0 ) {

        flyInFinishIcon( 3 );
      
      }
      
      tGame.setSubState( 15 );
      break;

    case 15: // try level again

      flyInFinishIcon( 4 );
      tGame.setSubState( 16 );
      break;
      
    case 16: // main menu

      touch.setTouchOccurred( false );

      flyInFinishIcon( 5 );
      tGame.setSubState( 30 );
      break;
      
    case 30: // wait for user touch

      if ( touch.getTouchOccurred() ) {

        sysLogger.addLine( "touch at ("
                           + touch.getTouchX()
                           + ","
                           + touch.getTouchY()
                           + ")", LOGGER_SOURCE_SEQUENCER );
        
        var thisY = touch.getTouchY();
        var thisSelected = null;
        var compY = null;
        
        var buttonComponent = new Array();
        
        buttonComponent[ 0 ] = 3;
        buttonComponent[ 1 ] = 4;
        buttonComponent[ 2 ] = 5;
        
        if ( wantHelpIsShowing ) buttonComponent[ 3 ] = 7;
        
        for ( buttonRef in buttonComponent ) {
          
          var i = buttonComponent[ buttonRef ];
         
          compY = finishIcons[ i ][ "top" ]
                  + finishIcons[ i ][ "numbertopdiff" ];
                  
          if ( ( thisY > compY )
               && ( thisY < ( compY + finishIcons[ i ][ "height" ] ) ) ) {
            
            thisSelected = i;
            
          }
          
        }
        
        switch ( thisSelected ) {
         
          case 3: // play next level

            if ( finishNextLevelNum > 0 ) {
              
              if ( ( finishNextLevelNum > VERSION_FREE_MAXLEVEL )
                   && ( cyborgWordsVersion == VERSION_FREE ) ) {
                // not available in free version;
                // show teaser menu instead
                
                tGame.playMediaEffect( FILE_SOUND_PICKUP );
              
                tGame.setSubState( 33 );
                
              } else {
                // play next level
                
                tGame.playMediaEffect( FILE_SOUND_PICKUP );
                
                tGame.setLevelNumber( finishNextLevelNum );
                tGame.setSubState( 31 );
                
              }
              
            }
            
            break;

          case 4: // try same level again

            tGame.playMediaEffect( FILE_SOUND_PICKUP );

            tGame.setSubState( 31 );
            break;

          case 5: // main menu            

            tGame.playMediaEffect( FILE_SOUND_PICKUP );

            tGame.setSubState( 32 );
            break;
            
          case 7: // link to help

            sysLogger.addLine( "selection: help", LOGGER_SOURCE_SEQUENCER );

            tGame.setState( GAME_HELP );
            tGame.setSubState( 0 );
            
            tGame.playMediaEffect( FILE_SOUND_PICKUP );
            
            break;
            
          default: // do nothing          

        }

        touch.setTouchOccurred( false );

      }
        
      break;
      
    case 31: // play level, must use tGame.setLevelNumber( level ) before

      for ( var i = ID_BLANKGRAYOUT; i < MAX_SPRITES; i++ ) {
        
        vScreen.removeSprite( i );
        
      }
      
      scoreNumber.hide();
      
      tGame.setState( GAME_LEVEL_PREPARING );
      tGame.setSubState( 0 );

      break;
      
    case 32: // wipe all and go to main menu

      for ( var i = 0; i < MAX_SPRITES; i++ ) {
        
        vScreen.removeSprite( i );
        
      }

      tGame.setState( GAME_INITIALIZING );
      tGame.setSubState( GAME_INITIALIZING_ENTRYSUBSTATE );

      break;
      
    case 33: // show level select menu

      for ( var i = 0; i < MAX_SPRITES; i++ ) {
        
        vScreen.removeSprite( i );
        
      }

      tGame.setState( GAME_MENU_PLAY );
      tGame.setSubState( 0 );
        
      break;
      
    case 50: // FINISH_STATE_BONUS_COUNTUP; expert bonus count-up

      exportBonusTotalCount++;

      switch ( expertBonusCountupSequence ) {
        
        case 0: // show blinking expert bonus picture

          sysLogger.addLine( "start expert mode bonus count-up", LOGGER_SOURCE_SEQUENCER );
          flyInFinishIcon( 8, GAME_MOVESPRITE_ZIGZAG_5, 12 );

          expertBonusCountupSequence++;
          break;
          
        case 1: // play sound

          tGame.playMediaEffect( FILE_SOUND_EXPERTBONUS, true );
          
          expertBonusCountupSequence++;
          break;
          
        case 2: // count-up

          if ( exportBonusTotalCount % exportBonusFramesPerTick == 0 ) {

            tGame.playMediaEffect( FILE_SOUND_EXPERTTICKS );
            
          }
          
          // simple bonus score count-up handler
          
          if ( !( exportBonusTotalCount % exportBonusFramesPerCount == 0 ) ) {
            // only perform the score count-up every so-and-so many frames;
            // right now: do nothing
            
          } else if ( finishScoreBonusCnt > 99999 ) {
            
            finishScoreBonusCnt -= 100000;
            finishScoreNumberObject.addValue( 100000 );
            
          } else if ( finishScoreBonusCnt > 9999 ) {
            
            finishScoreBonusCnt -= 10000;
            finishScoreNumberObject.addValue( 10000 );
            
          } else if ( finishScoreBonusCnt > 999 ) {
            
            finishScoreBonusCnt -= 1000;
            finishScoreNumberObject.addValue( 1000 );
            
          } else if ( finishScoreBonusCnt > 99 ) {
            
            finishScoreBonusCnt -= 100;
            finishScoreNumberObject.addValue( 100 );
            
          } else if ( finishScoreBonusCnt > 9 ) {
            
            finishScoreBonusCnt -= 10;
            finishScoreNumberObject.addValue( 10 );
            
          } else if ( finishScoreBonusCnt > 0 ) {
            
            finishScoreBonusCnt -= 1;
            finishScoreNumberObject.addValue( 1 );
            
          } else {
            // bonus count-up is done
            
            if ( exportBonusTotalCount > FINISH_EXPORT_MIN_COUNT ) {
              // move on
              
              expertBonusCountupSequence++;
              
            }
            
          }
          
          break;

        case 3: // hide bonus picture

          vScreen.removeSprite( finishIcons[ 8 ][ "id" ] );
          expertBonusCountupSequence++;
          break;
          
        case 4: // resume

          tGame.setSubState( FINISH_RESUME_SUBSTATE_FROM_BONUS_COUNTUP );
        
      }

      break;
      
    default: // do nothing
      
      break;

  }

  function drawFinishIcon( iconId ) {

    var sprite = new Sprite( finishIcons[ iconId ][ "filename" ],
                             finishIcons[ iconId ][ "width" ],
                             finishIcons[ iconId ][ "height" ] );
    sprite.setImgNode(
        document.getElementById( "bitmap" + finishIcons[ iconId ][ "id" ] ) );
    vScreen.putSprite( sprite, finishIcons[ iconId ][ "id" ] );
    sprite.show();
    sprite.move( finishIcons[ iconId ][ "left" ], finishIcons[ iconId ][ "top" ] );
    
    return sprite;

  }
  
  function flyInFinishIcon( iconId, flyInMode, flyInMovingSteps ) {
   
    var sprite = drawFinishIcon( iconId );
    
    if ( !( sprite ) ) return;
    
    if ( flyInMode == null )        flyInMode        = GAME_MOVESPRITE_SINE_FLYIN;
    if ( flyInMovingSteps == null ) flyInMovingSteps = finishMovingSteps;
    
    tGame.moveSprite(
          sprite,
          finishIcons[ iconId ][ "left" ],
          finishIcons[ iconId ][ "top" ] + finishIcons[ iconId ][ "numbertopdiff" ],
          flyInMovingSteps,
          flyInMode );
    
  }
  
  function drawFinishNumber( numberId, numberValue ) {
    
      var levelNumber = new Number(
                           finishIcons[ numberId ][ "id" ] + 1,
                           finishIcons[ numberId ][ "numberdigits" ],
                           490,
                           finishIcons[ numberId ][ "top" ] + finishIcons[ numberId ][ "numbertopdiff" ],
                           finishIcons[ numberId ][ "numberwidth" ],
                           finishIcons[ numberId ][ "numberheight" ],
                           finishIcons[ numberId ][ "numbercolor" ] );

      levelNumber.setValue( numberValue );
      levelNumber.show();

      tGame.moveNumber( levelNumber,
                        finishIcons[ numberId ][ "numberleft" ],
                        finishIcons[ numberId ][ "top" ] + finishIcons[ numberId ][ "numbertopdiff" ],
                        finishMovingSteps,
                        GAME_MOVESPRITE_SINE_FLYIN );
      
      return levelNumber;
    
  }

}

