/*
 * libHelpScreen.js 2012-07-29
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New HelpScreen()
 *
 * HelpScreen.addLink( HelpLink )
 * HelpScreen.getAllLinks() -- returns Array of HelpLink
 * HelpScreen.getAllLinkFilenames() -- returns Array of String
 * HelpScreen.getLinkByTargetString( String ) -- returns HelpLink
 * HelpScreen.getBackgroundFile() -- returns String
 * HelpScreen.getDisplayFile() -- returns String
 * HelpScreen.getHasStaticHeader() -- returns boolean
 * HelpScreen.setBackgroundFile( String )
 * HelpScreen.setDisplayFile( String )
 * HelpScreen.setHasStaticHeader( boolean )
 *
 */

function HelpScreen() {

  var helpLinks            = new Array();
  var helpLinkFilenames    = new Array();
  var helpLinkStringLookup = new Array();
  var hBgPicFile           = null;
  var hDisplayFile         = null;
  var hHasStaticHeader     = null;
  
  this.getAllLinks         = function()         { return helpLinks;          }
  this.getAllLinkFilenames = function()         { return helpLinkFilenames;  }
  this.getBackgroundFile   = function()         { return hBgPicFile;         }
  this.getDisplayFile      = function()         { return hDisplayFile;       }
  this.getHasStaticHeader  = function()         { return hHasStaticHeader;   }
  
  this.setBackgroundFile   = function( cValue ) { hBgPicFile       = cValue; }
  this.setDisplayFile      = function( cValue ) { hDisplayFile     = cValue; }
  this.setHasStaticHeader  = function( cValue ) { hHasStaticHeader = cValue; }
  
  this.addLink = function( newHelpLink ) {
    
    if ( !( newHelpLink ) ) return;
    
    helpLinks.push( newHelpLink );
    
    if ( newHelpLink.getFilename() ) {
     
      helpLinkFilenames.push( newHelpLink.getFilename() );
      
    }
    
    var newTargetString = String( newHelpLink.getLinkTarget() );
    
    if ( newTargetString ) {
     
      helpLinkStringLookup[ newTargetString ] = newHelpLink;
      
    }
    
  }
  
  this.getLinkByTargetString = function( inString ) {
   
    if ( !( inString ) ) return null;
    
    return helpLinkStringLookup[ String( inString ) ];
    
  }
  
}
