/*
 * libMovingSprite.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New MovingSprite(Sprite sprite, int xPosTo, int yPosTo
 *                 [, int numSteps] [, int typeOfMove] [, boolean killOnEnd] )
 *
 * MovingSprite.advance() -- returns boolean (true if move is finished)
 *
 */

// typeOfMove constants

var GAME_MOVESPRITE_LINEAR           = 0;
var GAME_MOVESPRITE_SINE             = 1;
var GAME_MOVESPRITE_SINE_FLYIN       = 2;
var GAME_MOVESPRITE_SINE_FLYOUT      = 3;
var GAME_MOVESPRITE_ZIGZAG           = 4;
var GAME_MOVESPRITE_ZAPINOUT         = 5;
var GAME_MOVESPRITE_SHOWANDOUT       = 6;
var GAME_MOVESPRITE_ZIGZAG_2         = 7;
var GAME_MOVESPRITE_ZIGZAG_3         = 8;
var GAME_MOVESPRITE_ZIGZAG_5         = 9;

// numSteps default

var GAME_MOVESPRITE_STEPS_DEFAULT    = 6;

function MovingSprite( sprite, xPosTo, yPosTo, numSteps, typeOfMove, killOnEnd ) {
  // constructor: set the arrays, map the path

  var mSprite     = sprite;
  var mSpriteId   = mSprite.getId();

  var mXPosFrom   = mSprite.getXpos();
  var mYPosFrom   = mSprite.getYpos();
  var mXPosTo     = xPosTo;
  var mYPosTo     = yPosTo;

  var mNumSteps   = numSteps;
  var mTypeOfMove = typeOfMove;
  var mThisStep   = 0;
  var mkillOnEnd  = killOnEnd;
  
  if ( !( mNumSteps ) )   mNumSteps   = GAME_MOVESPRITE_STEPS_DEFAULT;
  if ( !( mTypeOfMove ) ) mTypeOfMove = GAME_MOVESPRITE_LINEAR;

  if ( mNumSteps < 1 ) mNumSteps = 1; // move right away

  // tell the sprite that this class intends on moving it
  mSprite.setIsMoving( true );
  mSprite.setXposTo( xPosTo );
  mSprite.setYposTo( yPosTo );

  if ( mSprite.getIsPickedUp() ) {
    // sprite movement is currently handled elsewhere;
    // move immediately to set it's intended position to what
    // it needs to be if dropped, and flag it as a single-step
    // moving sprite for the action script
    
    mSprite.move( xPosTo, yPosTo );
    mNumSteps = 1;
    mTypeOfMove = GAME_MOVESPRITE_LINEAR;
    mkillOnEnd = false; // don't automatically destroy
    
  }

  this.advance = function() {
    // moves the sprite to the next position;
    // returns true if this is the last position (or if the sprite
    // has already been moved otherwise and MovingSprite can be destroyed)

    var stopMoving = false;
    
    // sprite has been removed from screen?
    if ( !( mSprite.getScreen() ) )    stopMoving = true;
    // sprite has been moved manually?
    if ( !( mSprite.getIsMoving() ) )  stopMoving = true;
    
    if ( stopMoving ) {
      // destroy references to the MovingSprite object
      
      mSprite.setIsMoving( false );      
      return true; // indicate that this has finished moving
      
    }

    mThisStep++;
    if ( mThisStep > mNumSteps ) { mThisStep = mNumSteps; }

    var mAngle   = null;
    var mPercent = null;

    switch ( mTypeOfMove ) {

      case GAME_MOVESPRITE_SINE:

        mAngle   = Math.PI * ( mThisStep / mNumSteps );
        mPercent = ( 1. - Math.cos( mAngle ) ) / 2.;
        break;

      case GAME_MOVESPRITE_SINE_FLYIN:

        mAngle   = ( Math.PI / 2. ) * ( mThisStep / mNumSteps );
        mPercent = Math.sin( mAngle );
        break;

      case GAME_MOVESPRITE_SINE_FLYOUT:

        mAngle   = ( Math.PI / 2. ) * ( mThisStep / mNumSteps );
        mPercent = 1. - Math.cos( mAngle );
        break;

      case GAME_MOVESPRITE_ZIGZAG:
        
        if ( ( ( mNumSteps - mThisStep ) % 2 ) == 0 ) {
          
          mPercent = 1;
          
        } else {
          
          mPercent = 0;
          
        }
        
        break;
        
      case GAME_MOVESPRITE_ZIGZAG_2:
        
        if ( ( ( mNumSteps - mThisStep ) % 4 ) < 2 ) {
          
          mPercent = 1;
          
        } else {
          
          mPercent = 0;
          
        }
        
        break;
        
      case GAME_MOVESPRITE_ZIGZAG_3:
        
        if ( ( ( mNumSteps - mThisStep ) % 6 ) < 3 ) {
          
          mPercent = 1;
          
        } else {
          
          mPercent = 0;
          
        }
        
        break;
        
      case GAME_MOVESPRITE_ZIGZAG_5:
        
        if ( ( ( mNumSteps - mThisStep ) % 10 ) < 5 ) {
          
          mPercent = 1;
          
        } else {
          
          mPercent = 0;
          
        }
        
        break;
        
      case GAME_MOVESPRITE_ZAPINOUT:
        
        var trimester = Math.floor( 3. * ( mThisStep - 1 ) / mNumSteps );
        
        switch ( trimester ) {
         
          case 0: // flying in

            mAngle   = ( Math.PI / 2. ) * ( 3. * ( mThisStep - 1 ) / mNumSteps );
            mPercent = 0.5 * Math.sin( mAngle );
            break;

          case 1: // holding (do nothing)
            
            mPercent = 0.5;
            break;

          case 2: // flying out

            var trimesterStartStep = Math.floor( 2. * mNumSteps / 3. );
            
            mAngle   = ( Math.PI / 2. )
                       * (   ( ( mThisStep - 1 ) - trimesterStartStep )
                           / ( ( mNumSteps - 1 ) - trimesterStartStep )
                         );
                       
            mPercent = 1. - 0.5 * Math.cos( mAngle );
            break;
          
        }
        
        break;

      case GAME_MOVESPRITE_SHOWANDOUT:

        if ( mThisStep > ( mNumSteps / 2 ) ) {

          var secondHalfStep = 2 * Math.floor( 0.01 + mThisStep - ( mNumSteps / 2 ) );
          mAngle   = ( Math.PI / 2. ) * ( secondHalfStep / mNumSteps );
          mPercent = 1. - Math.cos( mAngle );

        } else {

          mPercent = 0;

        }

        break;
        
      case GAME_MOVESPRITE_LINEAR:
        
      default:

        mPercent = mThisStep / mNumSteps;

    }

    newX = ( mXPosTo - mXPosFrom ) * mPercent + mXPosFrom;
    newY = ( mYPosTo - mYPosFrom ) * mPercent + mYPosFrom;

    mSprite.move( newX, newY );

    if ( mThisStep == mNumSteps ) {
      // finished moving
      
      if ( mkillOnEnd ) {
        // force removal of sprite from screen
        
        var mScreen = mSprite.getScreen();
        mScreen.removeSprite( mSprite.getId() );
        
      }
      
      return true;
      
    }

    // keep moving
    mSprite.setIsMoving( true );
    
    return false; // keep on moving

  }
  
}
