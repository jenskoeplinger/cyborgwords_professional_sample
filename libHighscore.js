/*
 * 
 * THIS FILE HAS BEEN MODIFIED FOR THE PROFESSIONAL SAMPLE
 * VERSION: THE PRIME NUMBERS AND SALT OF THE HASH ALGORITHM
 * HAVE BEEN ALTERED.
 * 
 * libLogger.js 2012-07-07
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Highscore(int levelNumber)
 *
 * Highscore.decode64plus(String inString) -- returns String
 * Highscore.encode64plus(String bits) -- returns String
 * Highscore.getEncodedString() -- returns String
 * Highscore.getLevelScore() -- returns int
 * Highscore.getGrid() -- returns returns char[][]
 * Highscore.getTilesPlaced() -- returns char[][]
 * Highscore.lesserHash(String inString) -- returns String
 * Highscore.read() -- returns boolean (not null)
 * Highscore.setEncodedString(String encodedString)
 * Highscore.setLevelScore(int score)
 * Highscore.setGrid(char[][])
 * Highscore.setTilesPlaced(char[][])
 * Highscore.write() -- returns boolean (not null)
 * 
 */

var HIGHSCORE_HASDATA     = false;

/*
 * 
 */

function Highscore( levelNumber ) {
  // constructor
  
  var hEncodedString = null;
  var hGrid          = null;
  var hTilesPlaced   = null;
  var hLevelScore    = null;

  this.getGrid            = function()           { return hGrid;            }
  this.getTilesPlaced     = function()           { return hTilesPlaced;     }
  this.getLevelScore      = function()           { return hLevelScore;      }

  this.setGrid = function( inGrid ) {

    hGrid             = inGrid;
    hEncodedString    = null;

  }

  this.setTilesPlaced = function( inTilesPlaced ) {

    hTilesPlaced      = inTilesPlaced;
    hEncodedString    = null;

  }

  this.setLevelScore = function( inScore ) {

    hLevelScore       = inScore;
    hEncodedString    = null;

  }

  this.lesserHash = function( inString ) {
    // returns a simple hash code

    var prime1 = 300; // ALTERED FOR PROFESSIONAL SAMPLE - OBVIOUSLY THESE
    var prime2 = 300; // ARE NOT PRIME NUMBERS, AND THEY ARE ALSO FAR TOO
    var prime3 = 300; // SMALL
    var hSharedSecret  = "ALTERED FOR PROFESSIONAL SAMPLE"

    sysLogger.addLine( "lesserHash request for '" + inString + "'",
                       LOGGER_SOURCE_HIGHSCORE );

    if ( inString.length < 10 )
        return sysLogError( "ERROR too short: '" + inString + "'" );

    var returnCodeInt = 1;
    var thisChar      = null;
    var prevChar      = inString.charCodeAt( 0 );
    var thisPos       = 1;

    inString += ( hSharedSecret + inString.charAt( 2 ) + inString.charAt( 9 ) );

    while ( thisPos < inString.length ) {

      thisChar = inString.charCodeAt( thisPos );

      returnCodeInt *= ( ( ( 2 + thisChar ) * ( 5 + prevChar ) ) % prime2 );
      returnCodeInt = ( returnCodeInt % prime1 )
                      + ( returnCodeInt % prime2 )
                      + ( returnCodeInt % prime3 );      

      prevChar = thisChar;
      thisPos++;

    }

    returnCodeInt = 65536 + ( returnCodeInt % 65536 );
    var returnCodeHex = returnCodeInt.toString( 16 );
    returnCodeHex = returnCodeHex.substr( 1, 4 );

    sysLogger.addLine( "returning: " + returnCodeHex, LOGGER_SOURCE_HIGHSCORE );

    return returnCodeHex;

  }

  var encode64string = "ABCDEFGHIJKLmNOPqRSTUVWYXZabcdefghijklMnopQrstuvwxyz1234567890-_.";

  this.encode64plus = function( inString ) {
    // takes incoming string of bits (characters "0" or "1") and
    // encodes them in some variant of base64, a couple of random
    // characters, and a hash for authentication

    sysLogger.addLine( "encoding request for '" + inString + "'",
                       LOGGER_SOURCE_HIGHSCORE );

    if ( inString.length < 60 )
        return sysLogError( "ERROR too short: '" + inString + "'" );

    var inStringLenMod6 = inString.length % 6;
    var thisString = inString;

    if ( inStringLenMod6 > 0 ) {
      // pad with trailing zeroes

      var sixZeroes = "0000000";
      thisString += sixZeroes.substr( 0, ( 6 - inStringLenMod6 ) );

    }

    // go in chunks of 6 characters, convert

    var returnPayload = "";

    while ( thisString.length > 0 ) {
      // go in chunks of 6 bits and prepare returnPayload

      var thisBits  = thisString.substr( 0, 6 );
      var thisIndex = parseInt( thisBits, 2 );
     
      returnPayload += encode64string.substr( thisIndex, 1 );

      // get the next 6 bits
      if ( thisString.length == 6 ) {

        thisString = "";

      } else {

        thisString = thisString.substr( 6, thisString.length - 6 );

      }

    }

    returnPayload += encode64string.substr( 64, 1 ); // terminate

    // insert additional random characters into the payload,
    // hash it, and append the hash

    returnPayload = encode64string.substr( Math.floor( Math.random() * 64 ), 1 )
                    + encode64string.substr( Math.floor( Math.random() * 64 ), 1 )
                    + returnPayload;

    returnPayload += this.lesserHash( returnPayload );

    sysLogger.addLine( "returning encoded: '" + returnPayload + "'",
                       LOGGER_SOURCE_HIGHSCORE );
    
    return returnPayload;

  }

  this.decode64plus = function( inString ) {
    // verifies the hash, the decodes the payload of inString into a series of bits ("0" and "1")

    sysLogger.addLine( "decoding request for '" + inString + "'",
                       LOGGER_SOURCE_HIGHSCORE );

    if ( inString.length < 10 )
        return sysLogError( "ERROR too short: '" + inString + "'" );

    var thisHash    = null;
    var thisPayload = null;

    if ( inString.charAt( inString.length - 5 ) == encode64string.substr( 64, 1 ) ) {
      
      thisHash = inString.substr( inString.length - 4, 4 );
      thisPayload = inString.substr( 0, inString.length - 4 );

    } else {

      return sysLogError( "ERROR syntax: '" + inString + "'" );

    }

    var compareHash = this.lesserHash( thisPayload );

    if ( compareHash == thisHash ) {
      // valid hash; strip of random charachters

      thisPayload = thisPayload.substr( 2, thisPayload.length - 3 );

    } else {

      return sysLogError( "ERROR not authentic: '" + inString + "'" );

    }

    // decode payload

    var returnString = "";
    var thisCharInt = null;
    var thisCharVal = null;
    var sixzeroes = "000000";

    while ( thisPayload.length > 0 ) {

      thisCharInt = encode64string.indexOf( thisPayload.charAt( 0 ) );

      if ( ( thisCharInt < 0 ) || ( thisCharInt > 63 ) ) {

        return sysLogError( "ERROR encoding: '" + inString + "'" );

      }

      thisCharVal = thisCharInt.toString( 2 );
      if ( thisCharVal.length < 6 ) thisCharVal = sixzeroes.substr( 0, 6 - thisCharVal.length ) + thisCharVal;

      returnString += thisCharVal;

      if ( thisPayload.length > 1 ) {

        thisPayload = thisPayload.substr( 1, thisPayload.length - 1 );

      } else {

        thisPayload = "";

      }

    }

    sysLogger.addLine( "returning decoded: '" + returnString + "'",
                       LOGGER_SOURCE_HIGHSCORE );

    return returnString;

  }

  this.getEncodedString = function() {
    // compiles game data into an encoded string
    
    sysLogger.addLine( "request to encode level variables",
                       LOGGER_SOURCE_HIGHSCORE );
    
    if ( !( hEncodedString == null ) ) return hEncodedString;

    if ( hGrid        == null ) return sysLogError( "ERROR grid is null" );
    if ( hTilesPlaced == null ) return sysLogError( "ERROR tiles placed is null" );
    if ( hLevelScore  == null ) return sysLogError( "ERROR level score is null" );
    
    // compute binary string from version, grid, letter tiles, and score;
    // then encode
    
    // (1) version
    
    var encBits   = "00";
    
    // (2) grid and letter tiles
    
    var posString      = null;
    var thisEnc        = null;
    var thisTile       = null;
    var thisTileLetter = null;
    var thisTileChar   = null;
    var thisTileBin    = null;
    
    for ( var yPos = 0; yPos < LEVEL_GRID_MAX_Y; yPos++ ) {
    
      for ( var xPos = 0; xPos < LEVEL_GRID_MAX_X; xPos++ ) {
        
        posString = "" + xPos + "," + yPos;
        
        thisEnc = LEVEL_GRID_ENCODE[ hGrid[ posString ] ];
        
        if ( thisEnc.length < 2 ) return sysLogError( 
                                         "ERROR unknown encoding '"
                                         + thisEnc
                                         + "' for grid type '"
                                         + hGrid[ posString ]
                                         + "' at "
                                         + posString );
        
        encBits += thisEnc;
        
        if ( !( hGrid[ posString ] == LEVEL_GRID_TYPE_NONE ) ) {
          // only append encoding for letter tile if there's a grid tile
          
          thisTile = hTilesPlaced[ posString ];
          
          if ( thisTile == LEVEL_TILE_NONE ) {
            // no tile placed, encode as "00"
            
            encBits += "00";
            
          } else {

            switch ( LEVEL_TILES[ thisTile ][ 3 ] ) {
            
              case 1: encBits += "1"; break
              case 2: encBits += "01"; break
              
              default:
                return sysLogError( "ERROR unknown letter tile '"
                                         + thisTile
                                         + " word multiplier '"
                                         + LEVEL_TILES[ thisTile ][ 3 ]
                                         + "' at "
                                         + posString );

            }
            
            thisTileLetter = LEVEL_TILES[ thisTile ][ 0 ];
            thisTileChar   = thisTileLetter.charCodeAt( 0 ) - 97;
            thisTileBin    = "00000" + thisTileChar.toString( 2 );
            
            encBits += thisTileBin.substr( thisTileBin.length - 5, 5 );
            
          }
          
        }
        
      }
      
    }
    
    // (3) score as 20 bits
    
    var scoreString = "00000000000000000000" + hLevelScore.toString( 2 );
    encBits += scoreString.substr( scoreString.length - 20, 20 );
    
    encBits += "1"; // indicator bit for 20 bit score
    
    // (4) encode
    
    return this.encode64plus( encBits );

  }

  this.setEncodedString = function( inString ) {
    // decode inString,
    // parse it into level variables (grid, tiles, score),
    // and update hEncodedString if successful;
    // clears all arrays if error
    
    sysLogger.addLine( "request to decode level variables from '"
                       + inString + "'",
                       LOGGER_SOURCE_HIGHSCORE );
    
    encBits = this.decode64plus( inString );
    
    if ( encBits == null ) return sysLogError( "ERROR decode64plus failed" );
    
    // (1) check version
    
    var thisVersion = encBits.substr( 0, 2 );
    if ( !( thisVersion == "00" ) ) return sysLogError( "ERROR version unknown" );
    
    encBits = encBits.substr( 2, encBits.length - 2 );
    
    sysLogger.addLine( "version OK", LOGGER_SOURCE_HIGHSCORE );
    
    // (2) get grid and letter tiles
    
    clearObjectData();
    
    var thisBits           = null;
    var thisGridType       = null;
    var letterTileMult     = null;
    var letterASCIIoffset  = null;
    var thisLetterTileBits = null;
    var thisLetterTileInt  = null;
    var thisLetterTile     = null;
    var gridTilesFound     = 0;
    var letterTilesFound   = 0;
    
    hGrid        = new Array();
    hTilesPlaced = new Array();
    
    for ( var yPos = 0; yPos < LEVEL_GRID_MAX_Y; yPos++ ) {
    
      for ( var xPos = 0; xPos < LEVEL_GRID_MAX_X; xPos++ ) {
        
        posString = "" + xPos + "," + yPos;
        
        thisBits = encBits.substr( 0, 2 );
        
        if ( thisBits == LEVEL_GRID_ENCODE[ LEVEL_GRID_TYPE_NONE ] ) {
          // no grid tile here
          
          hGrid[        posString ] = LEVEL_GRID_TYPE_NONE;
          hTilesPlaced[ posString ] = LEVEL_TILE_NONE;
          
          encBits = encBits.substr( 2, encBits.length - 2 );
          
        } else {
          // grid tile exists
          
          gridTilesFound++;
          
          thisBits = encBits.substr( 0, 4 );
          thisGridType = LEVEL_GRID_DECODE[ thisBits ];
          hGrid[ posString ] = thisGridType;
          
          encBits = encBits.substr( 4, encBits.length - 4 );
          
          // check for letter tile
          
          thisBits = encBits.substr( 0, 1 );
          
          if ( thisBits == "0" ) thisBits += encBits.substr( 1, 1 );
          
          switch ( thisBits ) {
           
            case "00": // no letter placed

              letterTileMult = 0;
              encBits = encBits.substr( 2, encBits.length - 2 );
              break;

            case "01": // letter placed with x2 word multiplier

              letterTileMult = 2;
              encBits = encBits.substr( 2, encBits.length - 2 );
              break;

            case "1":  // regular letter placed

              letterTileMult = 1;
              encBits = encBits.substr( 1, encBits.length - 1 );
              break;

            default:
            
              return sysLogError( "ERROR decoding letter tile type at '"
                                  + encBits + "'" );
            
          }
          
          letterASCIIoffset = 0;
          
          switch ( letterTileMult ) {
           
            case 2: letterASCIIoffset = 32;
            case 1:
              
              letterTilesFound++;
              
              thisLetterTileBits = encBits.substr( 0, 5 );
              thisLetterTileInt  = parseInt( thisLetterTileBits, 2 );
              thisLetterTileInt += ( 65 + letterASCIIoffset );
              
              thisLetterTile = String.fromCharCode( thisLetterTileInt );

              hTilesPlaced[ posString ] = thisLetterTile;
              encBits = encBits.substr( 5, encBits.length - 5 );
              
              break;
              
            default:
              // no tile
              
              hTilesPlaced[ posString ] = LEVEL_TILE_NONE;
            
          }
          
        }
    
      }
      
    }
    
    sysLogger.addLine( "decoded "
                       + gridTilesFound
                       + " grid and "
                       + letterTilesFound
                       + " letter tiles",
                       LOGGER_SOURCE_HIGHSCORE );
    
    // (3) get score
    
    if ( encBits.length < 16 )
        return sysLogError( "ERROR remaining bits too short for score '"
                            + encBits + "'" );
    
    var thisScoreBits = null;
        
    if (    ( encBits.length >= 21 )
         && ( encBits.substr( 20, 1 ) == "1" ) ) {
     
      // 20-bit encoded score (followed by "1" indicator bit)
        
      thisScoreBits = encBits.substr( 0, 20 );
      encBits = encBits.substr( 21, encBits.length - 21 );
      
      sysLogger.addLine( "20-bit encoded score "
                         + thisScoreBits,
                         LOGGER_SOURCE_HIGHSCORE );

    } else {
      // 16-bit encoded score
        
      thisScoreBits = encBits.substr( 0, 16 );
      encBits = encBits.substr( 16, encBits.length - 16 );
        
      sysLogger.addLine( "legacy 16-bit encoded score "
                         + thisScoreBits,
                         LOGGER_SOURCE_HIGHSCORE );
        
    }
    
    hLevelScore = parseInt( thisScoreBits, 2 );

    sysLogger.addLine( "decoded score "
                       + hLevelScore,
                       LOGGER_SOURCE_HIGHSCORE );
    
    if ( encBits.length > 0 ) {
     
      if ( parseInt( encBits, 2 ) > 0 ) {
        
        return sysLogError( "ERROR unexpected trailing payload '"
                            + encBits + "'" );
        
      }
      
    }
    
    // (4) done!
    
    hEncodedString = inString;

  }

  this.read = function() {
    // read encoded string from storage

    sysLogger.addLine( "load highscore data from storage for level " + levelNumber,
                       LOGGER_SOURCE_HIGHSCORE );

    var inString = null;

    try {

      inString = window.localStorage.getItem( "l" + levelNumber );

    } catch (err) {

      var thisGameError = new GameError( err );
      sysLogger.addLine( "Error: " + thisGameError.getString(), LOGGER_SOURCE_HIGHSCORE );

    }
    
    if ( inString == null ) {

      sysLogger.addLine( "data not found, clear object data", LOGGER_SOURCE_HIGHSCORE );

      clearObjectData();
      return false;

    }

    // and update
    sysLogger.addLine( "read '" + inString + "'", LOGGER_SOURCE_HIGHSCORE );
    this.setEncodedString( inString );

    // return success (if any)
    if ( hEncodedString ) return true; else return false;
    
  }

  this.write = function() {
    // write encoded string to storage

    sysLogger.addLine( "write highscore data to storage for level " + levelNumber,
                       LOGGER_SOURCE_HIGHSCORE );

    if ( hEncodedString == null ) hEncodedString = this.getEncodedString();

    if ( hEncodedString == null ) {

      sysLogger.addLine( "could not encode; abort write", LOGGER_SOURCE_HIGHSCORE );
      return false;

    }

    sysLogger.addLine( "writing " + hEncodedString, LOGGER_SOURCE_HIGHSCORE );

    try {

      window.localStorage.setItem( "l" + levelNumber, hEncodedString );

    } catch (err) {

      var thisGameError = new GameError(err);
      sysLogger.addLine( "Error: " + thisGameError.getString(), LOGGER_SOURCE_HIGHSCORE );

      return false;

    }

    // success
    return true;

  }

  /*
   *
   */

  function clearObjectData() {

    hEncodedString = null;
    hGrid          = null;
    hTilesPlaced   = null;
    hLevelScore    = null;

  }

  function sysLogError( inError ) {
   
    sysLogger.addLine( inError, LOGGER_SOURCE_HIGHSCORE );
    clearObjectData();
    
    return null;
    
  }

}
