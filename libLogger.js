/*
 * libLogger.js 2012-07-21
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Logger(int queue)
 *
 * Logger.addLine(String logText, int sourceIndicator)
 * Logger.clear()
 * Logger.getColor() -- returns String
 * Logger.getLines() -- returns String
 * Logger.setColor( String color )
 *
 */

var LOGGER_QUEUE_SYSTEM    = 0;
var LOGGER_QUEUE_STRING    = 1;
var LOGGER_QUEUE_DIV       = 2;
var LOGGER_QUEUE_VOID      = 3;
var LOGGER_QUEUE_DIVROLL   = 4;

var LOGGER_SOURCE_DEFAULT   = 0;
var LOGGER_SOURCE_SOUND     = 1;
var LOGGER_SOURCE_TOUCH     = 2;
var LOGGER_SOURCE_SEQUENCER = 3;
var LOGGER_SOURCE_LEVEL     = 4;
var LOGGER_SOURCE_NUMBER    = 5;
var LOGGER_SOURCE_HIGHSCORE = 6;
var LOGGER_SOURCE_GAME      = 7;
var LOGGER_SOURCE_SCORING   = 8;

var LOGGER_COLOR            = "white";

var LOGGER_DIV_ID           = "divLogger";
var LOGGER_DIV_FONTSIZE     = "0";
var LOGGER_DIV_XOFFSET      = 3;
var LOGGER_DIV_YOFFSET      = 160;

var LOGGER_DIVROLL_ID       = "divLoggerRoll";
var LOGGER_DIVROLL_COUNT    = 5;
var LOGGER_DIVROLL_YSTEP    = 32;
var LOGGER_DIVROLL_FONTSIZE = "0";
var LOGGER_DIVROLL_XOFFSET  = 3;
var LOGGER_DIVROLL_YOFFSET  = 3;

/*
 * 
 */

function Logger( loggerQueueCons ) {
  // constructor
  
  var loggerQueue     = loggerQueueCons;
  var loggerStringAll = "";
  var lColor          = LOGGER_COLOR;
  var isInitialized   = false;

  this.getColor = function()         { return lColor;   }
  this.setColor = function( cValue ) { lColor = cValue; }  

  this.addLine = function( logText, sourceIndicator ) {
    // adds the line to the log    

    if ( !( sourceIndicator ) ) sourceIndicator = LOGGER_SOURCE_DEFAULT;

    if ( loggerQueue == LOGGER_QUEUE_VOID ) return;    

    var thisTime  = (new Date()).getTime();
    var logString = "[" + sourceIndicator + "] " + thisTime + " " + logText;
    
    switch (loggerQueue) {
    
      case LOGGER_QUEUE_SYSTEM:
        
        if ( console ) {
          // log to firebug console
          
          console.log( logString );
          
        }
        
        break;
        
      case LOGGER_QUEUE_STRING:
        
        loggerStringAll += logString + "\n";
        
        break;
        
      case LOGGER_QUEUE_DIV:
        
        var dLogger = document.getElementById( LOGGER_DIV_ID );
        if ( !(  dLogger ) ) return;

        if( !( isInitialized ) ) {
          // first time the DIV node was successfully obtained; set style

            dLogger.style.position = "absolute";
            dLogger.style.display  = "block";   
            dLogger.style.left     = "" + LOGGER_DIV_XOFFSET + "px";
            dLogger.style.top      = "" + LOGGER_DIV_YOFFSET + "px";

            isInitialized = true;

        }

        dLogger.innerHTML
            = "<font color='" + lColor
              + "' size='" + LOGGER_DIV_FONTSIZE + "'>"
              + logString + "</font><br />";
        
        break;
        
      case LOGGER_QUEUE_DIVROLL:

        var divLoggers = new Array();

        for ( var i = 1; i <= LOGGER_DIVROLL_COUNT; i++ ) {

          divLoggers[ i ] = document.getElementById( LOGGER_DIVROLL_ID + i );
          if ( !( divLoggers[ i ] ) ) return;

          if( !( isInitialized ) ) {
            // first time the DIV nodes were successfully obtained; set style

            divLoggers[ i ].style.position = "absolute";
            divLoggers[ i ].style.display  = "block";   
            divLoggers[ i ].style.left     = "" + LOGGER_DIVROLL_XOFFSET + "px";
            divLoggers[ i ].style.top      = "" + ( LOGGER_DIVROLL_YOFFSET
                                              + ( LOGGER_DIVROLL_COUNT - i ) * LOGGER_DIVROLL_YSTEP )
                                              + "px";
          }

        }

        isInitialized = true;

        for ( var i = 5; i > 1; i-- ) {

          divLoggers[ i ].innerHTML = divLoggers[ (i - 1) ].innerHTML;

        }

        divLoggers[ 1 ].innerHTML
            = "<font color='" + lColor
              + "' size='" + LOGGER_DIVROLL_FONTSIZE + "'>"
              + logString + "</font><br />";

      default:
        // do nothing
        
    }
    
  }
  
  this.getLines = function() {
    // returns all lines (if in LOGGER_QUEUE_STRING mode)
    
    return loggerStringAll;
    
  }
  
  this.clear = function() {
    // clears all lines (if in LOGGER_QUEUE_STRING mode)
    
    loggerStringAll = "";
    
  }
  
}
