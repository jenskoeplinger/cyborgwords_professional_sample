/*
 * libTouch.js 2012-08-18
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Touch( Screen screen ) 
 * 
 * Touch.getIsTouching() -- returns boolean
 * Touch.getPickedUpSprite() -- returns Sprite
 * Touch.getScreen() -- returns Screen
 * Touch.getTouchOccurred() -- returns boolean
 * Touch.getTouchX() -- return int virtualX
 * Touch.getTouchY() -- return int virtualY
 * Touch.setPickedUpSprite( [ Sprite sprite ] )
 * Touch.setIsTouching(boolean isTouching)
 * Touch.setScreen( Screen screen )
 * Touch.setTouchOccurred(boolean touchOccurred)
 * Touch.setTouchNativeX(int touchNativeX)
 * Touch.setTouchNativeY(int touchNativeY)
 * 
 */

// Global reference to touch object, to be reached
// by the event listeners which must be global due
// to platform limitations:
var eventListenerTouch = null;
// (don't refer to it directly; use Touch class).
// Only one touch object is currently supported.

var TOUCH_DEBUG = false; // insanity logging; requires sysLogger object

function Touch( inScreen ) {
  // constructor:
  // - set private variables
  // - register even listeners
 
  eventListenerTouch = this; // make Touch object reachable by event listeners
  
  var isTouching     = false;
  var touchOccurred  = false;
  var touchX         = 0;
  var touchY         = 0;
  var vScreen        = inScreen;
  var pickedUpSprite = null;
  
  if ( isWebBrowser ) {
    // register mouse clicks

    document.addEventListener("mousedown", eventTouchstart, false);
    document.addEventListener("mousemove", eventTouchmove,  false);
    document.addEventListener("mouseup",   eventTouchend,   false);

  } else {
    // register touches
  
    document.addEventListener("touchstart", eventTouchstart, false);
    document.addEventListener("touchmove",  eventTouchmove,  false);
    document.addEventListener("touchend",   eventTouchend,   false);
        
  }
  
  if ( TOUCH_DEBUG ) { sysLogger.addLine( "touch constructed", LOGGER_SOURCE_TOUCH ); }
  
  // methods:
  
  this.getIsTouching      = function()         { return isTouching;       }
  this.setIsTouching      = function( cValue ) { isTouching = cValue;     }
  this.getPickedUpSprite  = function()         { return pickedUpSprite;   }
  this.setPickedUpSprite  = function( cValue ) { pickedUpSprite = cValue; }
  this.getScreen          = function()         { return vScreen;          }
  this.setScreen          = function( cValue ) { vScreen = cValue;        }
  this.getTouchOccurred   = function()         { return touchOccurred;    }
  this.setTouchOccurred   = function( cValue ) { touchOccurred = cValue;  }
  this.setTouchNativeX    = function( cValue ) { touchX = cValue;         }
  this.setTouchNativeY    = function( cValue ) { touchY = cValue;         }

  this.getTouchX = function() {
    // returns virtualX if screen is set, otherwise nativeX

    if ( vScreen ) {

      var tX = ( touchX - vScreen.getNativeXOffset() ) / vScreen.getXScale();
      return Math.floor( tX + 0.5 );

    } else {

      return touchX;
   
    }

  }

  this.getTouchY = function() {
    // returns virtualY if screen is set, otherwise nativeY

    if ( vScreen ) {

      var tY = ( touchY - vScreen.getNativeYOffset() ) / vScreen.getYScale();
      return Math.floor( tY + 0.5 );

    } else {

      return touchY;
   
    }

  }

}

/*
 * 
 */

function eventTouchstart( eventTouch ) {
  // capture the ontouchstart event
  
  if (eventTouch.pageX != undefined) {
    // browser supports eventTouch.pageX
                                      
    if ((eventTouch.pageX > 0) && (eventTouch.pageY > 0)) {
      // pageX and pageY have reasonable parameters
      
      eventListenerTouch.setTouchNativeX( eventTouch.pageX );
      eventListenerTouch.setTouchNativeY( eventTouch.pageY );
                                                
    } else {
      // try eventTouch.targetTouches[0].clientX / Y instead
      
      if ( eventTouch.targetTouches === undefined ) {
        // moved outside the screen bounds (web browser only)

        if ( TOUCH_DEBUG ) { sysLogger.addLine( "off screen", LOGGER_SOURCE_TOUCH ); }

        return;

      }

      eventListenerTouch.setTouchNativeX( eventTouch.targetTouches[0].clientX );
      eventListenerTouch.setTouchNativeY( eventTouch.targetTouches[0].clientY );
                                                 
    }
                                  
  } else {
    // browser (hopefully?) supports eventTouch.clientX / Y

    eventListenerTouch.setTouchNativeX( eventTouch.clientX );
    eventListenerTouch.setTouchNativeY( eventTouch.clientY );

  }
  
  eventListenerTouch.setIsTouching( true );
  eventListenerTouch.setTouchOccurred( true );

  eventTouch.preventDefault();

  if ( TOUCH_DEBUG ) {
    // insanity logging is on
    
    sysLogger.addLine(
      "touch "
      + eventListenerTouch.getTouchX()
      + ","
      + eventListenerTouch.getTouchY(),
      LOGGER_SOURCE_TOUCH
    );
    
  }
  
}

function eventTouchmove( eventTouch ) {
  // capture the ontouchmove event and defer to touchstart

  if ( eventListenerTouch.getIsTouching() ) {
    // currently touching; capture move

    eventTouchstart( eventTouch );
    
  }
  
}

function eventTouchend( eventTouch ) {
  // capture the ontouchend event

  eventListenerTouch.setIsTouching( false );
  
  eventTouch.preventDefault();

  if ( TOUCH_DEBUG ) { sysLogger.addLine( "touch end", LOGGER_SOURCE_TOUCH ); }

}
