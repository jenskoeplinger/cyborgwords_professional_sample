/*
 * gameNew.js 2012-07-29
 * Copyright (C) 2012 Jens Koeplinger
 */

function gameNew( tGame ) {
  // first time the sequencer runs; at this point:
  // - equipment is ready,
  // - sysLogger is ready,
  // - Game tGame is instantiated.
  // Now:
  // - instantiate Screen vScreen,
  // - instantiate Touch touch,
  // - draw "loading" sprite into ID 50 (in the middle),
  // - remove all other sprites (if any).

  sysLogger.addLine( "gameNew()", LOGGER_SOURCE_SEQUENCER );

  switch ( tGame.getSubState() ) {

    case 0: // first time ever run; instantiate global objects and listeners

      vScreen = new Screen( 490, 735, SCREEN_MORPH_MAX );
      touch = new Touch( vScreen );

      tGame.setSubState( 1 );

      break;

    case 1: // set the splash screen for loading

      var sprite = new Sprite( FILE_SPLASH_LOADING, 490, 735 );
      sprite.setImgNode( document.getElementById( "bitmap50" ) );
      vScreen.putSprite( sprite, 50 );
      sprite.move( 0, 0 );
      sprite.show();

      tGame.setSubState( 2 );

      break;

    case 2: // clear all other sprites (if any); add letterbox blackout (if any)

      for (i = 0; i < MAX_SPRITES; i++) {

        if ( !( i == 50 ) ) vScreen.removeSprite( i );        

      }

      if ( !( vScreen.getNativeYOffset() == 0 ) ) {
        // letterboxed with top and bottom black bars
        
        var sprite = new Sprite( FILE_SPLASH_BLACK, 490, 735 );
        sprite.setImgNode( document.getElementById( "bitmapBox1" ) );
        vScreen.putSprite( sprite, ID_LETTERBOX_1 );
        sprite.move( 0, -735 );
        sprite.show();
        
        var sprite = new Sprite( FILE_SPLASH_BLACK, 490, 735 );
        sprite.setImgNode( document.getElementById( "bitmapBox2" ) );
        vScreen.putSprite( sprite, ID_LETTERBOX_2 );
        sprite.move( 0, 735 );
        sprite.show();
        
      }
      
      if ( !( vScreen.getNativeXOffset() == 0 ) ) {
        // letterboxed with left and right black bars
        
        var sprite = new Sprite( FILE_SPLASH_BLACK, 490, 735 );
        sprite.setImgNode( document.getElementById( "bitmapBox1" ) );
        vScreen.putSprite( sprite, ID_LETTERBOX_1 );
        sprite.move( -490, 0 );
        sprite.show();
        
        var sprite = new Sprite( FILE_SPLASH_BLACK, 490, 735 );
        sprite.setImgNode( document.getElementById( "bitmapBox2" ) );
        vScreen.putSprite( sprite, ID_LETTERBOX_2 );
        sprite.move( 490, 0 );
        sprite.show();
        
      }
      
      tGame.setState( GAME_INITIALIZING );
      tGame.setSubState( 0 );

  }

}
