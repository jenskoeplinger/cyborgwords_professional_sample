/*
 * libNumber.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Number(int spriteIDstart [, int maxDigits, int xPos, int yPos, int numberWidth,
 *            int numberHeight, string numberColor ] )
 * 
 * Number.addValue(int valueToAdd)
 * Number.getSprites() -- returns Sprite[]
 * Number.getValue() -- returns int
 * Number.getXPos() -- returns int
 * Number.getYPos() -- returns int
 * Number.hide()
 * Number.setValue(int numberValue)
 * Number.setXPos(int newXPos)
 * Number.setYPos(int newYPos)
 * Number.show()
 *
 */

function Number( spriteIdstart, maxDigits, xPos, yPos, numberWidth, numberHeight, numberColor ) {
  // constructor
  
  sysLogger.addLine( "new Number()", LOGGER_SOURCE_NUMBER );

  var nSpriteIdStart = spriteIdstart;
  var nMaxDigits     = maxDigits;
  var nXPos          = xPos;
  var nYPos          = yPos;
  var nWidth         = numberWidth;
  var nHeight        = numberHeight;
  var nNumberColor   = numberColor;
  var nValue         = 0;
  var nSprites       = new Array();
  
  if ( !( nMaxDigits ) )   nMaxDigits   = 1;
  if ( !( nXPos ) )        nXPos        = 0;
  if ( !( nYPos ) )        nYPos        = 0;
  if ( !( nWidth ) )       nWidth       = 50;
  if ( !( nHeight ) )      nHeight      = 70;
  if ( !( nNumberColor ) ) nNumberColor = "Black";
  
  // get bitmaps assigned and ready to draw

  var spriteCnt = 0;
  
  for ( var i = nSpriteIdStart; i < ( nSpriteIdStart + nMaxDigits ); i++ ) {
    
    var sprite = new Sprite(
                         FILE_IMAGE_NUMBER_PREFIX + nNumberColor + "0" + FILE_IMAGE_NUMBER_SUFFIX,
                         nWidth,
                         nHeight );

    sprite.setImgNode(
        document.getElementById( "bitmap" + i ) );
    vScreen.putSprite( sprite, i );
    sprite.hide();
    sprite.move( xPos + spriteCnt * nWidth, yPos );
    
    nSprites[ spriteCnt ] = sprite;
    spriteCnt++;
    
  }

  // methods

  this.getSprites = function()         { return nSprites;  }
  this.getValue   = function()         { return nValue;    }
  this.getXPos    = function()         { return nXPos;     }
  this.getYPos    = function()         { return nYPos;     }
  this.setValue   = function( cValue ) { nValue = cValue;  }
  this.setXPos    = function( cValue ) { nXPos = cValue;   }
  this.setYPos    = function( cValue ) { nYPos = cValue;   }

  this.addValue = function( cValue ) {
    
    nValue += cValue;
    this.show();
    
  }
  
  this.hide = function() {
   
    for ( thisSprite in nSprites ) nSprites[ thisSprite ].hide();
    
  }
  
  this.show = function() {
    // goes through the digits in the number and shows it
    
    sysLogger.addLine( "Number.show(" + nValue + ")", LOGGER_SOURCE_NUMBER );
    
    var tempNumber  = Math.floor( nValue + 0.5 );
    var remainder   = -1;
    var showArray   = new Array();
    showArray[ 0 ]  = 0;
    var showCounter = 0;
    
    while ( ( showCounter < nMaxDigits ) && ( tempNumber > 0 ) ) {
      
      remainder                = tempNumber % 10;
      showArray[ showCounter ] = "" + remainder;
      tempNumber               = Math.floor( 0.01 + tempNumber / 10. );
      
      showCounter++;
      
    }
    
    // adjust showCounter (except for when it's zero, then show "0")
    if ( showCounter > 0 ) showCounter--;

    var nFileName = null;
    
    for ( var i = 0; i < nMaxDigits; i++ ) {
     
      if ( i <= showCounter ) {
      
        nFileName = FILE_IMAGE_NUMBER_PREFIX
                        + nNumberColor
                        + showArray[ i ]
                        + FILE_IMAGE_NUMBER_SUFFIX;

        nSprites[ showCounter - i ].show( nFileName );
        
      } else {
        
        nSprites[ i ].hide();
        
      }
      
    }

  }
  
}
