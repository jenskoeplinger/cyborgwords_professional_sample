/*
 * gamePlaying.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 */

var gamePlayingSequence          = null;
var gameAdvanceConveyor          = 0;

var gameMediaUserPreference      = null;

var GAME_PAUSETHINK_NUM_FREE     = 1;
var GAME_PAUSETHINK_NUM_ADDICT   = 3;
var GAME_RECYCLE_NUM_FREE        = 1;
var GAME_RECYCLE_NUM_ADDICT      = 3;
var GAME_SHOWNEXTFIVE_NUM_FREE   = 0;
var GAME_SHOWNEXTFIVE_NUM_ADDICT = 1;

var gamePausethinkNum            = null;
var gameShowPausethinkOnNextDrop = null;
var gamePausethinkIsShowing      = null;

var gameRecycleNum               = null;
var gameRecycleIsShowing         = null;
var gameRecycleLetterTile        = null;

var gameShowNextFiveNum          = null;
var gameShowNextFiveIsShowing    = null;
var gameShowNextFiveSequence     = null;
var gameShowNextFiveLetterCnt    = null;

var gameEndCountdown             = null;
var gameEndCountdownNumber       = null;

var touchHasReleased             = null;

var movingScoreNumber            = null; // Number object for positive score
var movingScoreSteps             = null; // steps of score fly-out animation
var movingLossNumber             = null; // Number object for negative score
var movingLossSteps              = null;

var pickupS                      = null; // sprite reference to currently picked up
var pickupTile                   = null;
var pickupGridType               = null;
var pickupSequence               = null;
var touchReleasedSteps           = null; // number of cycles to let released letter fly back

var firstFiveLettersDropped      = null; // false until the first five letters are on the belt
var firstLetterPickedUp          = null; // false until the first letter tile is picked up
var holdConveyorAndWiggle        = null; // false during normal play; otherwise true
var wiggleTheLettersCounter      = null;

var blankPlacedTempTilePosArray  = null; // temporary array and pbject to preserve state during
var blankPlacedTempPickupS       = null; // alphabet letter selection when placing blank tile
var blankPlacedFlySequence       = null;
var blankPlacedSteps             = null;
var blankPlacedSelectedTile      = null;

var wiggleMovableGridSequence    = 0;
var finishSequence               = null;

var exitDecision                 = null;
var helpMinTapTimer              = null;

var BELTTIMER_RAISE_LID    =  0;
var BELTTIMER_MOVE         =  5;
var BELTTIMER_LOWER_LID    = 24;
var BELTTIMER_WIGGLING     =  6;
var BELTTIMER_CHECKSHOW5   = 15;

// special situation awards
var awardsSteps            = null;

var AWARD_CRISSCROSS_PLUS  =  30;
var AWARD_CRISSCROSS_MULT  =   1;
var AWARD_CRISSCROSS_YPOS  = 625;

var GAME_COUNTDOWN_NUMBER_X      = 210;
var GAME_COUNTDOWN_NUMBER_Y      =  10;
var GAME_COUNTDOWN_NUMBER_WIDTH  =  70;
var GAME_COUNTDOWN_NUMBER_HEIGHT = 110;
var GAME_COUNTDOWN_NUMBER_COLOR  = NUMBER_COLORS_MAP[ "gameCountdown" ];

var GAME_ENDOFLEVEL_COUNTDOWN           = 120; // 6s countdown
var GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES =  20;

var GAME_PAUSETHINK_XPOS          = 300;
var GAME_PAUSETHINK_YPOS          = 115;
var GAME_PAUSETHINK_WIDTH         =  60;
var GAME_PAUSETHINK_HEIGHT        =  60;
var GAME_PAUSETHINK_SEQUENCECOUNT =  50;

var GAME_RECYCLE_XPOS             = 390;
var GAME_RECYCLE_YPOS             = 105;
var GAME_RECYCLE_WIDTH            =  60;
var GAME_RECYCLE_HEIGHT           =  60;
var GAME_RECYCLE_SEQUENCECOUNT    =  50;

var GAME_SHOWNEXTFIVE_XPOS          =  15;
var GAME_SHOWNEXTFIVE_YPOS          = 665;
var GAME_SHOWNEXTFIVE_WIDTH         = 239;
var GAME_SHOWNEXTFIVE_HEIGHT        =  65;
var GAME_SHOWNEXTFIVE_SEQUENCECOUNT =  30;

var GAME_MOVINGSCORE_WIDTH        = 65;
var GAME_MOVINGSCORE_COLOR        = NUMBER_COLORS_MAP[ "gameScoreFlyout" ];

var GAME_NEXT5LETTERSARE_YPOS     = 240;
var GAME_NEXT5LETTERSARE_ROW2     = 350;
var GAME_NEXT5OK_XPOS             = 197;
var GAME_NEXT5OK_YPOS             = 500;
var GAME_NEXT5OK_WIDTH            = 95;
var GAME_NEXT5OK_HEIGHT           = 95;

function gamePlaying( tGame ) {

  switch ( tGame.getSubState() ) {

    case 0: // initialize

      sysLogger.addLine( "gamePlaying()",
          LOGGER_SOURCE_SEQUENCER );
      sysLogger.addLine( "playing level: " + tGame.getLevelNumber(),
          LOGGER_SOURCE_SEQUENCER );

      if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE ) {

        movingScoreSteps   =  8;
        movingLossSteps    =  8;
        touchReleasedSteps =  4;
        blankPlacedSteps   =  4;
        awardsSteps        = 18;

      } else {

        movingScoreSteps   = 14;
        movingLossSteps    = 14;
        touchReleasedSteps =  7;
        blankPlacedSteps   =  7;
        awardsSteps        = 33;

      }

      gamePlayingSequence    = 0;
      gameAdvanceConveyor    = 0;
      gameEndCountdown       = null;
      blankPlacedFlySequence = 0;
      touchHasReleased       = true;

      BELTTIMER_LOWER_LID = BELTTIMER_MOVE
                            + 8
                            + Math.floor( level.getAdvanceTimer() / 4 );

      firstFiveLettersDropped = false;
      firstLetterPickedUp     = false;
      holdConveyorAndWiggle   = false;
      
      // calculate perks per level based on game type (free or purchased)
      
      switch ( cyborgWordsVersion ) {
        
        case VERSION_FREE:
          
          gamePausethinkNum   = GAME_PAUSETHINK_NUM_FREE;
          gameRecycleNum      = GAME_RECYCLE_NUM_FREE;
          gameShowNextFiveNum = GAME_SHOWNEXTFIVE_NUM_FREE;
         
          break;
          
        case VERSION_ADDICT:
        
          gamePausethinkNum   = GAME_PAUSETHINK_NUM_ADDICT;
          gameRecycleNum      = GAME_RECYCLE_NUM_ADDICT;
          gameShowNextFiveNum = GAME_SHOWNEXTFIVE_NUM_ADDICT;

          break;
          
      }
      
      scoreEngine.resetWordsFound();
      
      hideRecycleLetterTile();
      hidePauseAndThink();
      hideShowNextFive();
      gameShowPausethinkOnNextDrop = false;
      
      if ( gamePausethinkNum > 0 ) gameShowPausethinkOnNextDrop = true;
      
      tGame.playMediaSoundtrack( FILE_SOUND_STROLL );
      
      tGame.setSubState( 10 );

      break;

    case 10: // The GAME is ON baby! -- main loop:

      // ------------------
      // belt advance logic

      if ( gameAdvanceConveyor >= level.getAdvanceTimer() ) gameAdvanceConveyor = 0;

      switch ( gameAdvanceConveyor ) {

      case BELTTIMER_RAISE_LID:

        if ( !( tGame.getConveyorTile( 4 ) == LEVEL_TILE_NONE ) )
            gameRaiseLid( tGame );
        
        break;

      case BELTTIMER_MOVE:

        sysLogger.addLine( "cycle "
                           + gamePlayingSequence
                           + " move conveyor",
                           LOGGER_SOURCE_SEQUENCER );

        gameMoveConveyor( tGame );
        gameInjectNewLetter( tGame, level.getNextTile() );
        
        if ( !( tGame.getConveyorTile( 4 ) == LEVEL_TILE_NONE )
             && !( firstFiveLettersDropped ) ) {
          
          firstFiveLettersDropped = true;
          
        }
        
        if ( !( tGame.getConveyorTile( 5 ) == LEVEL_TILE_NONE ) )
            hideRecycleLetterTile();
        
        break;

      case BELTTIMER_LOWER_LID:

        if ( gameLidIsOpen ) {

          if ( pickupS ) {
            // if there's a letter currently being picked up then
            // remove from belt and assign some grid position as
            // touch-release position

            var convID5 = tGame.getConveyorPosImg( 5 );

            if ( convID5 == pickupS.getId() ) {
              // don't force picked-up letter into the trash bin;
              // assign to ID_LTR_PICKUPEXT and pick it up right away

              transferPickupToSpare( convID5 );
              pickUpSprite( tGame, 70, 70 );
              tGame.setConveyorTile( 5, LEVEL_TILE_NONE );

            }

          }

          gameLowerLid( tGame );

        }
        
        // check whether to hold the belt after the first 5 letters
                
        if ( !( firstLetterPickedUp )
             && firstFiveLettersDropped ) {
          // switch to wiggle mode until the first one is picked up
          
          sysLogger.addLine( "start wiggling due to first 5 letters on belt",
                             LOGGER_SOURCE_SEQUENCER );

          startWiggling();
          
        }
        
        // see if the last letter just dropped into the trash bin
        checkConveyorIsEmpty();
        
        // show recycle icon if applicable
        if ( !( gameEndCountdown )
             && !( gameRecycleIsShowing )
             && !( gameRecycleLetterTile == LEVEL_TILE_NONE )
             && ( gameRecycleNum > 0 ) ) {
          
          showRecycleLetterTile();
          
        }
        
        break;
        
      case BELTTIMER_CHECKSHOW5:
          
        if (    !( gameShowNextFiveIsShowing )
             && !( tGame.getConveyorTile( 3 ) == LEVEL_TILE_NONE )
             && ( gameShowNextFiveNum > 0 ) ) {
          
          showShowNextFive();
          
        }
        
        if ( level.getFutureTile( 0 ) == LEVEL_TILE_NONE ) {
          // last tile was placed on conveyor
          
          hideShowNextFive();
          gameShowNextFiveNum = 0;
          
        }

        break;

      }

      // ------------------
      // check for end-of-level countdown
      
      switch ( gameEndCountdown ) {
       
        case null:
          break;
          
        case ( GAME_ENDOFLEVEL_COUNTDOWN - 1 ):
          
          flyOutBeltComponent( ID_TRASHBINLID );
          break;
        
        case ( GAME_ENDOFLEVEL_COUNTDOWN - 2 ):
          
          flyOutBeltComponent( ID_LETTERSHOOT );
          break;
        
        case ( GAME_ENDOFLEVEL_COUNTDOWN - 3 ):
          
          flyOutBeltComponent( ID_CONVEYORBELT );
          flyOutBeltComponent( ID_CONVEYORMAIN );
          break;

        case ( GAME_ENDOFLEVEL_COUNTDOWN - 4 ):
          
          flyOutBeltComponent( ID_TRASHBIN   );
          flyOutBeltComponent( ID_PAUSETHINK );
          flyOutBeltComponent( ID_RECYCLE    );
          break;
          
        case ( GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 5 ):
          // create countdown number object
          
          gameEndCountdownNumber = new Number( ID_NUM_COUNTDOWN,
                                               1,
                                               GAME_COUNTDOWN_NUMBER_X,
                                               GAME_COUNTDOWN_NUMBER_Y,
                                               GAME_COUNTDOWN_NUMBER_WIDTH,
                                               GAME_COUNTDOWN_NUMBER_HEIGHT,
                                               GAME_COUNTDOWN_NUMBER_COLOR );
          
          // (no break)
          
        case ( GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 4 ):
        case ( GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 3 ):
        case ( GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 2 ):
        case ( GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 1 ):

          gameEndCountdownNumber.setValue(
              gameEndCountdown / GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES );
          
          gameEndCountdownNumber.show();
          
          tGame.playMediaEffect( FILE_SOUND_COUNTDOWN );
          
          break;
        
        case ( 5 + GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 4 ):
        case ( 5 + GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 3 ):
        case ( 5 + GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 2 ):
        case ( 5 + GAME_ENDOFLEVEL_COUNTDOWN_321CYCLES * 1 ):
        case ( 5 ):
          
          gameEndCountdownNumber.hide();
          break;
        
        default:
          break;
        
      }

      // ------------------
      // check logic for screen is touched

      if ( touch.getIsTouching() ) {

        pickupS = touch.getPickedUpSprite();
    
        if ( pickupS ) {
          // user is currently holding a sprite; update position
      
          pickupS.move( touch.getTouchX() - pickupS.getWidth() / 2,
                        touch.getTouchY() - pickupS.getHeight() / 2,
                        true );
      
        } else if ( touchHasReleased ) {
          // user has just started to touch the screen

          var tX = touch.getTouchX();
          var tY = touch.getTouchY();

          // check: pick up letter from belt?
          if ( tY < ( CONVEYOR_CONVEYOR_YPOS + 10 ) ) {

            var lPos = Math.floor ( ( tX - CONVEYOR_LETTER0_XPOS ) / 70 );
            if ( lPos < 0 ) lPos = 0;
            if ( lPos > 5 ) lPos = 5;

            var canPickUp = !( tGame.getConveyorTile( lPos ) == LEVEL_TILE_NONE );
            var canPickUpNext = false;
            
            if ( lPos < 5 ) {
              
              canPickUpNext = !( tGame.getConveyorTile( lPos + 1 ) == LEVEL_TILE_NONE );
              
            }
            
            if ( canPickUp ) {
              // playable tile is on conveyor; pick it up

              pickupS = vScreen.getSpriteById( tGame.getConveyorPosImg( lPos ) );

              if ( pickupS ) {            

                if ( ( lPos == 5 ) && ( pickupS.getYposTo() == CONVEYOR_TRASH_YPOS ) ) {
                  // tile is already sliding down into the trash bin; too late!

                  pickupS = null;

                } else {

                  if ( pickupS.getIsMoving() && canPickUpNext && ( !( holdConveyorAndWiggle ) ) ) {
                    // pickupS has just started moving into this position; probably user
                    // wanted to pick up the one that was just there before
                    
                    var nextPickupS = vScreen.getSpriteById ( tGame.getConveyorPosImg( lPos + 1 ) );
                    
                    if ( nextPickupS ) {
                      
                      pickupS = nextPickupS;
                      lPos++;
                      
                    }
                    
                  }
                  
                  pickUpSprite( tGame, 70, 70 ); // IN: pickupS

                  pickupTile     = tGame.getConveyorTile( lPos );
                  pickupGridType = null;
                  
                  tGame.playMediaEffect( FILE_SOUND_PICKUP );
                  
                  // picking up a letter always ends conveyor wiggling (if any)
                  stopWigglingIfAny();

                }

              }

            }

          }

          var tilePosArray = checkCanPickupTile( tX, tY );

          // check: pick up letter from grid, or movable grid;
          // check: touch objects in row above or below grid

          switch ( tilePosArray[ 0 ] ) {

            case 1: // letter tile

              var tileId = ID_LTR_START
                           + tilePosArray[ 1 ]
                           + tilePosArray[ 2 ] * LEVEL_GRID_MAX_X;

              pickupS = vScreen.getSpriteById ( tileId );

              if ( pickupS ) {

                pickUpSprite( tGame, 70, 70, tilePosArray[ 4 ] );

                pickupTile     = tilePosArray[ 3 ];
                pickupGridType = null;
                
                tGame.playMediaEffect( FILE_SOUND_PICKUP );

              }
              
              break;

            case 2: // grid tile

              var tileId = ID_GRID_START
                           + tilePosArray[ 1 ]
                           + tilePosArray[ 2 ] * LEVEL_GRID_MAX_X;

              pickupS = vScreen.getSpriteById ( tileId );

              if ( pickupS ) {

                pickUpSprite( tGame, GAME_GRID_SIZE, GAME_GRID_SIZE, null );
                
                pickupTile     = null;
                pickupGridType = tilePosArray[ 3 ];
                
                tGame.playMediaEffect( FILE_SOUND_PICKUP );

              }
              
              break;
              
            case -1: // outside grid; check objects in row above or below grid

              if ( tilePosArray[ 2 ] >= LEVEL_GRID_MAX_Y ) {
                // check objects in row below grid
                
                if (    ( tX > GAME_EXIT_LEFT )
                     && ( tX < ( GAME_EXIT_LEFT + GAME_EXIT_WIDTH ) ) ) {
                  // exit button clicked
                  
                  tGame.playMediaEffect( FILE_SOUND_PICKUP );
                
                  tGame.setSubState( 20 );
                  
                }
                
                if (    ( tX > GAME_SOUND_LEFT )
                     && ( tX < ( GAME_SOUND_LEFT + GAME_SOUND_WIDTH ) ) ) {
                  // sound button clicked
                  
                  gameMediaUserPreference = tGame.toggleMediaPreference();
                  
                  var soundSprite = vScreen.getSpriteById( ID_SOUND );
                  soundSprite.show( mediaFileArray[ gameMediaUserPreference ] );
                  
                }
                
                if (    ( gameShowNextFiveIsShowing )
                     && ( tX > GAME_SHOWNEXTFIVE_XPOS )
                     && ( tX < ( GAME_SHOWNEXTFIVE_XPOS + GAME_SHOWNEXTFIVE_WIDTH ) ) ) {
                  
                  tGame.playMediaEffect( FILE_SOUND_PICKUP );
                
                  useShowNextFive();
                  
                }
                
              } else if ( tilePosArray[ 2 ] == -1 ) {
                // check object in row above grid
                
                if ( gamePausethinkIsShowing ) {
                                   
                  if (    ( tY > GAME_PAUSETHINK_YPOS )
                       && ( tY < ( GAME_PAUSETHINK_YPOS + GAME_PAUSETHINK_HEIGHT ) )
                       && ( tX > GAME_PAUSETHINK_XPOS )
                       && ( tX < ( GAME_PAUSETHINK_XPOS + GAME_PAUSETHINK_WIDTH ) ) ) {
                    
                    tGame.playMediaEffect( FILE_SOUND_PICKUP );
                  
                    usePauseAndThink();
                    
                  }
                  
                }
                  
                if ( gameRecycleIsShowing ) {
                                   
                  if (    ( tY > GAME_RECYCLE_YPOS )
                       && ( tY < ( GAME_RECYCLE_YPOS + GAME_RECYCLE_HEIGHT ) )
                       && ( tX > GAME_RECYCLE_XPOS )
                       && ( tX < ( GAME_RECYCLE_XPOS + GAME_RECYCLE_WIDTH ) ) ) {
                    
                    tGame.playMediaEffect( FILE_SOUND_PICKUP );
                  
                    useRecycleLetterTile();
                    
                  }
                  
                }
                
              }

              break;

            default:

          }
    
          touchHasReleased = false;
      
        }
    
      } else {
        // user is not touching
   
        touchHasReleased = true;
    
      }

      // ------------------
      // check logic for touch released

      if ( !( touch.getIsTouching() ) && ( touch.getTouchOccurred() ) ) {
        // released
    
        touch.setTouchOccurred( false );
    
        pickupS = touch.getPickedUpSprite();
    
        if ( pickupS ) {

          var toxPos = pickupS.getXposTo();
          var toyPos = pickupS.getYposTo();
          var pxPos  = Math.floor( pickupS.getXposPickedUp() + pickupS.getWidth()  / 4 );
          var pyPos  = Math.floor( pickupS.getYposPickedUp() + pickupS.getHeight() / 4 );
    
          pickupS.setIsPickedUp( false );
      
          pickupS.setWidth(  pickupS.getWidth()  / 2 );
          pickupS.setHeight( pickupS.getHeight() / 2 );
          pickupS.show();
          pickupS.move( pxPos, pyPos );

          // now find out what to do with it

          if ( !( pickupTile == null ) ) {
            // a letter tile was picked up

            var tilePosArray
                = checkCanPlaceTile( touch.getTouchX(), touch.getTouchY() );

            if ( tilePosArray[ 0 ] >= 0 ) {
              // it is possible to place the tile at the position
              // released; do so!

              if ( pickupTile == " " ) {
                // blank tile placed; set temporary array and object
                // to preserve state, show letter selection, and then
                // re-inject tile placement with selected tile (substates 11-13)

                blankPlacedTempTilePosArray = tilePosArray;
                blankPlacedTempPickupS      = pickupS;
                
                sysLogger.addLine( "blank tile placed; change substate to 11",
                                   LOGGER_SOURCE_SEQUENCER );

                pickupS.move( tilePosArray[ 2 ], tilePosArray[ 3 ] );

                tGame.setSubState( 11 );

              } else {
                // regular letter tile

                placeTileOnGrid( tGame, tilePosArray );
                
              }
              
              tGame.playMediaEffect( FILE_SOUND_DROP );
              
            } else if ( tilePosArray[ 0 ] == -2 ) {
              // letter tile was placed in the trash can; detach from game and destroy

              moveTileToTrash( tGame );

            } else {
              // the tile that was picked up can't be placed there; fly home

              var destroyOnEnd = ( pickupS.getId() == ID_LTR_PICKUPEXT );
     
              tGame.moveSprite(
                  pickupS,
                  toxPos,
                  toyPos,
                  touchReleasedSteps,
                  GAME_MOVESPRITE_SINE,
                  destroyOnEnd );

            }

          }

          // check whether movable grid tile was released

          if ( pickupGridType ) {
            // a grid tile was picked up

            var gridPosArray = checkCanPlaceGrid( touch.getTouchX(), touch.getTouchY() );

            if ( gridPosArray[ 0 ] >= 0 ) {
              // it is possible to place the grid tile

              placeGridTileOnGrid( tGame, gridPosArray );
              
              tGame.playMediaEffect( FILE_SOUND_DROP );

            } else {
              // placing the grid tile is not possible here; slide back

              tGame.moveSprite(
                  pickupS,
                  toxPos,
                  toyPos,
                  touchReleasedSteps,
                  GAME_MOVESPRITE_SINE );

            }

          }

          pickupS = null;

        }

        detachSpriteFromTouch();
        checkConveyorIsEmpty();

      }

      // ------------------
      // check logic for placed (not not yet locked) letter
      // (includes checking for whether a word exists)

      for ( var lX = 0; lX < LEVEL_GRID_MAX_X; lX++ ) {
       
        for ( var lY = 0; lY < LEVEL_GRID_MAX_Y; lY++ ) {
         
          if ( !( level.getTilePlacedXY( lX, lY ) == LEVEL_TILE_NONE )
               && !( level.getTileIsLocked( lX, lY ) ) ) {
            // a tile is placed but not yet part of a word

            var tileCyclesUntilRemove =
                  level.getTileDroppedSequence( lX, lY )
                  + ( level.getTileDroppedTimeout() * GAME_TILETIMEOUT_ADJUST )
                  - gamePlayingSequence;
                
            tileCyclesUntilRemove = Math.floor( tileCyclesUntilRemove );
                      
            if ( ( tileCyclesUntilRemove % GAME_CYCLES_CHECKFORWORD ) == 0 ) {
              // reset timer if not blinking yet
              
              if ( tileCyclesUntilRemove > 101 )
                  checkHoldConveyorTileCycleReset( lX, lY );

            }
            
            // force check for word right before any fly-out
            if ( tileCyclesUntilRemove == 1 ) checkForWord();

            switch ( tileCyclesUntilRemove ) {

              case  21:
              case  44:
              case  71:
              case  94:
                // show

                var tileFilename =
                    FILE_IMAGE_LTR_PREFIX
                    + LEVEL_TILES[ level.getTilePlacedXY( lX, lY ) ][ 1 ]
                    + FILE_IMAGE_LTR_SUFFIX;
                    
                var tileId =
                    ID_LTR_START
                    + lX
                    + LEVEL_GRID_MAX_X * lY;
                    
                sysLogger.addLine( "show letter "
                    + tileFilename
                    + ", id "
                    + tileId
                    + ", with "
                    + tileCyclesUntilRemove
                    + " cycles remaining untile removal",
                    LOGGER_SOURCE_SEQUENCER );
                    
                var tileSprite = vScreen.getSpriteById( tileId );
                tileSprite.show( tileFilename );
                
                // (no break)
                
              case   1:
              case 101:
                
                checkHoldConveyorTileCycleReset( lX, lY );
                break;
                
              case 26:
              case 49:
              case 76:
              case 99:
                // blink

                var tileFilename = FILE_IMAGE_LTR_BLINK;
                    
                var tileId =
                    ID_LTR_START
                    + lX
                    + LEVEL_GRID_MAX_X * lY;
                    
                sysLogger.addLine( "blink letter "
                    + tileFilename
                    + ", id "
                    + tileId
                    + ", with "
                    + tileCyclesUntilRemove
                    + " cycles remaining untile removal",
                    LOGGER_SOURCE_SEQUENCER );
                    
                var tileSprite = vScreen.getSpriteById( tileId );
                tileSprite.show( tileFilename );
                
                break;
                
              default:
              
            }

            if ( tileCyclesUntilRemove <= 0 ) {
              // time's up

              detachAndFlyOut( lX, lY );

            }
            
          }
          
        }
        
      }

      // ------------------
      // wiggle movable grid tiles and movable letters on the grid
      
      wiggleMovableGridSequence++;
      if ( wiggleMovableGridSequence >= ( LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) )
          wiggleMovableGridSequence = 0;
      
      checkWiggleMovableGridTile( wiggleMovableGridSequence );
      
      // ------------------
      // check for scorable words placed
      
      if ( ( gamePlayingSequence % GAME_CYCLES_CHECKFORWORD ) == 0 ) {
        
        checkForWord();
        
      }
      
      // ------------------
      // main game loop ends

      gamePlayingSequence++;

      if ( gameEndCountdown ) {

        gameAdvanceConveyor = -1;
        gameEndCountdown--;
        
        if ( gameEndCountdown < 1 ) tGame.setSubState( 90 ); // level over

      } else if ( holdConveyorAndWiggle ) {

        wiggleTheLetters();

      } else {

        gameAdvanceConveyor++;

      }

      break;

    case 11: // blank tile placement requested, fly in alphabet for user selection

      if ( blankPlacedFlySequence == 0 ) {
        // draw 50% opaque gray background

        sysLogger.addLine( "start alphabet selector fly-in",
                            LOGGER_SOURCE_SEQUENCER );

        var sprite = new Sprite( FILE_SPLASH_OPAQUE_50, 490, 735 );
        sprite.setImgNode( document.getElementById( "bitmap" + ID_BLANKGRAYOUT ) );
        vScreen.putSprite( sprite, ID_BLANKGRAYOUT );
        sprite.show();
        sprite.move( 0, 0 );
        
        blankPlacedFlySequence++;
        
      }
      
      if ( ( blankPlacedFlySequence > 0 ) && ( blankPlacedFlySequence < 27 ) ) {
        // fly in an alphabet
        
        var selectTile = String.fromCharCode( 64 + blankPlacedFlySequence );
        var selectTileFileName =
                FILE_IMAGE_LTR_PREFIX
                + LEVEL_TILES[ selectTile ][ 1 ]
                + FILE_IMAGE_LTR_SUFFIX;

        var selectTileId = blankPlacedFlySequence + ID_BLANKSELECT - 1;
        var selectTileXPos = ( ( blankPlacedFlySequence + 1 ) % 5 ) * 98 + 1;
        var selectTileYPos = Math.floor( ( blankPlacedFlySequence - 4 ) / 5 ) * 98
                             + GAME_GRID_OFFSET_Y;
        
        var sprite = new Sprite( selectTileFileName, 97, 97 );
        
        sprite.setImgNode( document.getElementById( "bitmap" + selectTileId ) );
        vScreen.putSprite( sprite, selectTileId );
        sprite.show();
        sprite.move( selectTileXPos , 740 );
        
        tGame.moveSprite(
            sprite,
            selectTileXPos,
            selectTileYPos,
            blankPlacedSteps,
            GAME_MOVESPRITE_SINE_FLYIN );
        
        blankPlacedFlySequence++;
        
      }
      
      if ( blankPlacedFlySequence >= 27 ) {
        // all alphabets are flying in; reset touch and wait

        sysLogger.addLine( "last fly-in initiated; wait for user selection",
                            LOGGER_SOURCE_SEQUENCER );

        blankPlacedFlySequence = 0;
        touch.setTouchOccurred( false );
        tGame.setSubState( 12 );
        
      }

      break;
      
    case 12: // wait for user selection from alphabet

      if ( ( touch.getIsTouching() == false ) && ( touch.getTouchOccurred() ) ) {
        // player has made selection
        
        var touchColumn = Math.floor( touch.getTouchX() / 98 );
        var touchRow    = Math.floor( ( touch.getTouchY() - GAME_GRID_OFFSET_Y ) / 98 ) + 1;
        var touchLetterNum = ( touchColumn - 2 ) + ( touchRow * 5 );

        sysLogger.addLine( "touch recorded at "
                           + touch.getTouchX()
                           + ", "
                           + touch.getTouchY()
                           + "; corresponding letter position is "
                           + touchLetterNum,
                           LOGGER_SOURCE_SEQUENCER );
        
        if ( ( touchLetterNum >= 0 ) && ( touchLetterNum < 26 ) ) {
          // a letter was selected
          
          pickupTile = String.fromCharCode( 65 + touchLetterNum );
          blankPlacedSelectedTile = ID_BLANKSELECT + touchLetterNum;
          
          var sprite = vScreen.getSpriteById( blankPlacedSelectedTile );

          sprite.setWidth(  140 );
          sprite.setHeight( 140 );
          sprite.show();
          sprite.move( sprite.getXpos() - 24, sprite.getYpos() - 24 );
          sprite.getImgNode().style.zIndex = ID_ZINDEX_ALPHABETPICKUP;

          tGame.moveSprite(
              sprite,
              blankPlacedTempTilePosArray[ 2 ] - 14,
              blankPlacedTempTilePosArray[ 3 ] - 14,
              28,
              GAME_MOVESPRITE_SHOWANDOUT,
              true );
          
          sysLogger.addLine( "flying bitmap ID "
                             + blankPlacedSelectedTile
                             + " to target position, everything else out",
                             LOGGER_SOURCE_SEQUENCER );
          
          blankPlacedFlySequence = 0;
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
          tGame.setSubState( 13 );
          
        }
        
      }

      break;
      
    case 13: // user selected alphabet, prepare game resume

      switch ( blankPlacedFlySequence ) {
        
        case 26:
          // restore pickupS object and assign pickupSequence in case the
          // tile placed does not immediately form a word;
          // place it and remove opaque backdrop

          pickupS        = blankPlacedTempPickupS;
          pickupSequence = gamePlayingSequence;

          sysLogger.addLine( "restore 'blank' tile ID "
                               + pickupS.getId()
                               + ", set placement timeout sequence counter "
                               + pickupSequence
                               + ", and place on grid at "
                               + blankPlacedTempTilePosArray.toString(),
                               LOGGER_SOURCE_SEQUENCER );

          placeTileOnGrid( tGame, blankPlacedTempTilePosArray );
        
          blankPlacedSelectedTile     = null;
          blankPlacedTempTilePosArray = null;
          blankPlacedTempPickupS      = null;
        
          blankPlacedFlySequence++;

          break;
        
        case 27:
          // remove backdrop and return to play

          vScreen.removeSprite( ID_BLANKGRAYOUT );
        
          blankPlacedFlySequence = 0;
          tGame.setSubState( 10 );

          break;
          
        default: // 0 through 25

          blankPlacedFlySequence++;
        
          var selectTileId = blankPlacedFlySequence + ID_BLANKSELECT - 1;
        
          if ( !( blankPlacedSelectedTile == selectTileId ) ) {
        
            var selectTileXPos = ( ( blankPlacedFlySequence + 1 ) % 5 ) * 98 + 1;
            var sprite = vScreen.getSpriteById( selectTileId );
        
            tGame.moveSprite(
                sprite,
                selectTileXPos,
                -98,
                blankPlacedSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );
        
          }

      }

      break;
    
    case 20: // exit button pressed

      sysLogger.addLine( "exit button pressed", LOGGER_SOURCE_SEQUENCER );

      var sprite = new Sprite( FILE_SPLASH_OPAQUE_50, 490, 735 );
      sprite.setImgNode( document.getElementById( "bitmap" + ID_BLANKGRAYOUT ) );
      vScreen.putSprite( sprite, ID_BLANKGRAYOUT );
      sprite.show();
      sprite.move( 0, 0 );
      
      tGame.setSubState( 21 );
      break;
      
    case 21: 
      
      tGame.setSubState( 22 );
      break;
      
    case 22:
      
      var sprite = new Sprite( FILE_SPLASH_BLACK, 490, 735 );
      sprite.setImgNode( document.getElementById( "bitmap" + ( ID_BLANKGRAYOUT + 1 ) ) );
      vScreen.putSprite( sprite, ID_BLANKGRAYOUT + 1 );
      sprite.show();
      sprite.move( 0, 0 );

      tGame.setSubState( 23 );
      break;
      
    case 23:

      var sprite = new Sprite( FILE_IMAGE_KEEPPLAYING, 374, 80 );
      sprite.setImgNode( document.getElementById( "bitmap" + ( ID_BLANKGRAYOUT + 2 ) ) );
      vScreen.putSprite( sprite, ID_BLANKGRAYOUT + 2 );
      sprite.show();
      sprite.move( 45, 735 );

      tGame.moveSprite(
                sprite,
                45,
                210,
                blankPlacedSteps,
                GAME_MOVESPRITE_SINE_FLYIN );

      tGame.setSubState( 24 );
      break;
      
    case 24:
      
      var sprite = new Sprite( FILE_IMAGE_STARTLEVELOVER, 374, 80 );
      sprite.setImgNode( document.getElementById( "bitmap" + ( ID_BLANKGRAYOUT + 4 ) ) );
      vScreen.putSprite( sprite, ID_BLANKGRAYOUT + 4 );
      sprite.show();
      sprite.move( 45, 800 );

      tGame.moveSprite(
                sprite,
                45,
                310,
                blankPlacedSteps,
                GAME_MOVESPRITE_SINE_FLYIN );
      
      var sprite = new Sprite( FILE_IMAGE_EXITGAME, 374, 80 );
      sprite.setImgNode( document.getElementById( "bitmap" + ( ID_BLANKGRAYOUT + 3 ) ) );
      vScreen.putSprite( sprite, ID_BLANKGRAYOUT + 3 );
      sprite.show();
      sprite.move( 45, 950 );

      tGame.moveSprite(
                sprite,
                45,
                410,
                blankPlacedSteps,
                GAME_MOVESPRITE_SINE_FLYIN );

      tGame.setSubState( 25 );
      
      sysLogger.addLine( "exit decision drawn, waiting for user",
                         LOGGER_SOURCE_SEQUENCER );
      
      break;
      
    case 25: // wait for user decision on exit or resume

      if ( touch.getTouchOccurred() ) {

        var thisY = touch.getTouchY();
        
        if ( ( thisY > 210 ) && ( thisY < 290 ) ) {
          // resume playing
          
          exitDecision = 0;
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
          tGame.setSubState( 26 );
          
          sysLogger.addLine( "user wants to resume playing",
                             LOGGER_SOURCE_SEQUENCER );
          
        }
        
        if ( ( thisY > 310 ) && ( thisY < 390 ) ) {
          // start level over
          
          exitDecision = 1;
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
          tGame.stopMediaSoundtrack();
          
          tGame.setSubState( 26 );
          
          sysLogger.addLine( "user wants to start level over",
                             LOGGER_SOURCE_SEQUENCER );
          
        }
        
        if ( ( thisY > 410 ) && ( thisY < 490 ) ) {
          // exit game
          
          exitDecision = 2;
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
          tGame.stopMediaSoundtrack();
          
          tGame.setSubState( 26 );
          
          sysLogger.addLine( "user wants to exit",
                             LOGGER_SOURCE_SEQUENCER );
          
        }

        touch.setTouchOccurred( false );

      }
      
      break;
      
    case 26: // fly out exit decision menu
      
      var sprite = vScreen.getSpriteById( ID_BLANKGRAYOUT + 2 );
      
      tGame.moveSprite(
                sprite,
                45,
                -200,
                blankPlacedSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );

      tGame.setSubState( 27 );
      break;
      
    case 27:
      
      var sprite = vScreen.getSpriteById( ID_BLANKGRAYOUT + 4 );
      
      tGame.moveSprite(
                sprite,
                45,
                -300,
                blankPlacedSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );
      
      var sprite = vScreen.getSpriteById( ID_BLANKGRAYOUT + 3 );
      
      tGame.moveSprite(
                sprite,
                45,
                -150,
                blankPlacedSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );
      
    case 28:
    case 29:
    case 30:

      tGame.setSubState( tGame.getSubState() + 1 );
      break;

    case 31:
      
      if (    ( exitDecision == 1 )
           || ( exitDecision == 2 ) ) {
        // user wants to exit the game or restart the level; wipe the screen
        
        for ( var i = 0; i < ( ID_BLANKGRAYOUT + 2 ); i++ ) {
         
          vScreen.removeSprite( i );
          
        }
        
      } else {
        // user wants to continnue playing
        
        vScreen.removeSprite( ID_BLANKGRAYOUT + 1 );
        
      }
      
      tGame.setSubState( 32 );

      break;
      
    case 32:
      
      tGame.setSubState( 33 );
      break;
      
    case 33:
      
      vScreen.removeSprite( ID_BLANKGRAYOUT );

      tGame.setSubState( 34 );
      break;

    case 34: // move on depending on decision

      switch ( exitDecision ) {
        
        case 0:
          
          tGame.setSubState( 10 );
        
          sysLogger.addLine( "resuming play now",
                             LOGGER_SOURCE_SEQUENCER );
          
          break;
          
        case 1:
          
          tGame.setState( GAME_LEVEL_PREPARING );
          tGame.setSubState( 0 );
          
          sysLogger.addLine( "restarting the level",
                             LOGGER_SOURCE_SEQUENCER );
          
          break;
        
        case 2:
        
          tGame.setState( GAME_MENU_PLAY );
          tGame.setSubState( 0 );
        
          tGame.setCurrentMenuLevel( tGame.getLevelNumber() );
        
          sysLogger.addLine( "transitioning to level select menu",
                             LOGGER_SOURCE_SEQUENCER );
          
          break;
          
        default: // unknown decision; do nothing
            
      }
      
      break;
      
    case 40: // "show next five"

      if ( gameShowNextFiveSequence == 0 ) {
        // draw 50% opaque gray background

        sysLogger.addLine( "start building 'show next five' screen",
                            LOGGER_SOURCE_SEQUENCER );

        var sprite = new Sprite( FILE_SPLASH_OPAQUE_50, 490, 735 );
        sprite.setImgNode( document.getElementById( "bitmap" + ID_BLANKGRAYOUT ) );
        vScreen.putSprite( sprite, ID_BLANKGRAYOUT );
        sprite.show();
        sprite.move( 0, 0 );
        
        gameShowNextFiveLetterCnt = 0;
        
      }
      
      if ( gameShowNextFiveSequence == 2 ) {
       
        var sprite = new Sprite( FILE_IMAGE_NEXT5LETTERSARE, 296, 52 );
        
        sprite.setImgNode( document.getElementById( "bitmap" + ID_BLANKSELECT ) );
        vScreen.putSprite( sprite, ID_BLANKSELECT );
        sprite.show();
        sprite.move( 490, GAME_NEXT5LETTERSARE_YPOS );
        
        tGame.moveSprite(
            sprite,
            90,
            GAME_NEXT5LETTERSARE_YPOS,
            blankPlacedSteps,
            GAME_MOVESPRITE_SINE_FLYIN );
        
      }
      
      if (    ( gameShowNextFiveSequence >= 4 )
           && ( gameShowNextFiveSequence < 13 )
           && ( ( gameShowNextFiveSequence % 2 ) == 0 ) ) {
        
        var selectTile = level.getFutureTile( gameShowNextFiveLetterCnt );
        var selectTileFileName = null;

        if ( selectTile == LEVEL_TILE_NONE ) {
          
          selectTileFileName = FILE_IMAGE_NOLETTER;
          
        } else {
      
          selectTileFileName =
                  FILE_IMAGE_LTR_PREFIX
                  + LEVEL_TILES[ selectTile ][ 1 ]
                  + FILE_IMAGE_LTR_SUFFIX;

        }
                  
        var selectTileId = gameShowNextFiveLetterCnt + ID_BLANKSELECT + 1;
        var selectTileXPos = ( 4 - gameShowNextFiveLetterCnt ) * 98 + 1;
        
        var sprite = new Sprite( selectTileFileName, 97, 97 );
        
        sprite.setImgNode( document.getElementById( "bitmap" + selectTileId ) );
        vScreen.putSprite( sprite, selectTileId );
        sprite.show();
        sprite.move( -100, GAME_NEXT5LETTERSARE_ROW2 );
        
        tGame.moveSprite(
            sprite,
            selectTileXPos,
            GAME_NEXT5LETTERSARE_ROW2,
            blankPlacedSteps,
            GAME_MOVESPRITE_SINE_FLYIN );

        gameShowNextFiveLetterCnt++;
        
      }
      
      if ( gameShowNextFiveSequence == 14 ) {
       
        var sprite = new Sprite( FILE_IMAGE_NEXT5OK,
                                 GAME_NEXT5OK_WIDTH,
                                 GAME_NEXT5OK_HEIGHT );
        
        sprite.setImgNode( document.getElementById( "bitmap" + ( ID_BLANKSELECT + 7 ) ) );
        vScreen.putSprite( sprite, ID_BLANKSELECT + 7 );
        sprite.show();
        sprite.move( 490, GAME_NEXT5OK_YPOS );
        
        tGame.moveSprite(
            sprite,
            GAME_NEXT5OK_XPOS,
            GAME_NEXT5OK_YPOS,
            blankPlacedSteps,
            GAME_MOVESPRITE_SINE_FLYIN );
        
        touch.setTouchOccurred( false );
        tGame.setSubState( 41 );
        
        sysLogger.addLine( "next five letters are showing; wait for user touch",
                           LOGGER_SOURCE_SEQUENCER );
        
      }

      gameShowNextFiveSequence++;

      break;
      
    case 41:
        
      if ( ( touch.getIsTouching() == false ) && ( touch.getTouchOccurred() ) ) {
        // player has touched

        if (    ( touch.getTouchX() > GAME_NEXT5OK_XPOS )
             && ( touch.getTouchX() < ( GAME_NEXT5OK_XPOS + GAME_NEXT5OK_WIDTH ) )
             && ( touch.getTouchY() > GAME_NEXT5OK_YPOS )
             && ( touch.getTouchY() < ( GAME_NEXT5OK_YPOS + GAME_NEXT5OK_HEIGHT ) ) ) {

          sysLogger.addLine( "user touched 'OK', prepare game resume",
                             LOGGER_SOURCE_SEQUENCER );
          
          touch.setTouchOccurred( false );
          gameShowNextFiveSequence = 0;
          tGame.playMediaEffect( FILE_SOUND_PICKUP );
        
          tGame.setSubState( 42 );
      
        }
        
      }
      
      break;

    case 42: // remove "five letters" screen and resume playing
      
      if ( gameShowNextFiveSequence == 0 ) {
        
        var sprite = vScreen.getSpriteById( ID_BLANKSELECT );
        
        tGame.moveSprite(
            sprite,
            -400,
            sprite.getYpos(),
            blankPlacedSteps,
            GAME_MOVESPRITE_SINE_FLYOUT,
            true );

        gameShowNextFiveLetterCnt = 0;
        
      }
      
      if (    ( gameShowNextFiveSequence >= 2 )
           && ( gameShowNextFiveSequence < 11 )
           && ( ( gameShowNextFiveSequence % 2 ) == 0 ) ) {
        
        var selectTileId = gameShowNextFiveLetterCnt + ID_BLANKSELECT + 1;
        
        var sprite = vScreen.getSpriteById( selectTileId );

        tGame.moveSprite(
            sprite,
            sprite.getXpos() + 400,
            sprite.getYpos(),
            blankPlacedSteps,
            GAME_MOVESPRITE_SINE_FLYOUT,
            true );
      
        gameShowNextFiveLetterCnt++;
        
      }
      
      if ( gameShowNextFiveSequence == 12 ) {
        
        var sprite = vScreen.getSpriteById( ID_BLANKSELECT + 7 );
        
        tGame.moveSprite(
            sprite,
            -400,
            sprite.getYpos(),
            blankPlacedSteps,
            GAME_MOVESPRITE_SINE_FLYOUT,
            true );
        
      }
      
      if ( gameShowNextFiveSequence == 14 ) {
        
        vScreen.removeSprite( ID_BLANKGRAYOUT );

        tGame.setSubState( 10 );
        
        sysLogger.addLine( "resuming play",
                           LOGGER_SOURCE_SEQUENCER );
        
      }
      
      gameShowNextFiveSequence++;
      
      break;

    case 80: // show tap pause and think help

      sysLogger.addLine( "pause and think help showing the first time",
                         LOGGER_SOURCE_SEQUENCER );

      var sprite = new Sprite( FILE_HELP_TAPPAUSETHINKG, 490, 735 );
      sprite.setImgNode( document.getElementById( "bitmap" + ID_BLANKGRAYOUT ) );
      vScreen.putSprite( sprite, ID_BLANKGRAYOUT );
      sprite.show();
      sprite.move( 0, 0 );

      touch.setTouchOccurred( false );
      helpMinTapTimer = ( new Date() ).getTime();
      
      tGame.setNewbieStatus( tGame.getNewbieStatus() + "p" );
      tGame.setSubState( 81 );
      
      break;

    case 81:
    
      var thisHelpMinTapTimer = ( new Date() ).getTime();
      if ( ( thisHelpMinTapTimer - helpMinTapTimer ) < 1500 ) {
        
        touch.setTouchOccurred( false );
        return;
        
      }
      
      if ( ( touch.getIsTouching() == false ) && ( touch.getTouchOccurred() ) ) {
        // player has touched

        sysLogger.addLine( "user touched, prepare game resume",
                           LOGGER_SOURCE_SEQUENCER );
          
        touch.setTouchOccurred( false );
        tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
        vScreen.removeSprite( ID_BLANKGRAYOUT );
        
        tGame.setSubState( 82 );
      
      }
      
      break;
      
    case 82:
      
      tGame.setSubState( 10 );
      break;
      
    case 83: // show recycle letter help

      sysLogger.addLine( "recycle help showing the first time",
                         LOGGER_SOURCE_SEQUENCER );

      var sprite = new Sprite( FILE_HELP_TAPRECYCLE, 490, 735 );
      sprite.setImgNode( document.getElementById( "bitmap" + ID_BLANKGRAYOUT ) );
      vScreen.putSprite( sprite, ID_BLANKGRAYOUT );
      sprite.show();
      sprite.move( 0, 0 );

      touch.setTouchOccurred( false );
      helpMinTapTimer = ( new Date() ).getTime();
      
      tGame.setNewbieStatus( tGame.getNewbieStatus() + "r" );
      tGame.setSubState( 81 );
      
      break;
      
    case 90: // level over

      sysLogger.addLine( "cycle "
                         + gamePlayingSequence
                         + " level over",
                         LOGGER_SOURCE_SEQUENCER );
      
      tGame.setSubState( 91 );
      break;

    case 91: // fly out remaining unplayed letters

      for ( var lX = 0; lX < LEVEL_GRID_MAX_X; lX++ ) {
       
        for ( var lY = 0; lY < LEVEL_GRID_MAX_Y; lY++ ) {
         
          if ( !( level.getTilePlacedXY( lX, lY ) == LEVEL_TILE_NONE )
               && !( level.getTileIsLocked( lX, lY ) ) ) {
            // a tile is placed but not yet part of a word

            sysLogger.addLine( "fly out unlocked tile "
                               + level.getTilePlacedXY( lX, lY )
                               + " at "
                               + lX
                               + ", "
                               + lY,
                               LOGGER_SOURCE_SEQUENCER );

            detachAndFlyOut( lX, lY );
          
          }
          
        }
        
      }
      
      if ( pickupS ) {
        // player is still holding a sprite
        
        sysLogger.addLine( "delete picked up sprite "
                           + pickupS.getId()
                           + " and detach from touch",
                           LOGGER_SOURCE_SEQUENCER );

        vScreen.removeSprite( pickupS.getId() );
        detachSpriteFromTouch();
        
      }
      
      finishSequence = 0;
      
      tGame.setSubState( 92 );
      
      break;
      
    case 92:
      
      switch ( finishSequence ) {
       
        case 0:

          var sprite = new Sprite( FILE_SPLASH_OPAQUE_50, 490, 735 );
          sprite.setImgNode( document.getElementById( "bitmap" + ID_BLANKGRAYOUT ) );
          vScreen.putSprite( sprite, ID_BLANKGRAYOUT );
          sprite.show();
          sprite.move( 0, 0 );

          break;
          
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:

          break;
          
        case 12:
          // end-of-level screen
          
          tGame.setState( GAME_LEVEL_COMPLETE );
          tGame.setSubState( 0 );
          
          break;
          
        default:
        
      }
      
      finishSequence++;
      
      break;
      
    default:

  }

  // private functions

  function pickUpSprite( tGame, sWidth, sHeight, newPlayingSequence ) {
    // pick up a sprite

    if ( pickupS ) {

      touch.setPickedUpSprite( pickupS );

      pickupS.setIsPickedUp( true );
      pickupS.setWidth(  sWidth  * 2 );
      pickupS.setHeight( sHeight * 2 );
      pickupS.show();
      pickupS.move( touch.getTouchX() - sWidth, touch.getTouchY() - sHeight, true );     

      if ( newPlayingSequence ) {

        pickupSequence = newPlayingSequence;

      } else {

        pickupSequence = gamePlayingSequence;

      }

    }

  }

  function checkCanPlaceTile( pxPos, pyPos ) {
    // see whether a letter tile can be placed at these coordinates;
    // returns int[] array of grid X and grid Y, and virtual X/Y coordinates

    var returnArray = new Array();
    returnArray[ 0 ] = -1;

    if ( ( pxPos > CONVEYOR_TRASH_XPOS )
         && ( pyPos < ( CONVEYOR_TRASH_YPOS + 95 ) ) ) {
      // tile was dropped in the trash bin

      returnArray[ 0 ] = -2;
      return returnArray;

    }

    var thisX = Math.floor( pxPos / 70 );
    var thisY = Math.floor( ( pyPos - GAME_GRID_OFFSET_Y ) / 70 );

    if ( thisY < 0 ) return returnArray;
    if ( thisY >= LEVEL_GRID_MAX_Y ) return returnArray;

    if ( thisX < 0 ) thisX = 0;
    if ( thisX >= LEVEL_GRID_MAX_X ) thisX = LEVEL_GRID_MAX_X - 1;

    if ( level.getGridXY( thisX, thisY ) == LEVEL_GRID_TYPE_NONE ) return returnArray;
    if ( !( level.getTilePlacedXY( thisX, thisY ) == LEVEL_TILE_NONE ) ) return returnArray;

    // it can be placed
    
    returnArray[ 0 ] = thisX;
    returnArray[ 1 ] = thisY;
    returnArray[ 2 ] = thisX * 70;
    returnArray[ 3 ] = thisY * 70 + GAME_GRID_OFFSET_Y;

    return returnArray;

  }

  function checkCanPlaceGrid( pxPos, pyPos ) {
    // see whether a grid tile can be placed at these coordinates;
    // returns int[] array of grid X and grid Y, and virtual X/Y coordinates

    var returnArray = new Array();
    returnArray[ 0 ] = -1;

    var thisX = Math.floor( pxPos / 70 );
    var thisY = Math.floor( ( pyPos - GAME_GRID_OFFSET_Y ) / 70 );

    if ( thisY < 0 ) return returnArray;
    if ( thisY >= LEVEL_GRID_MAX_Y ) return returnArray;

    if ( thisX < 0 ) thisX = 0;
    if ( thisX >= LEVEL_GRID_MAX_X ) thisX = LEVEL_GRID_MAX_X - 1;

    if ( !( level.getGridXY( thisX, thisY ) == LEVEL_GRID_TYPE_NONE ) ) return returnArray;

    // it can be placed
    
    returnArray[ 0 ] = thisX;
    returnArray[ 1 ] = thisY;
    returnArray[ 2 ] = thisX * 70 + GAME_GRID_TILE_OFFSET_X;
    returnArray[ 3 ] = thisY * 70 + GAME_GRID_TILE_OFFSET_Y + GAME_GRID_OFFSET_Y;

    return returnArray;

  }

  function checkCanPickupTile( pxPos, pyPos ) {
    // checks whether a current letter or grid tile can be picked up;
    // returns int[] array of:
    // - type (1 letter or 2 grid),
    // - grid X,Y,
    // - tile or grid type,
    // - tile dropped sequence counter (if tile)

    var returnArray = new Array();

    returnArray[ 0 ] = -1;

    var thisX = Math.floor( pxPos / 70 );
    var thisY = Math.floor( ( pyPos - GAME_GRID_OFFSET_Y ) / 70 );

    if ( thisX < 0 ) thisX = 0;
    if ( thisX >= LEVEL_GRID_MAX_X ) thisX = LEVEL_GRID_MAX_X - 1;

    returnArray[ 1 ] = thisX;
    returnArray[ 2 ] = thisY;

    if ( thisY < 0 ) return returnArray;
    if ( thisY >= LEVEL_GRID_MAX_Y ) return returnArray;

    if ( level.getTileIsLocked( thisX, thisY ) ) return returnArray;
    
    var tilePlaced = level.getTilePlacedXY( thisX, thisY );

    if ( !( tilePlaced == LEVEL_TILE_NONE ) ) {
      // tile is placed and can be picked up (not locked)

      returnArray[ 0 ] = 1; // letter tile
      returnArray[ 3 ] = tilePlaced;
      returnArray[ 4 ] = level.getTileDroppedSequence( thisX, thisY );

      return returnArray;

    }

    var gridType = level.getGridXY( thisX, thisY );

    if ( LEVEL_GRID_ISMOVABLE[ gridType ] == true ) {
      // grid tile can be picked up (is movable)

      returnArray[ 0 ] = 2; // grid tile
      returnArray[ 3 ] = gridType;
      returnArray[ 4 ] = null;

      return returnArray;

    }

    return returnArray;

  }

  function transferPickupToSpare( convID5 ) { 

    vScreen.removeSprite( convID5 );

    var pickupFilename = 
              FILE_IMAGE_LTR_PREFIX
              + LEVEL_TILES[ pickupTile ][ 1 ]
              + FILE_IMAGE_LTR_SUFFIX;

    sysLogger.addLine( "auto transfer picked up sprite to spare "
              + ID_LTR_PICKUPEXT,
              LOGGER_SOURCE_SEQUENCER );

    pickupS = new Sprite( pickupFilename, 70, 70 );
    pickupS.setImgNode(
              document.getElementById( "bitmap" + ID_LTR_PICKUPEXT ) );

    vScreen.putSprite( pickupS, ID_LTR_PICKUPEXT );
    pickupS.show();
    pickupS.move( CONVEYOR_TRASH_XPOS,
                  CONVEYOR_TRASH_YPOS );

  }

  function placeTileOnGrid( tGame, tilePosArray ) {
    // place tile on grid,
    // register/update level variables,
    // if applicable, move up conveyor and get new tile,
    // detach pickupS from touch,
    // if applicable, show pause-and-think.

    // place new tile on grid

    var placeTileFilename = 
              FILE_IMAGE_LTR_PREFIX
              + LEVEL_TILES[ pickupTile ][ 1 ]
              + FILE_IMAGE_LTR_SUFFIX;

    var xPos      = tilePosArray[ 0 ];
    var yPos      = tilePosArray[ 1 ];
    var xOnScreen = tilePosArray[ 2 ];
    var yOnScreen = tilePosArray[ 3 ];

    var tileId = ID_LTR_START + xPos + LEVEL_GRID_MAX_X * yPos;

    sysLogger.addLine( "dropped tile "
              + pickupTile
              + " into id "
              + tileId
              + " at "
              + tilePosArray.toString()
              + " and pickup sequence "
              + pickupSequence,
              LOGGER_SOURCE_SEQUENCER );

    tileS = new Sprite( placeTileFilename, 70, 70 );
    tileS.setImgNode(
              document.getElementById( "bitmap" + tileId ) );

    vScreen.putSprite( tileS, tileId );
    tileS.show();
    tileS.move( xOnScreen, yOnScreen );

    // register level variables
    
    level.setTilePlacedXY( xPos, yPos, pickupTile );
    level.setTileDroppedSequence( xPos, yPos, pickupSequence );

    // register with scoring engine
    scoreEngine.checkForWordsHorizontal( level, yPos );
    scoreEngine.checkForWordsVertical(   level, xPos );
    
    if ( SCORING_LOGGING_INSANITY ) {
      
      sysLogger.addLine( "scoreEngine insanity logger: "
                         + "register new locked tile at position ("
                         + xPos
                         + ", "
                         + yPos
                         + ")",
                         LOGGER_SOURCE_SCORING );
      scoreEngine.dumpWordsFoundToLog( sysLogger );
      
    }
    
    // do graphics and animations
    
    var oldId = pickupS.getId();
    
    if ( ( oldId >= ID_LTR_START )
         && ( oldId < ( ID_LTR_START + LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) ) ) {
      // tile was picked up from another grid position; open it up

      var oldX = ( oldId - ID_LTR_START ) % LEVEL_GRID_MAX_X;
      var oldY = Math.floor( ( oldId - ID_LTR_START ) / LEVEL_GRID_MAX_X );

      level.setTilePlacedXY( oldX, oldY, LEVEL_TILE_NONE );
      vScreen.removeSprite( oldId );
    
      // register with scoring engine
      scoreEngine.checkForWordsHorizontal( level, oldY );
      scoreEngine.checkForWordsVertical(   level, oldX );

      if ( SCORING_LOGGING_INSANITY ) {
      
        sysLogger.addLine( "scoreEngine insanity logger: "
                           + "unregister previous placed tile from position ("
                           + oldX
                           + ", "
                           + oldY
                           + ")",
                           LOGGER_SOURCE_SCORING );
        scoreEngine.dumpWordsFoundToLog( sysLogger );
      
      }
    
    } else if ( ( oldId >= ID_LTR_BELTSTART )
         && ( oldId < ( ID_LTR_BELTSTART + 6 ) ) ) {
      // tile is from conveyor; unless this is the last letter,
      // move up and inject new letter; reset timer

      var moveConveyorFlag = false;

      for ( var i = 0; i < 4; i++ ) {

        if ( tGame.getConveyorPosImg( i ) == oldId ) moveConveyorFlag = true;

      }  

      if ( moveConveyorFlag ) {

        gameMoveConveyor( tGame, oldId );
        gameInjectNewLetter( tGame, level.getNextTile() );

      } else {
        // don't move the conveyor up, only remove the tile

        vScreen.removeSprite( oldId );

        for ( var i = 4; i < 6; i++ ) {

          if ( tGame.getConveyorPosImg( i ) == oldId )
              tGame.setConveyorTile( i, LEVEL_TILE_NONE );

        }

      }

      // give some extra time
      gameAdvanceConveyor = BELTTIMER_MOVE + 1;

    } else if ( oldId == ID_LTR_PICKUPEXT ) {
      // letter was the spare; remove

      vScreen.removeSprite( oldId );

      // give some extra time
      gameAdvanceConveyor = BELTTIMER_MOVE + 1;

    }

    // detach sprite from touch
    detachSpriteFromTouch();
    
    // pause and think
    if ( gameShowPausethinkOnNextDrop ) showPauseAndThink();

  }
  
  function placeGridTileOnGrid( tGame, gridPosArray ) {
    // place movable grid tile on grid,
    // register/update grid variables,
    // detach pickupS from touch. 

    // place movable grid tile on grid

    var placeTileFilename = LEVEL_GRID_FILES[ pickupGridType ];

    var xPos      = gridPosArray[ 0 ];
    var yPos      = gridPosArray[ 1 ];
    var xOnScreen = gridPosArray[ 2 ];
    var yOnScreen = gridPosArray[ 3 ];

    var tileId = ID_GRID_START + xPos + LEVEL_GRID_MAX_X * yPos;

    sysLogger.addLine( "dropped grid tile "
              + pickupGridType
              + " into id "
              + tileId
              + " at "
              + gridPosArray.toString(),
              LOGGER_SOURCE_SEQUENCER );

    tileS = new Sprite( placeTileFilename, GAME_GRID_SIZE, GAME_GRID_SIZE );
    tileS.setImgNode(
              document.getElementById( "bitmap" + tileId ) );

    vScreen.putSprite( tileS, tileId );
    tileS.show();
    tileS.move( xOnScreen, yOnScreen );

    // register grid variables
    
    level.setGridXY( xPos, yPos, pickupGridType );

    var oldId = pickupS.getId();

    var oldX = ( oldId - ID_GRID_START ) % LEVEL_GRID_MAX_X;
    var oldY = Math.floor( ( oldId - ID_GRID_START ) / LEVEL_GRID_MAX_X );

    level.setGridXY( oldX, oldY, LEVEL_GRID_TYPE_NONE );

    vScreen.removeSprite( oldId );

    // detach sprite from touch
    detachSpriteFromTouch();

  }

  function moveTileToTrash( tGame ) {
    // register/update level variables to remove tile,
    // if applcable, move up conveyor and get new tile,
    // detach pickupS from touch.

    sysLogger.addLine( "move tile "
              + pickupTile
              + " with id "
              + pickupS.getId()
              + " to trash",
              LOGGER_SOURCE_SEQUENCER );

    // unregister level variables
    
    var oldId = pickupS.getId();
    
    if ( ( oldId >= ID_LTR_START )
         && ( oldId < ( ID_LTR_START + LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) ) ) {
      // tile was picked up from the grid; open it up

      var oldX = ( oldId - ID_LTR_START ) % LEVEL_GRID_MAX_X;
      var oldY = Math.floor( ( oldId - ID_LTR_START ) / LEVEL_GRID_MAX_X );

      level.setTilePlacedXY( oldX, oldY, LEVEL_TILE_NONE );
      vScreen.removeSprite( oldId );
    
      // register with scoring engine
      scoreEngine.checkForWordsHorizontal( level, oldY );
      scoreEngine.checkForWordsVertical(   level, oldX );
      
      if ( SCORING_LOGGING_INSANITY ) {
      
        sysLogger.addLine( "scoreEngine insanity logger: "
                           + "unregister tile being moved to trash from position ("
                           + oldX
                           + ", "
                           + oldY
                           + ")",
                           LOGGER_SOURCE_SCORING );
        scoreEngine.dumpWordsFoundToLog( sysLogger );
      
      }

    } else if ( ( oldId >= ID_LTR_BELTSTART )
         && ( oldId < ( ID_LTR_BELTSTART + 6 ) ) ) {
      // tile is from conveyor; unless this is the last letter,
      // move up and inject new letter; reset timer

      var moveConveyorFlag = false;

      for ( var i = 0; i < 4; i++ ) {

        if ( tGame.getConveyorPosImg( i ) == oldId ) moveConveyorFlag = true;

      }  

      if ( moveConveyorFlag ) {

        gameMoveConveyor( tGame, oldId );
        gameInjectNewLetter( tGame, level.getNextTile() );

      } else {
        // don't move the conveyor up, only remove the tile

        vScreen.removeSprite( oldId );

        for ( var i = 4; i < 6; i++ ) {

          if ( tGame.getConveyorPosImg( i ) == oldId )
              tGame.setConveyorTile( i, LEVEL_TILE_NONE );

        }

      }

      // give some extra time
      gameAdvanceConveyor = BELTTIMER_MOVE + 1;

    } else if ( oldId == ID_LTR_PICKUPEXT ) {
      // letter was the spare; remove

      vScreen.removeSprite( oldId );

      // give some extra time
      gameAdvanceConveyor = BELTTIMER_MOVE + 1;

    }

    // detach sprite from touch
    detachSpriteFromTouch();

  }

  function lockTile( tileSprite, tileX, tileY ) {
    // locks the tile in place on the grid;
    // supply either tileSprite or ( tileX, tileY )
    
    var tileId = null;
    
    if ( tileSprite ) {
     
      tileId = tileSprite.getId();
      tileId -= ID_LTR_START;
      tileX  =  tileId % LEVEL_GRID_MAX_X;
      tileY  =  Math.floor( 0.01 + tileId / LEVEL_GRID_MAX_X );
      
    } else {
     
      tileId = ID_LTR_START + tileX + tileY * LEVEL_GRID_MAX_X;
      tileSprite = vScreen.getSpriteById( tileId );
      
    }

    sysLogger.addLine( "lock tile "
          + tileId
          + " at "
          + tileX
          + ","
          + tileY,
          LOGGER_SOURCE_SEQUENCER );
    
    // update level variables
    level.setTileIsLocked( tileX, tileY, true );
    
    // replace the grid tile with the locked tile backdrop
    var gridSprite = vScreen.getSpriteById( ID_GRID_START
                                            + tileX
                                            + tileY * LEVEL_GRID_MAX_X );
    gridSprite.setWidth( 70 );
    gridSprite.setHeight( 70 );
    gridSprite.show( LEVEL_GRID_BACKDROP_FILE );
    gridSprite.move( tileX * 70,
                     tileY * 70 + GAME_GRID_OFFSET_Y );
    
    // visually change sprite size
    tileSprite.setIsPickedUp( false );
    
    tileSprite.setWidth(  GAME_LTR_LOCKED_SIZE );
    tileSprite.setHeight( GAME_LTR_LOCKED_SIZE );
        
    // make sure actual sprite letter shows (in case it was blinking)
    var tileFilename =
             FILE_IMAGE_LTR_PREFIX
             + LEVEL_TILES[ level.getTilePlacedXY( tileX, tileY ) ][ 1 ]
             + FILE_IMAGE_LTR_SUFFIX;
    
    tileSprite.show( tileFilename );

    tileSprite.move( tileX * 70 + GAME_LTR_LOCKED_OFFSET, 
                     tileY * 70 + GAME_LTR_LOCKED_OFFSET + GAME_GRID_OFFSET_Y );

    // detach from touch in case it was picked up
    if ( touch.getPickedUpSprite() == tileSprite )
      detachSpriteFromTouch();
    
  }
  
  function detachSpriteFromTouch() {
    // clears the touch variables
        
    sysLogger.addLine( "detach touch tile "
                       + pickupTile
                       + " or grid "
                       + pickupGridType,
          LOGGER_SOURCE_SEQUENCER );
        
    touch.setPickedUpSprite();
    pickupS        = null;
    pickupTile     = null;
    pickupGridType = null;
    pickupSequence = null;
    
  }
  
  function getScoreFromWord( tileXstart, tileYstart, dX, dY, numLetters ) {
    // starting at tileX/Ystart and going in direction dX/dY, returns
    // an array of scoring info:
    // scoreData[ 0 ] = total score of word
    //          [ 1 ], [ 2 ], [ 3 ] number of single/double/triple letter
    //          [ 11 ], [ 12 ], [ 13 ] number of single/double/triple word
    //                                 (both from grid tile and letter tile)
    
    var tileX          = tileXstart;
    var tileY          = tileYstart;
    var thisTile       = null;
    var thisGridTile   = null;
    var thisLetterMult = null;

    var thisWordMult   = null;
    var totalWordMult  = 1;
    
    var scoreData = resetScoreDataArray();
    var ltrCount = numLetters;
    
    do {
      
      thisTile     = level.getTilePlacedXY( tileX, tileY );
      thisGridTile = level.getGridXY(       tileX, tileY );
      
      // word score multiplier grid tile
      thisWordMult                    = LEVEL_GRID_WORDMULTS[ thisGridTile ];
      scoreData[ thisWordMult + 10 ] += 1;
      totalWordMult                  *= thisWordMult;
     
      // word score multiplier letter tile
      thisWordMult                    = LEVEL_TILES[ thisTile ][ 3 ];
      scoreData[ thisWordMult + 10 ] += 1;
      totalWordMult                  *= thisWordMult;

      // letter score multiplier grid tile
      thisLetterMult               = LEVEL_GRID_LTRMULTS[ thisGridTile ];
      scoreData[ thisLetterMult ] += 1;
      
      // get the letter score data including multiplier from grid (if any)
      scoreData[ 0 ] += thisLetterMult * LEVEL_TILES[ thisTile ][ 2 ];
      
      tileX += dX;
      tileY += dY;
      
      ltrCount--;
      
    } while ( ltrCount > 0 );
    
    // score from letter tiles is compiled; apply word multiplier(s)
    
    scoreData[ 0 ] *= totalWordMult;

    sysLogger.addLine( "string from tile "
                  + tileXstart
                  + ", "
                  + tileYstart
                  + " in direction "
                  + dX
                  + ", "
                  + dY
                  + " has score data: "
                  + scoreData.toString(),
                  LOGGER_SOURCE_SEQUENCER );
    
    return scoreData;
    
  }

  function resetScoreDataArray() {
    // convenience function to return an empty scoreData array
    
    var returnArray = new Array();
    
    returnArray[ 0 ] = 0;
    
    for ( var i = 1; i <= LEVEL_MULTS_MAX; i++ ) {
      
      returnArray[ i ]      = 0;
      returnArray[ i + 10 ] = 0;
      
    }
    
    return returnArray;
    
  }
  
  function checkForWord() {
    // checks whether a word exists on the board;
    // if so, scores it, displays the score fly-out,
    // and locks in the letter tiles
    
    sysLogger.addLine( "check for word on grid",
                       LOGGER_SOURCE_SEQUENCER );
    
    if ( SCORING_LOGGING_INSANITY ) scoreEngine.dumpWordsFoundToLog( sysLogger );
    
    var forceClean = false;
    
    if ( gameScoringModel == SCORING_MODEL_CLEAN ) {
      
      sysLogger.addLine( "clean scoring model, flag non-chaos words",
                         LOGGER_SOURCE_SEQUENCER );
      
      scoreEngine.calculateNonChaosWords();
      forceClean = true;
      
      if ( SCORING_LOGGING_INSANITY ) scoreEngine.dumpWordsFoundToLog( sysLogger );
      
    }
    
    var foundData = scoreEngine.getFirstWordFound( forceClean );
    
    if ( foundData == null ) {
     
      sysLogger.addLine( "none found",
                         LOGGER_SOURCE_SEQUENCER );
      return;
      
    }
    
    var scoreDataHoriz = resetScoreDataArray();
    var scoreDataVert  = resetScoreDataArray();
    var tileX          = foundData[ 7 ];
    var tileY          = foundData[ 8 ];

    if ( foundData[ 0 ] == 1 ) {
      // a know word was placed horizontally
      
      scoreDataHoriz = getScoreFromWord(
                           foundData[ 2 ],
                           tileY,
                           1,
                           0,
                           foundData[ 4 ] );
      
    }
    
    if ( foundData[ 1 ] == 1 ) {
      // a know word was placed vertically
        
      scoreDataVert = getScoreFromWord(
                          tileX,
                          foundData[ 3 ],
                          0,
                          1,                                    
                          foundData[ 5 ] );
      
    }
    
    var sumData = new Array();
    
    // aggregate vertical and horizontal scores

    sumData[ 0 ] = scoreDataHoriz[ 0 ] + scoreDataVert[ 0 ];

    for ( var i = 1; i <= LEVEL_MULTS_MAX; i++ ) {
      
      sumData[ i      ] = scoreDataHoriz[ i      ] + scoreDataVert[ i      ];
      sumData[ i + 10 ] = scoreDataHoriz[ i + 10 ] + scoreDataVert[ i + 10 ];        
      
    }

    if ( ( scoreDataHoriz[ 0 ] > 0 ) && ( scoreDataVert[ 0 ] > 0 ) ) {
      // both horizontally and vertically forms a new word:
      // give additional points

      sumData[ 0 ] += AWARD_CRISSCROSS_PLUS;
      sumData[ 0 ] *= AWARD_CRISSCROSS_MULT;

      // show crisscross bonus motivational
        
      var sprite = new Sprite( FILE_IMAGE_CRISSCROSSBONUS, 179, 70 );
        
      sprite.setImgNode( document.getElementById( "bitmap" + ID_CRISSCROSSBONUS ) );
      vScreen.putSprite( sprite, ID_CRISSCROSSBONUS );
      sprite.show();
      sprite.move( -179 , AWARD_CRISSCROSS_YPOS );
        
      tGame.moveSprite(
          sprite,
          490,
          AWARD_CRISSCROSS_YPOS,
          awardsSteps,
          GAME_MOVESPRITE_ZAPINOUT,
          true );
        
      tGame.playMediaEffect( FILE_SOUND_CRISSCROSS );
        
    } else {
      // single word sound
        
      tGame.playMediaEffect( FILE_SOUND_WORDLOCK );
        
    }

    // add level score to display, and draw fly-out

    if ( sumData[ 0 ] > 999999 ) sumData[ 0 ] = 999999;
      
    var newScore = scoreNumber.getValue() + sumData[ 0 ];
    if ( newScore > 999999 ) newScore = 999999;
      
    scoreNumber.setValue( newScore );
    scoreNumber.show();
      
    tGame.setLevelScore( newScore );
     
    var sizeX = GAME_MOVINGSCORE_WIDTH;
    var sizeY = Math.floor( sizeX * 1.43 );
      
    var digits = Math.floor( Math.log( sumData[ 0 ] ) / Math.LN10 );
    var movingWidth = digits * sizeX;
      
    var movingX = tileX * ( 490 - movingWidth ) / LEVEL_GRID_MAX_X;
    var movingY = tileY * 70 + GAME_GRID_OFFSET_Y - 70;
      
    movingScoreNumber = new Number( ID_MOVINGSCORE,
                                    6,
                                    movingX,
                                    movingY,
                                    sizeX,
                                    sizeY,
                                    GAME_MOVINGSCORE_COLOR );
      
    movingScoreNumber.setValue( sumData[ 0 ] );
    movingScoreNumber.show();
      
    tGame.moveNumber( movingScoreNumber,
                      movingX,
                      -sizeY,
                      movingScoreSteps,
                      GAME_MOVESPRITE_SHOWANDOUT,
                      true );

    var lX = null;
    var lY = null;
    var dX = null;
    var dY = null;
            
    for ( var i = 0; i < 2; i++ ) {
      // lock tiles of successfully placed words
        
      lX = tileX;
      lY = tileY;
      dX = 0;
      dY = 0;
        
      if ( foundData[ i ] == 1 ) {
         
        if ( i == 0 ) {
          // horizontal word
          
          dX = 1;
          lX = foundData[ 2 ];
            
        } else {
          // vertical word
            
          dY = 1;
          lY = foundData[ 3 ];
            
        }
          
        for ( var j = 0; j < foundData[ 4 + i ]; j++ ) {
           
          lockTile( null, lX, lY );
            
          lX += dX;
          lY += dY;
            
        }
          
      }
        
    }
    
  }
  
  function detachAndFlyOut( lX, lY ) {
    // moves a tile to the trash bin, detaches from touch if needed,
    // unregisters from game grid if needed
   
    var tileId = ID_LTR_START + lX + LEVEL_GRID_MAX_X * lY;
                    
    sysLogger.addLine( "letter id "
                       + tileId
                       + " will be removed",
                       LOGGER_SOURCE_SEQUENCER );

    if ( pickupS ) {
      // detach from touch
                
      if ( tileId == pickupS.getId() ) {

        var pxPos = Math.floor( pickupS.getXposPickedUp()
                                + pickupS.getWidth()  / 4 );
        var pyPos = Math.floor( pickupS.getYposPickedUp()
                                + pickupS.getHeight() / 4 );
    
        pickupS.setIsPickedUp( false );
      
        pickupS.setWidth(  pickupS.getWidth()  / 2 );
        pickupS.setHeight( pickupS.getHeight() / 2 );
        pickupS.show();
        pickupS.move( pxPos, pyPos );

        detachSpriteFromTouch();
        
      }

    }

    // update grid
    level.setTilePlacedXY( lX, lY, LEVEL_TILE_NONE );

    // register with scoring engine
    scoreEngine.checkForWordsHorizontal( level, lY );
    scoreEngine.checkForWordsVertical(   level, lX );
   
    if ( SCORING_LOGGING_INSANITY ) {
      
      sysLogger.addLine( "scoreEngine insanity logger: "
                         + "detach and fly out from position ("
                         + lX
                         + ", "
                         + lY
                         + ")",
                         LOGGER_SOURCE_SCORING );
      scoreEngine.dumpWordsFoundToLog( sysLogger );
      
    }
    
    // fly to trash and destroy
    var tileSprite = vScreen.getSpriteById( tileId );
              
    tGame.moveSprite(
                  tileSprite,
                  CONVEYOR_TRASH_XPOS,
                  CONVEYOR_TRASH_YPOS,
                  touchReleasedSteps,
                  GAME_MOVESPRITE_SINE,
                  true );
    
  }
  
  function stopWigglingIfAny() {
    
    sysLogger.addLine( "stop wiggling",
                       LOGGER_SOURCE_SEQUENCER );
    
    firstLetterPickedUp   = true;
    holdConveyorAndWiggle = false;
    
  }
  
  function startWiggling() {
          
    sysLogger.addLine( "start wiggling",
                       LOGGER_SOURCE_SEQUENCER );
    
    holdConveyorAndWiggle   = true;
    gameAdvanceConveyor     = BELTTIMER_WIGGLING;
    wiggleTheLettersCounter = 0;
    
  }
  
  function wiggleTheLetters() {
    // five letters are currently on the belt, the belt is stopped and
    // the letters are wiggling; uses gamePlayingSequence
    
    var conveyorPos = gamePlayingSequence % 7;
    if ( conveyorPos > 5 ) return;
    
    wiggleTheLettersCounter++;
    
    var letterTileId = conveyorPos + ID_LTR_BELTSTART;
    
    if ( ( wiggleTheLettersCounter < 10 )
         && ( letterTileId == tGame.getConveyorPosImg( 5 ) ) ) {
      // don't wiggle most-right letter tile just yet, it might be
      // a recycled tile and we don't want to mess with that animation
      
      return;
      
    }
    
    var sprite = vScreen.getSpriteById( letterTileId );
    
    if ( !( sprite ) ) return;
    if ( sprite.getIsPickedUp() ) {
      // user managed to pick up a sprite while the model wanted
      // to initiate wiggling; override model
      
      stopWigglingIfAny();
      return;
      
    }
    
    var spriteX = sprite.getXposTo();
    var spriteY = sprite.getYposTo();
    
    sprite.move( spriteX, spriteY - 10 );
    sprite.show();
    
    tGame.moveSprite(
                  sprite,
                  spriteX,
                  spriteY,
                  3,
                  GAME_MOVESPRITE_SINE_FLYOUT );
     
  }
  
  function checkConveyorIsEmpty() {
    // returns true if conveyor is empty;
    // enters end-of-level countdown mode if not already

    if ( !( gameEndCountdown == null ) ) return true;
    
    var returnVal = false;

    if ( tGame.getConveyorTile( 4 ) == LEVEL_TILE_NONE ) {

      if ( ( tGame.getConveyorTile( 0 ) == LEVEL_TILE_NONE )
             && ( tGame.getConveyorTile( 1 ) == LEVEL_TILE_NONE )
             && ( tGame.getConveyorTile( 2 ) == LEVEL_TILE_NONE )
             && ( tGame.getConveyorTile( 3 ) == LEVEL_TILE_NONE )
             && ( !gameLidIsOpen ) ) {
        // last letter is gone - end-of-game countdown

        if ( !gameEndCountdown ) {
          
          sysLogger.addLine( "change game mode to end-of-level countdown",
                             LOGGER_SOURCE_SEQUENCER );

          gameEndCountdown    = GAME_ENDOFLEVEL_COUNTDOWN;
          gameAdvanceConveyor = -1;
          
          tGame.stopMediaSoundtrack();
          
        }
        
        returnVal = true;

      }
      
    }
    
    return returnVal;
    
  }
  
  function flyOutBeltComponent( spriteId ) {
    
    var sprite = vScreen.getSpriteById( spriteId );
    
    if ( !( sprite ) ) return;
    
    var spriteX = sprite.getXposTo();
    var spriteY = sprite.getYposTo();
    
    tGame.moveSprite(
                  sprite,
                  spriteX,
                  spriteY - ( GAME_GRID_OFFSET_Y + 2 ),
                  touchReleasedSteps,
                  GAME_MOVESPRITE_SINE_FLYOUT,
                  true );
    
  }

  function showPauseAndThink() {
    
    sysLogger.addLine( "show pause-and-think",
                       LOGGER_SOURCE_SEQUENCER );
    
    gameShowPausethinkOnNextDrop = false;
    if ( gamePausethinkIsShowing ) return;
    
    var sprite = new Sprite( FILE_IMAGE_PAUSETHINK,
                             GAME_PAUSETHINK_WIDTH,
                             GAME_PAUSETHINK_HEIGHT );
    
    sprite.setImgNode( document.getElementById( "bitmap" + ID_PAUSETHINK ) );
    vScreen.putSprite( sprite, ID_PAUSETHINK );
    sprite.show();
    sprite.move( 500, 740 );
        
    tGame.moveSprite(
            sprite,
            GAME_PAUSETHINK_XPOS,
            GAME_PAUSETHINK_YPOS,
            GAME_PAUSETHINK_SEQUENCECOUNT,
            GAME_MOVESPRITE_ZIGZAG_5 );
    
    gamePausethinkIsShowing = true;
    
    if ( tGame.getNewbieStatus().indexOf("p") < 0 ) {
      // first time ever; show help
      
      tGame.setSubState( 80 );
      
    }
    
  }
  
  function hidePauseAndThink() {
   
    sysLogger.addLine( "hide pause and think (if any)",
                       LOGGER_SOURCE_SEQUENCER );

    vScreen.removeSprite( ID_PAUSETHINK );
    gamePausethinkIsShowing = false;
    
  }
  
  function usePauseAndThink() {
        
    sysLogger.addLine( "use pause-and-think",
                       LOGGER_SOURCE_SEQUENCER );

    if ( checkConveyorIsEmpty() ) {
      
      sysLogger.addLine( "conveyor is empty? unexpected here",
                         LOGGER_SOURCE_SEQUENCER );
      
      hidePauseAndThink();
      return;
      
    }
    
    if ( holdConveyorAndWiggle ) {
      
      sysLogger.addLine( "conveyor is already stopped and letters are wiggling; ignore",
                         LOGGER_SOURCE_SEQUENCER );
      
      return;
      
    }
      
    var sprite  = vScreen.getSpriteById( ID_PAUSETHINK );
    var sWidth  = sprite.getWidth();
    var sHeight = sprite.getHeight();
    var newX    = Math.floor( sprite.getXposTo() - sWidth  / 2 );
    var newY    = Math.floor( sprite.getYposTo() - sHeight / 2 );
    
    sprite.setWidth(  Math.floor( sWidth  * 2 ) );
    sprite.setHeight( Math.floor( sHeight * 2 ) );
    sprite.show();
    sprite.move( newX, newY );
    
    tGame.moveSprite(
            sprite,
            newX,
            -( GAME_PAUSETHINK_HEIGHT * 2 ),
            movingScoreSteps,
            GAME_MOVESPRITE_SHOWANDOUT,
            true );

    gamePausethinkIsShowing = false;
    gamePausethinkNum--;
    if ( gamePausethinkNum > 0 ) gameShowPausethinkOnNextDrop = true;
    
    startWiggling();
    
  }

  function showRecycleLetterTile() {
    
    sysLogger.addLine( "show recycle letter tile icon",
                       LOGGER_SOURCE_SEQUENCER );
    
    if ( gameRecycleIsShowing ) return;
    
    var sprite = new Sprite( FILE_IMAGE_RECYCLE,
                             GAME_RECYCLE_WIDTH,
                             GAME_RECYCLE_HEIGHT );
    
    sprite.setImgNode( document.getElementById( "bitmap" + ID_RECYCLE ) );
    vScreen.putSprite( sprite, ID_RECYCLE );
    sprite.show();
    sprite.move( 500, 740 );
        
    tGame.moveSprite(
            sprite,
            GAME_RECYCLE_XPOS,
            GAME_RECYCLE_YPOS,
            GAME_RECYCLE_SEQUENCECOUNT,
            GAME_MOVESPRITE_ZIGZAG_5 );
    
    gameRecycleIsShowing = true;
    
    if ( tGame.getNewbieStatus().indexOf("r") < 0 ) {
      // first time ever; show help
      
      tGame.setSubState( 83 );
      
    }
    
  }
  
  function hideRecycleLetterTile() {
   
    sysLogger.addLine( "hide recycle letter tile icon (if any)",
                       LOGGER_SOURCE_SEQUENCER );

    vScreen.removeSprite( ID_RECYCLE );
    
    gameRecycleIsShowing  = false;
    gameRecycleLetterTile = LEVEL_TILE_NONE;
    
  }
  
  function useRecycleLetterTile() {
    
    sysLogger.addLine( "use recycle letter tile",
                       LOGGER_SOURCE_SEQUENCER );

    if ( checkConveyorIsEmpty() ) {
      
      sysLogger.addLine( "conveyor is empty? unexpected here",
                         LOGGER_SOURCE_SEQUENCER );
      
      hideRecycleLetterTile();
      return;
      
    }

    var sprite  = vScreen.getSpriteById( ID_RECYCLE );
    var sWidth  = sprite.getWidth();
    var sHeight = sprite.getHeight();
    var newX    = Math.floor( sprite.getXposTo() - sWidth  / 2 );
    var newY    = Math.floor( sprite.getYposTo() - sHeight / 2 );
    
    sprite.setWidth(  Math.floor( sWidth  * 2 ) );
    sprite.setHeight( Math.floor( sHeight * 2 ) );
    sprite.show();
    sprite.move( newX, newY );
    
    tGame.moveSprite(
            sprite,
            490,
            newY,
            movingScoreSteps,
            GAME_MOVESPRITE_SHOWANDOUT,
            true );
    
    gameRecycleIsShowing = false;
    gameRecycleNum--;

    startWiggling();
    
    sysLogger.addLine( "resurrect last letter tile '"
                       + gameRecycleLetterTile
                       + "'",
                       LOGGER_SOURCE_SEQUENCER );
    
    gameRaiseLid( tGame );
    tGame.setConveyorTile( 5, gameRecycleLetterTile );
    
    var spriteFilename =
        FILE_IMAGE_LTR_PREFIX
        + LEVEL_TILES[ gameRecycleLetterTile ][ 1 ]
        + FILE_IMAGE_LTR_SUFFIX;

    var imgId = tGame.getConveyorPosImg( 5 );
 
    sysLogger.addLine( "resurrected sprite file name '"
                       + spriteFilename
                       + "', imgId="
                       + imgId,
                       LOGGER_SOURCE_SEQUENCER );
    
    var sprite = new Sprite( spriteFilename, 70, 70 );
    
    sprite.setImgNode( document.getElementById( "bitmap" + imgId ) );
    vScreen.putSprite( sprite, imgId );
    sprite.show();
    sprite.move( CONVEYOR_TRASH_XPOS + 5, CONVEYOR_TRASH_YPOS );
  
    tGame.moveSprite(
        sprite,
        CONVEYOR_TRASH_XPOS + 5,
        CONVEYOR_CONVEYOR_YPOS - 70,
        touchReleasedSteps,
        GAME_MOVESPRITE_SINE_FLYIN );
    
  }
  
  function showShowNextFive() {
    
    sysLogger.addLine( "show 'show next five' icon",
                       LOGGER_SOURCE_SEQUENCER );
    
    if ( gameShowNextFiveIsShowing ) return;
    
    var sprite = new Sprite( FILE_IMAGE_SHOWNEXTFIVE,
                             GAME_SHOWNEXTFIVE_WIDTH,
                             GAME_SHOWNEXTFIVE_HEIGHT );
    
    sprite.setImgNode( document.getElementById( "bitmap" + ID_SHOWNEXTFIVE ) );
    vScreen.putSprite( sprite, ID_SHOWNEXTFIVE );
    sprite.show();
    sprite.move( 500, 740 );
        
    tGame.moveSprite(
            sprite,
            GAME_SHOWNEXTFIVE_XPOS,
            GAME_SHOWNEXTFIVE_YPOS,
            GAME_SHOWNEXTFIVE_SEQUENCECOUNT,
            GAME_MOVESPRITE_ZIGZAG_5 );
    
    gameShowNextFiveIsShowing = true;

  }
  
  function hideShowNextFive() {
    
    sysLogger.addLine( "hide 'show next five' icon (if any)",
                       LOGGER_SOURCE_SEQUENCER );

    vScreen.removeSprite( ID_SHOWNEXTFIVE );
    gameShowNextFiveIsShowing = false;

  }
  
  function useShowNextFive() {

    sysLogger.addLine( "use 'show next five' feature",
                       LOGGER_SOURCE_SEQUENCER );
    
    if ( gameShowNextFiveNum <=0 ) {
      
      sysLogger.addLine( "no 'show next five' available? unexpected here",
                       LOGGER_SOURCE_SEQUENCER );
      
      hideShowNextFive();
      gameShowNextFiveNum = 0;
      
      return;
      
    }
                    
    gameShowNextFiveNum--;
    
    if ( gameShowNextFiveNum == 0 ) {
      // this was the last available "show next five" feature
      
      hideShowNextFive();
      
    }
    
    // build screen
    gameShowNextFiveSequence = 0;
    tGame.setSubState( 40 );
    
  }
  
  function checkHoldConveyorTileCycleReset( lX, lY ) {

    if ( holdConveyorAndWiggle ) {
      // if conveyor is wiggling, all timers are resetScoreDataArray
      
      level.setTileDroppedSequence( lX,
                                    lY,
                                    gamePlayingSequence );

       sysLogger.addLine( "reset sequence of tile "
                          + lX
                          + ", "
                          + lY
                          + " to current game sequence "
                          + gamePlayingSequence
                          + ")",
                          LOGGER_SOURCE_SEQUENCER );
      
    }
    
  }
  
  function wiggleTileByTileId( tileId ) {
    // unless picked up, give this letter or grid tile
    // a gentle wiggle to indicate that it can still be moved
    
    var sprite = vScreen.getSpriteById( tileId );
    
    if ( sprite == null ) return;
    if ( sprite.getIsPickedUp() ) return;
    
    var spriteX = sprite.getXposTo();
    var spriteY = sprite.getYposTo();
    
    sprite.move( spriteX, spriteY - 6 );
    sprite.show();
    
    tGame.moveSprite(
                  sprite,
                  spriteX,
                  spriteY,
                  2,
                  GAME_MOVESPRITE_SINE_FLYOUT );
    
  }
  
  function checkWiggleMovableGridTile( gridCounter ) {

    if ( gridCounter >= LEVEL_GRID_MAX_X * LEVEL_GRID_MAX_Y ) return;
    
    var tileId = ID_GRID_START + gridCounter;
    var thisX  = gridCounter % LEVEL_GRID_MAX_X;
    var thisY  = Math.floor( gridCounter / LEVEL_GRID_MAX_X );
    
    if ( level.getTileIsLocked( thisX, thisY ) ) return;
    
    if ( !( level.getTilePlacedXY( thisX, thisY ) == LEVEL_TILE_NONE ) ) {
      
          wiggleTileByTileId( ID_LTR_START + gridCounter );          
          
    } else if ( LEVEL_GRID_ISMOVABLE[ level.getGridXY( thisX, thisY ) ] == true ) {
        
        wiggleTileByTileId( ID_GRID_START + gridCounter );
        
    }

  }

}
