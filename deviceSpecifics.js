// device / implementation specifics 2012-08-19
// prior to page load
// Copyright (C) 2012 Jens Koeplinger

var WHICH_OS_ANDROID        = "Android"; // Google Play
var WHICH_OS_APPSLIB        = "AppsLib";
var WHICH_OS_ANDROID_AMAZON = "AndroidAmazon";
var WHICH_OS_BLACKBERRY     = "BlackBerry";
var WHICH_OS_IOS            = "iOS";
var WHICH_OS_NOOK           = "Nook";
var WHICH_OS_WEB            = "web";

var RESOLUTION_REGULAR    = 0;
var RESOLUTION_HIRES      = 1;

// duplicate definitions (will be defined again in class JS):
var VERSION_FREE          = 0;
var VERSION_ADDICT        = 1;

var LOGGER_QUEUE_SYSTEM   = 0;
var LOGGER_QUEUE_STRING   = 1;
var LOGGER_QUEUE_DIV      = 2;
var LOGGER_QUEUE_VOID     = 3;
var LOGGER_QUEUE_DIVROLL  = 4;
// end of duplicate definitions

// everything above this line doesn't require device specific adjustment
// ---------------------------------------------------------------------

// this is version 1.0.9 (in preparation)

var whichOS                   = WHICH_OS_WEB;
var cyborgWordsResolution     = RESOLUTION_REGULAR;
var cyborgWordsVersion        = VERSION_ADDICT;
var SYSLOGGER_TYPE            = LOGGER_QUEUE_SYSTEM;
var TELELOGGER_TYPE           = LOGGER_QUEUE_SYSTEM;

// ---------------------------------------------------------------------
// everything below this line doesn't require device specific adjustment

var SCREEN_HEIGHT_DIFFCORRECT = 0;        // make screen smaller if wanted

// thresholds to make decision on simple graphics based on telemetry:
var TELEMETRY_WORDLIST_MINHIGHGRAPHICS = 200;
var TELEMETRY_RECOVERY_MINHIGHGRAPHICS = 1.02;

// sounds
var MEDIA_IS_SINGLE_THREADED = false;
var MEDIA_HAS_INCIDENTALS    = true;

switch ( whichOS ) {

  case WHICH_OS_NOOK:
  case WHICH_OS_APPSLIB:

    MEDIA_HAS_INCIDENTALS = false;
    // no break

  case WHICH_OS_BLACKBERRY:

    MEDIA_IS_SINGLE_THREADED = true;
    break;

}

var FILE_SOUND_PREFIX = "";
var FILE_SOUND_FORMAT = "";

switch ( whichOS ) {

  case WHICH_OS_ANDROID:
  case WHICH_OS_ANDROID_AMAZON:
  case WHICH_OS_NOOK:
  case WHICH_OS_APPSLIB:

    FILE_SOUND_PREFIX = "/android_asset/www/sndOGG/CyborgWords-";
    FILE_SOUND_FORMAT = ".ogg";
    break;

  default:

    FILE_SOUND_PREFIX = "sndMP3/CyborgWords-";
    FILE_SOUND_FORMAT = ".mp3";

}

// soundtrack files (format: "<location>@<loop length in seconds>")
var FILE_SOUND_STROLL        = FILE_SOUND_PREFIX + "01-Stroll" + FILE_SOUND_FORMAT + "@102";
var FILE_SOUND_MENU          = FILE_SOUND_PREFIX + "02-Play"   + FILE_SOUND_FORMAT + "@83";
var FILE_SOUND_PREPARELEVEL  = FILE_SOUND_PREFIX + "03-Build"  + FILE_SOUND_FORMAT + "@999";

// sound effects
var FILE_SOUND_COUNTDOWN     = FILE_SOUND_PREFIX + "Effect-countdown"  + FILE_SOUND_FORMAT;
var FILE_SOUND_CRISSCROSS    = FILE_SOUND_PREFIX + "Effect-crisscross" + FILE_SOUND_FORMAT;
var FILE_SOUND_DROP          = FILE_SOUND_PREFIX + "Effect-drop"       + FILE_SOUND_FORMAT;
var FILE_SOUND_PICKUP        = FILE_SOUND_PREFIX + "Effect-pickup"     + FILE_SOUND_FORMAT;
var FILE_SOUND_STAR0         = FILE_SOUND_PREFIX + "Effect-star0"      + FILE_SOUND_FORMAT;
var FILE_SOUND_STAR1         = FILE_SOUND_PREFIX + "Effect-star1"      + FILE_SOUND_FORMAT;
var FILE_SOUND_STAR2         = FILE_SOUND_PREFIX + "Effect-star2"      + FILE_SOUND_FORMAT;
var FILE_SOUND_STAR3         = FILE_SOUND_PREFIX + "Effect-star3"      + FILE_SOUND_FORMAT;
var FILE_SOUND_WORDLOCK      = FILE_SOUND_PREFIX + "Effect-wordlock"   + FILE_SOUND_FORMAT;
var FILE_SOUND_EXPERTBONUS   = FILE_SOUND_PREFIX + "Effect-crisscross" + FILE_SOUND_FORMAT;
var FILE_SOUND_EXPERTTICKS   = FILE_SOUND_PREFIX + "Effect-drop"       + FILE_SOUND_FORMAT;

var SCREEN_MORPH_MAX = 1.2;      // maximum distortion factor >= 1.0
if ( whichOS == WHICH_OS_WEB ) SCREEN_MORPH_MAX = 1.0;

// get package name for "purchase" button
var PACKAGE_NAME_BASE       = "com.dirtylittlecyborg.words"
var cyborgPackageName       = PACKAGE_NAME_BASE;
var cyborgPackageLikeMeName = PACKAGE_NAME_BASE;

switch ( cyborgWordsVersion ) {
  
  case VERSION_FREE:   cyborgPackageLikeMeName += ".free"; break;
  case VERSION_ADDICT: cyborgPackageLikeMeName += ".ace";  break;
  
}

switch ( cyborgWordsResolution ) {
  
  case RESOLUTION_REGULAR: cyborgPackageName += ".ace";      break;
  case RESOLUTION_HIRES  : cyborgPackageName += ".ace.hd";
                           cyborgPackageLikeMeName += ".hd"; break;

}

var STORE_PREFIX_ANDROID        = "market://details?id=";
var STORE_PREFIX_APPSLIB        = "market://details?id=";
var STORE_PREFIX_ANDROID_AMAZON = "amzn://apps/android?p=";
var STORE_PREFIX_IOS            = "itms-apps://itunes.com/apps/";
var STORE_PREFIX_NOOK           = "com.bn.sdk.shop.details";
var STORE_PREFIX_WEB            = "http://www.google.com/?q=";

var osRequiresStoreActionEan            = false;
var osStoreActionExtraValue             = null;
var osStoreActionExtraValueFree         = null;
var osStoreActionExtraValueLikeMe       = null;
var osRequiresStoreActionEanFailMessage = null;

if ( whichOS == WHICH_OS_NOOK ) {
  
  osRequiresStoreActionEan            = true;
  osStoreActionExtraValue             = "2940043918819"; // Nook Ace version EAN
  osStoreActionExtraValueFree         = "2940043918826"; // Nook Free version EAN;
  osRequiresStoreActionEanFailMessage =
                             "Please open 'NOOK Apps' and find\n"
                             + "us under 'Cyborg Words'.";
  
  if ( cyborgWordsVersion == VERSION_FREE ) { 
    
    osStoreActionExtraValueLikeMe = osStoreActionExtraValueFree;
    
  } else {
    
    osStoreActionExtraValueLikeMe = osStoreActionExtraValue;
    
  }
  
}

var osHasHDVersion = null;

switch ( whichOS ) {
  
  case WHICH_OS_ANDROID_AMAZON:
  case WHICH_OS_NOOK:
  case WHICH_OS_APPSLIB:

    osHasHDVersion = false;
    break;
  
  default:
    osHasHDVersion = true;
    
}

var STORE_LINK_URL = new Array();
STORE_LINK_URL[ WHICH_OS_ANDROID ]        = STORE_PREFIX_ANDROID        + cyborgPackageName;
STORE_LINK_URL[ WHICH_OS_APPSLIB ]        = STORE_PREFIX_APPSLIB        + cyborgPackageName;
STORE_LINK_URL[ WHICH_OS_ANDROID_AMAZON ] = STORE_PREFIX_ANDROID_AMAZON + cyborgPackageName;
STORE_LINK_URL[ WHICH_OS_IOS     ]        = STORE_PREFIX_IOS            + cyborgPackageName;
STORE_LINK_URL[ WHICH_OS_NOOK    ]        = STORE_PREFIX_NOOK;
STORE_LINK_URL[ WHICH_OS_WEB     ]        = STORE_PREFIX_WEB            + cyborgPackageName;

var STORE_LIKEME_URL = new Array();
STORE_LIKEME_URL[ WHICH_OS_ANDROID ]        = STORE_PREFIX_ANDROID        + cyborgPackageLikeMeName;
STORE_LIKEME_URL[ WHICH_OS_APPSLIB ]        = STORE_PREFIX_APPSLIB        + cyborgPackageLikeMeName;
STORE_LIKEME_URL[ WHICH_OS_ANDROID_AMAZON ] = STORE_PREFIX_ANDROID_AMAZON + cyborgPackageLikeMeName;
STORE_LIKEME_URL[ WHICH_OS_IOS     ]        = STORE_PREFIX_IOS            + cyborgPackageLikeMeName;
STORE_LIKEME_URL[ WHICH_OS_NOOK    ]        = STORE_PREFIX_NOOK;
STORE_LIKEME_URL[ WHICH_OS_WEB     ]        = STORE_PREFIX_WEB            + cyborgPackageLikeMeName;

var STORE_LINK_URL_FREE_REGULAR = new Array();
STORE_LINK_URL_FREE_REGULAR[ WHICH_OS_ANDROID ]        = STORE_PREFIX_ANDROID        + PACKAGE_NAME_BASE + ".free";
STORE_LINK_URL_FREE_REGULAR[ WHICH_OS_APPSLIB ]        = STORE_PREFIX_APPSLIB        + PACKAGE_NAME_BASE + ".free";
STORE_LINK_URL_FREE_REGULAR[ WHICH_OS_ANDROID_AMAZON ] = STORE_PREFIX_ANDROID_AMAZON + PACKAGE_NAME_BASE + ".free";
STORE_LINK_URL_FREE_REGULAR[ WHICH_OS_IOS     ]        = STORE_PREFIX_IOS            + PACKAGE_NAME_BASE + ".free";
STORE_LINK_URL_FREE_REGULAR[ WHICH_OS_NOOK    ]        = STORE_PREFIX_NOOK;
STORE_LINK_URL_FREE_REGULAR[ WHICH_OS_WEB     ]        = STORE_PREFIX_WEB            + PACKAGE_NAME_BASE + ".free";

var STORE_LINK_URL_FREE_HD      = new Array();
STORE_LINK_URL_FREE_HD[ WHICH_OS_ANDROID ] = STORE_PREFIX_ANDROID + PACKAGE_NAME_BASE + ".free.hd";
// note: as of 1 August 2012 there is no free HD Amazon Android app store version
// note: as of 5 August 2012 there is no free HD Nook Android app store version
// note: as of 6 August 2012 there is no free HD AppsLib Android app store version
STORE_LINK_URL_FREE_HD[ WHICH_OS_IOS     ] = STORE_PREFIX_IOS     + PACKAGE_NAME_BASE + ".free.hd";
STORE_LINK_URL_FREE_HD[ WHICH_OS_WEB     ] = STORE_PREFIX_WEB     + PACKAGE_NAME_BASE + ".free.hd";

var STORE_LINK_URL_ACE_REGULAR  = new Array();
STORE_LINK_URL_ACE_REGULAR[ WHICH_OS_ANDROID        ] = STORE_PREFIX_ANDROID        + PACKAGE_NAME_BASE + ".ace";
STORE_LINK_URL_ACE_REGULAR[ WHICH_OS_APPSLIB        ] = STORE_PREFIX_APPSLIB        + PACKAGE_NAME_BASE + ".ace";
STORE_LINK_URL_ACE_REGULAR[ WHICH_OS_ANDROID_AMAZON ] = STORE_PREFIX_ANDROID_AMAZON + PACKAGE_NAME_BASE + ".ace";
STORE_LINK_URL_ACE_REGULAR[ WHICH_OS_IOS            ] = STORE_PREFIX_IOS            + PACKAGE_NAME_BASE + ".ace";
STORE_LINK_URL_ACE_REGULAR[ WHICH_OS_NOOK           ] = STORE_PREFIX_NOOK;
STORE_LINK_URL_ACE_REGULAR[ WHICH_OS_WEB            ] = STORE_PREFIX_WEB            + PACKAGE_NAME_BASE + ".ace";

var STORE_LINK_URL_ACE_HD       = new Array();
STORE_LINK_URL_ACE_HD[ WHICH_OS_ANDROID ] = STORE_PREFIX_ANDROID + PACKAGE_NAME_BASE + ".ace.hd";
// note: as of 1 August 2012 there is no ace HD Amazon Android app store version
// note: as of 5 August 2012 there is no ace HD Nook Android app store version
// note: as of 6 August 2012 there is no ace HD AppsLib Android app store version
STORE_LINK_URL_ACE_HD[ WHICH_OS_IOS     ] = STORE_PREFIX_IOS     + PACKAGE_NAME_BASE + ".ace.hd";
STORE_LINK_URL_ACE_HD[ WHICH_OS_WEB     ] = STORE_PREFIX_WEB     + PACKAGE_NAME_BASE + ".ace.hd";

var NUMBER_COLORS_MAP = new Array();

NUMBER_COLORS_MAP[ "menuSelectLevel" ] = "weebairnazurk65p";
NUMBER_COLORS_MAP[ "menuSelectScore" ] = "weebairnazurk40p";
NUMBER_COLORS_MAP[ "gameLevel"       ] = "weebairnbrightwhite65p";
NUMBER_COLORS_MAP[ "gameScore"       ] = "weebairnbrightwhite65p";
NUMBER_COLORS_MAP[ "gameScoreFlyout" ] = "weebairnbrightyellow50p";
NUMBER_COLORS_MAP[ "gameCountdown"   ] = "weebairnbrightwhite65p";
NUMBER_COLORS_MAP[ "gameoverLevel"   ] = "weebairnazurk65p";
NUMBER_COLORS_MAP[ "gameoverScore"   ] = "weebairnazurk65p";

var isWebBrowser = false;
if ( whichOS == WHICH_OS_WEB ) isWebBrowser = true;

// device specific "open store" link execution
function deviceSpecificsOpenStore() {
  
  if ( osRequiresStoreActionEan ) {

    window.plugins.webintent.openEan(
      {
        intentAction: STORE_LINK_URL[ whichOS ],
        intentEan: osStoreActionExtraValue
      },
      function() {},
      function(e) {
        alert( osRequiresStoreActionEanFailMessage );
      }
    );

  } else {

    window.location.replace( STORE_LINK_URL[ whichOS ] );

  }
  
}

// device specific "like me" link execution
function deviceSpecificsOpenLikeMe() {
  
  if ( osRequiresStoreActionEan ) {

    window.plugins.webintent.openEan(
      {
        intentAction: STORE_LIKEME_URL[ whichOS ],
        intentEan: osStoreActionExtraValueLikeMe
      },
      function() {},
      function(e) {
        alert( osRequiresStoreActionEanFailMessage );
      }
    );

  } else {

    window.location.replace( STORE_LIKEME_URL[ whichOS ] );

  }
  
}
