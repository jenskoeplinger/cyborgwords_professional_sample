/*
 * 
 * THIS FILE HAS BEEN STRIPPED OF ACTUAL LEVEL DATA
 * AS I FEEL THIS IS PROPRIETARY AND WOULD PUT THE
 * FUN OF SOLVING THE ACTUAL PUZZLES AT RISK IF IT
 * WENT PUBLIC BY ACCIDENT. 27 AUGUST 2012
 * 
 * levelGetGrid.js 2012-07-30
 * Copyright (C) 2012 Jens Koeplinger
 *
 * see libLevel.js for gridType constants
 *
 */

LEVEL_HIGHESTLEVEL = 2;

function levelGetGrid( tLevel ) {
  // reads the level definitions into an
  // array and returns it

  function populateGridFromString( lString ) {

    var sGrid    = new Array();
    var sPos     = 0;
    var thisChar = null;

    for( var yPos = 0; yPos < LEVEL_GRID_MAX_X; yPos++ ) {

      for( var xPos = 0; xPos < LEVEL_GRID_MAX_Y; xPos++ ) {

        thisChar = lString.substring( sPos, sPos + 1 );
        sPos++;

        sGrid[ "" + xPos + "," + yPos ] = thisChar;

      }

    }

    return sGrid;

  }

  /*
   *
   */

  var lGrid = null;

  sysLogger.addLine( "levelGetGrid(" + tLevel.getNumber() + ")", LOGGER_SOURCE_LEVEL );

  switch ( tLevel.getNumber() ) {

    case 1:
    case 2:

      lGrid = populateGridFromString(
                "   r   "
              + "  rlr  "
              + " rrrrr "
              + "rlrwrlr"
              + " rrrrr "
              + "  rlr  "
              + "   r   " );

      tLevel.setTileSequence( "NOELZ LRANPOYUB MERHUNWAE" );
      tLevel.setMinimumPassScore(  80, 1 );
      tLevel.setMinimumPassScore( 160, 2 );
      tLevel.setMinimumPassScore( 300, 3 );
      
      if ( tLevel.getNumber() == 1 ) {
      
        tLevel.setNextLevelNumber( 2 );
        
      } else {
        
        tLevel.setNextLevelNumber( -1 );
        
      }

      break;

/*
 * 
 * HERE IS WHERE LEVELS 2 THROUGH 60 WOULD BE
 * 
 */
      
    default:
      // unknown level

      sysLogger.addLine(
        "unknown level: " + tLevel.getNumber() + ", fatal",
        LOGGER_SOURCE_LEVEL );

  }

  return lGrid;

}
