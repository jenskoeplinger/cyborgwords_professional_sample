/*
 * libLogger.js 2012-05-26
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New GameError(errorObj)
 *
 * GameError.getString() -- returns String
 *
 */

function GameError( errorObject ) {
  // constructor

  var errorString = null;

  addError( "message",     errorObject.message      );
  addError( "name",        errorObject.name         );
  addError( "description", errorObject.description  );
  addError( "number",      errorObject.number       );
  addError( "fileName",    errorObject.fileName     );
  addError( "lineNumber",  errorObject.lineNumber   );

  this.getString = function() { return errorString; }

  function addError( name, value ) {
    // convenience function to add errorObject properties
    // only if they exist

    if ( value ) {

      if ( errorString ) errorString += ", "; else errorString = "";

      errorString += name + ": [" + value + "] ";
      
    }
    
  }

}
