// device / implementation specifics 2012-08-19
// (post page load)
// Copyright (C) 2012 Jens Koeplinger

var PIC_FOLDER = null;

// menu icons and text
var FILE_MENU_BACK            = null;
var FILE_MENU_UP              = null;
var FILE_MENU_DOWN            = null;
var FILE_MENU_EXIT            = null;
var FILE_MENU_GAMETITLE_SMALL = null;
var FILE_MENU_LEVEL           = null;
var FILE_MENU_PLAY            = null;
var FILE_MENU_HIGHSCORE       = null;
var FILE_MENU_LEVELLOCKED     = null;
var FILE_MENU_LEVELPURCHASE   = null;
var FILE_MENU_STARS_1         = null;
var FILE_MENU_STARS_2         = null;
var FILE_MENU_STARS_3         = null;
var FILE_IMAGE_LIKEME         = null;

// level finish files
var FILE_FINISH_MAINMENU      = null;
var FILE_FINISH_FAIL          = null;
var FILE_FINISH_LEVEL         = null;
var FILE_FINISH_NEWHIGHSCORE  = null;
var FILE_FINISH_PASS          = null;
var FILE_FINISH_PLAYNEXT      = null;
var FILE_FINISH_RESULT        = null;
var FILE_FINISH_SCORE         = null;
var FILE_FINISH_TRYAGAIN      = null;
var FILE_FINISH_WANTHELP      = null;
var FILE_FINISH_EXPERTBONUS   = null;

// splash backgrounds
var FILE_SPLASH_LOADING                 = null;
var FILE_SPLASH_MENUBG_PREFIX           = null;
var FILE_SPLASH_GAMEBG_PREFIX           = null;
var FILE_SPLASH_GAMEBG_SUFFIX           = null;
var FILE_SPLASH_OPAQUE_50               = null;
var FILE_SPLASH_OPAQUE_ROUND_50         = null;
var FILE_SPLASH_BLACK                   = null;
var FILE_SPLASH_LEVELSELECT             = null;

// game image file location and type pointers
var FILE_IMAGE_LTR_PREFIX        = null;
var FILE_IMAGE_LTR_SUFFIX        = null;
var FILE_IMAGE_LTR_BLINK         = null;
var FILE_IMAGE_NUMBER_PREFIX     = null;
var FILE_IMAGE_NUMBER_SUFFIX     = null;
var FILE_IMAGE_GRID_PREFIX       = null;
var FILE_IMAGE_GRID_SUFFIX       = null;
var FILE_IMAGE_TRASHBIN          = null;
var FILE_IMAGE_TRASHBINLID       = null;
var FILE_IMAGE_LETTERSHOOT       = null;
var FILE_IMAGE_CONVEYORMAIN      = null;
var FILE_IMAGE_CONVEYORBELT      = null;
var FILE_IMAGE_CRISSCROSSBONUS   = null;
var FILE_IMAGE_EXIT              = null;
var FILE_IMAGE_KEEPPLAYING       = null;
var FILE_IMAGE_STARTLEVELOVER    = null;
var FILE_IMAGE_EXITGAME          = null;
var FILE_IMAGE_PAUSETHINK        = null;
var FILE_IMAGE_SHOWNEXTFIVE      = null;
var FILE_IMAGE_RECYCLE           = null;
var FILE_IMAGE_NEXT5LETTERSARE   = null;
var FILE_IMAGE_NOLETTER          = null;
var FILE_IMAGE_NEXT5OK           = null;
var FILE_IMAGE_INTRO_LEVEL       = null;
var FILE_IMAGE_SOUND_CYBORG      = null;
var FILE_IMAGE_SOUND_BUTTERFLY   = null;
var FILE_IMAGE_SOUND_CHURCHMOUSE = null;

// help and settings related graphics
var FILE_HELP_TAPPAUSETHINKG     = null;
var FILE_HELP_TAPRECYCLE         = null;
var FILE_SELECT_CHAOS            = null;
var FILE_SELECT_EXPERT           = null;
var FILE_SELECT_UNCHECKED        = null;
var FILE_SELECT_CHECKED          = null;
var FILE_SELECT_GAMEMODE         = null;
var FILE_SELECT_CHAOS_SAMPLE     = null;
var FILE_SELECT_EXPERT_SAMPLE    = null;
var FILE_SELECT_MAKEDEFAULT      = null;

var FILE_HELP_BODY_2                     = null;
var FILE_HELP_BODY_2_NO_HD               = null;
var FILE_HELP_BODY_3                     = null;
var FILE_HELP_BODY_4                     = null;
var FILE_HELP_BODY_5                     = null;
var FILE_HELP_BODY_6                     = null;
var FILE_HELP_BODY_7                     = null;
var FILE_HELP_BODY_8                     = null;
var FILE_HELP_BODY_9                     = null;
var FILE_HELP_BODY_10                    = null;
var FILE_HELP_SELECT_CHAOS_CHECKED       = null;
var FILE_HELP_SELECT_CHAOS_UNCHECKED     = null;
var FILE_HELP_SELECT_NODEFAULT_CHECKED   = null;
var FILE_HELP_SELECT_NODEFAULT_UNCHECKED = null;
var FILE_HELP_SELECT_EXPERT_CHECKED      = null;
var FILE_HELP_SELECT_EXPERT_UNCHECKED    = null;
var FILE_HELP_SUBLINK_CHAOS              = null;
var FILE_HELP_SUBLINK_CREDITS            = null;
var FILE_HELP_SUBLINK_HELP               = null;
var FILE_HELP_SUBLINK_LITTLEHELPERS      = null;
var FILE_HELP_SUBLINK_SETTINGS           = null;
var FILE_HELP_SUBLINK_STORE              = null;
var FILE_HELP_SUBLINK_TERMS              = null;
var FILE_HELP_SUBLINK_THANKS             = null;
var FILE_HELP_SUBLINK_TIPSTRICKS         = null;

function deviceSpecificsPostLoad() {

  sysLogger.addLine( "device specifics: post load" );
  
  // get icon library
  var tmpWindowSize = null;
  PIC_FOLDER        = "pic/outPix/";

  if ( window.innerHeight ) {
  
    tmpWindowSize = window.innerHeight + window.innerWidth;
    sysLogger.addLine( "use window.innerHeight and .innerWidth" );
  
  } else if ( screen.availHeight ) {
  
    tmpWindowSize = screen.availHeight + screen.availWidth
    sysLogger.addLine( "use screen.availHeight and .availWidth" );
  
  } else {
  
    tmpWindowSize = screen.height + screen.width;
    sysLogger.addLine( "use screen.height and .width" );

  }
  
  sysLogger.addLine( "tmpWindowSize is " + tmpWindowSize );
  
  if ( window.devicePixelRatio ) tmpWindowSize = tmpWindowSize * window.devicePixelRatio;
  
  sysLogger.addLine( "adjusted tmpWindowSize is " + tmpWindowSize );

  if (    ( whichOS == WHICH_OS_ANDROID )
       && ( cyborgWordsResolution == RESOLUTION_HIRES ) ) {
    // As of 29 July 2012 only one resolution is supported for
    // the hires version due to 50MBytes APK limitation.
    
    PIC_FOLDER += "65p/";
    sysLogger.addLine( "use 65p icons (Android hires)" );
    
  } else if ( whichOS == WHICH_OS_ANDROID ) {
    // As of 30 July 2012 only one resolution is supported for
    // the standard version due to customer feedback on APK size.
    
    PIC_FOLDER += "40p/";
    sysLogger.addLine( "use 40p icons (Android standard)" );
    
  } else if ( whichOS == WHICH_OS_ANDROID_AMAZON ) {
    // As of 31 July 2012 only one resolution is supported for Amazon Android store
    
    PIC_FOLDER += "40p/";
    sysLogger.addLine( "use 40p icons (Android Amazon standard)" );

  } else if ( whichOS == WHICH_OS_NOOK ) {
    // As of 5 August 2012 only one resolution is supported for Nook Android store
    
    PIC_FOLDER += "40p/";
    sysLogger.addLine( "use 40p icons (Nook Android standard)" );    

  } else if ( whichOS == WHICH_OS_APPSLIB ) {
    // As of 6 August 2012 only one resolution is supported for AppsLib Android store
    
    PIC_FOLDER += "40p/";
    sysLogger.addLine( "use 40p icons (AppsLib Android standard)" );

  } else if ( tmpWindowSize < 1200 ) {
  
    PIC_FOLDER += "20p/";
    sysLogger.addLine( "use 20p icons" );
  
  } else if ( tmpWindowSize < 2100 ) {
  
    PIC_FOLDER += "50p/";
    sysLogger.addLine( "use 50p icons" );
  
  } else {
  
    switch ( whichOS ) {
    
      case WHICH_OS_ANDROID:
        
        sysLogger.addLine( "high resolution Android device" );
      
        switch ( cyborgWordsResolution ) {
  
          case RESOLUTION_REGULAR: PIC_FOLDER += "50p/"; break; // note: not supported as of 30 July 2012
          case RESOLUTION_HIRES  : PIC_FOLDER += "80p/"; break; // note: not supported as of 27 July 2012

        }
      
        break;
      
      case WHICH_OS_IOS:
        
        sysLogger.addLine( "high resolution iOS device" );
      
        switch ( cyborgWordsResolution ) {
  
          case RESOLUTION_REGULAR: PIC_FOLDER += "50p/";  break;
          case RESOLUTION_HIRES  : PIC_FOLDER += "100p/"; break;

        }
      
        break;
      
      default:
        sysLogger.addLine( "other high resolution device" );
        PIC_FOLDER += "50p/";
    
    }
  
  }
  
  sysLogger.addLine( "picture folder is " + PIC_FOLDER );
  
  FILE_MENU_BACK            = PIC_FOLDER + "menuBack.png";
  FILE_MENU_UP              = PIC_FOLDER + "menuUp.png";
  FILE_MENU_DOWN            = PIC_FOLDER + "menuDown.png";
  FILE_MENU_EXIT            = PIC_FOLDER + "menuExit.png";
  FILE_MENU_GAMETITLE_SMALL = PIC_FOLDER + "menuGameTitleSmall.png";
  FILE_MENU_LEVEL           = PIC_FOLDER + "menuLevel.png";
  FILE_MENU_PLAY            = PIC_FOLDER + "menuPlay.png";
  FILE_MENU_HIGHSCORE       = PIC_FOLDER + "menuHighscore.png";
  FILE_MENU_LEVELLOCKED     = PIC_FOLDER + "menuLevelLocked.png";
  FILE_MENU_LEVELPURCHASE   = PIC_FOLDER + "menuGetProVersion.png";
  FILE_MENU_STARS_1         = PIC_FOLDER + "menuStars1.png";
  FILE_MENU_STARS_2         = PIC_FOLDER + "menuStars2.png";
  FILE_MENU_STARS_3         = PIC_FOLDER + "menuStars3.png";
  FILE_IMAGE_LIKEME         = PIC_FOLDER + "likeMe.png";

// level finish files
  FILE_FINISH_MAINMENU      = PIC_FOLDER + "finishExitToMainMenu.png";
  FILE_FINISH_FAIL          = PIC_FOLDER + "finishFail.png";
  FILE_FINISH_LEVEL         = PIC_FOLDER + "finishLevel.png";
  FILE_FINISH_NEWHIGHSCORE  = PIC_FOLDER + "finishNewHighscore.png";
  FILE_FINISH_PASS          = PIC_FOLDER + "finishPass.png";
  FILE_FINISH_PLAYNEXT      = PIC_FOLDER + "finishPlayNextLevel.png";
  FILE_FINISH_RESULT        = PIC_FOLDER + "finishResult.png";
  FILE_FINISH_SCORE         = PIC_FOLDER + "finishScore.png";
  FILE_FINISH_TRYAGAIN      = PIC_FOLDER + "finishTryLevelAgain.png";
  FILE_FINISH_WANTHELP      = PIC_FOLDER + "finishWantHelp.png";
  FILE_FINISH_EXPERTBONUS   = PIC_FOLDER + "finishExpertBonus.png";

// splash backgrounds
  FILE_SPLASH_LOADING                 = PIC_FOLDER + "splashLoading.png";
  FILE_SPLASH_MENUBG_PREFIX           = PIC_FOLDER + "splashMenuBg";
  FILE_SPLASH_GAMEBG_PREFIX           = PIC_FOLDER + "splashGameBg";
  FILE_SPLASH_GAMEBG_SUFFIX           = ".png";
  FILE_SPLASH_OPAQUE_50               = PIC_FOLDER + "splashOpaque50.png";
  FILE_SPLASH_OPAQUE_ROUND_50         = PIC_FOLDER + "splashOpaqueRound50.png";
  FILE_SPLASH_BLACK                   = PIC_FOLDER + "splashBlack.png";
  FILE_SPLASH_LEVELSELECT             = PIC_FOLDER + "splash2candymixbg2CharcoalAqua.png";

// game image file location and type pointers
  FILE_IMAGE_LTR_PREFIX        = PIC_FOLDER + "ltr";
  FILE_IMAGE_LTR_SUFFIX        = ".png";
  FILE_IMAGE_LTR_BLINK         = PIC_FOLDER + "ltrBlink.png";
  FILE_IMAGE_NUMBER_PREFIX     = PIC_FOLDER + "num";
  FILE_IMAGE_NUMBER_SUFFIX     = ".png";
  FILE_IMAGE_GRID_PREFIX       = PIC_FOLDER + "grid";
  FILE_IMAGE_GRID_SUFFIX       = ".png";
  FILE_IMAGE_TRASHBIN          = PIC_FOLDER + "trashBin.png";
  FILE_IMAGE_TRASHBINLID       = PIC_FOLDER + "trashBinLid.png";
  FILE_IMAGE_LETTERSHOOT       = PIC_FOLDER + "letterShoot.png";
  FILE_IMAGE_CONVEYORMAIN      = PIC_FOLDER + "conveyorMain.png";
  FILE_IMAGE_CONVEYORBELT      = PIC_FOLDER + "conveyorBelt.png";
  FILE_IMAGE_CRISSCROSSBONUS   = PIC_FOLDER + "crisscrossBonus.png";
  FILE_IMAGE_EXIT              = PIC_FOLDER + "gamePauseExit.png";
  FILE_IMAGE_KEEPPLAYING       = PIC_FOLDER + "gameKeepPlaying.png";
  FILE_IMAGE_STARTLEVELOVER    = PIC_FOLDER + "gameStartLevelOver.png";
  FILE_IMAGE_EXITGAME          = PIC_FOLDER + "gameExitGame.png";
  FILE_IMAGE_PAUSETHINK        = PIC_FOLDER + "gamePausethink.png";
  FILE_IMAGE_SHOWNEXTFIVE      = PIC_FOLDER + "gameShowNextFive.png";
  FILE_IMAGE_RECYCLE           = PIC_FOLDER + "gameRecycle.png";
  FILE_IMAGE_NEXT5LETTERSARE   = PIC_FOLDER + "gameNextFiveLettersAre.png";
  FILE_IMAGE_NOLETTER          = PIC_FOLDER + "gameNoLetter.png";
  FILE_IMAGE_NEXT5OK           = PIC_FOLDER + "gameNextFiveOk.png";
  FILE_IMAGE_INTRO_LEVEL       = PIC_FOLDER + "gameFileIntroLevel.png";
  FILE_IMAGE_SOUND_CYBORG      = PIC_FOLDER + "soundcyborg.png";
  FILE_IMAGE_SOUND_BUTTERFLY   = PIC_FOLDER + "soundbutterfly.png";
  FILE_IMAGE_SOUND_CHURCHMOUSE = PIC_FOLDER + "soundchurchmouse.png";

  // help and settings related graphics
  FILE_HELP_TAPPAUSETHINKG     = PIC_FOLDER + "helpTapPauseThink.png";
  FILE_HELP_TAPRECYCLE         = PIC_FOLDER + "helpTapRecycle.png";
  FILE_SELECT_CHAOS            = PIC_FOLDER + "selectChaos.png";
  FILE_SELECT_EXPERT           = PIC_FOLDER + "selectExpert.png";
  FILE_SELECT_UNCHECKED        = PIC_FOLDER + "selectUnchecked.png";
  FILE_SELECT_CHECKED          = PIC_FOLDER + "selectChecked.png";
  FILE_SELECT_GAMEMODE         = PIC_FOLDER + "selectGamemode.png";
  FILE_SELECT_CHAOS_SAMPLE     = PIC_FOLDER + "selectChaosSample.png";
  FILE_SELECT_EXPERT_SAMPLE    = PIC_FOLDER + "selectExpertSample.png";
  FILE_SELECT_MAKEDEFAULT      = PIC_FOLDER + "selectMakeDefault.png";

  FILE_HELP_BACKGROUND_DEFAULT         = PIC_FOLDER + "splashHelpBg.png";
  
  FILE_HELP_BODY_1                     = null;
  
  if ( whichOS == WHICH_OS_IOS ) {
    // show versions page with "retina" wording
    
    FILE_HELP_BODY_2                     = PIC_FOLDER + "helpBody2ios.png";
    
  } else {
    
    FILE_HELP_BODY_2                     = PIC_FOLDER + "helpBody2.png";
    FILE_HELP_BODY_2_NO_HD               = PIC_FOLDER + "helpBody2nohd.png";
    
  }
  
  FILE_HELP_BODY_3                     = PIC_FOLDER + "helpBody3.png";
  FILE_HELP_BODY_4                     = PIC_FOLDER + "helpBody4.png";
  FILE_HELP_BODY_5                     = PIC_FOLDER + "helpBody5.png";
  FILE_HELP_BODY_6                     = PIC_FOLDER + "helpBody6.png";
  FILE_HELP_BODY_7                     = PIC_FOLDER + "helpBody7.png";
  FILE_HELP_BODY_8                     = PIC_FOLDER + "helpBody8.png";
  FILE_HELP_BODY_9                     = PIC_FOLDER + "helpBody9.png";
  FILE_HELP_BODY_10                    = PIC_FOLDER + "helpBody10.png";
  FILE_HELP_SELECT_CHAOS_CHECKED       = PIC_FOLDER + "helpSelectChaosChecked.png";
  FILE_HELP_SELECT_CHAOS_UNCHECKED     = PIC_FOLDER + "helpSelectChaosUnchecked.png";
  FILE_HELP_SELECT_NODEFAULT_CHECKED   = PIC_FOLDER + "helpSelectClearDefaultChecked.png";
  FILE_HELP_SELECT_NODEFAULT_UNCHECKED = PIC_FOLDER + "helpSelectClearDefaultUnchecked.png";
  FILE_HELP_SELECT_EXPERT_CHECKED      = PIC_FOLDER + "helpSelectExpertChecked.png";
  FILE_HELP_SELECT_EXPERT_UNCHECKED    = PIC_FOLDER + "helpSelectExpertUnchecked.png";
  FILE_HELP_SUBLINK_CHAOS              = PIC_FOLDER + "helpSubChaos.png";
  FILE_HELP_SUBLINK_CREDITS            = PIC_FOLDER + "helpSubCredits.png";
  FILE_HELP_SUBLINK_HELP               = PIC_FOLDER + "helpSubHelp.png";
  FILE_HELP_SUBLINK_LITTLEHELPERS      = PIC_FOLDER + "helpSubLittle.png";
  FILE_HELP_SUBLINK_SETTINGS           = PIC_FOLDER + "helpSubSettings.png";
  FILE_HELP_SUBLINK_STORE              = PIC_FOLDER + "helpSubStore.png";
  FILE_HELP_SUBLINK_TERMS              = PIC_FOLDER + "helpSubTerms.png";
  FILE_HELP_SUBLINK_THANKS             = PIC_FOLDER + "helpSubThanks.png";
  FILE_HELP_SUBLINK_TIPSTRICKS         = PIC_FOLDER + "helpSubTips.png";

}
