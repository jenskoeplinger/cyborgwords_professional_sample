/*
 * gameMoveConveyor.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 */

var CONVEYOR_HEAVYDEBUG = true;

function gameMoveConveyor( tGame, moveUpToId ) {
  // moves tiles to next position; tile animation, conveyor animation

  sysLogger.addLine( "gameMoveConveyor()", LOGGER_SOURCE_SEQUENCER );
  if ( moveUpToId ) sysLogger.addLine( "up to ID: " + moveUpToId, LOGGER_SOURCE_SEQUENCER );

  var movingSSteps = 7;
  if ( tGame.getGraphicsComplexity() == GAME_COMPLEXITY_SIMPLE )
      movingSSteps = 4;
  
  // conveyor animation
  
  var sprite = vScreen.getSpriteById( ID_CONVEYORBELT );
  
  sprite.move( CONVEYOR_CONVEYOR_XPOS + 11, CONVEYOR_CONVEYOR_YPOS );

  tGame.moveSprite(
      sprite,
      CONVEYOR_CONVEYOR_XPOS + 19,
      CONVEYOR_CONVEYOR_YPOS,
      movingSSteps,
      GAME_MOVESPRITE_ZIGZAG );
  
  // move letters to the right
  
  var ltrTile  = null;
  var tileImg  = null;
  var idsUsed  = new Array(); // bitmap IDs usage array

  // see whether to move all or only part of the tiles
  var moveThis = true;
  if ( moveUpToId ) moveThis = false;

  if ( CONVEYOR_HEAVYDEBUG )
      sysLogger.addLine( "begin moveThis="
                         + moveThis
                         + ", moveUpToId="
                         + moveUpToId,
                         LOGGER_SOURCE_SEQUENCER );

  for ( var tilePos = 5; tilePos >= 1; tilePos-- ) {

    if ( !( moveThis ) ) {

      if ( moveUpToId == tGame.getConveyorPosImg( tilePos ) ) {
        // start moving

        moveThis = true;

      } else {

        idsUsed[ tGame.getConveyorPosImg( tilePos ) ] = 1;

        if ( CONVEYOR_HEAVYDEBUG )
            sysLogger.addLine( "don't move tilePos="
                               + tilePos
                               + ", imgId="
                               + tGame.getConveyorPosImg( tilePos ),
                               LOGGER_SOURCE_SEQUENCER );

      }

    }
    
    if ( moveThis ) {

      ltrTile = tGame.getConveyorTile( tilePos - 1 );
      tGame.setConveyorTile( tilePos, ltrTile );
    
      tileImg = tGame.getConveyorPosImg( tilePos - 1 );
      tGame.setConveyorPosImg( tilePos, tileImg );
      idsUsed[ tileImg ] = 1;

      if ( CONVEYOR_HEAVYDEBUG )
          sysLogger.addLine( "tilePos="
                             + tilePos
                             + ", ltrTile="
                             + ltrTile
                             + ", tileImg="
                             + tileImg,
                             LOGGER_SOURCE_SEQUENCER );

      if ( !( ltrTile == LEVEL_TILE_NONE ) ) {
    
        sprite = vScreen.getSpriteById( tileImg );
      
        tGame.moveSprite(
            sprite,
            CONVEYOR_LETTER0_XPOS + tilePos * 70,
            CONVEYOR_CONVEYOR_YPOS - 70,
            movingSSteps,
            GAME_MOVESPRITE_SINE );
      
      } else {

        vScreen.removeSprite( tGame.getConveyorPosImg( tilePos ) );

      }

    }
    
  }

  // remove first sprite from screen (if any)
  tGame.setConveyorTile( 0, LEVEL_TILE_NONE );

  // find the free conveyor image id
  for ( var i = ID_LTR_BELTSTART; i < ( ID_LTR_BELTSTART + 6 ); i++ ) {

    if ( !( idsUsed[ i ] ) ) tileImg = i;

  }

  if ( CONVEYOR_HEAVYDEBUG )
      sysLogger.addLine( "position 0 tileImg="
                         + tileImg,
                         LOGGER_SOURCE_SEQUENCER );

  tGame.setConveyorPosImg( 0, tileImg );
  vScreen.removeSprite( tileImg );
  
}
