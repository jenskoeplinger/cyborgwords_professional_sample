/*
 * gameSelectGamemode.js 2012-07-29
 * Copyright (C) 2012 Jens Koeplinger
 * 
 */

var SELECT_GAMEMODE_SELECT_X     = 30;
var SELECT_GAMEMODE_SELECT_Y     = 100;
var SELECT_GAMEMODE_BUTTONS_X    = 30;
var SELECT_GAMEMODE_SAMPLES_X    = 192;
var SELECT_GAMEMODE_CHAOS_Y      = 205;
var SELECT_GAMEMODE_EXPERT_Y     = 355;
var SELECT_GAMEMODE_CHECK_Y      = 550;
var SELECT_GAMEMODE_TOGGLEOFFSET = 31;

var selectGamemodeToggle = null;

function gameSelectGamemode( tGame ) {
  // constructor

  var movingLSteps = 4;
  
  switch ( tGame.getSubState() ) {

    case 0: 

      sysLogger.addLine( "gameSelectGamemode()", LOGGER_SOURCE_SEQUENCER );
      
      for ( var i = 0; i < MAX_SPRITES; i++ ) {
        
        vScreen.removeSprite( i );
        
      }
      
      tGame.setSubState( 1 );
      
      break;
      
    case 1:
      
      var sprite = new Sprite( FILE_SELECT_GAMEMODE,
                               414,
                               58 );
            
      sprite.setImgNode(
              document.getElementById( "bitmap" + 1 ) );
      vScreen.putSprite( sprite, 1 );
      sprite.show();
      sprite.move( 490, SELECT_GAMEMODE_SELECT_Y );

      tGame.moveSprite(
                sprite,
                SELECT_GAMEMODE_SELECT_X,
                SELECT_GAMEMODE_SELECT_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );

      tGame.setSubState( 2 );
      
      break;
      
    case 2:

      var sprite = new Sprite( FILE_SELECT_CHAOS_SAMPLE,
                               267,
                               133 );
            
      sprite.setImgNode(
              document.getElementById( "bitmap" + 3 ) );
      vScreen.putSprite( sprite, 3 );
      sprite.show();
      sprite.move( SELECT_GAMEMODE_SAMPLES_X - 490, SELECT_GAMEMODE_CHAOS_Y );

      tGame.moveSprite(
                sprite,
                SELECT_GAMEMODE_SAMPLES_X,
                SELECT_GAMEMODE_CHAOS_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );
      
      tGame.setSubState( 3 );
      
      break;
      
    case 3:
      
      var sprite = new Sprite( FILE_SELECT_CHAOS,
                               133,
                               133 );
            
      sprite.setImgNode(
              document.getElementById( "bitmap" + 2 ) );
      vScreen.putSprite( sprite, 2 );
      sprite.show();
      sprite.move( SELECT_GAMEMODE_BUTTONS_X - 490, SELECT_GAMEMODE_CHAOS_Y );

      tGame.moveSprite(
                sprite,
                SELECT_GAMEMODE_BUTTONS_X,
                SELECT_GAMEMODE_CHAOS_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );
      
      tGame.setSubState( 4 );
      
      break;
      
    case 4:

      var sprite = new Sprite( FILE_SELECT_EXPERT,
                               133,
                               133 );
            
      sprite.setImgNode(
              document.getElementById( "bitmap" + 4 ) );
      vScreen.putSprite( sprite, 4 );
      sprite.show();
      sprite.move( SELECT_GAMEMODE_BUTTONS_X + 490, SELECT_GAMEMODE_EXPERT_Y );

      tGame.moveSprite(
                sprite,
                SELECT_GAMEMODE_BUTTONS_X,
                SELECT_GAMEMODE_EXPERT_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );

      tGame.setSubState( 5 );
      
      break;
      
    case 5:
      
      var sprite = new Sprite( FILE_SELECT_EXPERT_SAMPLE,
                               267,
                               133 );
            
      sprite.setImgNode(
              document.getElementById( "bitmap" + 5 ) );
      vScreen.putSprite( sprite, 5 );
      sprite.show();
      sprite.move( SELECT_GAMEMODE_SAMPLES_X + 490, SELECT_GAMEMODE_EXPERT_Y );

      tGame.moveSprite(
                sprite,
                SELECT_GAMEMODE_SAMPLES_X,
                SELECT_GAMEMODE_EXPERT_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );
            
      tGame.setSubState( 6 );
      
      break;
      
    case 6:

      var sprite = new Sprite( FILE_SELECT_MAKEDEFAULT,
                               310,
                               59 );
            
      sprite.setImgNode(
              document.getElementById( "bitmap" + 7 ) );
      vScreen.putSprite( sprite, 7 );
      sprite.show();
      sprite.move( SELECT_GAMEMODE_SAMPLES_X - 525, SELECT_GAMEMODE_CHECK_Y );

      tGame.moveSprite(
                sprite,
                SELECT_GAMEMODE_SAMPLES_X - 35,
                SELECT_GAMEMODE_CHECK_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );
      
      tGame.setSubState( 7 );
      
      break;
      
    case 7:
      
      var sprite = new Sprite( FILE_SELECT_UNCHECKED,
                               65,
                               65 );
      
      sprite.setImgNode(
              document.getElementById( "bitmap" + 6 ) );
      vScreen.putSprite( sprite, 6 );
      sprite.show();
      sprite.move( SELECT_GAMEMODE_BUTTONS_X - 459, SELECT_GAMEMODE_CHECK_Y );

      tGame.moveSprite(
                sprite,
                SELECT_GAMEMODE_BUTTONS_X + SELECT_GAMEMODE_TOGGLEOFFSET,
                SELECT_GAMEMODE_CHECK_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYIN );
      
      tGame.setSubState( 8 );
      
      break;
      
    case 8: // prepare for touch
      
      selectGamemodeToggle = false;
      touch.setTouchOccurred( false );
      
      tGame.setSubState( 20 );
      
      break;
      
    case 20: // wait for touch

      if ( touch.getTouchOccurred() && ( !( touch.getIsTouching() ) ) ) {

        var thisX = touch.getTouchX();
        var thisY = touch.getTouchY();
        touch.setTouchOccurred( false );

        if ( thisX < ( SELECT_GAMEMODE_SAMPLES_X ) ) {
          
          if (    ( thisY > SELECT_GAMEMODE_CHAOS_Y )
               && ( thisY < SELECT_GAMEMODE_EXPERT_Y ) ) {

            gameScoringModel = SCORING_MODEL_CHAOS;
            tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
            tGame.setSubState( 80 );
            
          }
          
          if (    ( thisY > SELECT_GAMEMODE_EXPERT_Y )
               && ( thisY < SELECT_GAMEMODE_CHECK_Y ) ) {

            gameScoringModel = SCORING_MODEL_CLEAN;
            tGame.playMediaEffect( FILE_SOUND_PICKUP );
          
            tGame.setSubState( 80 );
            
          }
          
          if ( thisY > SELECT_GAMEMODE_CHECK_Y ) {
            
            selectGamemodeToggle = !( selectGamemodeToggle );
            var toggleFileName = null;
            
            if ( selectGamemodeToggle ) {
              
              toggleFileName = FILE_SELECT_CHECKED;
              
            } else {
              
              toggleFileName = FILE_SELECT_UNCHECKED;
              
            }
            
            var sprite = vScreen.getSpriteById( 6 );
            sprite.show( toggleFileName );
            
            tGame.playMediaEffect( FILE_SOUND_PICKUP );
            
          }
          
        }
        
      }
      
      break;
      
    case 80: // fly out

      vScreen.removeSprite( 3 );
      vScreen.removeSprite( 5 );
      
      var spriteToMove = null;

      if ( gameScoringModel == SCORING_MODEL_CLEAN ) {
        
        vScreen.removeSprite( 2 );
        spriteToMove = vScreen.getSpriteById( 4 );
        
      } else {
        
        vScreen.removeSprite( 4 );
        spriteToMove = vScreen.getSpriteById( 2 );
        
      }
      
      tGame.moveSprite(
                spriteToMove,
                178,
                ( SELECT_GAMEMODE_CHAOS_Y + SELECT_GAMEMODE_EXPERT_Y ) / 2,
                movingLSteps * 2,
                GAME_MOVESPRITE_SINE );
      
      tGame.setSubState( 81 );
      
    case 81:
    case 82:
    case 83:
    case 84:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 85:

      var spriteToMove = vScreen.getSpriteById( 1 );
      
      tGame.moveSprite(
                spriteToMove,
                SELECT_GAMEMODE_SELECT_X - 490,
                SELECT_GAMEMODE_SELECT_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 86:
      
      var spriteToMove = vScreen.getSpriteById( 7 );
      
      tGame.moveSprite(
                spriteToMove,
                SELECT_GAMEMODE_SAMPLES_X + 490,
                SELECT_GAMEMODE_CHECK_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 87:
      
      var spriteToMove = vScreen.getSpriteById( 6 );
      
      tGame.moveSprite(
                spriteToMove,
                490,
                SELECT_GAMEMODE_CHECK_Y,
                movingLSteps,
                GAME_MOVESPRITE_SINE_FLYOUT,
                true );
      
    case 88:
    case 89:
    case 90:
      
      tGame.setSubState( tGame.getSubState() + 1 );
      break;
      
    case 91: // save and move on

      if ( selectGamemodeToggle ) tGame.setGamemodeDefault( gameScoringModel );
      
      vScreen.removeSprite( 2 );
      vScreen.removeSprite( 4 );
      
      tGame.setState( GAME_LEVEL_PREPARING );
      tGame.setSubState( 0 );

      break;

  }
  
}
