/*
 * libScreen.js 2012-07-29
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Screen(int virtualWidth, int virtualHeight, double maxAspectMorphRatio)
 *
 * Screen.getAspectMorphRatio() -- returns double
 * Screen.getNativeHeight() -- returns int
 * Screen.getNativeWidth() -- returns int
 * Screen.getNativeXOffset() -- returns int
 * Screen.getNativeYOffset() -- returns int
 * Screen.getSpriteById(String id) -- returns Sprite
 * Screen.getSprites() -- returns Sprite[]
 * Screen.getXScale() -- returns double
 * Screen.getYScale() -- returns double
 * Screen.putSprite(Sprite sprite, String id)
 * Screen.removeSprite(String id)
 * Screen.removeSpriteObject(Sprite sprite)
 * 
 */

function Screen( virtualWidth, virtualHeight, maxAspectMorphRatio ) {
  // constructor: gets the native screen resolution (x and y)
  // and calculates offset and scaling factors to make the
  // screen area as big as possible, but not morphing beyond a
  // certain factor

  var nativeHeight     = null;
  var nativeWidth      = null;
 
  if ( window.innerHeight ) {
 
    nativeWidth  = window.innerWidth;
    nativeHeight = window.innerHeight;
   
    sysLogger.addLine( "window.innerHeight found; native width: "
                       + nativeWidth
                       + ", height: "
                       + nativeHeight );

  } else if ( screen.availHeight ) {
 
    nativeWidth  = screen.availWidth;
    nativeHeight = screen.availHeight;

    sysLogger.addLine( "screen.availHeight found; native width: "
                       + nativeWidth
                       + ", height: "
                       + nativeHeight );

  } else {
 
    nativeWidth  = screen.width;
    nativeHeight = screen.height;

    sysLogger.addLine( "screen.height used; native width: "
                       + nativeWidth
                       + ", height: "
                       + nativeHeight );

  }
 
  sysLogger.addLine( "virtual width: "
                     + virtualWidth
                     + ", height: "
                     + virtualHeight );
  
  sysLogger.addLine( "max aspect morph ratio requested: " + maxAspectMorphRatio );

  var nativePixelRatio = 1;

  if ( window.devicePixelRatio ) {
 
    nativePixelRatio = window.devicePixelRatio;
    sysLogger.addLine( "window.devicePixelRatio found: " + nativePixelRatio );

    // note: value not used as of 14 July 2012
 
  }
 
  if ( !( SCREEN_HEIGHT_DIFFCORRECT == 0 ) ) {

    sysLogger.addLine( "add Y offset per configuration: " + SCREEN_HEIGHT_DIFFCORRECT );
    nativeHeight += SCREEN_HEIGHT_DIFFCORRECT;
   
  }

  var xScale = nativeWidth  / virtualWidth;
  var yScale = nativeHeight / virtualHeight;
  var nativeXOffset = 0;
  var nativeYOffset = 0;

  var nativeAspect     = nativeWidth  / nativeHeight;
  var virtualAspect    = virtualWidth / virtualHeight;
  var aspectVariance   = nativeAspect / virtualAspect;
  var aspectMorphRatio = -1;

  if ( aspectVariance > 1 ) {
    // native screen is wider than the requested virtual aspect

    if ( aspectVariance > maxAspectMorphRatio ) {
      // don't morph to fill the screen, instead insert an x offset
      
      aspectMorphRatio = maxAspectMorphRatio;
      xScale = yScale * maxAspectMorphRatio;
      nativeXOffset = ( nativeWidth - ( virtualWidth * xScale ) ) / 2;

    } else {

      aspectMorphRatio = aspectVariance;

    }

  }

  if ( aspectVariance < 1 ) {
    // native screen is higher than the requested virtual aspect

    if ( ( 1 / aspectVariance ) > maxAspectMorphRatio ) {
      // don't morph to fill the screen, instead insert an y offset
      
      aspectMorphRatio = maxAspectMorphRatio;
      yScale = xScale * maxAspectMorphRatio;
      nativeYOffset = ( nativeHeight - ( virtualHeight * yScale ) ) / 2;

    } else {

      aspectMorphRatio = 1 / aspectVariance;

    }

  }

  sysLogger.addLine( "effective morph ratio: " + aspectMorphRatio );
  sysLogger.addLine( "x offset: " + nativeXOffset + ", scale: " + xScale );
  sysLogger.addLine( "y offset: " + nativeYOffset + ", scale: " + yScale );

  var sprites = new Array();

  this.getAspectMorphRatio = function()          { return aspectMorphRatio;    }
  this.getNativeHeight     = function()          { return nativeHeight;        }
  this.getNativeWidth      = function()          { return nativeWidth;         }
  this.getNativeXOffset    = function()          { return nativeXOffset;       }
  this.getNativeYOffset    = function()          { return nativeYOffset;       }
  this.getSpriteById       = function( spriteId) { return sprites[ spriteId ]; }
  this.getSprites          = function()          { return sprites;             }
  this.getXScale           = function()          { return xScale;              }
  this.getYScale           = function()          { return yScale;              }

  this.putSprite = function( sprite, spriteId ) {
    // Take the incoming Sprite object and associate
    // it with the screen scaling and offset

    sprites[ spriteId ] = sprite;
    sprite.setScreen( this );
    sprite.setId( spriteId );

  }

  this.removeSprite = function( spriteId ) {
    // clears the sprite from the Screen array

    var sprite = sprites[ spriteId ];
    if ( sprite ) {
      // sprite object still exists; dissociate screen

      sprite.hide();
      sprite.setScreen( null );
      sprite.setId( null );
      sprites[ spriteId ] = null;

    }

  }
  
  this.removeSpriteObject = function( sprite ) {
   
    if ( !( sprite ) ) return;
    this.removeSprite( sprite.getId() );
    
  }

}
