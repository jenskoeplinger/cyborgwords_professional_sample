/*
 * libSprite.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Sprite( String filename, int width, int height ) 
 * 
 * Sprite.getFilename() -- returns String
 * Sprite.getHeight() -- returns int
 * Sprite.getId() -- returns String
 * Sprite.getIsMoving() -- returns boolean
 * Sprite.getIsPickedUp() -- returns boolean
 * Sprite.getImgNode() -- returns jsDOM.IMG
 * Sprite.getScreen() -- returns Screen
 * Sprite.getWidth() -- returns int
 * Sprite.getXpos() -- return int
 * Sprite.getXposTo() -- return int
 * Sprite.getXposPickedUp() -- return int
 * Sprite.getYpos() -- return int
 * Sprite.getYposTo() -- return int
 * Sprite.getYposPickedUp() -- return int
 * Sprite.hide()
 * Sprite.move(int xpos [ , int ypos ] [, boolean isPickUp ] )
 * Sprite.setHeight(int height)
 * Sprite.setId(String id)
 * Sprite.setImgNode(jsDOM.IMG node)
 * Sprite.setIsMoving(boolean isMoving)
 * Sprite.setIsPickedUp(boolean isPickedUp)
 * Sprite.setScreen(Screen screen)
 * Sprite.setWidth(int width)
 * Sprite.show( [string newFileName ] )
 * 
 */

function Sprite(fileName, sWidthNew, sHeightNew) {
  // constructor: load file and read virtual width and height

  var sWidth        = sWidthNew; // sprite virtual width and height
  var sHeight       = sHeightNew;
  var sFileName     = fileName;
  var sId           = null;
  var sImgNode      = null;
  var sNativeXpos   = 0; // sprite position in native screen coordinates
  var sNativeYpos   = 0;
  var sXpos         = 0;
  var sXposPickedUp = 0;
  var sXposTo       = 0;
  var sYpos         = 0;
  var sYposPickedUp = 0;
  var sYposTo       = 0;
  var sScreen       = null; // reference to parent Screen object

  var sIsMoving   = false;
    // Set my Game.moveSprite to indicate that the game object
    // is currently moving the sprite to a new position during
    // Game.action() calls. If Sprite.move() is called then this
    // flag is cleared to false so that Game.action() will not
    // attempt to continue to move it along its old path.
  
  var isPickedUp  = false;
    // When the sprite is picked up, its intended position is
    // controlled by Game or MovingSprite, however, it will not
    // be redrawn unless the isPickUp flag in Sprite.move()
    // is set.

  this.getFilename     = function()          { return sFileName;       }
  this.getHeight       = function()          { return sHeight;         }
  this.setHeight       = function( cValue )  { sHeight = cValue;       }
  this.getId           = function()          { return sId;             }
  this.setId           = function( cValue )  { sId = cValue;           }
  this.getIsMoving     = function()          { return sIsMoving;       }
  this.setIsMoving     = function( cValue )  { sIsMoving = cValue;     }
  this.getScreen       = function()          { return sScreen;         }
  this.setScreen       = function( cValue )  { sScreen = cValue;       }
  this.getWidth        = function()          { return sWidth;          }
  this.setWidth        = function( cValue )  { sWidth = cValue;        }
  this.getXpos         = function()          { return sXpos;           }
  this.getXposPickedUp = function()          { return sXposPickedUp;   }
  this.getXposTo       = function()          { return sXposTo;         }
  this.setXposTo       = function( cValue )  { sXposTo = cValue;       }
  this.getYpos         = function()          { return sYpos;           }
  this.getYposPickedUp = function()          { return sYposPickedUp;   }
  this.getYposTo       = function()          { return sYposTo;         }
  this.setYposTo       = function( cValue )  { sYposTo = cValue;       }

  this.getImgNode = function() {

    return sImgNode;

  }

  this.setImgNode = function( cValue ) {
    // sets the image node; also prepares some of its style attributes;
    // initial style is hidden

    sImgNode = cValue;
    sImgNode.style.position = "absolute";
    this.hide();

  }
  
  this.hide = function() {
    // if existing, sets the IMG node visibility to false
    
    if ( sImgNode ) {

      sImgNode.style.display = "none";

    }

  }

  this.show = function ( newFileName ) {
    // set the IMG style to visible; also sets its
    // x and y position

    if ( newFileName ) sFileName = newFileName;
    
    if ( sImgNode ) {

      sImgNode.src               = sFileName;
      sImgNode.style.display     = "block";
      sImgNode.style.width       = Math.floor ( sWidth  * sScreen.getXScale() + 0.5 ) + "px";
      sImgNode.style.height      = Math.floor ( sHeight * sScreen.getYScale() + 0.5 ) + "px";
      
      if ( isPickedUp ) {
        
        sImgNode.style.zIndex      = ID_ZINDEX_PICKUP;
        
      } else {
      
        sImgNode.style.zIndex      = ID_ZINDEX_DEFAULT;
        
      }
      
    }

  }

  this.getIsPickedUp = function() {
    
    return isPickedUp;
    
  }
  
  this.setIsPickedUp = function( cValue ) {
    
    isPickedUp = cValue;
    
    if ( sImgNode ) {
      
      if ( isPickedUp ) {
        
        sImgNode.style.zIndex = ID_ZINDEX_PICKUP;
        
      } else {
        
        sImgNode.style.zIndex = ID_ZINDEX_DEFAULT;
        
      }
      
    }
    
  }
  
  this.move = function( vXpos, vYpos, isPickUp ) {
    // moves the sprite to the given virtual coordinates

    if ( vYpos == null ) vYpos = sYpos;
    
    // temporary ("t") position variables
    var tXpos = vXpos;
    var tYpos = vYpos;
    var tNativeXpos = null;
    var tNativeYpos = null;
    
    tNativeXpos = vXpos * sScreen.getXScale() + sScreen.getNativeXOffset();
    tNativeXpos = Math.floor( tNativeXpos + 0.5 );
    
    tNativeYpos = vYpos * sScreen.getYScale() + sScreen.getNativeYOffset();
    tNativeYpos = Math.floor( tNativeYpos + 0.5 );

    if ( ( isPickUp ) || ( !( this.getIsPickedUp() ) ) ) {
    
      sImgNode.style.left = tNativeXpos + "px";
      sImgNode.style.top  = tNativeYpos + "px";
      
    }
        
    if ( isPickUp ) {
      // Sprite move() was called from the handler of the picked-up
      // sprite. Make sure it's flagged picked up but don't update
      // the position(s) on where it is actually supposed to be
      
      sXposPickedUp = vXpos;
      sYposPickedUp = vYpos;
      
      this.setIsPickedUp( true );
      
      return;
      
    }
    
    // update the sprite's properties of where it is supposed to be
    // (sXpos/sYpos are the actual drawn virtual coordinates,
    // sXposTo/sYposTo are the final coordinates if currently moving)
    
    sXpos = vXpos;
    sYpos = vYpos;
    sNativeXpos = tNativeXpos;
    sNativeYpos = tNativeYpos;
    
    if ( !( sIsMoving ) ) {
      // sprite is not being moved automatically;
      // current x/y position on screen is the final position
      
      sXposTo = sXpos;
      sYposTo = sYpos;
      
    }
    
    // ensure Game.action() does not move it as MovingSprite unless intended
    sIsMoving = false;
    
  }

}
