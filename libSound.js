/*
 * libSound.js 2012-08-05
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Sound(String fileName)
 * 
 * Sound.play()
 * Sound.stop()
 * Sound.unload()
 * 
 */

/*
 * 
 */

function Sound( fileName ) {
  // Constructor: create new Sound object from
  // fileName into resourceIndex.
  
  var thisMedia       = null;
  var mFileName       = fileName;
  
  if ( !( isWebBrowser ) ) {
    // only load and play sounds when using phonegap

    thisMedia = new Media( mFileName, mediaSuccessCallback, mediaErrorCallback );
    
  }
    
  this.play = function() {
    // play the sound

    sysLogger.addLine("play sound: " + mFileName, LOGGER_SOURCE_SOUND);
    
    if ( !( isWebBrowser ) ) {
      
      if ( thisMedia ) {
      
        thisMedia.play();
        sysLogger.addLine("done", LOGGER_SOURCE_SOUND);
	
      } else {
       
        sysLogger.addLine("media null? unexpected", LOGGER_SOURCE_SOUND);
        
      }
      
    } else {
     
      sysLogger.addLine("(web browser without sound)", LOGGER_SOURCE_SOUND);
      
    }
    
  }
  
  this.stop = function() {
    // stops playing the sound
    
    if ( !( isWebBrowser ) ) {
      
      if ( thisMedia ) {
	
	thisMedia.stop();
	
      }
      
    }
    
  }
  
  this.unload = function() {
    // frees up the media resource
    
    if ( !( isWebBrowser ) ) {
      
      if ( thisMedia ) {
	
	thisMedia.stop();
	thisMedia.release();
	
      }
      
    }
    
  }
  
}

/*
 * 
 */

function mediaSuccessCallback() {
  // callback function for successful sound load
  // (global due to platform limitation; do not call directly).
  // The scope of "this" is the Media object that triggers the callback.
  
  if ( sysLogger ) {
    // write error to log

    sysLogger.addLine("mediaSuccessCallback", LOGGER_SOURCE_SOUND);

  }
    
}

/*
 * 
 */

function mediaErrorCallback( mediaError ) {
  // callback function for sound error
  
  if ( sysLogger ) {
    // write error to log

    sysLogger.addLine("mediaErrorCallback error " + mediaError, LOGGER_SOURCE_SOUND);

  }
  
}
