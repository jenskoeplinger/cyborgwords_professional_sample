/*
 * libLevel.js 2012-07-30
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Level()
 * 
 * Level.clearGrid()
 * Level.getAdvanceTimer() -- returns int
 * Level.getNumber() -- returns int
 * Level.getNextLevelNumber() -- returns int
 * Level.getNextTile() -- returns String
 * Level.getBgPicture() -- returns String
 * Level.getFutureTile( int offset ) -- returns String
 * Level.getGrid() -- returns char[][]
 * Level.getGridXY( int xPos, int yPos ) -- returns char
 * Level.getMinimumPassScore( [ int numStars ] ) -- returns int
 * Level.getTileDroppedSequence( int xPos, int yPos ) -- return int
 * Level.getTileDroppedTimeout() -- return int
 * Level.getTileIsLocked( int xPos, int yPos ) -- returns boolean
 * Level.getTileSequence() -- returns String
 * Level.getTilesPlaced() -- returns char[][]
 * Level.getTilePlacedXY( int xPos, int yPos ) -- returns String
 * Level.getStarsFilename( int score ) -- returns String
 * Level.loadGrid( int levelNumber )
 * Level.returnRandomLetters( int howMany ) -- returns String
 * Level.setAdvanceTimer( int timerCycles )
 * Level.setGridXY( int xPos, int yPos, char gridType )
 * Level.setMinimumPassScore( int score [, int numStars ] )
 * Level.setNextLevelNumber( int levelNumber )
 * Level.setTileDroppedSequence( int xPos, int yPos, int gameSequenceOnDrop )
 * Level.setTileDroppedTimeout( int tileTimeout )
 * Level.setTileIsLocked( int xPos, int yPos, boolean isLocked )
 * Level.setTilePlacedXY( int xPos, int yPos, String ltr )
 * Level.setTileSequence( String tileSequence )
 *
 */

// Grid variables:

var LEVEL_GRID_MAX_X = 7;
var LEVEL_GRID_MAX_Y = 7;

var VERSION_FREE          = 0;
var VERSION_ADDICT        = 1;
var VERSION_FREE_MAXLEVEL = 1; // MODIFIED FOR PROFESSIONAL SAMPLE

var LEVEL_GRID_BACKDROP_FILE = null; // initialized in constructor

var LEVEL_GRID_TYPE_NONE        = " ";
var LEVEL_GRID_TYPE_REGULAR     = "r";
var LEVEL_GRID_TYPE_MOVABLE     = "R";
var LEVEL_GRID_TYPE_REGULAR_Wx2 = "w"; // word multiplier x2
var LEVEL_GRID_TYPE_MOVABLE_Wx2 = "W";
var LEVEL_GRID_TYPE_REGULAR_Wx3 = "x"; // word multiplier x3
var LEVEL_GRID_TYPE_MOVABLE_Wx3 = "X";
var LEVEL_GRID_TYPE_REGULAR_Lx2 = "l"; // letter multiplier x2
var LEVEL_GRID_TYPE_MOVABLE_Lx2 = "L";
var LEVEL_GRID_TYPE_REGULAR_Lx3 = "m"; // letter multiplier x3
var LEVEL_GRID_TYPE_MOVABLE_Lx3 = "M";

// (arrays are initialized in constructor)
var LEVEL_GRID_FILES            = new Array();
var LEVEL_GRID_WORDMULTS        = new Array();
var LEVEL_GRID_LTRMULTS         = new Array();
var LEVEL_GRID_ISMOVABLE        = new Array();
var LEVEL_GRID_ENCODE           = new Array();
var LEVEL_GRID_DECODE           = new Array();

var LEVEL_MULTS_MAX             = 3;

// Tiles placed variables:

var LEVEL_TILE_NONE = ".";

// LEVEL_TILES[a][b] is a two dimensional array
// - [a] is the letter as it is stored in Level.getTilesPlaced()
// - [b] is:
// - - 0: the literal letter in the wList#[] arrays for word matching
// - - 1: the picture file name identifier, e.g., $ in pic/ltr$.png
// - - 2: the letter point score
// - - 3: the letter word multiplier

var LEVEL_TILES = new Array();

LEVEL_TILES[ "A" ] = [ "a", "A",  1, 1 ];
LEVEL_TILES[ "B" ] = [ "b", "B",  4, 1 ]; // compare to Scrabble: 3
LEVEL_TILES[ "C" ] = [ "c", "C",  4, 1 ]; // compare to Scrabble: 3
LEVEL_TILES[ "D" ] = [ "d", "D",  2, 1 ];
LEVEL_TILES[ "E" ] = [ "e", "E",  1, 1 ];
LEVEL_TILES[ "F" ] = [ "f", "F",  4, 1 ];
LEVEL_TILES[ "G" ] = [ "g", "G",  3, 1 ]; // compare to Scrabble: 2
LEVEL_TILES[ "H" ] = [ "h", "H",  4, 1 ];
LEVEL_TILES[ "I" ] = [ "i", "I",  1, 1 ];
LEVEL_TILES[ "J" ] = [ "j", "J",  9, 1 ]; // compare to Scrabble: 8
LEVEL_TILES[ "K" ] = [ "k", "K",  6, 1 ]; // compare to Scrabble: 5
LEVEL_TILES[ "L" ] = [ "l", "L",  2, 1 ]; // compare to Scrabble: 1
LEVEL_TILES[ "M" ] = [ "m", "M",  4, 1 ];
LEVEL_TILES[ "N" ] = [ "n", "N",  1, 1 ];
LEVEL_TILES[ "O" ] = [ "o", "O",  1, 1 ];
LEVEL_TILES[ "P" ] = [ "p", "P",  4, 1 ]; // compare to Scrabble: 4
LEVEL_TILES[ "Q" ] = [ "q", "Q", 12, 1 ]; // compare to Scrabble: 10
LEVEL_TILES[ "R" ] = [ "r", "R",  1, 1 ];
LEVEL_TILES[ "S" ] = [ "s", "S",  1, 1 ];
LEVEL_TILES[ "T" ] = [ "t", "T",  1, 1 ];
LEVEL_TILES[ "U" ] = [ "u", "U",  2, 1 ]; // compare to Scrabble: 2
LEVEL_TILES[ "V" ] = [ "v", "V",  6, 1 ]; // compare to Scrabble: 4
LEVEL_TILES[ "W" ] = [ "w", "W",  5, 1 ]; // compare to Scrabble: 4
LEVEL_TILES[ "X" ] = [ "x", "X",  9, 1 ]; // compare to Scrabble: 8
LEVEL_TILES[ "Y" ] = [ "y", "Y",  5, 1 ]; // compare to Scrabble: 4
LEVEL_TILES[ "Z" ] = [ "z", "Z", 11, 1 ]; // compare to Scrabble: 3

// letter tiles with built-in word multiplier x2
// (must be at ASCII offset +32 as compared to single multiplier)

LEVEL_TILES[ "a" ] = [ "a", "AWx2",  1, 2 ];
LEVEL_TILES[ "b" ] = [ "b", "BWx2",  4, 2 ];
LEVEL_TILES[ "c" ] = [ "c", "CWx2",  4, 2 ];
LEVEL_TILES[ "d" ] = [ "d", "DWx2",  2, 2 ];
LEVEL_TILES[ "e" ] = [ "e", "EWx2",  1, 2 ];
LEVEL_TILES[ "f" ] = [ "f", "FWx2",  4, 2 ];
LEVEL_TILES[ "g" ] = [ "g", "GWx2",  3, 2 ];
LEVEL_TILES[ "h" ] = [ "h", "HWx2",  4, 2 ];
LEVEL_TILES[ "i" ] = [ "i", "IWx2",  1, 2 ];
LEVEL_TILES[ "j" ] = [ "j", "JWx2",  9, 2 ];
LEVEL_TILES[ "k" ] = [ "k", "KWx2",  6, 2 ];
LEVEL_TILES[ "l" ] = [ "l", "LWx2",  2, 2 ];
LEVEL_TILES[ "m" ] = [ "m", "MWx2",  4, 2 ];
LEVEL_TILES[ "n" ] = [ "n", "NWx2",  1, 2 ];
LEVEL_TILES[ "o" ] = [ "o", "OWx2",  1, 2 ];
LEVEL_TILES[ "p" ] = [ "p", "PWx2",  4, 2 ];
LEVEL_TILES[ "q" ] = [ "q", "QWx2", 12, 2 ];
LEVEL_TILES[ "r" ] = [ "r", "RWx2",  1, 2 ];
LEVEL_TILES[ "s" ] = [ "s", "SWx2",  1, 2 ];
LEVEL_TILES[ "t" ] = [ "t", "TWx2",  1, 2 ];
LEVEL_TILES[ "u" ] = [ "u", "UWx2",  2, 2 ];
LEVEL_TILES[ "v" ] = [ "v", "VWx2",  6, 2 ];
LEVEL_TILES[ "w" ] = [ "w", "WWx2",  5, 2 ];
LEVEL_TILES[ "x" ] = [ "x", "XWx2",  9, 2 ];
LEVEL_TILES[ "y" ] = [ "y", "YWx2",  5, 2 ];
LEVEL_TILES[ "z" ] = [ "z", "ZWx2", 11, 2 ];

// the blank tile must be replaced with a
// scored letter tile upon placement:
LEVEL_TILES[ " " ] = [ "-", "Blank", 0, 1 ];

// other
var LEVEL_ADVANCETIMER_DEFAULT       = 100; // advance letters after 100 cycles (5s) by default
var LEVEL_TILEDROPPEDTIMEOUT_DEFAULT = 400; // if no word after 20s, move letter to trash

var LEVEL_BACKGROUND_COUNT           = 9;
var LEVEL_BACKGROUNDS                = new Array();

LEVEL_BACKGROUNDS[ 0 ] = "3silkyelectriccopy";
LEVEL_BACKGROUNDS[ 1 ] = "3silkyorangecopy";
LEVEL_BACKGROUNDS[ 2 ] = "3drseuss";
LEVEL_BACKGROUNDS[ 3 ] = "SublimeGrayEnhRed";
LEVEL_BACKGROUNDS[ 4 ] = "2sublimegrayEnh";
LEVEL_BACKGROUNDS[ 5 ] = "2hippybluebgEnh";
LEVEL_BACKGROUNDS[ 6 ] = "Red2";
LEVEL_BACKGROUNDS[ 7 ] = "Blue2";
LEVEL_BACKGROUNDS[ 8 ] = "Yellow2";

/*
 *
 */

function Level() {
  // constructor: load grid into array
  
  var lNumber             = null;
  var lNextLevelNumber    = null;
  var lGrid               = null;
  var lTileSequence       = null;
  var lTilesPlaced        = null;
  var lTilesIsLocked      = null; // lock tiles once they're part of a word
  var lTilesDroppedSeq    = null; // array of game sequence counters at time of drop
  var lNextTile           = null;
  var lMinimumPassScore   = new Array();
  var lAdvanceTimer       = LEVEL_ADVANCETIMER_DEFAULT;
  var lTileDroppedTimeout = LEVEL_TILEDROPPEDTIMEOUT_DEFAULT;
  var lLevelBackground    = null;

  sysLogger.addLine( "new Level()", LOGGER_SOURCE_LEVEL );

  // initialize variables with picture locations 
  LEVEL_GRID_BACKDROP_FILE
    = FILE_IMAGE_GRID_PREFIX + "WordBackdrop" + FILE_IMAGE_GRID_SUFFIX;

  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_NONE    ] = "00";

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_REGULAR ]
    = FILE_IMAGE_GRID_PREFIX + "Pod" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_REGULAR ] = 1;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_REGULAR ] = 1;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_REGULAR ] = false;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_REGULAR ] = "0100";
  LEVEL_GRID_DECODE[    "0100"                  ] = LEVEL_GRID_TYPE_REGULAR;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_MOVABLE ]
    = FILE_IMAGE_GRID_PREFIX + "PodMovable" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_MOVABLE ] = 1;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_MOVABLE ] = 1;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_MOVABLE ] = true;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_MOVABLE ] = "0101";
  LEVEL_GRID_DECODE[    "0101"                  ] = LEVEL_GRID_TYPE_MOVABLE;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_REGULAR_Wx2 ]
    = FILE_IMAGE_GRID_PREFIX + "PodWx2" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_REGULAR_Wx2 ] = 2;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_REGULAR_Wx2 ] = 1;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_REGULAR_Wx2 ] = false;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_REGULAR_Wx2 ] = "0110";
  LEVEL_GRID_DECODE[    "0110"                      ] = LEVEL_GRID_TYPE_REGULAR_Wx2;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_MOVABLE_Wx2 ]
    = FILE_IMAGE_GRID_PREFIX + "PodMovableWx2" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_MOVABLE_Wx2 ] = 2;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_MOVABLE_Wx2 ] = 1;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_MOVABLE_Wx2 ] = true;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_MOVABLE_Wx2 ] = "0111";
  LEVEL_GRID_DECODE[    "0111"                      ] = LEVEL_GRID_TYPE_MOVABLE_Wx2;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_REGULAR_Wx3 ]
    = FILE_IMAGE_GRID_PREFIX + "PodWx3" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_REGULAR_Wx3 ] = 3;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_REGULAR_Wx3 ] = 1;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_REGULAR_Wx3 ] = false;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_REGULAR_Wx3 ] = "1000";
  LEVEL_GRID_DECODE[    "1000"                      ] = LEVEL_GRID_TYPE_REGULAR_Wx3;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_MOVABLE_Wx3 ]
    = FILE_IMAGE_GRID_PREFIX + "PodMovableWx3" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_MOVABLE_Wx3 ] = 3;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_MOVABLE_Wx3 ] = 1;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_MOVABLE_Wx3 ] = true;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_MOVABLE_Wx3 ] = "1001";
  LEVEL_GRID_DECODE[    "1001"                      ] = LEVEL_GRID_TYPE_MOVABLE_Wx3;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_REGULAR_Lx2 ]
    = FILE_IMAGE_GRID_PREFIX + "PodLx2" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_REGULAR_Lx2 ] = 1;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_REGULAR_Lx2 ] = 2;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_REGULAR_Lx2 ] = false;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_REGULAR_Lx2 ] = "1010";
  LEVEL_GRID_DECODE[    "1010"                      ] = LEVEL_GRID_TYPE_REGULAR_Lx2;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_MOVABLE_Lx2 ]
    = FILE_IMAGE_GRID_PREFIX + "PodMovableLx2" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_MOVABLE_Lx2 ] = 1;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_MOVABLE_Lx2 ] = 2;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_MOVABLE_Lx2 ] = true;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_MOVABLE_Lx2 ] = "1011";
  LEVEL_GRID_DECODE[    "1011"                      ] = LEVEL_GRID_TYPE_MOVABLE_Lx2;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_REGULAR_Lx3 ]
    = FILE_IMAGE_GRID_PREFIX + "PodLx3" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_REGULAR_Lx3 ] = 1;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_REGULAR_Lx3 ] = 3;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_REGULAR_Lx3 ] = false;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_REGULAR_Lx3 ] = "1100";
  LEVEL_GRID_DECODE[    "1100"                      ] = LEVEL_GRID_TYPE_REGULAR_Lx3;

  LEVEL_GRID_FILES[     LEVEL_GRID_TYPE_MOVABLE_Lx3 ]
    = FILE_IMAGE_GRID_PREFIX + "PodMovableLx3" + FILE_IMAGE_GRID_SUFFIX;
  LEVEL_GRID_WORDMULTS[ LEVEL_GRID_TYPE_MOVABLE_Lx3 ] = 1;
  LEVEL_GRID_LTRMULTS[  LEVEL_GRID_TYPE_MOVABLE_Lx3 ] = 3;
  LEVEL_GRID_ISMOVABLE[ LEVEL_GRID_TYPE_MOVABLE_Lx3 ] = true;
  LEVEL_GRID_ENCODE[    LEVEL_GRID_TYPE_MOVABLE_Lx3 ] = "1101";
  LEVEL_GRID_DECODE[    "1101"                      ] = LEVEL_GRID_TYPE_MOVABLE_Lx3;
  
  // methods
  this.getAdvanceTimer       = function()         { return lAdvanceTimer;          }
  this.setAdvanceTimer       = function( cValue ) { lAdvanceTimer = cValue;        }
  this.getNumber             = function()         { return lNumber;                }
  this.getNextLevelNumber    = function()         { return lNextLevelNumber;       }
  this.setNextLevelNumber    = function( cValue ) { lNextLevelNumber = cValue;     } 
  this.getGrid               = function()         { return lGrid;                  }
  this.getTileDroppedTimeout = function()         { return lTileDroppedTimeout;    }
  this.setTileDroppedTimeout = function( cValue ) { lTileDroppedTimeout = cValue;  }
  this.getTilesPlaced        = function()         { return lTilesPlaced;           }

  lMinimumPassScore[ 1 ] = 100;
  lMinimumPassScore[ 2 ] = 200;
  lMinimumPassScore[ 3 ] = 300;

  this.getMinimumPassScore = function( numStars ) {
    
    if ( !( numStars) ) numStars = 1;
    return lMinimumPassScore[ numStars ];
    
  }
  
  this.setMinimumPassScore = function( scoreValue, numStars ) {
    
    if ( !( numStars) ) numStars = 1;
    lMinimumPassScore[ numStars ] = scoreValue;
    
  }

  this.getTileSequence = function() {
    
    return lTileSequence;
    
  }
  
  this.setTileSequence = function( cValue ) {
    // sets the tile sequence for this level; also
    // resets the getNextTile index counter to 0.
    
    lTileSequence = cValue;
    lNextTile = 0;
    
  }
  
  this.getNextTile = function() {
   
    var returnString = null;
    
    if ( lNextTile >= lTileSequence.length ) {
      
      returnString = LEVEL_TILE_NONE;
      
    } else {
    
      returnString = lTileSequence.substring( lNextTile, lNextTile + 1 );
      lNextTile++;
    
    }
    
    return returnString;
    
  }
  
  this.getFutureTile = function( tileOffset ) {
    
    var returnString = null;
    
    if ( ( tileOffset == null ) || ( tileOffset < 0 ) ) return LEVEL_TILE_NONE;
    
    if ( ( lNextTile + tileOffset ) >= lTileSequence.length ) {
      
      returnString = LEVEL_TILE_NONE;
      
    } else {
    
      returnString = lTileSequence.substring(
                         lNextTile + tileOffset,
                         lNextTile + tileOffset + 1 );
    
    }
    
    return returnString;
    
  }
  
  this.getBgPicture = function() {
    // returns a background picture;
    // uses lLevelBackground if set, otherwise level number mod count

    var bgFile = null;
    
    if ( lLevelBackground )
      bgFile = LEVEL_BACKGROUNDS[ lLevelBackground ];
    else
      bgFile = LEVEL_BACKGROUNDS[ ( lNumber - 1 ) % LEVEL_BACKGROUND_COUNT ];
    
    var bgPicture = FILE_SPLASH_GAMEBG_PREFIX + bgFile + FILE_SPLASH_GAMEBG_SUFFIX;
    return bgPicture;

  }

  this.loadGrid = function( levelNumber ) {

    lNumber = levelNumber;
    lGrid = levelGetGrid( this );
    this.clearGrid();

  }

  this.getGridXY = function( xPos, yPos ) {

    return lGrid[ "" + xPos + "," + yPos ];

  }

  this.setGridXY = function( xPos, yPos, gridType ) {

    lGrid[ "" + xPos + "," + yPos ] = gridType;

  }

  this.getTilePlacedXY = function ( xPos, yPos ) {

    return lTilesPlaced[ "" + xPos + "," + yPos ];

  }

  this.setTilePlacedXY = function ( xPos, yPos, ltr ) {

    lTilesPlaced[ "" + xPos + "," + yPos ] = ltr;

  }

  this.getTileIsLocked = function ( xPos, yPos ) {

    return lTilesIsLocked[ "" + xPos + "," + yPos ];

  }

  this.setTileIsLocked = function ( xPos, yPos, isLocked ) {

    lTilesIsLocked[ "" + xPos + "," + yPos ] = isLocked;

  }

  this.getTileDroppedSequence = function ( xPos, yPos ) {

    return lTilesDroppedSeq[ "" + xPos + "," + yPos ];

  }

  this.setTileDroppedSequence = function ( xPos, yPos, sequenceOnDrop ) {

    lTilesDroppedSeq[ "" + xPos + "," + yPos ] = sequenceOnDrop;

  }

  this.clearGrid = function( inScreen, clearGrid ) {
    // clears all placed tiles (also initializes
    // the lTilesPlaced[], lTilesDroppedSeq[], and lTilesIsLocked[] arrays);
    // if inScreen is provided then removes letter tile sprites from screen also;
    // if clearGrid is true then removes grid tile sprites from screen also

   lTilesPlaced     = new Array();
   lTilesDroppedSeq = new Array();
   lTilesIsLocked   = new Array();

   var xyString     = null;

    for (var i = 0; i < LEVEL_GRID_MAX_X; i++) {

      for (var j = 0; j < LEVEL_GRID_MAX_Y; j++) {

        xyString = "" + i + "," + j;

        lTilesPlaced[     xyString ] = LEVEL_TILE_NONE;
        lTilesDroppedSeq[ xyString ] = -1;
        lTilesIsLocked[   xyString ] = false;

        if ( inScreen ) {
          // screen object provided; remove sprite

          var spriteId = ID_LTR_START + i + j * LEVEL_GRID_MAX_X;
          inScreen.removeSprite( spriteId );

          if ( clearGrid ) {
            // delete grid tile sprites

            spriteId = ID_GRID_START + i + j * LEVEL_GRID_MAX_X;
            inScreen.removeSprite( spriteId );

          }

        }

      }

    }

  }

  this.returnRandomLetters = function( howMany ) {

    var tilePool =
        "AAAAAAAAAB"
      + "BBCCDDDDEE"
      + "EEEEEEEEEE"
      + "FFGGGHHIII"
      + "IIIIIJJKKL"
      + "LLLMMMNNNN"
      + "NNNOOOOOOO"
      + "PPQRRRRRRS"
      + "SSSSTTTTTT"
      + "UUUUVVWWXY"
      + "YZ   "; // compare to Scrabble's 100 tiles

    var tilePoolLength = 105;
    var returnString = "";
    var letterPos = null;

    if ( !( howMany ) ) howMany = 1;

    for ( var i = howMany; i > 0; i-- ) {

      letterPos = Math.floor( Math.random() * tilePoolLength );
      returnString += tilePool.substring( letterPos, letterPos + 1 );

    }

    return returnString;

  }
  
  this.getStarsFilename = function( scoreNum ) {
   
    if ( scoreNum >= lMinimumPassScore[ 3 ] ) return FILE_MENU_STARS_3;
    if ( scoreNum >= lMinimumPassScore[ 2 ] ) return FILE_MENU_STARS_2;
    if ( scoreNum >= lMinimumPassScore[ 1 ] ) return FILE_MENU_STARS_1;
    
    return null;
    
  }

}
