/*
 * libGame.js 2012-08-19
 * Copyright (C) 2012 Jens Koeplinger
 *
 * New Game()
 *
 * Game.action()
 * Game.getConveyorPosImg(int conveyorPos) -- returns int;
 * Game.getConveyorTile(int conveyorPos) -- returns String
 * Game.getConveyorTiles() -- returns String[]
 * Game.getCurrentMenuLevel() -- returns int
 * Game.getExpertBonusFromScore(int score) -- returns int
 * Game.getGamemodeDefault() -- returns int
 * Game.getHighestNextLevel() -- returns int
 * Game.getLastSequencerDuraction() -- returns int
 * Game.getLastSequencerTimeout() -- return int
 * Game.getLevelNumber() -- returns int
 * Game.getSequencerTimeout() -- return int
 * Game.getMediaEffect() -- return Media
 * Game.getMediaPreference() -- returns int
 * Game.getMediaSoundtrack() -- return Media
 * Game.getNewbieStatus() -- returns string
 * Game.getGraphicsComplexity() -- return int
 * Game.getLevelScore() -- return int
 * Game.getState() -- returns int
 * Game.getSubState() -- returns int
 * Game.getTotalScore() -- returns int
 * Game.mediaSountrackIsPlaying() -- returns boolean
 * Game.moveSprite(Sprite sprite, int xPos, int yPos, int numSteps,
                   int typeOfMove [, boolean destroyOnEnd ])
 * Game.moveNumber(Number number, int xPos, int yPos, int numSteps,
                   int typeOfMove [, boolean destroyOnEnd ])
 * Game.playMediaEffect(String filename [, boolean forcePlayIncidental ])
 * Game.playMediaSoundtrack(String filename)
 * Game.resumeMediaSoundtrack()
 * Game.setConveyorPosImg(int conveyorPos, int imgId)
 * Game.setConveyorTile(int conveyorPos, string tile)
 * Game.setCurrentMenuLevel(int level) -- returns success boolean
 * Game.setGamemodeDefault(int gameMode)
 * Game.setHighestNextLevel(int level) -- returns success boolean
 * Game.setLevelNumber(int levelNumber)
 * Game.setMediaEffect(Media media)
 * Game.setMediaPreference(int preference)
 * Game.setMediaSoundtrack(Media media)
 * Game.setNewbieStatus(String status)
 * Game.setSequencerStart()
 * Game.setGraphicsComplexity(int complexity)
 * Game.setLevelScore(int score)
 * Game.setState(int state)
 * Game.setSubState(int subState)
 * Game.setTotalScore(int score)
 * Game.stopMediaSoundtrack()
 * Game.toggleMediaPreference( [ int currentPreference ] ) -- returns int
 *
 */

var SEQUENCE_DEBUG = false; // insanity logging (LOGGER_SOURCE_SEQUENCER)

var GAME_FRAMEDURATION_TARGET = 50; // milliseconds
var GAME_FRAMETIMEOUT_MINIMUM = 10; // milliseconds

// Game.getState() constants
var GAME_NEW                    = 0;
var GAME_INITIALIZING           = 1;
var GAME_MAINMENU               = 2;
var GAME_LEVEL_PREPARING        = 3;
var GAME_LEVEL_PLAYING          = 4;
var GAME_LEVEL_COMPLETE         = 5;
var GAME_SELECT_GAMEMODE        = 6;
var GAME_MENU_PLAY              = 7;
var GAME_HELP                   = 8;

// Game.getSubState() constants
var GAME_SUB_UNKNOWN = 0;

// Game.setGraphicsComplexity() constants
var GAME_COMPLEXITY_SIMPLE           = 0;
var GAME_COMPLEXITY_RICH             = 10;

// timing constants
var GAME_CYCLES_CHECKFORWORD         = 10;
var GAME_TILETIMEOUT_ADJUST          = 1.0;
var GAME_MEDIA_QUEUE_COUNT           = 8; // play effect sounds at the most every 8 sequencer counts

// sound preferences
var GAME_MEDIA_PREFERENCE_CHURCHMOUSE = 0;
var GAME_MEDIA_PREFERENCE_BUTTERFLY   = 1;
var GAME_MEDIA_PREFERENCE_CYBORG      = 2;

var mediaFileArray = new Array();

// scoring models:
// 0 - Only score word if all directions make sense
// 1 - Score if either horizontally or vertically makes a word.
//     The other direction doesn't need to make a word.
var gameScoringModel = null;

function Game() {

  var gLevelNumber        = null;
  var gState              = GAME_NEW;
  var gSubState           = GAME_SUB_UNKNOWN;
  var gSequencerStartTime = null;
  var gSequencerPrevTime  = null;
  var gSequencerEndTime   = null;
  var gSequencerPrevEnd   = null;
  var gSequencerDelay     = GAME_FRAMEDURATION_TARGET;
  var gComplexity         = GAME_COMPLEXITY_RICH;
  var gLastDuration       = GAME_FRAMEDURATION_TARGET;
  var gConveyorTile       = new Array();
  var gConveyorPosImg     = new Array();
  var gTotalScore         = null;
  var gLevelScore         = null;
  var gMediaEffect        = null; // Media object for effects
  var gMediaEffectQueue   = new Array(); // files to play
  var gMediaQueueCounter  = 0;    // sequences
  var gMediaSoundtrack    = null;
  var gSoundtrackFilename = null;
  var gSoundtrackStart    = null; // date/time in millis when soundtrack started playing
  var gSoundtrackDuration = null; // duration of soundtrack file in millis
  var gMediaPreference    = null;

  // initialize sound file icons
  mediaFileArray[ GAME_MEDIA_PREFERENCE_CHURCHMOUSE ] = FILE_IMAGE_SOUND_CHURCHMOUSE;
  mediaFileArray[ GAME_MEDIA_PREFERENCE_BUTTERFLY   ] = FILE_IMAGE_SOUND_BUTTERFLY;
  mediaFileArray[ GAME_MEDIA_PREFERENCE_CYBORG      ] = FILE_IMAGE_SOUND_CYBORG;
  
  // initialize map of conveyor positions 0-5 to IMG tag
  for ( var i = 0; i < 6; i++ ) {
   
    gConveyorPosImg[ i ] = ID_LTR_BELTSTART + i;
    
  }
  
  // variables for moving a sprite
  var movingSprites = new Array();
  
  this.getConveyorTiles          = function()         { return gConveyorTile;      }
  this.getLastSequencerDuraction = function()         { return gLastDuration;      }
  this.getLastSequencerTimeout   = function()         { return gSequencerDelay;    }
  this.getLevelNumber            = function()         { return gLevelNumber;       }
  this.setLevelNumber            = function( cValue ) { gLevelNumber = cValue;     }
  this.getLevelScore             = function()         { return gLevelScore;        }
  this.setLevelScore             = function( cValue ) { gLevelScore = cValue;      }
  this.getMediaEffect            = function()         { return gMediaEffect;       }
  this.setMediaEffect            = function( cValue ) { gMediaEffect = cValue;     }
  this.getMediaSoundtrack        = function()         { return gMediaSoundtrack;   }
  this.setMediaSoundtrack        = function( cValue ) { gMediaSoundtrack = cValue; }
  this.getState                  = function()         { return gState;             }
  this.setState                  = function( cValue ) { gState = cValue;           }
  this.getSubState               = function()         { return gSubState;          }
  this.setSubState               = function( cValue ) { gSubState = cValue;        }
  this.getGraphicsComplexity     = function()         { return gComplexity;        }
  this.setGraphicsComplexity     = function( cValue ) { gComplexity = cValue;      }
  this.getTotalScore             = function()         { return gTotalScore;        }
  this.setTotalScore             = function( cValue ) { gTotalScore = cValue;      }
  
  this.setSequencerStart = function() {
    // records the current time as start of the sequencer

    gSequencerPrevTime = gSequencerStartTime;

    var seqTimeDate  = new Date();
    gSequencerStartTime = seqTimeDate.getTime();
    
    if ( gSequencerPrevTime == null ) {
      // first time called; set dummy value 

      gSequencerPrevTime = gSequencerStartTime - GAME_FRAMEDURATION_TARGET;

    }

  }

  this.getSequencerTimeout = function() {
    // records the time it took for this run through the
    // JavaScript code, compares it to the previous time it
    // took, and attempts to adjust the target frame rate
    // accordingly

    gSequencerPrevEnd = gSequencerEndTime;

    var seqTimeDate  = new Date();
    gSequencerEndTime = seqTimeDate.getTime();
    
    if ( gSequencerPrevEnd == null ) {
      // first time run, set to some dummy value

      gSequencerPrevEnd = gSequencerEndTime - GAME_FRAMEDURATION_TARGET;

    }

    // now perform dynamic adaption of frame rate

    var actualDuration = gSequencerEndTime - gSequencerPrevEnd;
    gLastDuration = actualDuration;

    var durationRatio  = actualDuration / GAME_FRAMEDURATION_TARGET;

    if ( durationRatio < 0.95 ) {
      // game is at least 5% too fast; slow down

      gSequencerDelay++;

      if ( gSequencerDelay > GAME_FRAMEDURATION_TARGET ) {
        // something is wrong; cap

        gSequencerDelay = GAME_FRAMEDURATION_TARGET;
      
      }

    }

    if ( durationRatio > 1.05 ) {
      // game is at least 5% too slow; speed up

      gSequencerDelay--;
      if ( gSequencerDelay < GAME_FRAMETIMEOUT_MINIMUM ) {
        // protect slower equipment from frame overruns

        gSequencerDelay = GAME_FRAMETIMEOUT_MINIMUM;

      }

    }

    if ( SEQUENCE_DEBUG ) {
      sysLogger.addLine(
        "actual: " + actualDuration
        + "ms, delay: " + gSequencerDelay
        + "ms",
        LOGGER_SOURCE_SEQUENCER );
    }

    return gSequencerDelay;

  }

  this.action = function() {
    // -------------------------
    // main game branching point

    switch ( gState ) {

      case GAME_NEW:                    gameNew( this );                  break;
      case GAME_INITIALIZING:           gameInitialize( this );           break;
      case GAME_MAINMENU:               gameMainmenu( this );             break;
      case GAME_LEVEL_PREPARING:        gamePrepareLevel( this );         break;
      case GAME_LEVEL_PLAYING:          gamePlaying( this );              break;
      case GAME_LEVEL_COMPLETE:         gameLevelComplete( this );        break;
      case GAME_SELECT_GAMEMODE:        gameSelectGamemode( this );       break;
      case GAME_MENU_PLAY:              gameMenuPlay( this );             break;
      case GAME_HELP:                   gameHelp( this );                 break;

      default:
        throw new Error( "Unknown game state: " + gState );

    }

    // ----------------------------
    // move sprites in MovingSprite

    var newMovingSprites = new Array(); // output array

    for (thisId in movingSprites) {

      var thisMovingSprite = movingSprites[ thisId ];
      var hasFinished = thisMovingSprite.advance();

      if ( hasFinished ) {
        // the sprite has finished moving (or is already
        // moved manually elsewhere); destroy the objects

          thisMovingSprite = null;
          movingSprites[ thisId ] = null;

      } else {
        // add moving sprite to output array
        
          newMovingSprites[ thisId ] = thisMovingSprite;
        
      }
      
    }

    movingSprites = newMovingSprites;
    
    // ------------------------------------------
    // play media effects from the queue (if any)
    
    if ( gMediaQueueCounter > 0 ) {
      // current media counter delay is active
      
      gMediaQueueCounter++;
      
      if ( gMediaQueueCounter >= GAME_MEDIA_QUEUE_COUNT ) {
        // we have waited long enough; playing
        
        gMediaQueueCounter = 0;
        
        if ( gMediaEffectQueue.length > 0 ) {
          
          sysLogger.addLine( "play sound from queue; total queue length: "
                             + gMediaEffectQueue.length,
                             LOGGER_SOURCE_SEQUENCER );
          
          this.playMediaEffect();
        
        }
        
      }
      
    }
    
    // ----------------------
    // loop soundtrack if any
    
    if ( !( gSoundtrackStart == null ) ) {
      
      var thisTime = ( new Date() ).getTime();
      
      if ( ( thisTime - gSoundtrackStart ) > gSoundtrackDuration ) {
        // it's time!

        if ( gMediaSoundtrack ) this.stopMediaSoundtrack();
        
        gSoundtrackStart = ( new Date() ).getTime();

        sysLogger.addLine( "sequencer initiates soundtrack file '"
                             + gSoundtrackFilename
                             + "' at time "
                             + gSoundtrackStart,
                             LOGGER_SOURCE_SEQUENCER );
        
        if ( gMediaPreference == GAME_MEDIA_PREFERENCE_CYBORG ) {
        
          gMediaSoundtrack = new Sound( gSoundtrackFilename );
          gMediaSoundtrack.play();
          
        } else {
          
          sysLogger.addLine( "sound not playing due to user preference",
                             LOGGER_SOURCE_SEQUENCER );
          
        }
        
      }
      
    }

  }

  this.moveSprite = function( sprite, xPosTo, yPosTo, numSteps, typeOfMove, destroyOnEnd ) {
    // add a sprite to the movingSprites array,
    // create a MovingSprite object, and let Game.action() do the actual motion.

    var thisMovingSprite = new MovingSprite(
                                 sprite,
                                 xPosTo,
                                 yPosTo,
                                 numSteps,
                                 typeOfMove,
                                 destroyOnEnd
                               );

    movingSprites[ sprite.getId() ] = thisMovingSprite;

  }

  this.moveNumber = function( number, xPosTo, yPosTo, numSteps, typeOfMove, destroyOnEnd ) {
    // add all sprites from Number to the movingSprites array,
    // create a MovingSprite object, and let Game.action() do the actual motion.

    if ( !( number ) ) return;

    var diffX = xPosTo - number.getXPos();
    var diffY = yPosTo - number.getYPos();
    var numberSprites = number.getSprites(); // array of Sprite

    for (thisID in numberSprites) {

      var sprite = numberSprites[ thisID ];

      var thisMovingSprite = new MovingSprite(
                                 sprite,
                                 sprite.getXposTo() + diffX,
                                 sprite.getYposTo() + diffY,
                                 numSteps,
                                 typeOfMove,
                                 destroyOnEnd
                               );

      movingSprites[ sprite.getId() ] = thisMovingSprite;

    }

    // update the number's position with where it'll be

    number.setXPos( xPosTo );
    number.setYPos( yPosTo ); 

  }

  this.getConveyorTile = function( tilePos ) {
   
    return gConveyorTile[ tilePos ];
    
  }
  
  this.setConveyorTile = function( tilePos, tileLtr ) {
   
    gConveyorTile[ tilePos ] = tileLtr;
    
  }
  
  this.getConveyorPosImg = function( tilePos ) {
   
    return gConveyorPosImg[ tilePos ];
    
  }
  
  this.setConveyorPosImg = function( tilePos, imgId ) {
   
    gConveyorPosImg[ tilePos ] = imgId;
    
  }
  
  /*
   * get variables from storage
   */
  
  function gameGetVariable( variableName ) {
   
    var variableTemp = null;
    
    try {
          
      variableTemp = window.localStorage.getItem( variableName );
      
    } catch (err) {

      var thisGameError = new GameError(err);
      sysLogger.addLine( "Error gameGetVariable("
                         + variableName
                         + "): "
                         + thisGameError.getString(),
                         LOGGER_SOURCE_GAME );

      return null;

    }
    
    return variableTemp;
    
  }
  
  this.getGamemodeDefault = function() {
    
    var tmpGamemodeDefault = gameGetVariable( "gamemodeDefault" );
    if ( tmpGamemodeDefault ) {
     
      return parseInt( tmpGamemodeDefault );
      
    } else {
      
      return SCORING_MODEL_NONE;
      
    }
    
  }
  
  this.getNewbieStatus = function() {
    
    var tmpNewbieStatus = gameGetVariable( "newbieStatus" );
    if ( tmpNewbieStatus ) {
     
      return tmpNewbieStatus;
      
    } else {
      
      return "";
      
    }
    
  }
  
  this.getMediaPreference = function() {
   
    var tmpMediaPreference = gameGetVariable( "mediaPreference" );
    if ( tmpMediaPreference ) {
     
      gMediaPreference = parseInt( tmpMediaPreference );
      return gMediaPreference;
      
    } else {
      
      return null;
      
    }
    
  }
  
  this.getHighestNextLevel = function() {
    
    var tempLevel = gameGetVariable( "nextLevel" );
    if ( tempLevel ) return parseInt( tempLevel ); else return null;
    
  }
  
  this.getCurrentMenuLevel = function() {
    
    var tempLevel = gameGetVariable( "currentMenuLevel" );
    if ( tempLevel ) return parseInt( tempLevel ); else return null;
    
  }
  
  /*
   * Set variables in storage
   */
  
  function gameSetVariable( variableName, variableValue ) {
       
    try {
          
      highestNextLevel = window.localStorage.setItem( variableName, variableValue );
      
    } catch (err) {

      var thisGameError = new GameError(err);
      sysLogger.addLine( "Error gameSetVariable("
                         + variableName
                         + ", "
                         + variableValue
                         + "): "
                         + thisGameError.getString(),
                         LOGGER_SOURCE_GAME );

      return false;

    }
    
    return true;
    
  } 
  
  this.setGamemodeDefault = function( newGamemodeDefault ) {
   
    gameSetVariable( "gamemodeDefault", newGamemodeDefault );
    
  }
  
  this.setNewbieStatus = function( newNewbieStatus ) {
   
    gameSetVariable( "newbieStatus", newNewbieStatus );
    
  }
  
  this.setMediaPreference = function( newMediaPref ) {
    
    gMediaPreference = newMediaPref;
    gameSetVariable( "mediaPreference", newMediaPref );
    
  }
  
  this.setHighestNextLevel = function( highestNextLevel ) {

    gameSetVariable( "nextLevel", highestNextLevel );
    
  }
  
  this.setCurrentMenuLevel = function( currentMenuLevel ) {
    
    gameSetVariable( "currentMenuLevel", currentMenuLevel );
    
  }
  
  this.playMediaEffect = function( fileMedia, forcePlayIncidental ) {
    // play sound file fileMedia (if present) or otherwise play next file from queue;
    // push fileMedia on queue if too early to play
    
    if ( MEDIA_IS_SINGLE_THREADED && this.mediaSountrackIsPlaying() ) return;

    if ( !( MEDIA_HAS_INCIDENTALS ) ) {

      if ( !( forcePlayIncidental ) ) {

        return;

      }

    }

    if ( !( fileMedia ) ) {
     
      if ( gMediaEffectQueue.length < 1 ) return;
      
      fileMedia = gMediaEffectQueue.pop();
      sysLogger.addLine( "retrieved sound file from queue: "
                         + fileMedia,
                         LOGGER_SOURCE_GAME );
      
    } else {
      // request to play a new sound
      
      if ( gMediaQueueCounter > 0 ) {
        // a sound is just playing (or has been added);
        // queue this one (let the Game sequencer handle the counter)
        
        if ( gMediaEffectQueue.length > 0 ) {
         
          if ( gMediaEffectQueue.lastIndexOf( fileMedia ) >= 0 ) {
            
            sysLogger.addLine( "file is already queued for playing, ignore additional play request: "
                           + fileMedia,
                           LOGGER_SOURCE_GAME );
            
            return;
            
          }
          
        }
        
        gMediaEffectQueue.push( fileMedia );
        
        sysLogger.addLine( "put media on queue for later playback: "
                           + fileMedia,
                           LOGGER_SOURCE_GAME );
        
        return;
        
      }
      
    }
    
    // make sure every prior effect has stopped
    if ( gMediaEffect ) {
          
      gMediaEffect.unload();
      gMediaEffect = null;

    }

    sysLogger.addLine( "play media: "
                       + fileMedia,
                       LOGGER_SOURCE_GAME );
    
    if ( gMediaPreference == GAME_MEDIA_PREFERENCE_CHURCHMOUSE ) {
     
      sysLogger.addLine( "sound not playing due to user preference",
                       LOGGER_SOURCE_GAME );
      
    } else {
    
      // play the new effect
      gMediaEffect = new Sound( fileMedia );
      gMediaEffect.play();
      
    }
    
    gMediaQueueCounter = 1; // start the wait timer
    
  }
  
  this.playMediaSoundtrack = function( fileSoundtrack ) {
    // tell the sequencer to loop a soundtrack
    
    if ( !( fileSoundtrack ) ) return;
    
    sysLogger.addLine( "request to play soundtrack: "
                         + fileSoundtrack,
                         LOGGER_SOURCE_GAME );

    // parse duration
    var durationSeparatorIdx = fileSoundtrack.indexOf( "@" );
    
    if ( gSoundtrackFilename && ( !( gSoundtrackStart == null ) ) ) {
     
      if ( gSoundtrackFilename == fileSoundtrack.substr( 0, durationSeparatorIdx ) ) {
        // is already playing
        
        return;
        
      }
      
    }
    
    gSoundtrackFilename = fileSoundtrack.substr( 0, durationSeparatorIdx );
    gSoundtrackDuration = parseInt(
                              fileSoundtrack.substr(
                                  durationSeparatorIdx + 1, 
                                  fileSoundtrack.length - ( durationSeparatorIdx + 1 )
                              )
                          );
    
    gSoundtrackDuration *= 1000;
      
    sysLogger.addLine( "sound file name '"
                         + gSoundtrackFilename
                         + "' with duration "
                         + gSoundtrackDuration
                         + "ms",
                         LOGGER_SOURCE_GAME );
    
    // let sequencer kick of the sounds by setting the last-started time to the epoch
    gSoundtrackStart = 0;
    
  }
  
  this.stopMediaSoundtrack = function() {

    if ( gMediaSoundtrack ) gMediaSoundtrack.unload();
    
    gMediaSoundtrack    = null;
    gSoundtrackStart    = null;
    
  }
  
  this.resumeMediaSoundtrack = function() {
    
    if ( gSoundtrackFilename ) gSoundtrackStart = 0; // force immediate play
    
  }
  
  this.mediaSountrackIsPlaying = function() {
   
    if ( gMediaSoundtrack ) return true; else return false;
    
  }
  
  this.toggleMediaPreference = function( currentPreference ) {
    
    if ( currentPreference == null ) currentPreference = gMediaPreference;
    if ( currentPreference == null ) return;
    
    var newPreference = null;
    
    switch ( currentPreference ) {
      
      case GAME_MEDIA_PREFERENCE_CHURCHMOUSE:
        
        newPreference = GAME_MEDIA_PREFERENCE_CYBORG;
        
        this.resumeMediaSoundtrack();
        break;
        
      case GAME_MEDIA_PREFERENCE_BUTTERFLY:
        
        newPreference = GAME_MEDIA_PREFERENCE_CHURCHMOUSE;
        
        this.stopMediaSoundtrack();
        break;
        
      case GAME_MEDIA_PREFERENCE_CYBORG:
      
        if ( MEDIA_HAS_INCIDENTALS ) {

          newPreference = GAME_MEDIA_PREFERENCE_BUTTERFLY;

        } else {

          newPreference = GAME_MEDIA_PREFERENCE_CHURCHMOUSE;

        }
        
        this.stopMediaSoundtrack();
        break;
      
    }
    
    this.setMediaPreference( newPreference );
    
    return newPreference;
    
  }
  
  this.getExpertBonusFromScore = function( inScore ) {
    // given a score, returns the expert bonus (int)
    
    if ( !( inScore ) ) return 0;
    
    return Math.floor( inScore * 0.3 ); // 30%
    
  }
  
}
